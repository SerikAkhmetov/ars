#ifndef TASKBRWWINDOW_H
#define TASKBRWWINDOW_H

#include <Wt/WWidget>
#include <Wt/WContainerWidget>
#include <Wt/WVBoxLayout>
#include <Wt/WHBoxLayout>
#include <Wt/WDialog>
#include <Wt/WLineEdit>
#include <Wt/WCheckBox>
#include <Wt/WTableView>
#include <Wt/WItemDelegate>
#include <Wt/WComboBox>

#include "uDataLayer.h"
#include "widget.h"

class TtaskBrw : public Twindow
{
 public:
    TtaskBrw(Wt::WContainerWidget *parent, TDataLayer * dl);
    //virtual ~TuserBrw();

    void setupUi();
    void refresh();

   // void setIC(const int id);

    //void onCityChanged(const int);
private:
    Wt::WPushButton *bNew, *bEdit, *bDelete;
    Wt::WTableView * tableView; WTableViewPage * tvPage; Wt::WStandardItemModel * mt;

    TComboBox *cbIC, *cbExecutor, *cbTaskType, * cbState;

   // int ic_id;

    void refresh(const int);
    void redraw(const int);
    void onEdit();
    void onNew();
    void onDelete();
    void onSelectionChanged();
  //  void onPartnerChanged();
    void onFilterChanged();
    void initUser(Wt::WStandardItemModel * mt);
};

#endif