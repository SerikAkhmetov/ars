#include <Wt/WStandardItem>

#include "userUpdWindow.h"

//--------------------------------------------------------------------------
//--------------------------------------------------------------------------
TuserUpd::TuserUpd(Wt::WContainerWidget *parent, TDataLayer * p_dl, const int p_user_id) : Twindow(p_dl, "TuserUpdDefault", parent), TUser(p_dl)
{
 current_can_create_user = 0;
 current_partner_id = 0;
 dl = p_dl;
 user_id = p_user_id;
 isActive = true;
 if (user_id > 0)
 {
  read();
  isActive = isInDBActive();
 }
 refresh();
}
//--------------------------------------------------------------------------
//TuserUpd::~TuserUpd()
//{
//}
//--------------------------------------------------------------------------
void TuserUpd::setupUi()
{
 Wt::WTable * tMain = new Wt::WTable(this); tMain->setObjectName("tMain");

 int r(0);

 Wt::WText * l = new Wt::WText(L"�����:", tMain->elementAt(r, 0));
 eLogin = new Wt::WLineEdit(tMain->elementAt(r, 1)); eLogin->setObjectName("eLogin");
 eLogin->textInput().connect(this, &TuserUpd::onTextInput);
 eLogin->setEnabled(true);

 l = new Wt::WText(L"������:", tMain->elementAt(r, 2));
 ePassword = new Wt::WLineEdit(tMain->elementAt(r, 3)); ePassword->setObjectName("ePassword");
 ePassword->textInput().connect(this, &TuserUpd::onTextInput);
 ePassword->setEnabled(true);
 r++;

 l = new Wt::WText(L"�������:", tMain->elementAt(r, 0));
 eLastName = new Wt::WLineEdit(tMain->elementAt(r, 1)); eLastName->setObjectName("eLastName");
 eLastName->textInput().connect(this, &TuserUpd::onTextInput);
 eLastName->setEnabled(true);
 r++;

 l = new Wt::WText(L"���:", tMain->elementAt(r, 0));
 eFirstName = new Wt::WLineEdit(tMain->elementAt(r, 1)); eFirstName->setObjectName("eFirstName");
 eFirstName->textInput().connect(this, &TuserUpd::onTextInput);
 eFirstName->setEnabled(true);
 r++;

 l = new Wt::WText(L"��������:", tMain->elementAt(r, 0));
 eMiddleName = new Wt::WLineEdit(tMain->elementAt(r, 1)); eMiddleName->setObjectName("eMiddleName");
 eMiddleName->textInput().connect(this, &TuserUpd::onTextInput);
 eMiddleName->setEnabled(true);
 r++;

 l = new Wt::WText(L"e-mail:", tMain->elementAt(r, 0));
 eEmail = new Wt::WLineEdit(tMain->elementAt(r, 1)); eEmail->setObjectName("eEmail");
 eEmail->textInput().connect(this, &TuserUpd::onTextInput);
 eEmail->setEnabled(true);
 r++;

 l = new Wt::WText(L"�������:", tMain->elementAt(r, 0));
 cbPartner = new TComboBox(tMain->elementAt(r, 1)); cbPartner->setObjectName("cbPartner");
 cbPartner->setNoSelectionEnabled(true);
 if (dl->TableS("PARTNER", cbPartner->mt) < 0) { ShowMessage(0, Wt::WString::fromUTF8(str2html(dl->errorstr)), "DB Error");}
 cbPartner->mt->insertRow(0, new Wt::WStandardItem());
 cbPartner->mt->setData(0, 0, 0);
 cbPartner->mt->setData(0, 1, Wt::WString(""));
 cbPartner->setLabelKeyColumn("", "partner_id", "name");
 cbPartner->setValue(partner_id);
 cbPartner->valueChanged.connect(this, &TuserUpd::onTextInput);

 l = new Wt::WText(L"����:", tMain->elementAt(r, 2));
 cbJob = new TComboBoxDirLine(dl, 20, tMain->elementAt(r, 3)); cbJob->setObjectName("cbJob");
 cbJob->setNoSelectionEnabled(true);
 cbJob->mt->insertRow(0, new Wt::WStandardItem());
 cbJob->mt->setData(0, 0, 0);
 cbJob->mt->setData(0, 1, Wt::WString(""));
 cbJob->setValue(job);
 cbJob->valueChanged.connect(this, &TuserUpd::onTextInput);
 r++;

 l = new Wt::WText(L"�������������:", tMain->elementAt(r, 0));
 l->setWordWrap(true);
 cbCanCreateUser = new Wt::WCheckBox(tMain->elementAt(r, 1)); cbCanCreateUser->setObjectName("cbCanCreateUser");
 cbCanCreateUser->setEnabled(true);
 cbCanCreateUser->changed().connect(this, &TuserUpd::onTextInput);
 
 l = new Wt::WText(L"���� � ������� ��������:", tMain->elementAt(r, 2));
 l->setWordWrap(true);
 cbEnabled = new Wt::WCheckBox(tMain->elementAt(r, 3)); cbEnabled->setObjectName("cbEnabled");
 cbEnabled->setEnabled(true);
 cbEnabled->changed().connect(this, &TuserUpd::onTextInput);
 r++;

 /*Wt::WHBoxLayout * lh1 = new Wt::WHBoxLayout();
 bOk = new Wt::WPushButton(Wt::WString(L"��")); bOk->setObjectName("bOk");
 bOk->setEnabled(false);
 bOk->clicked().connect(this, &TuserUpd::onbOk);
 bOk->setIcon(Wt::WLink("image/Apply.png"));

 Wt::WPushButton * bCancel = new Wt::WPushButton(Wt::WString(L"������"));
 bCancel->setObjectName("bCancel");
 bCancel->clicked().connect(this, &TuserUpd::onbCancel);
 bCancel->setIcon(Wt::WLink("image/Cancel.png"));

 lh1->addStretch(1);
 lh1->addWidget(bOk);
 lh1->addWidget(bCancel);

 Wt::WContainerWidget * footer = new Wt::WContainerWidget();
 footer->addStyleClass("modal-footer");
 footer->setLayout(lh1);
 tMain->elementAt(r, 0)->addWidget(footer);
 tMain->elementAt(r, 0)->setColumnSpan(tMain->columnCount());*/

 TOkFooter * footer = new TOkFooter(tMain->elementAt(r, 0));
 footer->bCancel->clicked().connect(this, &TuserUpd::onbCancel);
 bOk = footer->bOk;
 bOk->clicked().connect(this, &TuserUpd::onbOk);
 tMain->elementAt(r, 0)->setColumnSpan(tMain->columnCount());

 setTablePadding(tMain, 2);
}
//--------------------------------------------------------------------------
void TuserUpd::refresh()
{
  Twindow::refresh();

  TUser currentUser(dl);
  int id = dl->SYSGetUser();
  if (id < 0) 
  {
      ShowMessage(0, Wt::WString::fromUTF8(str2html(TUser::dl->errorstr)), "DB Error");
      return;
  }
  currentUser.user_id = id;
  currentUser.read();
  current_can_create_user = currentUser.can_create_user;
  current_partner_id = currentUser.partner_id;

  eLogin->setText(Upper(Wt::WString::fromUTF8(user_name)));
  ePassword->setText(Wt::WString::fromUTF8(pass));
  eFirstName->setText(Wt::WString::fromUTF8(first_name));
  eMiddleName->setText(Wt::WString::fromUTF8(middle_name));
  eLastName->setText(Wt::WString::fromUTF8(last_name));
  eEmail->setText(Wt::WString::fromUTF8(email));
  
  cbCanCreateUser->setChecked(can_create_user > 0);
  cbCanCreateUser->setEnabled(current_can_create_user == 2);

  if(partner_id == 0 && current_can_create_user < 2) partner_id = current_partner_id;

  cbEnabled->setChecked(isActive);
  cbEnabled->setEnabled(current_can_create_user == 2 || (current_can_create_user == 1 && current_partner_id == partner_id));

  cbPartner->setValue(partner_id);
  cbPartner->setEnabled(current_can_create_user == 2);
}
//--------------------------------------------------------------------------
void TuserUpd::onTextInput()
{
    Wt::WObject * w = sender();
    if (w == NULL || w->objectName().empty()) return;

    if (w->objectName() == "eLogin")
    {
        user_name = Upper(eLogin->text().toUTF8());
    }
    else
    if (w->objectName() == "ePassword")
    {
       pass = ePassword->text().toUTF8();
    }
    else
    if (w->objectName() == "eFirstName")
    {
        first_name = eFirstName->text().toUTF8();
    }
    else
    if (w->objectName() == "eMiddleName")
    {
        middle_name = eMiddleName->text().toUTF8();
    }
    else
    if (w->objectName() == "eLastName")
    {
        last_name = eLastName->text().toUTF8();
    }
    else
    if (w->objectName() == "eEmail")
    {
        email = eEmail->text().toUTF8();
    }
    else
    if (w->objectName() == "cbCanCreateUser")
    {
        can_create_user = cbCanCreateUser->isChecked() ? 1 : 0;
    }
    else
    if (w->objectName() == "cbEnabled")
    {
        isActive = cbEnabled->isChecked();
    }
    else
    if (w->objectName() == "cbPartner")
    {
        partner_id = cbPartner->value();
    }
    else
    if (w->objectName() == "cbJob")
    {
        job = cbJob->value();
    }

    bOk->setEnabled(isValid());
}
//--------------------------------------------------------------------------
void TuserUpd::onbOk()
{
    int code;
    std::string name, comment;

    if(save())
    {
        onCloseInt(user_id);
    }
    else
    {
        ShowMessage(0, Wt::WString::fromUTF8(str2html(TUser::dl->errorstr)), "DB Error");
    }
}
//--------------------------------------------------------------------------
void TuserUpd::onbCancel()
{
 onCloseInt(0);
}
//--------------------------------------------------------------------------