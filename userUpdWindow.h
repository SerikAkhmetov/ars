#ifndef USERUPDWINDOW_H
#define USERUPDWINDOW_H

#include <Wt/WWidget>
#include <Wt/WContainerWidget>
#include <Wt/WVBoxLayout>
#include <Wt/WHBoxLayout>
#include <Wt/WDialog>
#include <Wt/WLineEdit>
#include <Wt/WTable>
#include <Wt/WTabWidget>
#include <Wt/WTextArea>
#include <Wt/WDateEdit>

#include "uDataLayer.h"
#include "widget.h"

//--------------------------------------------------------------------------
//--------------------------------------------------------------------------
class TuserUpd : public Twindow, public TUser
{
public:
    TuserUpd(Wt::WContainerWidget *parent, TDataLayer * p_dl, const int user_id = 0);
    //virtual ~TuserUpd();

    void setupUi();
    void refresh();

private:
    TDataLayer * dl;

    int current_can_create_user, current_partner_id;

    Wt::WLineEdit * eLogin, *ePassword, * eFirstName, *eMiddleName, *eLastName, *eEmail;
    Wt::WCheckBox * cbEnabled, * cbCanCreateUser;
    TComboBox * cbPartner, * cbJob;
    Wt::WPushButton * bOk;//, * bCancel;

    void onTextInput();
    void onbOk();
    void onbCancel();
};

#endif
