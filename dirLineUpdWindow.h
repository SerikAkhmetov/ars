#ifndef DIRLINEUPDWINDOW_H
#define DIRLINEUPDWINDOW_H

#include <Wt/WWidget>
#include <Wt/WContainerWidget>
#include <Wt/WVBoxLayout>
#include <Wt/WHBoxLayout>
#include <Wt/WDialog>
#include <Wt/WLineEdit>
#include <Wt/WTable>
#include <Wt/WTabWidget>
#include <Wt/WTextArea>
#include <Wt/WDateEdit>

#include "uDataLayer.h"
#include "widget.h"

//--------------------------------------------------------------------------
//--------------------------------------------------------------------------
class TdirLineUpd : public Twindow
{
public:
    TdirLineUpd(Wt::WContainerWidget *parent, TDataLayer * p_dl, const int directory_id, const int line_id = 0);
    //virtual ~TdirLineUpd();

    void setupUi();
    void refresh();

private:
    int line_id, directory_id;

    Wt::WSpinBox * spCode;
    Wt::WLineEdit * eName, *eComment;

    Wt::WPushButton * bOk, * bCancel;

    void onTextInput();
    void onbOk();
    void onbCancel();
};

#endif
