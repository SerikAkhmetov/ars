#include <Wt/WCheckBox>
#include <Wt/WPushButton>
#include <Wt/WMessageBox>
#include <Wt/WServer>

#include "showLogWindow.h"
//--------------------------------------------------------------------------
TshowLogBrw::TshowLogBrw (Wt::WContainerWidget *parent, TDataLayer * dl): Twindow(dl, "TshowLogBrw", parent)
{
 Twindow::refresh();
 refresh();
}
//--------------------------------------------------------------------------
/*TshowLogBrw::~TshowLogBrw()
{
}*/
//--------------------------------------------------------------------------
void TshowLogBrw::setupUi()
{
 Wt::WVBoxLayout * lmain;
 lmain = new Wt::WVBoxLayout();

 setLayout(lmain);

 Wt::WHBoxLayout * lh1 = new Wt::WHBoxLayout();

 lmain->addItem(lh1);

 tvPage = new WTableViewPageControl(dl, this); tvPage->setObjectName("tvPage");
 //tvPage->curentPageChanged.connect(this, &TdocBrwWindow::onTextInput);
 lh1->addWidget(tvPage->lFilter, 0, Wt::AlignmentFlag::AlignTop);
 lh1->addWidget(tvPage->eFilter, 0, Wt::AlignmentFlag::AlignTop);
 tvPage->eFilter->textInput().connect(this, &Twindow::delayedRefresh);
// tvPage->filterChanged.connect(this, &TshowLogBrw::refresh);
 //tvPage->layout->addStretch(1);

 lh1->addLayout(tvPage->layout, 0, Wt::AlignmentFlag::AlignTop);

 lh1->addStretch(1);

 tableView = new Wt::WTableView(); //tableView->setObjectName("tableView");
 tableView->setAlternatingRowColors(true);
 tableView->setSortingEnabled(false);
 tableView->setColumnResizeEnabled(true);
 //tableView->setSelectionMode(Wt::SelectionMode::SingleSelection);
 tableView->setSelectionMode(Wt::ExtendedSelection);
 tableView->setSelectable(true);
 tableView->setEditTriggers(Wt::WAbstractItemView::NoEditTrigger);
 //tableView->doubleClicked().connect(this, &TdocBrwWindow::onDoubleClicked);
 tableView->columnResized().connect(this, &Twindow::onColumnResized);
 tableView->selectionChanged().connect(this, &TshowLogBrw::onSelectionChanged);
 tableView->setItemDelegate(new TItemDelegate);
 tableView->setHeight(500);

 lmain->addWidget(tableView);

 tvPage->tableView = tableView;

 taPair = new Wt::WTextArea();
 taPair->setObjectName("taPair");
 lmain->addWidget(taPair, 0);

 bOk = new Wt::WPushButton("Ok"); bOk->setObjectName("bOk");
 bOk->setEnabled(false);
 bOk->clicked().connect(this, &TshowLogBrw::onbOk);
 bOk->setIcon(Wt::WLink("image/Apply.png"));
 bCancel = new Wt::WPushButton("Cancel");
 bCancel->setObjectName("bCancel");
 bCancel->clicked().connect(this, &TshowLogBrw::onbCancel);
 bCancel->setIcon(Wt::WLink("image/Cancel.png"));

 lh1 = new Wt::WHBoxLayout();
 lh1->addStretch(1);
 lh1->addWidget(bOk);
 lh1->addWidget(bCancel);

 footer = new Wt::WContainerWidget();
 footer->addStyleClass("modal-footer");
 footer->setLayout(lh1);

 lmain->addWidget(footer);

 lmain->addStretch(1);

// tvPage->xField.clear();
// tvPage->xField.push_back(std::string("CELL_NAME"));
// tvPage->xField.push_back(std::string("DESCRIPT"));

 tvPage->bFirst->clicked().connect(this, &TshowLogBrw::onbClicked);
 tvPage->bPrev->clicked().connect(this, &TshowLogBrw::onbClicked);
 tvPage->sbPage->valueChanged().connect(this, &TshowLogBrw::onbClicked);
 tvPage->sbPage->textInput().connect(this, &TshowLogBrw::onbClicked);
 tvPage->bNext->clicked().connect(this, &TshowLogBrw::onbClicked);
 tvPage->bLast->clicked().connect(this, &TshowLogBrw::onbClicked);
// tvPage->eFilter->textInput().connect(this, &WTableViewPage::onTextInputFilter);
}
//--------------------------------------------------------------------------
void TshowLogBrw::refresh()
{ 
 extern TSQLayer sl;
 int c(sl.event_count(tvPage->eFilter->text().toUTF8()));

 mt = new Wt::WStandardItemModel(this);

 tvPage->lastPage = c > 0 ? (c / tvPage->line + ((c % tvPage->line > 0) ? 1 : 0) ): 1;
 std::stringstream stm;
 stm << "/" << tvPage->lastPage;
 tvPage->lCount->setText(Wt::WString::fromUTF8(stm.str()));

 tvPage->sbPage->setMinimum(1); tvPage->sbPage->setMaximum(tvPage->lastPage);

 if (tvPage->sbPage->value() > tvPage->sbPage->maximum()) tvPage->sbPage->setValue(tvPage->sbPage->maximum());

 if (sl.event_s(mt, tvPage->offset(), tvPage->line, tvPage->eFilter->text().toUTF8()) < 0)
 {
  ShowMessage(0, Wt::WString::fromUTF8(str2html(sl.errmsg())), "DB Error");
 }

//int c = mt->rowCount();

// tvPage->field.clear();
// tvPage->field.push_back(std::string("CELL_NAME"));  tvPage->field.push_back(std::string("Cell"));
// tvPage->field.push_back(std::string("DESCRIPT"));   tvPage->field.push_back(std::string("Description"));

// tvPage->mt = mt;
// tvPage->refresh();

 tableView->setModel(mt);

 tableView->setColumnHidden(0, true);

 dl->setColumnWidth(tableView, name(tableView));

 for(int i = 0; i < tableView->model()->columnCount(); i++)
 {tableView->setHeaderAlignment(i, Wt::AlignCenter);}

// bEdit->setEnabled(false);
// bDelete->setEnabled(false);
 tvPage->bFirst->setEnabled(tvPage->sbPage->value() > 1);
 tvPage->bPrev->setEnabled(tvPage->sbPage->value() > 1);
 tvPage->bNext->setEnabled(tvPage->sbPage->value() != tvPage->lastPage);
 tvPage->bLast->setEnabled(tvPage->sbPage->value() != tvPage->lastPage);
}
//--------------------------------------------------------------------------
void TshowLogBrw::refresh(int page)
{
 bool pageChanged(false);

 tvPage->line = (tableView->height().toPixels() - tableView->headerHeight().toPixels()) /
         tableView->rowHeight().toPixels();

 if (tvPage->line < 1) tvPage->line = 10;

 if (page < 1) page = 1;
 if (page > tvPage->lastPage) page = tvPage->lastPage;

 if (tvPage->sbPage->value() != page)
 {
  tvPage->sbPage->setValue(page);
  pageChanged = true;
 }

 refresh();
}
//--------------------------------------------------------------------------
//--------------------------------------------------------------------------
void TshowLogBrw::onbOk()
{
 onCloseInt(1);
}
//--------------------------------------------------------------------------
void TshowLogBrw::onbCancel()
{
 onCloseInt(0);
}
//--------------------------------------------------------------------------
void TshowLogBrw::onSelectionChanged()
{
 if (tableView->model() == NULL) return;
 if (tableView->selectedIndexes().empty()) return;
 int row (tableView->selectedIndexes().begin()->row());
 if (tableView->model()->rowCount() < row) return;
 int f(fieldByName(tableView->model(), "pair"));
 if (f == -1) return;
 taPair->setText(Wt::WString::fromUTF8(to_string(tableView->model()->data(row, f))));
}
//--------------------------------------------------------------------------
void TshowLogBrw::onKeyPressed(Wt::WKeyEvent event)
{
  Wt::WObject * w = sender();
  if (w == NULL) return;
  if (event.key() == Wt::Key_Enter)
  {
   if (w->objectName() == "edStrFlt") refresh();
  }
}
//--------------------------------------------------------------------------
void TshowLogBrw::onbClicked()
{
 Wt::WObject * w = sender();
 if (w == 0) return;

 if (w->objectName() == "sbPage")
 {
  refresh(tvPage->sbPage->value());
 }
 if (w->objectName() == "bFirst")
 {
  refresh(1);
 }
 if (w->objectName() == "bLast")
 {
  refresh(tvPage->lastPage);
 }
 if (w->objectName() == "bNext")
 {
  refresh(tvPage->sbPage->value() + 1);
 }
 if (w->objectName() == "bPrev")
 {
  refresh(tvPage->sbPage->value() - 1);
 }
}
//--------------------------------------------------------------------------
//--------------------------------------------------------------------------
//void TshowLogBrw::onbNew()
//{
//}
////--------------------------------------------------------------------------
//void TshowLogBrw::onbEdit()
//{
// if (tableView->model() == NULL || tableView->model()->rowCount() == 0) return;
// if (tableView->selectedIndexes().empty()) return;
// int row = tvPage->rowIndex(tableView->selectedIndexes().begin()->row());

//}
////--------------------------------------------------------------------------
//void TshowLogBrw::onbDelete()
//{
// if (tableView->model() == NULL || tableView->model()->rowCount() == 0) return;
// if (tableView->selectedIndexes().empty()) return;
// int row = tvPage->rowIndex(tableView->selectedIndexes().begin()->row());


// if (Wt::StandardButton::Yes != Wt::WMessageBox::show("Confirm", "Remove cell ?",
//                                                      Wt::StandardButton::Yes | Wt::StandardButton::No))
// {
//  return;
// }

// int str_cell_id = to_int(mt->data(row, fieldByName(mt, "str_cell_id")));
// if (str_cell_id > 0)
// {
//  if (dl->StrStorageCellIU(str_cell_id, 0, std::string(), std::string()) == 0)
//  {
//   refresh();
//   tvPage->selectRow(row);
//  }
//  else
//  {ShowMessage(0, Wt::WString::fromUTF8("Remove error"), "DB Error");}
// }
//}
//--------------------------------------------------------------------------
//--------------------------------------------------------------------------
TshowSessionBrw::TshowSessionBrw (Wt::WContainerWidget *parent, TDataLayer * dl): Twindow(dl, "TshowSessionBrw", parent)
{
 Twindow::refresh();
 refresh();
}
//--------------------------------------------------------------------------
void TshowSessionBrw::setupUi()
{
 clear();

 Wt::WVBoxLayout * lv0 = new Wt::WVBoxLayout();
 setLayout(lv0);
 lv0->setSpacing(0);

 tab = new Wt::WTabWidget();
 tab->currentChanged().connect(this, &TshowSessionBrw::ontabCurrentChanged);
 lv0->addWidget(tab, 1);
 Wt::WContainerWidget * doc1 = new Wt::WContainerWidget(); doc1->setObjectName("doc1");
 tab->addTab(doc1, "Session");
 Wt::WContainerWidget * doc2 = new Wt::WContainerWidget();
 tab->addTab(doc2, "Resources");

 Wt::WVBoxLayout * lmain;
 lmain = new Wt::WVBoxLayout();

 doc1->setLayout(lmain);

 tableView1 = new Wt::WTableView(); //tableView->setObjectName("tableView");
 tableView1->setAlternatingRowColors(true);
 tableView1->setSortingEnabled(false);
 tableView1->setColumnResizeEnabled(true);
 //tableView->setSelectionMode(Wt::SelectionMode::SingleSelection);
 tableView1->setSelectionMode(Wt::ExtendedSelection);
 tableView1->setSelectable(true);
 tableView1->setEditTriggers(Wt::WAbstractItemView::NoEditTrigger);
 //tableView->doubleClicked().connect(this, &TdocBrwWindow::onDoubleClicked);
 tableView1->columnResized().connect(this, &Twindow::onColumnResized);
 //tableView->selectionChanged().connect(this, &TshowLogBrw::onSelectionChanged);
 tableView1->setItemDelegate(new TItemDelegate);
 tableView1->setHeight(500);

 lmain->addWidget(tableView1);

 lmain = new Wt::WVBoxLayout();
 doc2->setLayout(lmain);

 tableView2 = new Wt::WTableView(); //tableView->setObjectName("tableView");
 tableView2->setAlternatingRowColors(true);
 tableView2->setSortingEnabled(false);
 tableView2->setColumnResizeEnabled(true);
 //tableView->setSelectionMode(Wt::SelectionMode::SingleSelection);
 tableView2->setSelectionMode(Wt::ExtendedSelection);
 tableView2->setSelectable(true);
 tableView2->setEditTriggers(Wt::WAbstractItemView::NoEditTrigger);
 //tableView->doubleClicked().connect(this, &TdocBrwWindow::onDoubleClicked);
 tableView2->columnResized().connect(this, &Twindow::onColumnResized);
// tableView->selectionChanged().connect(this, &TshowLogBrw::onSelectionChanged);
 tableView2->setItemDelegate(new TItemDelegate);
 tableView2->setHeight(500);

 lmain->addWidget(tableView2);

 bOk = new Wt::WPushButton("Ok"); bOk->setObjectName("bOk");
 bOk->setEnabled(false);
 bOk->clicked().connect(this, &Twindow::onbOk);
 bOk->setIcon(Wt::WLink("image/Apply.png"));
 bCancel = new Wt::WPushButton("Cancel"); bCancel->setObjectName("bCancel");
 bCancel->clicked().connect(this, &Twindow::onbCancel);
 bCancel->setIcon(Wt::WLink("image/Cancel.png"));

 Wt::WHBoxLayout * lh1 = new Wt::WHBoxLayout();
 lh1->addStretch(1);
 lh1->addWidget(bOk);
 lh1->addWidget(bCancel);

 footer = new Wt::WContainerWidget();
 footer->addStyleClass("modal-footer");
 footer->setLayout(lh1);

 lv0->addWidget(footer);

 lv0->addStretch(1);
}
//--------------------------------------------------------------------------
void TshowSessionBrw::refresh()
{
 if (tab->currentIndex() == 0)
 {
  refreshSession();
 }
 else if (tab->currentIndex() == 1)
 {
  refreshResources();
 }
}
//--------------------------------------------------------------------------
void TshowSessionBrw::refreshSession()
{
 Wt::WServer * server;
 Wt::WStandardItemModel * mt;

 server = Wt::WServer::instance();

 if (server == NULL) return;

 mt = new Wt::WStandardItemModel;
 mt->insertColumn(0);
 mt->setHeaderData(0, Wt::Orientation::Horizontal, std::string("SessionId"));

 auto sessions = server->sessions();
 for(int i = 0; i < sessions.size(); i++)
 {
  mt->insertRow(i);
  mt->setData(i, 0, sessions[i].sessionId);
 }

 tableView1->setModel(mt);
}
//--------------------------------------------------------------------------
void TshowSessionBrw::refreshResources()
{
 Wt::WStandardItemModel * mt;
 extern TQueryResourceList qrList;

 mt = new Wt::WStandardItemModel;
 mt->insertColumn(0);
 mt->setHeaderData(0, Wt::Orientation::Horizontal, std::string("Resource"));
 mt->insertColumn(1);
 mt->setHeaderData(1, Wt::Orientation::Horizontal, std::string("Info"));

 int i(0);
 for(auto itr = qrList.lqr.begin(); itr != qrList.lqr.end(); ++itr, i++)
 {
  mt->insertRow(i);
  mt->setData(i, 0, itr->first);
  if (itr->second->session_mode && !itr->second->isFree())
  {
   std::stringstream stm;
   stm << itr->second->dl->userName() << "@"
       << itr->second->dl->hostName() << ":"
       << itr->second->dl->dbName() << " ";
   mt->setData(i, 1, stm.str());
  }
 }

 tableView2->setModel(mt);
}
//--------------------------------------------------------------------------
void TshowSessionBrw::ontabCurrentChanged()
{
 refresh();
}
//--------------------------------------------------------------------------
//--------------------------------------------------------------------------
