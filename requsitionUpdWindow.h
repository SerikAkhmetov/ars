#ifndef REQUSITIONUPDWINDOW_H
#define REQUSITIONUPDWINDOW_H

#include <Wt/WWidget>
#include <Wt/WContainerWidget>
#include <Wt/WVBoxLayout>
#include <Wt/WHBoxLayout>
#include <Wt/WDialog>
#include <Wt/WLineEdit>
#include <Wt/WTable>
#include <Wt/WTabWidget>
#include <Wt/WTextArea>
#include <Wt/WDateEdit>

#include "uDataLayer.h"
#include "widget.h"

//--------------------------------------------------------------------------
//--------------------------------------------------------------------------
class TrequsitionUpdDefault : public Twindow
{
 public:
    TrequsitionUpdDefault (Wt::WContainerWidget *parent, TDataLayer * p_dl);
    //virtual ~TrequsitionUpdDefault();

    void setupUi();
    void refresh();

    int city;

 private:
    TRequisition requisition;
    Wt::WTabWidget * tabMain;
    Wt::WLineEdit * eCustomer;
    TDateEdit * deTa;
    Wt::WLineEdit * ePhone;
    Wt::WCalendar * cal;
    Wt::WTable * tCarLifter;

    void onCalendarActivated(Wt::WDate p);
    void refreshCarLifterPlan(const int city, const Wt::WDate d);
    void onbTime();
    void onbOk();
};
//--------------------------------------------------------------------------
//--------------------------------------------------------------------------
class TrequsitionUpd : public Twindow
{
public:
    TrequsitionUpd(Wt::WContainerWidget *parent, TDataLayer * p_dl, const int requsition_id, const int city);
    //virtual ~TrequsitionUpd();

    void setupUi();
    void refresh();
    void fixPlan();
    void setRequisition(const TRequisition& requisition);

private:
    int city;
    TRequisition requisition;
    Wt::WLineEdit * eCustomer;
    TDateEdit * deTa, *deCl;
    Wt::WLineEdit * ePhone;
    TComboBox * cbCarLifter, * cbCarLifterPlan;
    Wt::WPushButton * bOk, * bCancel;

    void refreshCarlifterPlan();
    //void onTextInput();
    void onbOk();
    void onbCancel();
};

#endif // REQUSITIONUPDWINDOW_H
