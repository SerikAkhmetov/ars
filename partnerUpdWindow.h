#ifndef PARTNERUPDWINDOW_H
#define PARTNERUPDWINDOW_H

#include <Wt/WWidget>
#include <Wt/WContainerWidget>
#include <Wt/WVBoxLayout>
#include <Wt/WHBoxLayout>
#include <Wt/WDialog>
#include <Wt/WLineEdit>
#include <Wt/WTable>
#include <Wt/WTabWidget>
#include <Wt/WTextArea>
#include <Wt/WDateEdit>

#include "uDataLayer.h"
#include "widget.h"

//--------------------------------------------------------------------------
//--------------------------------------------------------------------------
class TpartnerUpd : public Twindow
{
public:
    TpartnerUpd(Wt::WContainerWidget *parent, TDataLayer * p_dl, const int partner_id = 0);
    //virtual ~TpartnerUpd();

    void setupUi();
    void refresh();

private:
    int partner_id;

    Wt::WLineEdit * eName;

    Wt::WPushButton * bOk;//, * bCancel;

    void onTextInput();
    void onbOk();
    void onbCancel();
};

#endif
