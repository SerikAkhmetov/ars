#include <Wt/WStackedWidget>
#include <Wt/WMenu>
#include "parameter.h"
#include "mainWindow.h"
#include "requisitionBrwWindow.h"
#include "requsitionUpdWindow.h"
#include "carlifterPlanBrwWindow.h"
#include "insuranceCaseBrwWindow.h"
#include "dirLineBrwWindow.h"
#include "partnerBrwWindow.h"
#include "userBrwWindow.h"
#include "userUpdWindow.h"
#include "taskBrwWindow.h"
#include "showLogWindow.h"
#include "carlifterBrwWindow.h"
//--------------------------------------------------------------------------
TmainWindow::TmainWindow (Wt::WContainerWidget * parent) :
 Twindow(parent), isDefaultConnect(true)
{
 setObjectName("TmainWindow");
 menuDefault.clear();
 menuLogged.clear();

 dl = DataLayerFactory();
 defaultConnect();

 if (!dl->isOpen())
 {
     Wt::log("Error") << " Database is not open. " << dl->errorstr;
     return;
 }

 menuDefault.push_back(new TMenu("Заявки", "Подать заявку"));

 menuLogged.push_back(new TMenu("Документы", "Заявки"));
 menuLogged.push_back(new TMenu("Документы", "Страховые дела"));
 menuLogged.push_back(new TMenu("Документы", "Задачи"));
 menuLogged.push_back(new TMenu("Документы", "Расписание подъемников"));

 menuLogged.push_back(new TMenu("Справочники", "Вид страхования"));
 menuLogged.push_back(new TMenu("Справочники", "Тип происшествия"));
 menuLogged.push_back(new TMenu("Справочники", "Причина ущерба"));
 menuLogged.push_back(new TMenu("Справочники", "Тип места ДТП"));
 menuLogged.push_back(new TMenu("Справочники", "Тип улицы"));
 menuLogged.push_back(new TMenu("Справочники", "Регионы"));
 menuLogged.push_back(new TMenu("Справочники", "Справка о ДТП"));
 menuLogged.push_back(new TMenu("Справочники", "Подал заявление"));
 //menuLogged.push_back(new TMenu("Справочники", "Номер типовой схемы ДТП"));
 menuLogged.push_back(new TMenu("Справочники", "ТС заявителя на схеме"));
 menuLogged.push_back(new TMenu("Справочники", "Вид осмотра"));
 menuLogged.push_back(new TMenu("Справочники", "Тип документа"));

 menuLogged.push_back(new TMenu("Настройки", "Города"));
 menuLogged.push_back(new TMenu("Настройки", "Тип задачи"));
 menuLogged.push_back(new TMenu("Настройки", "Автоподъемники"));
 menuLogged.push_back(new TMenu("Настройки", "Статус дела (АРС)"));
 menuLogged.push_back(new TMenu("Настройки", "Статус дела (Партнер)"));

 menuLogged.push_back(new TMenu("Система", "Пользователи"));
// menuLogged.push_back(new TMenu("Система", "Настройка"));
 menuLogged.push_back(new TMenu("Система", "О программе"));

 setupUi();

 eLogin->setFocus(true);
}
//--------------------------------------------------------------------------
TmainWindow::~TmainWindow()
{
 clear();
// for(auto itr = menu.begin(); itr != menu.end(); ++itr) delete(*itr);
// menu.clear();
 delete(dl);
}
//--------------------------------------------------------------------------
void TmainWindow::setupUi()
{
 lmain = new Wt::WVBoxLayout();
 setLayout(lmain);

 Wt::WHBoxLayout * lh1 = new Wt::WHBoxLayout();
 lh1->setSpacing(0);

 Wt::WVBoxLayout * lv1 = new Wt::WVBoxLayout();
 lv1->setSpacing(0);

 lh1->addLayout(lv1, 0 /*, Wt::AlignmentFlag::AlignLeft*/);

 panel = new Wt::WPanel();
 panel->addStyleClass("navbar-inner");

 container = new Wt::WContainerWidget();
 panel->setCentralWidget(container);
 panel->setTitleBar(false);
 panel->setCollapsible(false);
 container->setLayout(lh1);

 container->setMargin(Wt::WLength(0));
 panel->setMargin(Wt::WLength(0));

 lmain->addWidget(panel);

 cbCity = new TComboBox(); cbCity->setObjectName("cbCity");
 dl->DIRLineS(cbCity->mt, 1);
 cbCity->setLabelKeyColumn(""/*"Город:"*/, "line_id", "name");
 //cbCity->activated.connect(this, &TdocBrwWindow::onTextInput);

 lv1->addWidget(cbCity);

 m = new Wt::WMenu();
 m->setStyleClass("nav nav-pills");
 m->setMargin(Wt::WLength(0));
// m->setWidth(150);

 menu1 = new Wt::WMenu();

 lv1->addWidget(m);

 createMenu(menuDefault);

 Wt::WHBoxLayout * lmenu1 = new Wt::WHBoxLayout();
 lmenu1->setContentsMargins(5, 0, 5, 0);
 lv1->addLayout(lmenu1);
// lv1->addWidget(menu1);

 menu1->setStyleClass("nav nav-pills");
 menu1->setMargin(Wt::WLength(0));
 //menu1->addItem(new Wt::WMenuItem(""));
 lmenu1->addWidget(menu1);
 lmenu1->addStretch(1);

 lv1->addStretch(1);

 //lh1->addStretch(1);
 tLogin = new Wt::WTable();
 lh1->addWidget(tLogin, 1,  Wt::AlignmentFlag::AlignRight);

 int r(0);
 eLogin = new Wt::WLineEdit(Wt::WString("")); //
 eLogin->setObjectName("eLogin");
 eLogin->keyPressed().connect(this, &TmainWindow::onKeyPressed);
 Wt::WText * l = new Wt::WText(Wt::WString::fromUTF8("Логин:"));
 tLogin->elementAt(r, 1)->addWidget(l);
 tLogin->elementAt(r, 2)->addWidget(eLogin);

 r++;

 ePassword = new Wt::WLineEdit(Wt::WString("")); //
 ePassword->setObjectName("ePassword");
 ePassword->keyPressed().connect(this, &TmainWindow::onKeyPressed);
 ePassword->setEchoMode(Wt::WLineEdit::Password);
 l = new Wt::WText(Wt::WString::fromUTF8("Пароль:"));
 tLogin->elementAt(r, 1)->addWidget(l);
 tLogin->elementAt(r, 2)->addWidget(ePassword);

 r++;
 Wt::WPushButton * bOk;
 bOk = new Wt::WPushButton(Wt::WString::fromUTF8("Вход"));              // create a button
 bOk->setMargin(5, Wt::Left);
 bOk->clicked().connect(this, &TmainWindow::onbOk);
 bOk->setIcon(Wt::WLink("image/Apply.png"));

 tLogin->elementAt(r, 2)->addWidget(bOk);

 setTablePadding(tLogin);

 wLogOut = new Wt::WContainerWidget();
 Wt::WVBoxLayout * lv2 = new Wt::WVBoxLayout();
 lv2->addStretch(1);
 lv2->setSpacing(0);
 Wt::WHBoxLayout * lh3 = new Wt::WHBoxLayout();
 lv2->addLayout(lh3); 
 wLogOut->setLayout(lv2);
 lLogin = new Wt::WText();
 lh3->addWidget(lLogin, 0,  Wt::AlignmentFlag::AlignRight);

 Wt::WText * lExit = new Wt::WText();
 lExit->addStyleClass("icon-off");
 //lExit->setStyleClass("cursor:pointer;");
 //lExit->addStyleClass("button.close");
 //lExit->addStyleClass("nav-header");
 lExit->setToolTip(Wt::WString("Logout"));
 lExit->clicked().connect(this, &TmainWindow::onExitClecked);

 lh3->addWidget(lExit);

 wLogOut->setHidden(true);

 lh1->addWidget(wLogOut, 0,  Wt::AlignmentFlag::AlignRight);

 /*bMenu = new Wt::WPushButton(this);
 bMenu->setEnabled(true);
 Wt::WPopupMenu * pmenu = new Wt::WPopupMenu();
 Wt::WMenuItem * item = pmenu->addItem(Wt::WString::fromUTF8("Выйти из системы"));
 item->triggered().connect(this, &TmainWindow::onLogout);
 bMenu->setMenu(pmenu);
 bMenu->setHidden(true);
 lh1->addWidget(bMenu, 1, Wt::AlignmentFlag::AlignRight | Wt::AlignmentFlag::AlignBottom);*/

 doc = new Wt::WContainerWidget();
 lmain->addWidget(doc);
}
//--------------------------------------------------------------------------
void TmainWindow::onMenu()
{
 Wt::WObject * w = sender();
 if (w == NULL) return;

 doc->clear();

 std::list<TMenu *> * menu = (isDefaultConnect) ? &menuDefault : &menuLogged;

 std::string menu0("");
 for(auto itr = menu->begin(); itr != menu->end(); ++itr)
 {
  if (menu0 != (*itr)->menu0)
  {
   menu0 = (*itr)->menu0;
   if (menu0 == w->objectName())
   {
    createMenu(menu0);
    return;
   }
  }
 }
 //dl->setCookie(dl->dbName() + "_" + dl->userName() + "_" + "menu1", w->objectName());
 createWindow(w->objectName());
 menu1->select(-1);
}
//--------------------------------------------------------------------------
void TmainWindow::createMenu(const std::list<TMenu *>& menu)
{
    while(!menu1->items().empty())
    {
        Wt::WMenuItem * itm = menu1->items().front();
        menu1->removeItem(itm);
        delete(itm);
    }
    while(!m->items().empty())
    {
        Wt::WMenuItem * itm = m->items().front();
        m->removeItem(itm);
        delete(itm);
    }
    std::string menu0("");
    for(auto itr = menu.begin(); itr != menu.end(); ++itr)
    {
        if (menu0 != (*itr)->menu0)
        {
            menu0 = (*itr)->menu0;
            Wt::WMenuItem * item = new Wt::WMenuItem(Wt::WString::fromUTF8(menu0));
            //  item->setStyleClass("cursor:pointer;");
            item->setObjectName(menu0);
            item->triggered().connect(this, &TmainWindow::onMenu);
            m->addItem(item);
        }
    }
}
//--------------------------------------------------------------------------
void TmainWindow::createMenu(const std::string& name)
{
 while(!menu1->items().empty())
 {
  Wt::WMenuItem * itm = menu1->items().front();
  menu1->removeItem(itm);
  delete(itm);
 }

 std::list<TMenu *> * menu = (isDefaultConnect) ? &menuDefault : &menuLogged;

 for(auto itr = menu->begin(); itr != menu->end(); ++itr)
 {
  if (name == (*itr)->menu0)
  {
   Wt::WMenuItem * item = new Wt::WMenuItem(Wt::WString::fromUTF8((*itr)->label));
   item->setObjectName((*itr)->menu1);
   if (!(*itr)->ico.empty() && Wt::WApplication::instance() != NULL)
   { //std::string file; file = Wt::WApplication::instance()->docRoot() + (*itr)->ico;
     item->setIcon(Wt::WApplication::instance()->docRoot() + (*itr)->ico);}
   item->triggered().connect(this, &TmainWindow::onMenu);
   menu1->addItem(item);
  }
 }
// lmenu1->addStretch(1);
}
//--------------------------------------------------------------------------
bool TmainWindow::isCurrentUserAdmin()
{
 return(Lower(dl->userName()) == "pereladov");
}
//--------------------------------------------------------------------------
//--------------------------------------------------------------------------
//--------------------------------------------------------------------------
void TmainWindow::createWindow(const std::string& name)
{
    if (name == "Подать заявку")
    {
        doc->clear();
        TrequsitionUpdDefault * frm = new TrequsitionUpdDefault(doc, dl);
        frm->close.connect(this, &TmainWindow::onCloseWindow);
        frm->city = cbCity->value();
        frm->refresh();
        return;
    }
 
    if (name == "Заявки")
    {
        doc->clear();
        TrequisitionBrw * frm = new TrequisitionBrw(cbCity->value(), doc, dl);
        cbCity->valueChanged.connect(frm, &TrequisitionBrw::onCityChanged);
        return;
    }

    if (name == "Расписание подъемников")
    {
        doc->clear();
        TcarlifterPlanBrw * frm = new TcarlifterPlanBrw(cbCity->value(), doc, dl);
        cbCity->valueChanged.connect(frm, &TcarlifterPlanBrw::onCityChanged);
        return;
    }

    if (name == "Страховые дела")
    {
        doc->clear();
        TinsuranceCaseBrw * frm = new TinsuranceCaseBrw(doc, dl);
        cbCity->valueChanged.connect(frm, &TinsuranceCaseBrw::onCityChanged);
        return;
    }

    if (name == "Задачи")
    {
        doc->clear();
        TtaskBrw * frm = new TtaskBrw(doc, dl);
        return;
    }

    if (name == "Города")
    {
        doc->clear();
        TdirLineBrw * frm = new TdirLineBrw(doc, dl, 1);
        return;
    }
    if (name == "Вид страхования")
    {
        doc->clear();
        TdirLineBrw * frm = new TdirLineBrw(doc, dl, 2);
        return;
    }
    if (name == "Тип происшествия")
    {
        doc->clear();
        TdirLineBrw * frm = new TdirLineBrw(doc, dl, 3);
        return;
    }
    if (name == "Причина ущерба")
    {
        doc->clear();
        TdirLineBrw * frm = new TdirLineBrw(doc, dl, 4);
        return;
    }
    if (name == "Тип места ДТП")
    {
        doc->clear();
        TdirLineBrw * frm = new TdirLineBrw(doc, dl, 5);
        return;
    }
    if (name == "Тип улицы")
    {
        doc->clear();
        TdirLineBrw * frm = new TdirLineBrw(doc, dl, 6);
        return;
    }
    if (name == "Регионы")
    {
        doc->clear();
        TdirLineBrw * frm = new TdirLineBrw(doc, dl, 7);
        return;
    }
    if (name == "Справка о ДТП")
    {
        doc->clear();
        TdirLineBrw * frm = new TdirLineBrw(doc, dl, 8);
        return;
    }
    if (name == "Подал заявление")
    {
        doc->clear();
        TdirLineBrw * frm = new TdirLineBrw(doc, dl, 10);
        return;
    }
    if (name == "Статус дела (АРС)")
    {
        doc->clear();
        TdirLineBrw * frm = new TdirLineBrw(doc, dl, 23);
        return;
    }
    if (name == "Статус дела (Партнер)")
    {
        doc->clear();
        TdirLineBrw * frm = new TdirLineBrw(doc, dl, 24);
        return;
    }
    if (name == "ТС заявителя на схеме")
    {
        doc->clear();
        TdirLineBrw * frm = new TdirLineBrw(doc, dl, 12);
        return;
    }
    if (name == "Вид осмотра")
    {
        doc->clear();
        TdirLineBrw * frm = new TdirLineBrw(doc, dl, 13);
        return;
    }
    if (name == "Тип документа")
    {
        doc->clear();
        TdirLineBrw * frm = new TdirLineBrw(doc, dl, 16);
        return;
    }
    if (name == "Тип задачи")
    {
        doc->clear();
        TdirLineBrw * frm = new TdirLineBrw(doc, dl, 22);
        return;
    }
    if (name == "Партнеры")
    {
        doc->clear();
        TpartnerBrw * frm = new TpartnerBrw(doc, dl);
        return;
    }
    if (name == "Журнал")
    {
        doc->clear();
        TshowLogBrw * frm = new TshowLogBrw(doc, dl);
        return;
    }
    if (name == "Автоподъемники")
    {
        doc->clear();
        TcarlifterBrw * frm = new TcarlifterBrw(doc, dl, cbCity->value());
        cbCity->valueChanged.connect(frm, &TcarlifterBrw::onCityChanged);
        return;
    }
    if (name == "Пользователи")
    {
        TUser user(dl);
        user.user_id = dl->SYSGetUser();
        user.read();

        doc->clear();

        if (user.can_create_user > 0)
          TuserBrw * frm = new TuserBrw(doc, dl);
        else
          TuserUpd * frm = new TuserUpd(doc, dl, user.user_id);
        return;
    }
}
//--------------------------------------------------------------------------
void TmainWindow::onCloseWindow()
{
 doc->clear();
 //clear();
 //setupUi();
 createMenu((isDefaultConnect) ? menuDefault : menuLogged);
}
//--------------------------------------------------------------------------
void TmainWindow::onLogout()
{
 dl->close();
 onClose();
}

void TmainWindow::onbOk()
{
    dl->close();

    std::string HostName("localhost"), DatabaseName("ars"), Role("master");
    Wt::WApplication::instance()->readConfigurationProperty("HostName", HostName);
    Wt::WApplication::instance()->readConfigurationProperty("DatabaseName", DatabaseName);
    Wt::WApplication::instance()->readConfigurationProperty("Role", Role);
    dl->setUserName(Upper(eLogin->text().narrow()));
    dl->setRole(Role);
    dl->setPassword(ePassword->text().narrow());
    dl->setHostName(HostName);
    dl->setDatabaseName(DatabaseName);

    state.clear();
    state["_"] = __PRETTY_FUNCTION__;
    if (Wt::WApplication::instance() != NULL)
        state["sessionId"] = Wt::WApplication::instance()->sessionId();

    state["Login"] = dl->userName();
    state["Host"] = dl->hostName();
    state["DB"] = dl->dbName();

    if (!dl->open())
    {
        Wt::log("error") << dl->errorstr;

        state["connect"] = std::string("Fail");
        state["errorstr"] = dl->errorstr;
        logtxt(&state);

        Wt::WDialog dialog;
        dialog.setWindowTitle("DB connect error");

        new Wt::WText(str2html(dl->errorstr), dialog.contents());
        Wt::WPushButton ok("Ok", dialog.footer());
        ok.setDefault(true);
        ok.clicked().connect(&dialog, &Wt::WDialog::accept);
        ok.setIcon(Wt::WLink("image/Apply.png"));
        dialog.setClosable(true);
        dialog.setResizable(true);
        dialog.rejectWhenEscapePressed(true);
        dialog.exec();

        defaultConnect();
    }
    else
    {
        state["connect"] = std::string("Success");
        logtxt(&state);

        TUser user(dl);
        user.user_id = dl->SYSGetUser();
        user.read();
        lLogin->setText(Wt::WString::fromUTF8(!user.fio.empty() ? user.fio : user.user_name));

        setDefaultConnect(false);
    }
}

void TmainWindow::onKeyPressed(Wt::WKeyEvent event)
{
 Wt::WObject * w = sender();
 if (w == NULL) return;
 if (event.key() == Wt::Key_Enter || event.key() == Wt::Key_Tab)
 {
  if (w->objectName() == "eLogin")   ePassword->setFocus(true);
  if (w->objectName() == "ePassword") onbOk();
 }
}

void TmainWindow::onExitClecked(Wt::WMouseEvent event)
{
 if(event.button() == Wt::WMouseEvent::Button::LeftButton)
 {
     dl->close();
     close.emit();
 }
}

void TmainWindow::defaultConnect()
{
    extern TParameterList params;
    std::string HostName("localhost"), DatabaseName("ars"), UserName("default"), Password("d12102020");
    if (!Wt::WApplication::instance()->readConfigurationProperty("HostName", HostName))
        Wt::log("Error") << "Parameter HostName not found in " << params.get("config")->v_str;
    if (!Wt::WApplication::instance()->readConfigurationProperty("DatabaseName", DatabaseName))
        Wt::log("Error") << "Parameter DatabaseName not found in " << params.get("config")->v_str;
    if (!Wt::WApplication::instance()->readConfigurationProperty("DefaultUserName", UserName))
       Wt::log("Error") << "Parameter DefaultUserName not found in " << params.get("config")->v_str;
    if (!Wt::WApplication::instance()->readConfigurationProperty("DefaultUserPassword", Password))
       Wt::log("Error") << "Parameter DefaultUserPassword not found in " << params.get("config")->v_str;

    if (!HostName.empty()  && !DatabaseName.empty() && !UserName.empty() && !Password.empty())
    {
        dl->setHostName(HostName);
        dl->setDatabaseName(DatabaseName);
        dl->setUserName(UserName);
        dl->setPassword(Password);
        dl->setRole("SITE");
        dl->open();
        isDefaultConnect = true;
    }
}

void TmainWindow::setDefaultConnect(const bool v)
{
  isDefaultConnect = v;
  tLogin->setHidden(!isDefaultConnect);
 // bMenu->setHidden(isDefaultConnect);

  if(!isDefaultConnect && isCurrentUserAdmin())
  {
      menuLogged.push_back(new TMenu("Система", "Партнеры"));
      menuLogged.push_back(new TMenu("Система", "Журнал"));
  }

  wLogOut->setHidden(isDefaultConnect);

  createMenu((isDefaultConnect) ? menuDefault : menuLogged);
}
//--------------------------------------------------------------------------
/*void TmainContainerWidget::clear()
{
 for(int i = 0; i < count(); i++)
 {
  Wt::WWidget * w = widget(i);
  if (dynamic_cast<Twindow *>(w) != NULL)
  {
   Twindow * win = dynamic_cast<Twindow *>(w);
   win->stateSave();
  }
 }

 Wt::WContainerWidget::clear();
}*/
//--------------------------------------------------------------------------
