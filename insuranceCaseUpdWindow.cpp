#include <Wt/WButtonGroup>
#include <Wt/WGroupBox>
#include <Wt/WAnchor>
#include <Wt/WFileResource>
#include <Wt/WMessageBox>
#include <Wt/WStandardItem>

#include "insuranceCaseUpdWindow.h"
#include "taskUpdWindow.h"
#include "report.h"
//--------------------------------------------------------------------------
TDriverWidget::TDriverWidget(Wt::WContainerWidget *parent, TDataLayer * p_dl) : Wt::WContainerWidget(parent), TDriver(p_dl), changed(this)
{
    setupUi();
}

void TDriverWidget::setupUi()
{
    Wt::WText * l;
    t1 = new Wt::WTable(); t1->setObjectName("t1");

    addWidget(t1);

    int r(0);

    rowCitizenship = r;
   /* l = new Wt::WText(L"��������:", t1->elementAt(r, 0));
    l->setWordWrap(true);
    cbResident = new Wt::WCheckBox(t1->elementAt(r, 1)); cbResident->setObjectName("cbResident");
    cbResident->setEnabled(true);
    cbResident->changed().connect(this, &TDriverWidget::onTextInput);*/

    Wt::WContainerWidget * container = new Wt::WContainerWidget(t1->elementAt(r, 1));
    bgResident = new Wt::WButtonGroup(this); bgResident->setObjectName("bgResident");
    bgResident->checkedChanged().connect(this, &TDriverWidget::onCheckedChanged);
    rbResident1 = new Wt::WRadioButton(L"��������", container); rbResident1->setObjectName("rbResident1");
    bgResident->addButton(rbResident1, 1);
    rbResident2 = new Wt::WRadioButton(L"����������", container); rbResident2->setObjectName("rbResident2");
    bgResident->addButton(rbResident2, 0);

    l = new Wt::WText(L"�����������:", t1->elementAt(r, 2));
    l->setWordWrap(true);
    cbCitizenship = new TComboBoxDirLine(dl, 9, t1->elementAt(r, 3)); cbCitizenship->setObjectName("cbCitizenship");
    cbCitizenship->setNoSelectionEnabled(true);
    cbCitizenship->valueChanged.connect(this, &TDriverWidget::onTextInput);
    r++;

    l = new Wt::WText(L"���� ��������:", t1->elementAt(r, 0));
    l->setWordWrap(true);
    deDate_birth = new TDateEdit(t1->elementAt(r, 1)); deDate_birth->setObjectName("deDate_birth");
    deDate_birth->setEnabled(true);
    deDate_birth->textInput().connect(this, &TDriverWidget::onTextInput);

    l = new Wt::WText(L"���:", t1->elementAt(r, 2));
    l->setWordWrap(true);
    container = new Wt::WContainerWidget(t1->elementAt(r, 3));
    bgGender = new Wt::WButtonGroup(this); bgGender->setObjectName("bgGender");
    bgGender->checkedChanged().connect(this, &TDriverWidget::onCheckedChanged);
    rbGender1 = new Wt::WRadioButton(L"�", container); rbGender1->setObjectName("rbGender1");
    bgGender->addButton(rbGender1, 1);
    rbGender2 = new Wt::WRadioButton(L"�", container); rbGender2->setObjectName("rbGender2");
    bgGender->addButton(rbGender2, 2);

    r++;

    l = new Wt::WText(L"�������:", t1->elementAt(r, 0));
    l->setWordWrap(true);
    eLast_name = new Wt::WLineEdit(t1->elementAt(r, 1)); eLast_name->setObjectName("eLast_name");
    eLast_name->setEnabled(true);
    eLast_name->setMaxLength(128);
    eLast_name->textInput().connect(this, &TDriverWidget::onTextInput);

    l = new Wt::WText(L"���:", t1->elementAt(r, 2));
    l->setWordWrap(true);
    eFirst_name = new Wt::WLineEdit(t1->elementAt(r, 3)); eFirst_name->setObjectName("eFirst_name");
    eFirst_name->setEnabled(true);
    eFirst_name->setMaxLength(128);
    eFirst_name->textInput().connect(this, &TDriverWidget::onTextInput);

    r++;

    l = new Wt::WText(L"��������:", t1->elementAt(r, 0));
    l->setWordWrap(true);
    eMiddle_name = new Wt::WLineEdit(t1->elementAt(r, 1)); eMiddle_name->setObjectName("eMiddle_name");
    eMiddle_name->setEnabled(true);
    eMiddle_name->setMaxLength(128);
    eMiddle_name->textInput().connect(this, &TDriverWidget::onTextInput);

    l = new Wt::WText(L"��� ��������:", t1->elementAt(r, 2));
    l->setWordWrap(true);
    cbWithout_mname = new Wt::WCheckBox(t1->elementAt(r, 3)); cbWithout_mname->setObjectName("cbWithout_mname");
    cbWithout_mname->setEnabled(true);
    cbWithout_mname->changed().connect(this, &TDriverWidget::onTextInput);

    r++;

    l = new Wt::WText(L"�������:", t1->elementAt(r, 0));
    l->setWordWrap(true);
    ePhone = new Wt::WLineEdit(t1->elementAt(r, 1)); ePhone->setObjectName("ePhone");
    ePhone->setEnabled(true);
    ePhone->setMaxLength(12);
    ePhone->textInput().connect(this, &TDriverWidget::onTextInput);

   /* setTablePadding(t1);

    Wt::WPanel * p2 = new Wt::WPanel(); p2->setObjectName("p2");
    p2->setTitle(Wt::WString(L"������������ �������������"));
    p2->setTitleBar(true);
    p2->titleBarWidget()->addStyleClass("nav-header");
    lv0->addWidget(p2);

    Wt::WTable * t2 = new Wt::WTable(); t2->setObjectName("t2");
    p2->setCentralWidget(t2);

    r = 0;*/

    r++;

    rowDl = r;
    l = new Wt::WText(L"������������ �������������", t1->elementAt(r, 0));
    l->addStyleClass("nav-header");
    t1->elementAt(r, 0)->setColumnSpan(t1->columnCount());

    r++;

    l = new Wt::WText(L"������ �� �����������:", t1->elementAt(r, 0));
    l->setWordWrap(true);
    cbDl_no_data = new Wt::WCheckBox(t1->elementAt(r, 1)); cbDl_no_data->setObjectName("cbDl_no_data");
    cbDl_no_data->setEnabled(true);
    cbDl_no_data->changed().connect(this, &TDriverWidget::onTextInput);

    r++;

    l = new Wt::WText(L"����� ��:", t1->elementAt(r, 0));
    l->setWordWrap(true);
    eDl_series = new Wt::WLineEdit(t1->elementAt(r, 1)); eDl_series->setObjectName("eDl_series");
    eDl_series->setEnabled(true);
    eDl_series->setMaxLength(16);
    eDl_series->textInput().connect(this, &TDriverWidget::onTextInput);

    l = new Wt::WText(L"����� ��:", t1->elementAt(r, 2));
    l->setWordWrap(true);
    eDl_number = new Wt::WLineEdit(t1->elementAt(r, 3)); eDl_number->setObjectName("eDl_number");
    eDl_number->setEnabled(true);
    eDl_number->setMaxLength(16);
    eDl_number->textInput().connect(this, &TDriverWidget::onTextInput);

    r++;

    l = new Wt::WText(L"�������", t1->elementAt(r, 0));
    l->addStyleClass("nav-header");
    t1->elementAt(r, 0)->setColumnSpan(t1->columnCount());

    r++;

    l = new Wt::WText(L"�����:", t1->elementAt(r, 0));
    l->setWordWrap(true);
    eP_series = new Wt::WLineEdit(t1->elementAt(r, 1)); eP_series->setObjectName("eP_series");
    eP_series->setEnabled(true);
    eP_series->setMaxLength(16);
    eP_series->textInput().connect(this, &TDriverWidget::onTextInput);

    l = new Wt::WText(L"�����:", t1->elementAt(r, 2));
    l->setWordWrap(true);
    eP_number = new Wt::WLineEdit(t1->elementAt(r, 3)); eP_number->setObjectName("eP_number");
    eP_number->setEnabled(true);
    eP_number->setMaxLength(16);
    eP_number->textInput().connect(this, &TDriverWidget::onTextInput);

    r++;

    l = new Wt::WText(L"������:", t1->elementAt(r, 0));
    l->setWordWrap(true);
    eIssued = new Wt::WLineEdit(t1->elementAt(r, 1)); eIssued->setObjectName("eIssued");
    eIssued->setEnabled(true);
    eIssued->setMaxLength(128);
    eIssued->textInput().connect(this, &TDriverWidget::onTextInput);

    l = new Wt::WText(L"���� ������:", t1->elementAt(r, 2));
    l->setWordWrap(true);
    deIssue = new TDateEdit(t1->elementAt(r, 3)); deIssue->setObjectName("deIssue");
    deIssue->setEnabled(true);
    deIssue->textInput().connect(this, &TDriverWidget::onTextInput);

    r++;

    l = new Wt::WText(L"�����������", t1->elementAt(r, 0));
    l->addStyleClass("nav-header");
    t1->elementAt(r, 0)->setColumnSpan(t1->columnCount());

    r++;

    l = new Wt::WText(L"������:", t1->elementAt(r, 0));
    l->setWordWrap(true);
    ePostcode = new Wt::WLineEdit(t1->elementAt(r, 1)); ePostcode->setObjectName("ePostcode");
    ePostcode->setEnabled(true);
    ePostcode->setMaxLength(10);
    ePostcode->textInput().connect(this, &TDriverWidget::onTextInput);

    l = new Wt::WText(L"�����:", t1->elementAt(r, 2));
    l->setWordWrap(true);
    eAddr_field1 = new /*Wt::WLineEdit*/Wt::WTextArea(t1->elementAt(r, 3)); eAddr_field1->setObjectName("eAddr_field1");
    eAddr_field1->setEnabled(true);
    eAddr_field1->textInput().connect(this, &TDriverWidget::onTextInput);

    Twindow::setTablePadding(t1);
}

void TDriverWidget::refresh()
{    
    if (resident) {rbResident1->setChecked(); rbResident2->setUnChecked();}
    else {rbResident2->setChecked(); rbResident1->setUnChecked();}
    cbCitizenship->setValue(citizenship);
    deDate_birth->setDate(date_birth);
    if (gender == 1) rbGender1->setChecked(); else rbGender1->setUnChecked();
    if (gender == 2) rbGender2->setChecked(); else rbGender2->setUnChecked();
    eLast_name->setText(Wt::WString::fromUTF8(last_name));
    eFirst_name->setText(Wt::WString::fromUTF8(first_name));
    eMiddle_name->setText(Wt::WString::fromUTF8(middle_name));
    cbWithout_mname->setChecked(without_mname);
    ePhone->setText(Wt::WString::fromUTF8(phone));
    eDl_series->setText(Wt::WString::fromUTF8(dl_series));
    eDl_number->setText(Wt::WString::fromUTF8(dl_number));
    cbDl_no_data->setChecked(dl_no_data);
    eP_series->setText(Wt::WString::fromUTF8(p_series));
    eP_number->setText(Wt::WString::fromUTF8(p_number));
    eIssued->setText(Wt::WString::fromUTF8(issued));
    deIssue->setDate(date_issue);
    ePostcode->setText(Wt::WString::fromUTF8(postcode));
    eAddr_field1->setText(Wt::WString::fromUTF8(addr_field1));
  /*  eAddr_field2->setText(Wt::WString::fromUTF8(addr_field2));
    eAddr_field3->setText(Wt::WString::fromUTF8(addr_field3));
    eAddr_field4->setText(Wt::WString::fromUTF8(addr_field4));
    eAddr_field5->setText(Wt::WString::fromUTF8(addr_field5));
    cbHouse->setValue(house);
    cbApartment->setValue(apartment);*/
}

void TDriverWidget::onTextInput()
{
    Wt::WObject * w = sender();
    if (w == NULL || w->objectName().empty()) return;

    if (w->objectName() == "cbCitizenship")
    {
        citizenship = cbCitizenship->value();
    }
    else if (w->objectName() == "deDate_birth")
    {
        date_birth = deDate_birth->date();
    }
 /*   else if (w->objectName() == "bgGender")
    {
        gender = 0;
        if (rbGender1->isChecked()) gender = 1;
        if (rbGender2->isChecked()) gender = 2;
    }*/
    else if (w->objectName() == "eLast_name")
    {
        last_name = eLast_name->text().trim().toUTF8();
    }
    else if (w->objectName() == "eFirst_name")
    {
        first_name = eFirst_name->text().trim().toUTF8();
    }
    else if (w->objectName() == "eMiddle_name")
    {
        middle_name = eMiddle_name->text().trim().toUTF8();
    }
    else if (w->objectName() == "cbWithout_mname")
    {
        without_mname = cbWithout_mname->isChecked();
    }
    else if (w->objectName() == "ePhone")
    {
        phone = ePhone->text().trim().toUTF8();
    }
    else if (w->objectName() == "eDl_series")
    {
        dl_series = eDl_series->text().trim().toUTF8();
    }
    else if (w->objectName() == "eDl_number")
    {
        dl_number = eDl_number->text().trim().toUTF8();
    }
    else if (w->objectName() == "cbDl_no_data")
    {
        dl_no_data = cbDl_no_data->isChecked();
    }
    else if (w->objectName() == "eP_series")
    {
        p_series = eP_series->text().trim().toUTF8();
    }
    else if (w->objectName() == "eP_number")
    {
        p_number = eP_number->text().trim().toUTF8();
    }
    else if (w->objectName() == "eIssued")
    {
        issued = eIssued->text().trim().toUTF8();
    }
    else if (w->objectName() == "deIssue")
    {
        date_issue = deIssue->date();
    }
    else if (w->objectName() == "ePostcode")
    {
        postcode = ePostcode->text().trim().toUTF8();
    }
    else if (w->objectName() == "eAddr_field1")
    {
        addr_field1 = eAddr_field1->text().trim().toUTF8().substr(0, 128);
    }
  /*  else if (w->objectName() == "eAddr_field2")
    {
        addr_field2 = eAddr_field2->text().trim().toUTF8();
    }
    else if (w->objectName() == "eAddr_field3")
    {
        addr_field3 = eAddr_field3->text().trim().toUTF8();
    }
    else if (w->objectName() == "eAddr_field4")
    {
        addr_field4 = eAddr_field4->text().trim().toUTF8();
    }
    else if (w->objectName() == "eAddr_field5")
    {
        addr_field5 = eAddr_field5->text().trim().toUTF8();
    }
    else if (w->objectName() == "cbHouse")
    {
        house = cbHouse->value();
    }
    else if (w->objectName() == "cbApartment")
    {
        apartment = cbApartment->value();
    }*/

    changed.emit();
}

void TDriverWidget::onCheckedChanged(Wt::WRadioButton *)
{
    gender = 0;
    if (rbGender1->isChecked()) gender = 1;
    if (rbGender2->isChecked()) gender = 2;

    if (rbResident1->isChecked())  resident = true;
    else resident = false;

    changed.emit();
}

void TDriverWidget::hideDriversLicense()
{
    t1->rowAt(rowDl)->setHidden(true);
    t1->rowAt(rowDl+1)->setHidden(true);
    t1->rowAt(rowDl+2)->setHidden(true);
}

void TDriverWidget::hideCitizenship()
{
    t1->rowAt(rowCitizenship)->setHidden(true);
}
//--------------------------------------------------------------------------
//--------------------------------------------------------------------------
//--------------------------------------------------------------------------
TVehicleWidget::TVehicleWidget(Wt::WContainerWidget *parent, TDataLayer * p_dl) : Wt::WContainerWidget(parent), TVehicle(p_dl), changed(this)
{
    setupUi();
}

void TVehicleWidget::setupUi()
{
    Wt::WText * l;
 
    Wt::WVBoxLayout * lv0 = new Wt::WVBoxLayout(); lv0->setObjectName("lv0");

    setLayout(lv0);
    setMargin(0);

    p0 = new Wt::WPanel(); p0->setObjectName("p0");
    p0->setTitle(Wt::WString(L"������������ ��������"));
    p0->setTitleBar(true);
    p0->titleBarWidget()->addStyleClass("nav-header");
    lv0->addWidget(p0);

    Wt::WTable * t0 = new Wt::WTable(); t0->setObjectName("t0");
    p0->setCentralWidget(t0);

    int r(0); 

    l = new Wt::WText(L"��� ��:", t0->elementAt(r, 0));
    l->setWordWrap(true);
    cbVehicle_type = new TComboBoxDirLine(dl, 14, t0->elementAt(r, 1)); cbVehicle_type->setObjectName("cbVehicle_type");
    cbVehicle_type->setNoSelectionEnabled(true);
    cbVehicle_type->valueChanged.connect(this, &TVehicleWidget::onTextInput);

    r++;

    l = new Wt::WText(L"�����:", t0->elementAt(r, 0));
    l->setWordWrap(true);
    eBrand = new Wt::WLineEdit(t0->elementAt(r, 1)); eBrand->setObjectName("eBrand");
    eBrand->setMaxLength(64);
    eBrand->textInput().connect(this, &TVehicleWidget::onTextInput);

    l = new Wt::WText(L"������:", t0->elementAt(r, 2));
    l->setWordWrap(true);
    eModel = new Wt::WLineEdit(t0->elementAt(r, 3)); eModel->setObjectName("eModel");
    eModel->setMaxLength(64);
    eModel->textInput().connect(this, &TVehicleWidget::onTextInput);

    r++;

    l = new Wt::WText(L"VIN/����� ������:", t0->elementAt(r, 0));
    l->setWordWrap(true);
    eVin = new Wt::WLineEdit(t0->elementAt(r, 1)); eVin->setObjectName("eVin");
    eVin->setEnabled(true);
    eVin->setMaxLength(17);
    eVin->textInput().connect(this, &TVehicleWidget::onTextInput);

    l = new Wt::WText(L"������ ����:", t0->elementAt(r, 2));
    l->setWordWrap(true);
    cbRight_hand_drive = new Wt::WCheckBox(t0->elementAt(r, 3)); cbRight_hand_drive->setObjectName("cbRight_hand_drive");
    cbRight_hand_drive->setEnabled(true);
    cbRight_hand_drive->changed().connect(this, &TVehicleWidget::onTextInput);

    r++;

    l = new Wt::WText(L"��� �������:", t0->elementAt(r, 0));
    l->setWordWrap(true);
    sbCar_year = new Wt::WSpinBox(t0->elementAt(r, 1)); sbCar_year->setObjectName("sbCar_year");
    sbCar_year->setEnabled(true);
    sbCar_year->setMinimum(1915); sbCar_year->setMaximum(Wt::WDate::currentDate().year());
    sbCar_year->changed().connect(this, &TVehicleWidget::onTextInput);

    r++;

    l = new Wt::WText(L"��������:", t0->elementAt(r, 0));
    l->setWordWrap(true);
    eRegistration_number = new Wt::WLineEdit(t0->elementAt(r, 1)); eRegistration_number->setObjectName("eRegistration_number");
    eRegistration_number->setEnabled(true);
    eRegistration_number->setMaxLength(16);
    eRegistration_number->textInput().connect(this, &TVehicleWidget::onTextInput);
    r++;
    Twindow::setTablePadding(t0);

    p1 = new Wt::WPanel(); p1->setObjectName("p0");
    p1->setTitle(Wt::WString(L"��������� �� ��"));
    p1->setTitleBar(true);
    p1->titleBarWidget()->addStyleClass("nav-header");
    lv0->addWidget(p1);

    t0 = new Wt::WTable(); t0->setObjectName("t0");
    p1->setCentralWidget(t0);

    r = 0;

    l = new Wt::WText(L"��� ���������:", t0->elementAt(r, 0));
    l->setWordWrap(true);
    cbVehicle_doc = new TComboBoxDirLine(dl, 16, t0->elementAt(r, 1)); cbVehicle_doc->setObjectName("cbVehicle_doc");
    cbVehicle_doc->setNoSelectionEnabled(true);
    cbVehicle_doc->valueChanged.connect(this, &TVehicleWidget::onTextInput);

    l = new Wt::WText(L"���� ������ ���������:", t0->elementAt(r, 2));
    l->setWordWrap(true);
    deDate_doc = new TDateEdit(t0->elementAt(r, 3)); deDate_doc->setObjectName("deDate_doc");
    deDate_doc->changed().connect(this, &TVehicleWidget::onTextInput);

    r++;

    l = new Wt::WText(L"�����:", t0->elementAt(r, 0));
    l->setWordWrap(true);
    eSeries = new Wt::WLineEdit(t0->elementAt(r, 1)); eSeries->setObjectName("eSeries");
    eSeries->setMaxLength(16);
    eSeries->setEnabled(true);
    eSeries->textInput().connect(this, &TVehicleWidget::onTextInput);

    l = new Wt::WText(L"�����:", t0->elementAt(r, 2));
    l->setWordWrap(true);
    eNumberV = new Wt::WLineEdit(t0->elementAt(r, 3)); eNumberV->setObjectName("eNumberV");
    eNumberV->setEnabled(true);
    eNumberV->setMaxLength(16);
    eNumberV->textInput().connect(this, &TVehicleWidget::onTextInput);

    r++;

    l = new Wt::WText(L"���� ������ ������������:", t0->elementAt(r, 0));
    l->setWordWrap(true);
    deDate_start = new TDateEdit(t0->elementAt(r, 1)); deDate_start->setObjectName("deDate_start");
    deDate_start->changed().connect(this, &TVehicleWidget::onTextInput);

    Twindow::setTablePadding(t0);

    p2 = new Wt::WPanel(); p2->setObjectName("p2");
    p2->setTitle(Wt::WString(L"�������������"));
    p2->setTitleBar(true);
    p2->titleBarWidget()->addStyleClass("nav-header");
    lv0->addWidget(p2);

    t0 = new Wt::WTable(); t0->setObjectName("t0");
    p2->setCentralWidget(t0);

    r = 0;

    l = new Wt::WText(L"������ ����������� ��� ��� ��������� �����:", t0->elementAt(r, 0));
    l->setWordWrap(true);
    cbTd_passport = new Wt::WCheckBox(t0->elementAt(r, 1)); cbTd_passport->setObjectName("cbTd_passport");
    cbTd_passport->setEnabled(true);
    cbTd_passport->changed().connect(this, &TVehicleWidget::onTextInput);

    r++;

    l = new Wt::WText(L"���� ������ ������� ��:", t0->elementAt(r, 0));
    l->setWordWrap(true);
    deDate_sale = new TDateEdit(t0->elementAt(r, 1)); deDate_sale->setObjectName("deDate_sale");
    deDate_sale->changed().connect(this, &TVehicleWidget::onTextInput);

    l = new Wt::WText(L"���� ��������� �������:", t0->elementAt(r, 2));
    l->setWordWrap(true);
    deLast_sale = new TDateEdit(t0->elementAt(r, 2)); deLast_sale->setObjectName("deLast_sale");
    deLast_sale->changed().connect(this, &TVehicleWidget::onTextInput);

    r++;

    l = new Wt::WText(L"�� �� ��������:", t0->elementAt(r, 0));
    l->setWordWrap(true);
    cbVehicle_warranty = new Wt::WCheckBox(t0->elementAt(r, 1)); cbVehicle_warranty->setObjectName("cbVehicle_warranty");
    cbVehicle_warranty->setEnabled(true);
    cbVehicle_warranty->changed().connect(this, &TVehicleWidget::onTextInput);

    Twindow::setTablePadding(t0);
}

void TVehicleWidget::refresh()
{
    cbVehicle_type->setValue(vehicle_type);
    eBrand->setText(Wt::WString::fromUTF8(brand)); //onBrandChanged(brand);
    eModel->setText(Wt::WString::fromUTF8(model));
    eVin->setText(Wt::WString::fromUTF8(vin));
    cbRight_hand_drive->setChecked(right_hand_drive);
    sbCar_year->setValue(car_year);
    eRegistration_number->setText(Wt::WString::fromUTF8(registration_number));
    cbVehicle_doc->setValue(vehicle_doc);
    eSeries->setText(Wt::WString::fromUTF8(series));
    eNumberV->setText(Wt::WString::fromUTF8(number));
    deDate_doc->setDate(date_doc);
    deDate_start->setDate(date_start);
    cbTd_passport->setChecked(td_passport);
    deDate_sale->setDate(date_sale);
    deLast_sale->setDate(last_sale);
    cbVehicle_warranty->setChecked(vehicle_warranty);
}

void TVehicleWidget::onTextInput()
{
    Wt::WObject * w = sender();
    if (w == NULL || w->objectName().empty()) return;

    if (w->objectName() == "cbVehicle_type")
    {
        vehicle_type = cbVehicle_type->value();
    }
    else if (w->objectName() == "eBrand")
    {
        brand = eBrand->text().trim().toUTF8();
    }
    else if (w->objectName() == "eModel")
    {
        model = eModel->text().trim().toUTF8();
    }
    else if (w->objectName() == "eVin")
    {
        vin = eVin->text().trim().toUTF8();
    }
    else if (w->objectName() == "cbRight_hand_drive")
    {
        right_hand_drive = cbRight_hand_drive->isChecked();
    }
    else if (w->objectName() == "sbCar_year")
    {
        car_year = sbCar_year->value();
    }
    else if (w->objectName() == "eRegistration_number")
    {
        registration_number = eRegistration_number->text().trim().toUTF8();
    }
    else if (w->objectName() == "cbVehicle_doc")
    {
        vehicle_doc = cbVehicle_doc->value();
    }
    else if (w->objectName() == "eSeries")
    {
        series = eSeries->text().trim().toUTF8();
    }
    else if (w->objectName() == "eNumberV")
    {
        number = eNumberV->text().trim().toUTF8();
    }
    else if (w->objectName() == "deDate_doc")
    {
        date_doc = deDate_doc->date();
    }
    else if (w->objectName() == "deDate_start")
    {
        date_start = deDate_start->date();
    }
    else if (w->objectName() == "cbTd_passport")
    {
        td_passport = cbTd_passport->isChecked();
    }
    else if (w->objectName() == "deDate_sale")
    {
       date_sale = deDate_sale->date();
    }
    else if (w->objectName() == "deLast_sale")
    {
        last_sale = deLast_sale->date();
    }
    else if (w->objectName() == "cbVehicle_warranty")
    {
        vehicle_warranty = cbVehicle_warranty->isChecked();
    }
    changed.emit();
}

//--------------------------------------------------------------------------
TDamageWidget::TDamageWidget(Wt::WContainerWidget * parent, TDataLayer * p_dl) : Wt::WPanel(parent), TDamage(p_dl)
{
    mtKind.insertRow(0, new Wt::WStandardItem());
    mtKind.setData(0, 0, 0);
    mtKind.setData(0, 3, Wt::WString(""));

    setupUi();
}

void TDamageWidget::setupUi()
{
    setTitleBar(true);
    setCollapsible(true);

    setObjectName("TDamageWidget");
    t0 = new Wt::WTable(); t0->setObjectName("t2");
    setCentralWidget(t0);

    collapsed().connect(this, &TDamageWidget::onUnCollapsed);
    expanded().connect(this, &TDamageWidget::onUnCollapsed);

    setCollapsed(true);
}
void TDamageWidget::refresh()
{
 Wt::WText * l; Wt::WString name; TCommandParam * cmd;
 int element_id;
 TComboBox * cbKind;

 t0->clear();
 for(int i = 0; i < mtElement.rowCount(); i++)
 {
   name = to_wstring(mtElement.data(i, fieldByName(&mtElement, "name")));
   element_id = to_int(mtElement.data(i, fieldByName(&mtElement, "line_id")));
   l = new Wt::WText(name, t0->elementAt(i, 0)); l->setObjectName("l");
   l->setWordWrap(true);

   cbKind = new TComboBox(t0->elementAt(i, 1)); cbKind->setObjectName("cbKind");
   cbKind->setNoSelectionEnabled(true);
   cbKind->setModel(&mtKind);
   cbKind->setLabelKeyColumn(""/*"�����:"*/, "line_id", "name");
   int k = getKind(element_id);
   if (k > 0) cbKind->setValue(k); 

   cmd = new TCommandParam(element_id, cbKind);
   cmd->fire.connect(this, &TDamageWidget::onKindChanged);
   cbKind->valueChanged.connect(cmd, &TCommandParam::exec);
 }

 Twindow::setTablePadding(t0, 2);

 onUnCollapsed();
}

Wt::WString TDamageWidget::getString()
{
    int idx;
    std::stringstream stm;
    for(int i = 0; i < mtElement.rowCount(); i++)
    {
        int element_id = to_int(mtElement.data(i, fieldByName(&mtElement, "line_id")));
        idx = getKind(element_id);
        if (idx == 0) continue;
        stm << " " << to_string(mtElement.data(i, fieldByName(&mtElement, "name")));
    }
    return(Wt::WString::fromUTF8(stm.str()));
}

void TDamageWidget::onUnCollapsed()
{
 if(isCollapsed())
 {
  setTitle(Wt::WString(L"�����������:") + getString());
 }
 else
 {
  setTitle(Wt::WString(L"�����������"));
 }
}

void TDamageWidget::onKindChanged(int element_id)
{
    TCommandParam * cmd = dynamic_cast<TCommandParam *>(sender());
    if (cmd == NULL) return;
    TComboBox * b = dynamic_cast<TComboBox *>(cmd->parent());
    if (b == NULL) return;

    int kind_id = b->value(); 

    setKind(cmd->parameter, kind_id);
}
//--------------------------------------------------------------------------
//--------------------------------------------------------------------------
TExecutorItemWidget::TExecutorItemWidget(const int p_job, const int p_partner_id, TDataLayer * dl) : Wt::WContainerWidget(NULL), TBasic(dl), 
  partner_id(p_partner_id), task(dl)
{
    task.job = p_job;
    setupUi();
}
void TExecutorItemWidget::setupUi()
{
    int r(0);
    Wt::WGridLayout * lmain = new Wt::WGridLayout();
    lmain->setContentsMargins(0, 0, 0, 0);
    setLayout(lmain);

    cbExecutor = new TComboBox(NULL); cbExecutor->setObjectName("cbExecutor");
    cbExecutor->setNoSelectionEnabled(true);    
    cbExecutor->setLabelKeyColumn("", "USER_ID", "FIO");
    cbExecutor->key = 0; // "USER_ID"
    cbExecutor->comboBox->setModelColumn(1); // "FIO"
    cbExecutor->setMargin(0);
    cbExecutor->valueChanged.connect(this, &TExecutorItemWidget::onExecutorChanged);

 //   cbExecutor->valueChanged.connect(this, &TExecutorWidget::onUserChanged0);
    lmain->addWidget(cbExecutor, r, 0, 0, 0, Wt::AlignLeft);
    r++;

    bTask = new Wt::WPushButton(Wt::WString(L"������")); bTask->setObjectName("bTask");
    lmain->addWidget(bTask, r, 0, 0, 0, Wt::AlignLeft);
    bTask->clicked().connect(this, &TExecutorItemWidget::onTask);
    bTask->setMargin(0);
    bTask->setEnabled(false);
}
void TExecutorItemWidget::refresh()
{
 cbExecutor->setValue(task.executor);
}

void TExecutorItemWidget::clear()
{
    int job(task.job);
    task.clear();
    task.job = job;
}

bool TExecutorItemWidget::read()
{
  Wt::WStandardItemModel mt;
  clear();
  if(dl->TaskS(&mt, ic_id) > 0)
  {
      TUser user(dl);
      for(int i = 0; i < mt.rowCount(); i++)
      {
          if (task.job != to_int(mt.data(i, fieldByName(&mt, "job")))) continue;
          user.user_id = to_int(mt.data(i, fieldByName(&mt, "executor")));
          user.read();
          if (user.partner_id != partner_id) continue;

          task.task_id = to_int(mt.data(i, fieldByName(&mt, "task_id")));
          task.ic_id = to_int(mt.data(i, fieldByName(&mt, "ic_id")));
          task.initiator = to_int(mt.data(i, fieldByName(&mt, "initiator")));
          task.executor = to_int(mt.data(i, fieldByName(&mt, "executor")));
          task.task = to_string(mt.data(i, fieldByName(&mt, "task")));
          task.execute_before = to_wdate(mt.data(i, fieldByName(&mt, "execute_before")));
          task.type_task = to_int(mt.data(i, fieldByName(&mt, "type_task")));
          task.job = to_int(mt.data(i, fieldByName(&mt, "job")));

          break;
      }
  }
  return(task.task_id);
}

int TExecutorItemWidget::save()
{
    task.ic_id = ic_id;

    if (task.executor == 0 && task.task_id == 0) return(0);
    if (task.executor == 0 && task.task_id != 0) {task.remove(); return(0);}

    return(task.save());
}

void TExecutorItemWidget::setPartner(const int p_partner_id)
{
  partner_id = p_partner_id;
  dl->UsersS(cbExecutor->mt, 0);
  cbExecutor->setLabelKeyColumn("", "USER_ID", "FIO");
}

void TExecutorItemWidget::onTask()
{
    Wt::WDialog * dialog = new Wt::WDialog(L"������", this);
    dialog->finished().connect(this, &TExecutorItemWidget::onTaskDialogDone);

    Wt::WTable * t0 = new Wt::WTable(dialog->contents()); t0->setObjectName("t0");

    int r(0);

    Wt::WText *  l = new Wt::WText(L"��� ������:", t0->elementAt(r, 0));
    cbTypeTask = new TComboBoxDirLine(dl, 22, t0->elementAt(r, 1)); cbTypeTask->setObjectName("cbTypeTask");
    cbTypeTask->setNoSelectionEnabled(true);
    //  cbTypeTask->valueChanged.connect(this, &TtaskUpd::onTextInput);
    r++;

    l = new Wt::WText(L"����:", t0->elementAt(r, 0));
    deExecuteBefore = new TDateEdit(t0->elementAt(r, 1)); deExecuteBefore->setObjectName("deExecuteBefore");
    //  deExecuteBefore->textInput().connect(this, &TtaskUpd::onTextInput);
    deExecuteBefore->setEnabled(true);
    r++;

    l = new Wt::WText(L"������:", t0->elementAt(r, 0));
    t0->elementAt(r, 1)->setColumnSpan(t0->columnCount() - 1);
    teTask = new Wt::WTextArea(t0->elementAt(r, 1)); teTask->setObjectName("teTask");
    //    teTask->textInput().connect(this, &TtaskUpd::onTextInput);
    teTask->setEnabled(true);
    r++;

    Twindow::setTablePadding(t0, 2);

    TOkFooter * footer = new TOkFooter(dialog->footer());
    footer->bCancel->clicked().connect(dialog, &Wt::WDialog::reject);
    footer->bOk->setEnabled(true);
    footer->bOk->clicked().connect(dialog, &Wt::WDialog::accept);

    cbTypeTask->setValue(task.type_task);
    deExecuteBefore->setDate(task.execute_before);
    teTask->setText(Wt::WString::fromUTF8(task.task));

   dialog->show();
}

void TExecutorItemWidget::onTaskDialogDone(Wt::WDialog::DialogCode code)
{
    Wt::WDialog * dialog = dynamic_cast<Wt::WDialog *>(sender());
    if (dialog == NULL) return;

    if (code == Wt::WDialog::Accepted)
    {
        task.type_task = cbTypeTask->value();
        task.execute_before = deExecuteBefore->date();
        task.task = teTask->text().toUTF8();
    }
    delete(dialog);
}

void TExecutorItemWidget::onExecutorChanged()
{
  if(task.executor != cbExecutor->value())
  {
    task.executor = cbExecutor->value();
    task.initiator = dl->SYSGetUser();
  }
  bTask->setEnabled(task.executor != 0);
}

//--------------------------------------------------------------------------
//--------------------------------------------------------------------------
TExecutorWidget::TExecutorWidget(Wt::WContainerWidget * parent, TDataLayer * p_dl) : Wt::WPanel(parent), TBasic(p_dl), state0(0, dl), state1(0, dl)
{
    bd = NULL;
    ai = NULL;

    p_dl->DIRLineS(&mtJob, 20);
    p_dl->UsersS(&mtUser, 0);

    initmtUser(&mtUser0, 0);

    setupUi();
}

void TExecutorWidget::setupUi()
{
    int r(0);

    Wt::WContainerWidget * c = new Wt::WContainerWidget();

    Wt::WGridLayout * lmain = new Wt::WGridLayout();
    lmain->setContentsMargins(2, 2, 2, 2);
    c->setLayout(lmain);

    setCentralWidget(c);

    Wt::WText * l = new Wt::WText(L"�������:");
    lmain->addWidget(l, r, 0, Wt::AlignLeft);

    //l = new Wt::WText(L"���"); 
    //lmain->addWidget(l, r, 1, Wt::AlignLeft);

    cbFirm = new TComboBoxDirLine(dl, 25); cbFirm->setObjectName("cbFirm");
    cbFirm->setNoSelectionEnabled(true);
    cbFirm->setValue(0);
    cbFirm->valueChanged.connect(this, &TExecutorWidget::onFirmChanged);
    lmain->addWidget(cbFirm, r, 1, Wt::AlignLeft);

    cbPartner = new TComboBox(); cbPartner->setObjectName("cbPartner");
    lmain->addWidget(cbPartner, r, 2);
    cbPartner->setNoSelectionEnabled(true);
    if (dl->TableS("PARTNER", cbPartner->mt) < 0)
    {
        ShowMessage(0, Wt::WString::fromUTF8(str2html(dl->errorstr)), "DB Error");
    }
    cbPartner->setLabelKeyColumn("", "PARTNER_ID", "NAME");
    cbPartner->valueChanged.connect(this, &TExecutorWidget::onPartnerChanged);
    r++;

    for(int i = 0; i < mtJob.rowCount(); i++)
    {
        std::stringstream stm;
        stm << to_string(mtJob.data(i, fieldByName(&mtJob, "NAME"))) << ":";
        l = new Wt::WText(Wt::WString::fromUTF8(stm.str()));
        lmain->addWidget(l, r, 0,  0, 0, Wt::AlignLeft);

        TExecutorItemWidget * item = new TExecutorItemWidget(to_int(mtJob.data(i, fieldByName(&mtJob, "line_id"))), 0, dl);
        //item->setPartner(0);
        initmtUser(item->cbExecutor->mt, 0);
        lstItem.push_back(item);
        lmain->addWidget(item, r, 1, 0, 0, Wt::AlignLeft);

        item = new TExecutorItemWidget(to_int(mtJob.data(i, fieldByName(&mtJob, "line_id"))), 1, dl);
        lstItem.push_back(item);
        lmain->addWidget(item, r, 2, 0, 0, Wt::AlignLeft);

        r++;
    }

    l = new Wt::WText(L"������:");
    lmain->addWidget(l, r, 0, Wt::AlignLeft);

    cbStatus0 = new TComboBoxDirLine(dl, 23); cbStatus0->setObjectName("cbStatus0");
    cbStatus0->setNoSelectionEnabled(true);
    cbStatus0->setValue(0);
    // cbStatus0->valueChanged.connect(this, &TDriverWidget::onTextInput);
    lmain->addWidget(cbStatus0, r, 1, 0, 0, Wt::AlignLeft);

    cbStatus1 = new TComboBoxDirLine(dl, 24); cbStatus1->setObjectName("cbStatus1");
    cbStatus1->setNoSelectionEnabled(true);
    cbStatus1->setValue(0);
    // cbStatus0->valueChanged.connect(this, &TDriverWidget::onTextInput);
    lmain->addWidget(cbStatus1, r, 2,  0, 0, Wt::AlignLeft);

    lmain->setColumnStretch(2, 1);
}

void TExecutorWidget::refresh()
{
  if (bd != NULL) cbPartner->setValue(bd->partner_id);
  for(auto itr = lstItem.begin(); itr != lstItem.end(); ++itr)
  {(*itr)->refresh();}

  cbStatus0->setValue(state0.state);
  cbStatus1->setValue(state1.state);
}

void TExecutorWidget::clear()
{
    for(auto itr = lstItem.begin(); itr != lstItem.end(); ++itr)
    {
        (*itr)->clear();
    }
    state0.clear();
    int partner_id = state1.partner_id;
    state1.clear();
    state1.partner_id = partner_id;
}

bool TExecutorWidget::read()
{
 Wt::WStandardItemModel mt;
 clear();

 if (bd != NULL)
     cbFirm->setValue(bd->firm);

 state0.ic_id = ic_id;
 state1.ic_id = ic_id;
 if (bd != NULL)
   state1.partner_id = bd->partner_id;

 if (ic_id > 0 && dl->TableS("STATE", &mt, ic_id) > 0)
 {
    state0.read(&mt);
    state1.read(&mt);
 }

 for(auto itr = lstItem.begin(); itr != lstItem.end(); ++itr)
 {
   (*itr)->ic_id = ic_id;
   (*itr)->read();
 }
 return(true);
}
int TExecutorWidget::save()
{
    for(auto itr = lstItem.begin(); itr != lstItem.end(); ++itr)
    {
        (*itr)->ic_id = ic_id;
        (*itr)->save();
    }

    if (state0.state != cbStatus0->value()) {state0.state = cbStatus0->value(); state0.state_id = 0; state0.save();}
    if (state1.state != cbStatus1->value()) {state1.state = cbStatus1->value(); state1.state_id = 0; state1.save();}

 return(1);
}

bool TExecutorWidget::remove()
{
 return(true);
}

void TExecutorWidget::initmtUser(Wt::WStandardItemModel * mt, const int partner_id)
{
    int fieldId(fieldByName(&mtUser, "USER_ID")),
        fieldFio(fieldByName(&mtUser, "FIO")),
        fieldPartner(fieldByName(&mtUser, "partner_id"));

    mt->clear();

    if (mt->columnCount() < 2)
    {
        mt->insertColumn(0); mt->setHeaderData(0, Wt::Orientation::Horizontal, "USER_ID");
        mt->insertColumn(1); mt->setHeaderData(1, Wt::Orientation::Horizontal, "FIO");
        mt->insertRow(0, new Wt::WStandardItem()); // ������ ������
        mt->setData(0, 0, 0);mt->setData(0, 1, Wt::WString(""));
    }

    for(int i = 0; i < mtUser.rowCount(); i++)
    {
        if(partner_id != to_int(mtUser.data(i, fieldPartner))) continue;
        if(10 > to_int(mtUser.data(i, fieldId))) continue;               // ��������� ������������� �� ����������   // TODO : ������ �������

        int row = mt->rowCount();
        mt->insertRow(row);
        mt->setData(row, 0, to_int(mtUser.data(i, fieldId)));
        mt->setData(row, 1, to_string(mtUser.data(i, fieldFio)));
    }
}

void TExecutorWidget::onPartnerChanged()
{
 if (bd != NULL) bd->partner_id = cbPartner->value();
 state1.partner_id = cbPartner->value();

 if (ai != NULL)
 {
     ai->responsible = cbPartner->comboBox->currentText().toUTF8();
     eResponsible->setText(cbPartner->comboBox->currentText());
 }

 for(auto itr = lstItem.begin(); itr != lstItem.end(); ++itr)
 {
     if ((*itr)->partner_id == 0) continue;
     (*itr)->partner_id = cbPartner->value();
     initmtUser((*itr)->cbExecutor->mt, cbPartner->value());
 }
}

void TExecutorWidget::onFirmChanged()
{
    if (bd != NULL)
        bd->firm = cbFirm->value();
}

//--------------------------------------------------------------------------
//--------------------------------------------------------------------------

TEntityWidget::TEntityWidget(Wt::WContainerWidget *parent, TDataLayer * p_dl) : Wt::WContainerWidget(parent), TEntity(p_dl), changed(this)
{
    setupUi();
}

void TEntityWidget::setupUi()
{
    Wt::WText * l;
    t0 = new Wt::WTable(); t0->setObjectName("t0");

    addWidget(t0);

    int r(0);

    l = new Wt::WText(L"��������:", t0->elementAt(r, 0));
    l->setWordWrap(true);
    eName = new Wt::WLineEdit(t0->elementAt(r, 1)); eName->setObjectName("eName");
    eName->setEnabled(true);
    eName->setMaxLength(128);
    eName->textInput().connect(this, &TEntityWidget::onTextInput);

    l = new Wt::WText(L"���:", t0->elementAt(r, 2));
    l->setWordWrap(true);
    eITN = new Wt::WLineEdit(t0->elementAt(r, 3)); eITN->setObjectName("eITN");
    eITN->setEnabled(true);
    eITN->setMaxLength(10);
    eITN->textInput().connect(this, &TEntityWidget::onTextInput);

    r++;

    Twindow::setTablePadding(t0);
}
void TEntityWidget::refresh()
{
    eName->setText(Wt::WString::fromUTF8(name));
    eITN->setText(Wt::WString::fromUTF8(itn));
}
void TEntityWidget::onTextInput()
{
    Wt::WObject * w = sender();
    if (w == NULL || w->objectName().empty()) return;

    if (w->objectName() == "eName")
    {
        name = eName->text().trim().toUTF8();
    }
    else if (w->objectName() == "eITN")
    {
        itn = eITN->text().trim().toUTF8();
    }

    changed.emit();
}
//--------------------------------------------------------------------------
//--------------------------------------------------------------------------
TPaymentDetailsWidget::TPaymentDetailsWidget(Wt::WContainerWidget *parent, TDataLayer * p_dl) : Wt::WContainerWidget(parent), TPaymentDetails(p_dl), changed(this)
{
    setupUi();
}
void TPaymentDetailsWidget::setupUi()
{
    Wt::WText * l;
    t0 = new Wt::WTable(); t0->setObjectName("t0");

    addWidget(t0);

    int r(0);

    l = new Wt::WText(L"����������:", t0->elementAt(r, 0));
    l->setWordWrap(true);
    eRecipient = new Wt::WLineEdit(t0->elementAt(r, 1)); eRecipient->setObjectName("eRecipient");
    eRecipient->setMaxLength(128);
    eRecipient->setEnabled(true);
    eRecipient->textInput().connect(this, &TPaymentDetailsWidget::onTextInput);

    r++;

    l = new Wt::WText(L"����. ���� ����������:", t0->elementAt(r, 0));
    l->setWordWrap(true);
    eCheckingAccount = new Wt::WLineEdit(t0->elementAt(r, 1)); eCheckingAccount->setObjectName("eCheckingAccount");
    eCheckingAccount->setMaxLength(20);
    eCheckingAccount->setEnabled(true);
    eCheckingAccount->textInput().connect(this, &TPaymentDetailsWidget::onTextInput);

    l = new Wt::WText(L"������������ �����:", t0->elementAt(r, 2));
    l->setWordWrap(true);
    eBank = new Wt::WLineEdit(t0->elementAt(r, 3)); eBank->setObjectName("eBank");
    eBank->setEnabled(true);
    eBank->setMaxLength(128);
    eBank->textInput().connect(this, &TPaymentDetailsWidget::onTextInput);

    r++;

    l = new Wt::WText(L"������/���������:", t0->elementAt(r, 0));
    l->setWordWrap(true);
    eDepartment = new Wt::WLineEdit(t0->elementAt(r, 1)); eDepartment->setObjectName("eDepartment");
    eDepartment->setEnabled(true);
    eDepartment->setMaxLength(128);
    eDepartment->textInput().connect(this, &TPaymentDetailsWidget::onTextInput);

    l = new Wt::WText(L"���. ����:", t0->elementAt(r, 2));
    l->setWordWrap(true);
    eCorrespondentAccount = new Wt::WLineEdit(t0->elementAt(r, 3)); eCorrespondentAccount->setObjectName("eBank");
    eCorrespondentAccount->setMaxLength(20);
    eCorrespondentAccount->setEnabled(true);
    eCorrespondentAccount->textInput().connect(this, &TPaymentDetailsWidget::onTextInput);

    r++;

    l = new Wt::WText(L"���:", t0->elementAt(r, 0));
    l->setWordWrap(true);
    eBic = new Wt::WLineEdit(t0->elementAt(r, 1)); eBic->setObjectName("eBic");
    eBic->setEnabled(true);
    eBic->setMaxLength(9);
    eBic->textInput().connect(this, &TPaymentDetailsWidget::onTextInput);

    l = new Wt::WText(L"���:", t0->elementAt(r, 2));
    l->setWordWrap(true);
    eITN = new Wt::WLineEdit(t0->elementAt(r, 3)); eITN->setObjectName("eITN");
    eITN->setMaxLength(10);
    eITN->setEnabled(true);
    eITN->textInput().connect(this, &TPaymentDetailsWidget::onTextInput);

    r++;

    l = new Wt::WText(L"����� �����:", t0->elementAt(r, 0));
    l->setWordWrap(true);
    eCard = new Wt::WLineEdit(t0->elementAt(r, 1)); eCard->setObjectName("eCard");
    eCard->setEnabled(true);
    eCard->setMaxLength(16);
    eCard->textInput().connect(this, &TPaymentDetailsWidget::onTextInput);

    r++;

    Twindow::setTablePadding(t0);
}
void TPaymentDetailsWidget::refresh()
{
    eCheckingAccount->setText(Wt::WString::fromUTF8(checking_account));
    eBank->setText(Wt::WString::fromUTF8(bank));
    eDepartment->setText(Wt::WString::fromUTF8(department));
    eCorrespondentAccount->setText(Wt::WString::fromUTF8(correspondent_account));
    eBic->setText(Wt::WString::fromUTF8(bic));
    eITN->setText(Wt::WString::fromUTF8(itn));
    eCard->setText(Wt::WString::fromUTF8(card));
    eRecipient->setText(Wt::WString::fromUTF8(recipient));
}
void TPaymentDetailsWidget::onTextInput()
{
    Wt::WObject * w = sender();
    if (w == NULL || w->objectName().empty()) return;

    if (w->objectName() == "eCheckingAccount")
    {
        checking_account = eCheckingAccount->text().trim().toUTF8();
    }
    else if (w->objectName() == "eDepartment")
    {
        department = eDepartment->text().trim().toUTF8();
    }
    else if (w->objectName() == "eCorrespondentAccount")
    {
        correspondent_account = eCorrespondentAccount->text().trim().toUTF8();
    }
    else if (w->objectName() == "eBic")
    {
        bic = eBic->text().trim().toUTF8();
    }
    else if (w->objectName() == "eITN")
    {
        itn = eITN->text().trim().toUTF8();
    }
    else if (w->objectName() == "eCard")
    {
        card = eCard->text().trim().toUTF8();
    }
    else if (w->objectName() == "eRecipient")
    {
        recipient = eRecipient->text().trim().toUTF8();
    }

    changed.emit();
}
//--------------------------------------------------------------------------
//--------------------------------------------------------------------------
TinsuranceCaseUpd::TinsuranceCaseUpd(Wt::WContainerWidget *parent, TDataLayer * p_dl, const int ic_id) : Twindow(p_dl, "TinsuranceCaseUpd", parent), 
  basicData(p_dl), applicationInspection(p_dl), culprit(p_dl), victim(p_dl)
{
 if (ic_id > 0)
 {
  basicData.ic_id = ic_id;
  basicData.read();                         // TODO: ��� ������ ������
  applicationInspection.ic_id = ic_id;
  applicationInspection.read();
  culprit.ic_id = ic_id;
  culprit.read();
  victim.ic_id = ic_id;
  victim.read();
 }
 setupUi();
 refresh();
}
//--------------------------------------------------------------------------
//TinsuranceCaseUpd::~TinsuranceCaseUpd()
//{
//}
//--------------------------------------------------------------------------
void TinsuranceCaseUpd::setupUi()
{
 Wt::WVBoxLayout * lv = new Wt::WVBoxLayout(this); lv->setObjectName("lv");

 setLayout(lv);

 tab = new Wt::WTabWidget();
 
 lv->addWidget(tab, 0, Wt::AlignmentFlag::AlignTop);

 Wt::WWidget * wApplicationInspection = setupUiApplicationInspection();

 tab->addTab(setupUiExecutor(), Wt::WString(L"�����������"));
 tab->addTab(setupUiBasic(), Wt::WString(L"�������� ������"));
 tab->addTab(wApplicationInspection, Wt::WString(L"��������� � ������"));
 tab->addTab(setupUiCulprit(), Wt::WString(L"��������"));
 tab->addTab(setupUiVictim(), Wt::WString(L"�����������"));
 tab->addTab(setupUiBeneficiary(), Wt::WString(L"�������������������"));
 tab->addTab(setupUiFile(), Wt::WString(L"��������"));
 //tab->addTab(setupUiTask(), Wt::WString(L"������"));

 TOkFooter * footer = new TOkFooter();
 footer->bCancel->clicked().connect(this, &TinsuranceCaseUpd::onbCancel);
 bOk = footer->bOk;
 bOk->clicked().connect(this, &TinsuranceCaseUpd::onbOk);

 Wt::WBoxLayout * bl = dynamic_cast<Wt::WBoxLayout *>(footer->layout());
 if (bl != NULL) 
 {
     bReport= new Wt::WPushButton(Wt::WString(L"���������")); bReport->setObjectName("bReport");
     bReport->setEnabled(true);
     bReport->clicked().connect(this, &TinsuranceCaseUpd::onbReport);
     bReport->setIcon(Wt::WLink("image/Printer.png"));
     bl->insertWidget(0, bReport);

     bOrder= new Wt::WPushButton(Wt::WString(L"�����������")); bOrder->setObjectName("bOrder");
     bOrder->setEnabled(true);
     bOrder->clicked().connect(this, &TinsuranceCaseUpd::onbOrder);
     bOrder->setIcon(Wt::WLink("image/Printer.png"));
     bl->insertWidget(0, bOrder);
 }

 lv->addWidget(footer);
}

 Wt::WWidget * TinsuranceCaseUpd::setupUiBasic()
 {
     Wt::WText * l;

     Wt::WVBoxLayout * lv0 = new Wt::WVBoxLayout(); lv0->setObjectName("lv0");

   //  Wt::WPanel * pBasic = new Wt::WPanel(); pBasic->setObjectName("pBasic");
     Wt::WContainerWidget * cBasic = new Wt::WContainerWidget();
     cBasic->setLayout(lv0);
     Wt::WTable * tMain = new Wt::WTable(); tMain->setObjectName("tMain");
     Wt::WPanel * p0 = new Wt::WPanel(); p0->setObjectName("p0");
     p0->setTitle(Wt::WString(L"�������������� �� �������"));
     p0->setTitleBar(true);
     p0->titleBarWidget()->addStyleClass("nav-header");
     lv0->addWidget(p0);

     Wt::WTable * t0 = new Wt::WTable(); t0->setObjectName("t2");
     p0->setCentralWidget(t0);

     int r(0); 

     cbSettlement = new Wt::WCheckBox(t0->elementAt(r, 1)); cbSettlement->setObjectName("cbSettlement");
     cbSettlement->changed().connect(this, &TinsuranceCaseUpd::onTextInput);
     l = new Wt::WText(L"�������������� �� �������:",  t0->elementAt(r, 0));
     r++;

     cbReimbursement_lmv = new Wt::WCheckBox(t0->elementAt(r, 1)); cbReimbursement_lmv->setObjectName("cbReimbursement_lmv");
     cbReimbursement_lmv->changed().connect(this, &TinsuranceCaseUpd::onTextInput);
     l = new Wt::WText(L"�������� ���������� ���:",  t0->elementAt(r, 0));
     r++;

     cbCalculation_lmv_possible = new Wt::WCheckBox(t0->elementAt(r, 1)); cbCalculation_lmv_possible->setObjectName("cbCalculation_lmv_possible");
     cbCalculation_lmv_possible->changed().connect(this, &TinsuranceCaseUpd::onTextInput);
     l = new Wt::WText(L"������ ��� ��������:",  t0->elementAt(r, 0));
     r++;

     Wt::WPanel * p1 = new Wt::WPanel(); p1->setObjectName("p1");
     p1->setTitle(Wt::WString(L"���������������� ������"));
     p1->setTitleBar(true);
     p1->titleBarWidget()->addStyleClass("nav-header");
     lv0->addWidget(p1);
     p1->setCentralWidget(tMain);

     r = 0;

     eNumber = new Wt::WLineEdit(); eNumber->setObjectName("eNumber");
     eNumber->setEnabled(true);
     eNumber->setMaxLength(64);
     eNumber->textInput().connect(this, &TinsuranceCaseUpd::onTextInput);
     l = new Wt::WText(L"����� ����:");
     l->setWordWrap(false);
     tMain->elementAt(r, 0)->addWidget(l);
     tMain->elementAt(r, 1)->addWidget(eNumber);

     cbInsuranceType = new TComboBoxDirLine(dl, 2); cbInsuranceType->setObjectName("cbInsuranceType");
     cbInsuranceType->setNoSelectionEnabled(true);
     cbInsuranceType->valueChanged.connect(this, &TinsuranceCaseUpd::onTextInput);
     l = new Wt::WText(L"��� �����������:");
     tMain->elementAt(r, 2)->addWidget(l);
     tMain->elementAt(r, 3)->addWidget(cbInsuranceType);
     r++;

     deTa = new TDateEdit(); deTa->setObjectName("deTa");
     deTa->setEnabled(true);
     deTa->textInput().connect(this, &TinsuranceCaseUpd::onTextInput);
     teTa = new TTimeEdit(); teTa->setObjectName("teTa");
     teTa->setEnabled(true);
     teTa->textInput().connect(this, &TinsuranceCaseUpd::onTextInput);
     l = new Wt::WText(L"���� ���:");
     tMain->elementAt(r, 0)->addWidget(l);
     tMain->elementAt(r, 1)->addWidget(deTa);
     l = new Wt::WText(L"����� ���:");
     tMain->elementAt(r, 2)->addWidget(l);
     tMain->elementAt(r, 3)->addWidget(teTa);
     r++;

     eInspection = new Wt::WLineEdit(); eInspection->setObjectName("eInspection");
     eInspection->setEnabled(true);
     eInspection->setMaxLength(64);
     eInspection->textInput().connect(this, &TinsuranceCaseUpd::onTextInput);
     l = new Wt::WText(L"��� ������� �:");
     tMain->elementAt(r, 0)->addWidget(l);
     tMain->elementAt(r, 1)->addWidget(eInspection);

     sbParticipants = new Wt::WSpinBox(); sbParticipants->setObjectName("sbParticipants");
     sbParticipants->setEnabled(true);
     sbParticipants->setRange(0, 99);
     sbParticipants->textInput().connect(this, &TinsuranceCaseUpd::onTextInput);
     sbParticipants->valueChanged().connect(this, &TinsuranceCaseUpd::onTextInput);

     l = new Wt::WText(L"���������� ���������� ���:");
     tMain->elementAt(r, 2)->addWidget(l);
     tMain->elementAt(r, 3)->addWidget(sbParticipants);
     r++;

     spCulprits = new Wt::WSpinBox(); spCulprits->setObjectName("spCulprits");
     spCulprits->setEnabled(true);
     spCulprits->setRange(0, 99);
     spCulprits->textInput().connect(this, &TinsuranceCaseUpd::onTextInput);
     spCulprits->valueChanged().connect(this, &TinsuranceCaseUpd::onTextInput);
     l = new Wt::WText(L"���������� ����������:");
     tMain->elementAt(r, 0)->addWidget(l);
     tMain->elementAt(r, 1)->addWidget(spCulprits);

     cbTa_certificate = new TComboBoxDirLine(dl, 8); cbTa_certificate->setObjectName("cbTa_certificate");
     cbTa_certificate->setNoSelectionEnabled(true);
     //cbTa_certificate->setLabelKeyColumn("�����:", "carlifter_id", "number");
     cbTa_certificate->valueChanged.connect(this, &TinsuranceCaseUpd::onTextInput);
     l = new Wt::WText(L"������� � ���:");
     tMain->elementAt(r, 2)->addWidget(l);
     tMain->elementAt(r, 3)->addWidget(cbTa_certificate);
     r++;

     cbOptional_equipment = new Wt::WCheckBox(); cbOptional_equipment->setObjectName("cbOptional_equipment");
     cbOptional_equipment->changed().connect(this, &TinsuranceCaseUpd::onTextInput);
     l = new Wt::WText(L"�������������� ������������:");
     tMain->elementAt(r, 0)->addWidget(l);
     tMain->elementAt(r, 1)->addWidget(cbOptional_equipment);

     cbInspection_type = new TComboBoxDirLine(dl, 13); cbInspection_type->setObjectName("cbInspection_type");
     cbInspection_type->setNoSelectionEnabled(true);
     cbInspection_type->valueChanged.connect(this, &TinsuranceCaseUpd::onTextInput);
     l = new Wt::WText(L"������:");
     tMain->elementAt(r, 2)->addWidget(l);
     tMain->elementAt(r, 3)->addWidget(cbInspection_type);
     r++;

     cbApplication = new Wt::WCheckBox(); cbApplication->setObjectName("cbApplication");
     cbApplication->changed().connect(this, &TinsuranceCaseUpd::onTextInput);
     l = new Wt::WText(L"������� ���������:");
     tMain->elementAt(r, 0)->addWidget(l);
     tMain->elementAt(r, 1)->addWidget(cbApplication);

     cbIncident_type = new TComboBoxDirLine(dl, 3); cbIncident_type->setObjectName("cbIncident_type");
     cbIncident_type->setNoSelectionEnabled(true);
     //cbTa_certificate->setLabelKeyColumn("�����:", "carlifter_id", "number");
     cbIncident_type->valueChanged.connect(this, &TinsuranceCaseUpd::onTextInput);
     l = new Wt::WText(L"��� ������������:");
     tMain->elementAt(r, 2)->addWidget(l);
     tMain->elementAt(r, 3)->addWidget(cbIncident_type);
     r++;

     cbCause_damage = new TComboBoxDirLine(dl, 4); cbCause_damage->setObjectName("cbCause_damage");
     cbCause_damage->setNoSelectionEnabled(true);
     //cbTa_certificate->setLabelKeyColumn("�����:", "carlifter_id", "number");
     cbCause_damage->valueChanged.connect(this, &TinsuranceCaseUpd::onTextInput);
     l = new Wt::WText(L"������� ������:");
     tMain->elementAt(r, 0)->addWidget(l);
     tMain->elementAt(r, 1)->addWidget(cbCause_damage);

     cbTa_location_type = new TComboBoxDirLine(dl, 5); cbTa_location_type->setObjectName("cbTa_location_type");
     cbTa_location_type->setNoSelectionEnabled(true);
     //cbTa_certificate->setLabelKeyColumn("�����:", "carlifter_id", "number");
     cbTa_location_type->valueChanged.connect(this, &TinsuranceCaseUpd::onTextInput);
     l = new Wt::WText(L"��� ����� ���:");
     tMain->elementAt(r, 2)->addWidget(l);
     tMain->elementAt(r, 3)->addWidget(cbTa_location_type);
     r++;

     cbFranchise = new Wt::WCheckBox(); cbFranchise->setObjectName("cbFranchise");
     cbFranchise->changed().connect(this, &TinsuranceCaseUpd::onTextInput);
     l = new Wt::WText(L"��������� �� ��������:");
     tMain->elementAt(r, 0)->addWidget(l);
     tMain->elementAt(r, 1)->addWidget(cbFranchise);

     l = new Wt::WText(L"������� �����-�������:",  tMain->elementAt(r, 2));
     l->setWordWrap(true);
     cbContract_sale = new Wt::WCheckBox(tMain->elementAt(r, 3)); cbContract_sale->setObjectName("cbContract_sale");
     cbContract_sale->changed().connect(this, &TinsuranceCaseUpd::onTextInput);
     r++;

     setTablePadding(tMain, 2);

     Wt::WPanel * pTa_place = new Wt::WPanel(); pTa_place->setObjectName("pTa_place");
     pTa_place->setTitle(Wt::WString(L"����� ������������"));
     pTa_place->setTitleBar(true);
     pTa_place->titleBarWidget()->addStyleClass("nav-header");
     lv0->addWidget(pTa_place);

     Wt::WTable * tTa_place = new Wt::WTable(this); tTa_place->setObjectName("tTa_place");
     pTa_place->setCentralWidget(tTa_place);

     int r1(0); 
     ePost_code = new Wt::WLineEdit(tTa_place->elementAt(r1, 1)); ePost_code->setObjectName("ePost_code");
     ePost_code->setEnabled(true);
     ePost_code->setMaxLength(10);
     ePost_code->textInput().connect(this, &TinsuranceCaseUpd::onTextInput);
     l = new Wt::WText(L"������:", tTa_place->elementAt(r1, 0));

     eAddr_field1 = new /*Wt::WLineEdit*/Wt::WTextArea(tTa_place->elementAt(r1, 3)); eAddr_field1->setObjectName("eAddr_field1");
     eAddr_field1->setEnabled(true);
   //  eAddr_field1->setRows(3); eAddr_field1->setColumns(42); 
   //  eAddr_field1->setMaxLength(128);
     eAddr_field1->textInput().connect(this, &TinsuranceCaseUpd::onTextInput);
     l = new Wt::WText(L"�����:", tTa_place->elementAt(r1, 2));
     r1++;

/*     cbStreet_type = new TComboBoxDirLine(dl, 6, tTa_place->elementAt(r1, 3)); cbStreet_type->setObjectName("cbStreet_type");
     cbStreet_type->setNoSelectionEnabled(true);
     //cbTa_certificate->setLabelKeyColumn("�����:", "carlifter_id", "number");
     cbStreet_type->valueChanged.connect(this, &TinsuranceCaseUpd::onTextInput);
     l = new Wt::WText(L"��� �����:", tTa_place->elementAt(r1, 2));*/

     setTablePadding(tTa_place, 2);

     Wt::WPanel * p2 = new Wt::WPanel(); p2->setObjectName("p2");
     p2->setTitle(Wt::WString(L"�����������"));
     p2->setTitleBar(true);
     p2->titleBarWidget()->addStyleClass("nav-header");
     lv0->addWidget(p2);

     Wt::WTable * t2 = new Wt::WTable(this); t2->setObjectName("t2");
     p2->setCentralWidget(t2);

     int r2(0); 

     cbNight = new Wt::WCheckBox(t2->elementAt(r2, 1)); cbNight->setObjectName("cbNight");
     cbNight->changed().connect(this, &TinsuranceCaseUpd::onTextInput);
     l = new Wt::WText(L"��� ��������� � ��������� �����/������ �����:",  t2->elementAt(r2, 0));
     r2++;

     cbMinimal_contact = new Wt::WCheckBox(t2->elementAt(r2, 1)); cbMinimal_contact->setObjectName("cbMinimal_contact");
     cbMinimal_contact->changed().connect(this, &TinsuranceCaseUpd::onTextInput);
     l = new Wt::WText(L"����� �� ������������/����������� � �� ��������� �� ���� �������� ��� ��� ����������� �������:",  t2->elementAt(r2, 0));
     r2++;

     cbCasualties = new Wt::WCheckBox(t2->elementAt(r2, 1)); cbCasualties->setObjectName("cbCasualties");
     cbCasualties->changed().connect(this, &TinsuranceCaseUpd::onTextInput);
     l = new Wt::WText(L"� ���������� ��� ������� ������������/�������� (������� � ������� �����):",  t2->elementAt(r2, 0));
     r2++;

     cbTransfer = new Wt::WCheckBox(t2->elementAt(r2, 1)); cbTransfer->setObjectName("cbTransfer");
     cbTransfer->changed().connect(this, &TinsuranceCaseUpd::onTextInput);
     l = new Wt::WText(L"��������� �� ������������� � ��������� ������ (��� �� ����� ���������� ���������, ��� � �� ����� ���������� ���������):",  t2->elementAt(r2, 0));
     r2++;

     cbMmobile_app = new Wt::WCheckBox(t2->elementAt(r2, 1)); cbMmobile_app->setObjectName("cbMmobile_app");
     cbMmobile_app->changed().connect(this, &TinsuranceCaseUpd::onTextInput);
     l = new Wt::WText(L"������ �� �������������, ������� �������� ����� ��������� ���������� ���:",  t2->elementAt(r2, 0));
     r2++;

     setTablePadding(t2);

     return (cBasic);
 }

 Wt::WWidget * TinsuranceCaseUpd::setupUiApplicationInspection()
 {
     Wt::WText * l;
     Wt::WVBoxLayout * lv0 = new Wt::WVBoxLayout(); lv0->setObjectName("lv0");

     Wt::WContainerWidget * cAI = new Wt::WContainerWidget(); cAI->setObjectName("cAI");
     cAI->setLayout(lv0);

     Wt::WPanel * p0 = new Wt::WPanel(); p0->setObjectName("p0");
     p0->setTitle(Wt::WString(L"������ ��������� � ������������ ������"));
     p0->setTitleBar(true);
     p0->titleBarWidget()->addStyleClass("nav-header");
     lv0->addWidget(p0);

     Wt::WTable * t0 = new Wt::WTable(); t0->setObjectName("t0");
     p0->setCentralWidget(t0);

     int r(0); 

     cbUnlimited_ep = new Wt::WCheckBox(t0->elementAt(r, 1)); cbUnlimited_ep->setObjectName("cbUnlimited_ep");
     cbUnlimited_ep->changed().connect(this, &TinsuranceCaseUpd::onTextInput);
     l = new Wt::WText(L"����������� ������������:",  t0->elementAt(r, 0));
     l->setWordWrap(true);

     cbDisagreements = new Wt::WCheckBox(t0->elementAt(r, 3)); cbDisagreements->setObjectName("cbDisagreements");
     cbDisagreements->changed().connect(this, &TinsuranceCaseUpd::onTextInput);
     l = new Wt::WText(L"������� ����������� � ���������� ���:",  t0->elementAt(r, 2));
     l->setWordWrap(true);

     setTablePadding(t0);

     Wt::WPanel * p1 = new Wt::WPanel(); p1->setObjectName("p1");
     p1->setTitle(Wt::WString(L"���������"));
     p1->setTitleBar(true);
     p1->titleBarWidget()->addStyleClass("nav-header");
     lv0->addWidget(p1);

     Wt::WTable * t1 = new Wt::WTable(); t1->setObjectName("t1");
     p1->setCentralWidget(t1);
      
     r = 0;

     cbExit_ad = new Wt::WCheckBox(t1->elementAt(r, 1)); cbExit_ad->setObjectName("cbExit_ad");
     cbExit_ad->changed().connect(this, &TinsuranceCaseUpd::onTextInput);
     l = new Wt::WText(L"�������� ����� ����������:",  t1->elementAt(r, 0));
     l->setWordWrap(true);

     cbRegion_circulation = new TComboBoxDirLine(dl, 7, t1->elementAt(r, 3)); cbRegion_circulation->setObjectName("cbRegion_circulation");
     cbRegion_circulation->setNoSelectionEnabled(true);
     cbRegion_circulation->valueChanged.connect(this, &TinsuranceCaseUpd::onTextInput);
     l = new Wt::WText(L"������ ���������:", t1->elementAt(r, 2));
     l->setWordWrap(true);
     r++;

     deDt_application = new TDateEdit(); deDt_application->setObjectName("deDt_application");
    // deDt_application->addStyleClass("input-small");
     deDt_application->textInput().connect(this, &TinsuranceCaseUpd::onTextInput);
     teDt_application = new TTimeEdit(); teDt_application->setObjectName("teDt_application");
    // teDt_application->addStyleClass("input-small");
     teDt_application->textInput().connect(this, &TinsuranceCaseUpd::onTextInput);
     l = new Wt::WText(L"���� � ����� ������ ���������:",  t1->elementAt(r, 0));
     l->setWordWrap(true);

     t1->elementAt(r, 1)->addWidget(deDt_application);
     t1->elementAt(r, 1)->addWidget(teDt_application);

     cbWho_applied = new TComboBoxDirLine(dl, 10, t1->elementAt(r, 3)); cbWho_applied->setObjectName("cbWho_applied");
     cbWho_applied->setNoSelectionEnabled(true);
     cbWho_applied->valueChanged.connect(this, &TinsuranceCaseUpd::onTextInput);
     l = new Wt::WText(L"����� ���������:", t1->elementAt(r, 2));
     l->setWordWrap(true);
     r++;

     /*cbScheme_number = new TComboBoxDirLine(dl, 11, t1->elementAt(r, 1)); cbScheme_number->setObjectName("cbScheme_number");
     cbScheme_number->setNoSelectionEnabled(true);
     cbScheme_number->valueChanged.connect(this, &TinsuranceCaseUpd::onTextInput);
     l = new Wt::WText(L"����� ������� ����� ���:", t1->elementAt(r, 0));
     l->setWordWrap(true);*/

     cbApplicants = new TComboBoxDirLine(dl, 12, t1->elementAt(r, 1)); cbApplicants->setObjectName("cbApplicants");
     cbApplicants->setNoSelectionEnabled(true);
     cbApplicants->valueChanged.connect(this, &TinsuranceCaseUpd::onTextInput);
     l = new Wt::WText(L"�� ��������� �� �����:", t1->elementAt(r, 0));
     l->setWordWrap(true);
     r++;

     setTablePadding(t1, 2);

     Wt::WPanel * p2 = new Wt::WPanel(); p2->setObjectName("p2");
     p2->setTitle(Wt::WString(L"������"));
     p2->setTitleBar(true);
     p2->titleBarWidget()->addStyleClass("nav-header");
     lv0->addWidget(p2);

     Wt::WTable * t2 = new Wt::WTable(); t2->setObjectName("t2");
     p2->setCentralWidget(t2);

     r = 0;

     cbRegion_inspection = new /*TComboBox(t2->elementAt(r, 1));*/TComboBoxDirLine(dl, 7, t2->elementAt(r, 1)); cbRegion_inspection->setObjectName("cbRegion_inspection");
//     if (dl->DIRLineS(cbRegion_inspection->mt, 7) < 0)
//     {ShowMessage(0, Wt::WString::fromUTF8(str2html(dl->errorstr)), "DB Error");}
//     cbRegion_inspection ->setLabelKeyColumn(std::string(), "line_id", "codename");
     cbRegion_inspection->setNoSelectionEnabled(true);
     cbRegion_inspection->valueChanged.connect(this, &TinsuranceCaseUpd::onTextInput);
     l = new Wt::WText(L"������ ���������� �������:", t2->elementAt(r, 0));
     l->setWordWrap(true);

   /*  eSubdivision = new Wt::WLineEdit(t2->elementAt(r, 3)); eSubdivision->setObjectName("eSubdivision");
     eSubdivision->setEnabled(true);
     eSubdivision->textInput().connect(this, &TinsuranceCaseUpd::onTextInput);
     l = new Wt::WText(L"�������������:", t2->elementAt(r, 2));
     l->setWordWrap(true);*/
     r++;

     eResponsible = new Wt::WLineEdit(t2->elementAt(r, 1)); eResponsible->setObjectName("eResponsible");
     eResponsible->setEnabled(false);
     eResponsible->textInput().connect(this, &TinsuranceCaseUpd::onTextInput);
     l = new Wt::WText(L"������������� �����������:", t2->elementAt(r, 0));
     l->setWordWrap(true);

     eInspection_address = new /*Wt::WLineEdit*/Wt::WTextArea(t2->elementAt(r, 3)); eInspection_address->setObjectName("eInspection_address");
     eInspection_address->setEnabled(true);
     eInspection_address->textInput().connect(this, &TinsuranceCaseUpd::onTextInput);
     l = new Wt::WText(L"����� �������:", t2->elementAt(r, 2));
     l->setWordWrap(true);
     r++;

     deDt_inspection = new TDateEdit(); deDt_inspection->setObjectName("deDt_inspection");
   //  deDt_inspection->setMargin(0,0);
     deDt_inspection->textInput().connect(this, &TinsuranceCaseUpd::onTextInput);
     teDt_inspection = new TTimeEdit(); teDt_inspection->setObjectName("teDt_inspection");
   //  teDt_inspection->setMargin(0,0);
     teDt_inspection->textInput().connect(this, &TinsuranceCaseUpd::onTextInput);
     l = new Wt::WText(L"������������� ���� � ����� �������:",  t2->elementAt(r, 0));
     l->setWordWrap(true);

     t2->elementAt(r, 1)->addWidget(deDt_inspection);
     t2->elementAt(r, 1)->addWidget(teDt_inspection);

     r++;

     cbOn_move = new Wt::WCheckBox(t2->elementAt(r, 1)); cbOn_move->setObjectName("cbOn_move");
     cbOn_move->changed().connect(this, &TinsuranceCaseUpd::onTextInput);
     l = new Wt::WText(L"�� �� ����:",  t2->elementAt(r, 0));
     l->setWordWrap(true);

     cbFlip = new Wt::WCheckBox(t2->elementAt(r, 3)); cbFlip->setObjectName("cbFlip");
     cbFlip->changed().connect(this, &TinsuranceCaseUpd::onTextInput);
     l = new Wt::WText(L"��������� ��:",  t2->elementAt(r, 2));
     l->setWordWrap(true);
     r++;

     cbFrontal = new Wt::WCheckBox(t2->elementAt(r, 1)); cbFrontal->setObjectName("cbFrontal");
     cbFrontal->changed().connect(this, &TinsuranceCaseUpd::onTextInput);
     l = new Wt::WText(L"������� ������� ������������:",  t2->elementAt(r, 0));
     l->setWordWrap(true);

     cbAirbags = new Wt::WCheckBox(t2->elementAt(r, 3)); cbAirbags->setObjectName("cbAirbags");
     cbAirbags->changed().connect(this, &TinsuranceCaseUpd::onTextInput);
     l = new Wt::WText(L"��������� ������� ������������:",  t2->elementAt(r, 2));
     l->setWordWrap(true);
     r++;

   //  eDamage = new Wt::WLineEdit(t2->elementAt(r, 1)); eDamage->setObjectName("eDamage");
   //  eDamage->setEnabled(true);
   //  eDamage->textInput().connect(this, &TinsuranceCaseUpd::onTextInput);
   //  l = new Wt::WText(L"�������� �����������:", t2->elementAt(r, 0));
   //  l->setWordWrap(true);

     Damage = new TDamageWidget(t2->elementAt(r, 0), dl); Damage->setObjectName("Damage");
     t2->elementAt(r, 0)->setColumnSpan(t2->columnCount());

     r++;

     l = new Wt::WText(L"���� ���������� �����������:", t2->elementAt(r, 0));
     l->setWordWrap(true);
     deRereferral = new TDateEdit(t2->elementAt(r, 1)); deRereferral->setObjectName("deRereferral");
     deRereferral->setEnabled(true);
     deRereferral->textInput().connect(this, &TinsuranceCaseUpd::onTextInput);

     l = new Wt::WText(L"���� ���������:", t2->elementAt(r, 2));
     l->setWordWrap(true);
     deReceiving = new TDateEdit(t2->elementAt(r, 3)); deReceiving->setObjectName("deReceiving");
     deReceiving->setEnabled(true);
     deReceiving->textInput().connect(this, &TinsuranceCaseUpd::onTextInput);

     r++;

     l = new Wt::WText(L"����� �������:", t2->elementAt(r, 0));
     l->setWordWrap(true);
     eRepair_cost = new Wt::WDoubleSpinBox(t2->elementAt(r, 1)); eRepair_cost->setObjectName("eRepair_cost");
     eRepair_cost->setRange(0, 9999999999);
     eRepair_cost->setDecimals(2);
     eRepair_cost->setSingleStep(1);
     eRepair_cost->setEnabled(true);
     eRepair_cost->textInput().connect(this, &TinsuranceCaseUpd::onTextInput);

     l = new Wt::WText(L"����� ������� ��� ������:", t2->elementAt(r, 2));
     l->setWordWrap(true);
     eWithout_wear = new Wt::WDoubleSpinBox(t2->elementAt(r, 3)); eWithout_wear->setObjectName("eWithout_wear");
     eWithout_wear->setRange(0, 9999999999);
     eWithout_wear->setDecimals(2);
     eWithout_wear->setSingleStep(1);
     eWithout_wear->setEnabled(true);
     eWithout_wear->textInput().connect(this, &TinsuranceCaseUpd::onTextInput);

     r++;

     l = new Wt::WText(L"������������� �����:", t2->elementAt(r, 0));
     l->setWordWrap(true);
     eAgreed_cost = new Wt::WDoubleSpinBox(t2->elementAt(r, 1)); eAgreed_cost->setObjectName("eAgreed_cost");
     eAgreed_cost->setRange(0, 9999999999);
     eAgreed_cost->setDecimals(2);
     eAgreed_cost->setSingleStep(1);
     eAgreed_cost->setEnabled(true);
     eAgreed_cost->textInput().connect(this, &TinsuranceCaseUpd::onTextInput);

     l = new Wt::WText(L"����� ������ � ������ ������:", t2->elementAt(r, 2));
     l->setWordWrap(true);
     eIncluding_wear = new Wt::WDoubleSpinBox(t2->elementAt(r, 3)); eIncluding_wear->setObjectName("eIncluding_wear");
     eIncluding_wear->setRange(0, 9999999999);
     eIncluding_wear->setDecimals(2);
     eIncluding_wear->setSingleStep(1);
    // eIncluding_wear->setInputMask(Wt::WString("000000009.00"));
     eIncluding_wear->setEnabled(true);
     eIncluding_wear->textInput().connect(this, &TinsuranceCaseUpd::onTextInput);

     r++;

     l = new Wt::WText(L"������� ������:", t2->elementAt(r, 0));
     l->setWordWrap(true);
     cbTerms_payment = new TComboBoxDirLine(dl, 17, t2->elementAt(r, 1)); cbTerms_payment->setObjectName("cbTerms_payment");
     cbTerms_payment->setNoSelectionEnabled(true);
     cbTerms_payment->valueChanged.connect(this, &TinsuranceCaseUpd::onTextInput);

     l = new Wt::WText(L"��������� ����������:", t2->elementAt(r, 2));
     l->setWordWrap(true);
     eExpertise_cost = new Wt::WDoubleSpinBox(t2->elementAt(r, 3)); eExpertise_cost->setObjectName("eExpertise_cost");
     eExpertise_cost->setRange(0, 9999999999);
     eExpertise_cost->setDecimals(2);
     eExpertise_cost->setSingleStep(1);
     eExpertise_cost->setEnabled(true);
     eExpertise_cost->setInputMask(Wt::WString("000000009.99;_"));
    // eExpertise_cost->addStyleClass("text-right");
     eExpertise_cost->textInput().connect(this, &TinsuranceCaseUpd::onTextInput);

     setTablePadding(t2);

     p0 = new Wt::WPanel(); p0->setObjectName("p0");
     p0->setTitle(Wt::WString(L"����"));
     p0->setTitleBar(true);
     p0->titleBarWidget()->addStyleClass("nav-header");
     lv0->addWidget(p0);

     t2 = new Wt::WTable(); t2->setObjectName("t2");
     p0->setCentralWidget(t2);

     r = 0;

     l = new Wt::WText(L"����������:",  t2->elementAt(r, 0));
     l->setWordWrap(true);
     cbSearch_defects = new Wt::WCheckBox(t2->elementAt(r, 1)); cbSearch_defects->setObjectName("cbSearch_defects");
     cbSearch_defects->changed().connect(this, &TinsuranceCaseUpd::onTextInput);
     r++;
     l = new Wt::WText(L"����������� ��������� ������������� ������ ����:",  t2->elementAt(r, 0));
     l->setWordWrap(true);
     cbDetermination = new Wt::WCheckBox(t2->elementAt(r, 1)); cbDetermination->setObjectName("cbDetermination");
     cbDetermination->changed().connect(this, &TinsuranceCaseUpd::onTextInput);

     r++;

     l = new Wt::WText(L"����������� �������� ��������� ������ ��������:",  t2->elementAt(r, 0));
     l->setWordWrap(true);
     cbResidual_value = new Wt::WCheckBox(t2->elementAt(r, 1)); cbResidual_value->setObjectName("cbResidual_value");
     cbResidual_value->changed().connect(this, &TinsuranceCaseUpd::onTextInput);

     r++;

     l = new Wt::WText(L"���:",  t2->elementAt(r, 0));
     l->setWordWrap(true);
     cbLoss_value = new Wt::WCheckBox(t2->elementAt(r, 1)); cbLoss_value->setObjectName("cbLoss_value");
     cbLoss_value->changed().connect(this, &TinsuranceCaseUpd::onTextInput);
     r++;
     l = new Wt::WText(L"����������:",  t2->elementAt(r, 0));
     l->setWordWrap(true);
     cbTrace_evidence = new Wt::WCheckBox(t2->elementAt(r, 1)); cbTrace_evidence->setObjectName("cbTrace_evidence");
     cbTrace_evidence->changed().connect(this, &TinsuranceCaseUpd::onTextInput);

     r++;

     l = new Wt::WText(L"������ ������� ����������� � ������� ��� � ���������� ����������� ����������� ����������:",  t2->elementAt(r, 0));
     l->setWordWrap(true);
     cbIndependent = new Wt::WCheckBox(t2->elementAt(r, 1)); cbIndependent->setObjectName("cbIndependent");
     cbIndependent->changed().connect(this, &TinsuranceCaseUpd::onTextInput);
     r++;
     l = new Wt::WText(L"������ ���� + �����������:",  t2->elementAt(r, 0));
     l->setWordWrap(true);
     cbPhoto = new Wt::WCheckBox(t2->elementAt(r, 1)); cbPhoto->setObjectName("cbPhoto");
     cbPhoto->changed().connect(this, &TinsuranceCaseUpd::onTextInput);

     setTablePadding(t2);

     Damage->ic_id = basicData.ic_id;
     Damage->read();

     return(cAI);
 }

 Wt::WWidget * TinsuranceCaseUpd::setupUiCulprit() // ��������
 {
     Wt::WText * l;
     Wt::WVBoxLayout * lv0 = new Wt::WVBoxLayout(); lv0->setObjectName("lv0");

     Wt::WContainerWidget * cC = new Wt::WContainerWidget(); cC->setObjectName("cC");
     cC->setLayout(lv0);

     Wt::WPanel * p0 = new Wt::WPanel(); p0->setObjectName("p0");
     p0->setTitle(Wt::WString(L"��������"));
     p0->setTitleBar(true);
     p0->titleBarWidget()->addStyleClass("nav-header");
     lv0->addWidget(p0);

     Wt::WTable * t0 = new Wt::WTable(); t0->setObjectName("t0");
     p0->setCentralWidget(t0);

     int r(0); 

     l = new Wt::WText(L"��������� ��������:", t0->elementAt(r, 0));
     l->setWordWrap(true);
     eInsurance_company = new Wt::WLineEdit(t0->elementAt(r, 1)); eInsurance_company->setObjectName("eInsurance_company");
     eInsurance_company->setEnabled(true);
     eInsurance_company->setMaxLength(128);
     eInsurance_company->textInput().connect(this, &TinsuranceCaseUpd::onTextInputCulprit);
     r++;

     l = new Wt::WText(L"�����, �����:", t0->elementAt(r, 0));
     l->setWordWrap(true);
     ePolis_series = new Wt::WLineEdit(t0->elementAt(r, 1)); ePolis_series->setObjectName("ePolis_series");
     ePolis_series->setEnabled(true);
     ePolis_series->setMaxLength(16);
     ePolis_series->textInput().connect(this, &TinsuranceCaseUpd::onTextInputCulprit);

     l = new Wt::WText(L"�����, �����:", t0->elementAt(r, 2));
     l->setWordWrap(true);
     ePolicy_number = new Wt::WLineEdit(t0->elementAt(r, 3)); ePolicy_number->setObjectName("ePolicy_number");
     ePolicy_number->setEnabled(true);
     ePolicy_number->setMaxLength(16);
     ePolicy_number->textInput().connect(this, &TinsuranceCaseUpd::onTextInputCulprit);
     r++;

     l = new Wt::WText(L"���� ���������� ��������:", t0->elementAt(r, 0));
     l->setWordWrap(true);
     deDate_conclusion = new TDateEdit(t0->elementAt(r, 1)); deDate_conclusion->setObjectName("deDate_conclusion");
     deDate_conclusion->setEnabled(true);
     deDate_conclusion->textInput().connect(this, &TinsuranceCaseUpd::onTextInputCulprit);
     r++;

     setTablePadding(t0);

     vCulprit = new TVehicleWidget(0, dl); vCulprit->setObjectName("vCulprit");
     vCulprit->p1->setHidden(true);
     vCulprit->p2->setHidden(true);
     vCulprit->changed.connect(this, &TinsuranceCaseUpd::onTextInputCulprit);
     vCulprit->vehicle_id = culprit.vehicle_id;
     vCulprit->read();

     lv0->addWidget(vCulprit);

     p0 = new Wt::WPanel(); p0->setObjectName("p0");
     p0->setTitle(Wt::WString(L"�������� �� ���������"));
     p0->setTitleBar(true);
     p0->titleBarWidget()->addStyleClass("nav-header");
     lv0->addWidget(p0);

     dCulprit = new TDriverWidget(0, dl); dCulprit->setObjectName("dCulprit");
     dCulprit->hideCitizenship();
     dCulprit->changed.connect(this, &TinsuranceCaseUpd::onTextInput);
     p0->setCentralWidget(dCulprit);

     dCulprit->driver_id = culprit.driver_id;
     dCulprit->read();

     cbCulpritDriverIsOwner = new Wt::WCheckBox(); cbCulpritDriverIsOwner->setObjectName("cbCulpritDriverIsOwner");
     cbCulpritDriverIsOwner->setText(L"�������� �������� ������������� ��������� ��");
     cbCulpritDriverIsOwner->changed().connect(this, &TinsuranceCaseUpd::onCulpritDriverIsOwnerChanged);
     lv0->addWidget(cbCulpritDriverIsOwner);


     pCulpritOwner = new Wt::WPanel(); pCulpritOwner->setObjectName("pCulpritOwner");
     pCulpritOwner->setTitle(Wt::WString(L"����������� �� ���������"));
     pCulpritOwner->setTitleBar(true);
     pCulpritOwner->titleBarWidget()->addStyleClass("nav-header");
     lv0->addWidget(pCulpritOwner);

     dCulpritOwner = new TDriverWidget(0, dl); dCulpritOwner->setObjectName("dCulpritOwner");
     dCulpritOwner->hideCitizenship();
     dCulpritOwner->changed.connect(this, &TinsuranceCaseUpd::onTextInput);
     pCulpritOwner->setCentralWidget(dCulpritOwner);

     dCulpritOwner->driver_id = culprit.vehicle_owner;
     dCulpritOwner->read();

     return(cC);
 }

 Wt::WWidget * TinsuranceCaseUpd::setupUiVictim()
 {
     Wt::WText * l;
     Wt::WVBoxLayout * lv0 = new Wt::WVBoxLayout(); lv0->setObjectName("lv0");

     Wt::WContainerWidget * cV = new Wt::WContainerWidget(); cV->setObjectName("cV");
     cV->setLayout(lv0);

     Wt::WPanel * p0 = new Wt::WPanel(); p0->setObjectName("p0");
     p0->setTitle(Wt::WString(L"�����������"));
     p0->setTitleBar(true);
     p0->titleBarWidget()->addStyleClass("nav-header");
     lv0->addWidget(p0);

     Wt::WTable * t0 = new Wt::WTable(); t0->setObjectName("t0");
     p0->setCentralWidget(t0);

     int r(0); 

     l = new Wt::WText(L"��������� ��������:", t0->elementAt(r, 0));
     l->setWordWrap(true);
     eInsurance_companyV = new Wt::WLineEdit(t0->elementAt(r, 1)); eInsurance_companyV->setObjectName("eInsurance_companyV");
     eInsurance_companyV->setEnabled(true);
     eInsurance_companyV->setMaxLength(128);
     eInsurance_companyV->textInput().connect(this, &TinsuranceCaseUpd::onTextInputVictim);
     r++;

     l = new Wt::WText(L"�����, �����:", t0->elementAt(r, 0));
     l->setWordWrap(true);
     ePolicy_seriesV = new Wt::WLineEdit(t0->elementAt(r, 1)); ePolicy_seriesV->setObjectName("ePolicy_seriesV");
     ePolicy_seriesV->setEnabled(true);
     ePolicy_seriesV->setMaxLength(16);
     ePolicy_seriesV->textInput().connect(this, &TinsuranceCaseUpd::onTextInputVictim);

     l = new Wt::WText(L"�����, �����:", t0->elementAt(r, 2));
     l->setWordWrap(true);
     ePolicy_numberV = new Wt::WLineEdit(t0->elementAt(r, 3)); ePolicy_numberV->setObjectName("ePolicy_numberV");
     ePolicy_numberV->setEnabled(true);
     ePolicy_numberV->setMaxLength(16);
     ePolicy_numberV->textInput().connect(this, &TinsuranceCaseUpd::onTextInputVictim);
     r++;

     setTablePadding(t0);

     vVictim = new TVehicleWidget(0, dl); vVictim->setObjectName("vVictim");
     vVictim->changed.connect(this, &TinsuranceCaseUpd::onTextInputVictim);
     lv0->addWidget(vVictim);

     vVictim->vehicle_id = victim.vehicle_id;
     vVictim->read();

     p0 = new Wt::WPanel(); p0->setObjectName("p0");
     p0->setTitle(Wt::WString(L"�������� ������������� ��"));
     p0->setTitleBar(true);
     p0->titleBarWidget()->addStyleClass("nav-header");
     lv0->addWidget(p0);

     dVictim = new TDriverWidget(0, dl); dVictim->setObjectName("dVictim");
     dVictim->changed.connect(this, &TinsuranceCaseUpd::onTextInput);
     p0->setCentralWidget(dVictim);

     dVictim->driver_id = victim.driver_id;
     dVictim->read();

     cbVictimDriverIsOwner = new Wt::WCheckBox(); cbVictimDriverIsOwner->setObjectName("cbVictimDriverIsOwner");
     cbVictimDriverIsOwner->setText(L"�������� �������� ������������� ������������� ��");
     cbVictimDriverIsOwner->changed().connect(this, &TinsuranceCaseUpd::onVictimDriverIsOwnerChanged);
     lv0->addWidget(cbVictimDriverIsOwner);

     pOwner = new Wt::WPanel(); pOwner->setObjectName("pOwner");
     pOwner->setTitle(Wt::WString(L"����������� ������������� ��"));
     pOwner->setTitleBar(true);
     pOwner->titleBarWidget()->addStyleClass("nav-header");
     lv0->addWidget(pOwner);

     dOwner = new TDriverWidget(0, dl); dOwner->setObjectName("dOwner");
     dOwner->changed.connect(this, &TinsuranceCaseUpd::onTextInput);
     pOwner->setCentralWidget(dOwner);

     dOwner->driver_id = victim.vehicle_owner;
     dOwner->read();

     return(cV);
 }

 Wt::WWidget * TinsuranceCaseUpd::setupUiFile()
 {

     Wt::WTabWidget * tabFile = new Wt::WTabWidget(); tab->setObjectName("tabFile");

     fileDoc1 = new TFileDoc(dl); fileDoc1->setObjectName("fileDoc1");
     fileDoc1->setFilters("image/jpeg,application/pdf");
     tabFile->addTab(fileDoc1, Wt::WString(L"������ ��������"));

     fileDoc2 = new TFileDoc(dl); fileDoc2->setObjectName("fileDoc2");
     fileDoc2->setFilters("image/jpeg,application/pdf");
     tabFile->addTab(fileDoc2, Wt::WString(L"������+�����������"));

     fileDoc3 = new TFileDoc(dl); fileDoc3->setObjectName("fileDoc3");
     fileDoc3->setFilters("image/jpeg,application/pdf");
     tabFile->addTab(fileDoc3, Wt::WString(L"���� ���������"));

     return(tabFile);
 }

void TinsuranceCaseUpd::fileRefresh()
 {

    fileDoc1->refresh();
    fileDoc2->refresh();
    fileDoc3->refresh();

  /*  Wt::WText * l;
    TNode node(dl);
    int r(0);
    fView->clear();
    int c = fieldByName(&mtFile, "status");
    for(int i = 0; i < mtFile.rowCount(); i++, r++)
    {
     if(to_int(mtFile.data(i, c)) == 2) continue; // ���������
     
     node.p3 = to_string(mtFile.data(i, fieldByName(&mtFile, "p3")));
     node.p2 = to_string(mtFile.data(i, fieldByName(&mtFile, "p2")));
     node.p1 = to_string(mtFile.data(i, fieldByName(&mtFile, "p1")));
     node.name_server = to_string(mtFile.data(i, fieldByName(&mtFile, "name_server")));

     Wt::WFileResource * res = new Wt::WFileResource(node.fullName());
     res->suggestFileName(to_string(mtFile.data(i, fieldByName(&mtFile, "name_user"))));
     Wt::WAnchor *anchor = new Wt::WAnchor(res, Wt::WString::fromUTF8(to_string(mtFile.data(i, fieldByName(&mtFile, "name_user")))), fView->elementAt(r, 1));

     Wt::WPushButton * bDelete = new Wt::WPushButton(Wt::WString(L"�������"), fView->elementAt(r, 2));
     TCommandParam * param = new TCommandParam(i, bDelete);
     param->fire.connect(this, &TinsuranceCaseUpd::fileDelete);
     bDelete->clicked().connect(param, &TCommandParam::exec);

    }

    setTablePadding(fView);*/
    bOk->setEnabled(basicData.isValid());
}

void TinsuranceCaseUpd::fileRead(const int id)
 {
    fileDoc1->read(1, id);
    fileDoc2->read(2, id);
    fileDoc3->read(3, id);

  /*  mtFile.clear();
    if (dl->FileDocS(&mtFile, 1, id) < 0)
    {
        // Error
    }
    int c(mtFile.columnCount());
    mtFile.insertColumn(c - 1);
    mtFile.setHeaderData(c - 1, Wt::Orientation::Horizontal, "status");
    c = fieldByName(&mtFile, "status");
    for(int i = 0; i < mtFile.rowCount(); i++) {mtFile.setData(i, c, (int)0);} */// 0 - �� ��, 1 - ��������/��������, 2 - �������
 }
 
void TinsuranceCaseUpd::fileSave(const int id)
 {
    fileDoc1->save(1, id);
    fileDoc2->save(2, id);
    fileDoc3->save(3, id);
  /*  int c = fieldByName(&mtFile, "status");
    for(int i = 0; i < mtFile.rowCount(); i++)
    {
        int file_doc_id = to_int(mtFile.data(i, fieldByName(&mtFile, "file_doc_id")));
        int status = to_int(mtFile.data(i, c));
        if (status == 0) continue;
        if (status == 1)
        {            
            int file_id = to_int(mtFile.data(i, fieldByName(&mtFile, "file_id")));
            dl->FileDocIU(file_doc_id, file_id, 1, basicData.ic_id);
        }
        if (status == 2 && file_doc_id > 0) // ���������
        {            
            dl->TableD("FDB_FILE_DOC", file_doc_id);
        }
    }*/
 }

/*void TinsuranceCaseUpd::fileDelete(int i)
{
    if (Wt::StandardButton::Yes != Wt::WMessageBox::show("Confirm", Wt::WString(L"������� ���� ?"),
        Wt::StandardButton::Yes | Wt::StandardButton::No))
    {
        return;
    }

    mtFile.setData(i, fieldByName(&mtFile, "status"), int(2));
    fileRefresh();
}*/

Wt::WWidget * TinsuranceCaseUpd::setupUiTask()
{
    taskBrw = new TtaskBrw(NULL, dl);

    return(taskBrw);
}

Wt::WWidget * TinsuranceCaseUpd::setupUiExecutor()
{
 executor = new TExecutorWidget(NULL, dl);
 executor->bd = &basicData;
 executor->ai = &applicationInspection;
 executor->eResponsible = eResponsible;

 return(executor);
}

 //--------------------------------------------------------------------------
void TinsuranceCaseUpd::refresh()
{
// Twindow::refresh();

    if (basicData.ic_id > 0)
    {
       // basicData.ic_id = ic_id;
        basicData.read();
        applicationInspection.ic_id = basicData.ic_id;
        applicationInspection.read();
        culprit.ic_id = basicData.ic_id;
        culprit.read();
        victim.ic_id = basicData.ic_id;
        victim.read();
        // taskBrw.ic_
    }

 if (basicData.ic_id > 0)
 {
  cbSettlement->setChecked(basicData.settlement);
  cbReimbursement_lmv->setChecked(basicData.reimbursement_lmv);
  cbCalculation_lmv_possible->setChecked(basicData.calculation_lmv_possible);
  eNumber->setText(Wt::WString::fromUTF8(basicData.number));
  cbInsuranceType->setValue(basicData.insurance_type);
  deTa->setDate(basicData.dt_ta.date());
  teTa->setTime(basicData.dt_ta.time());
  eInspection->setText(Wt::WString::fromUTF8(basicData.inspection));
  sbParticipants->setValue(basicData.participants);
  spCulprits->setValue(basicData.culprits);
  cbTa_certificate->setValue(basicData.ta_certificate);
  cbOptional_equipment->setChecked(basicData.optional_equipment); 
  cbInspection_type->setValue(basicData.inspection_type);
  cbApplication->setChecked(basicData.application);
  cbIncident_type->setValue(basicData.incident_type);
  cbCause_damage->setValue(basicData.cause_damage);
  cbTa_location_type->setValue(basicData.ta_location_type);
  cbFranchise->setChecked(basicData.franchise);
  ePost_code->setText(Wt::WString::fromUTF8(basicData.postcode));
  eAddr_field1->setText(Wt::WString::fromUTF8(basicData.addr_field1));
  cbNight->setChecked(basicData.night > 0);
  cbMinimal_contact->setChecked(basicData.minimal_contact);
  cbCasualties->setChecked(basicData.casualties);
  cbTransfer->setChecked(basicData.transfer);
  cbMmobile_app->setChecked(basicData.mobile_app);
  cbContract_sale->setChecked(basicData.contract_sale);
  fileRead(basicData.ic_id);

  dBeneficiary->driver_id = basicData.beneficiary;
  dBeneficiary->read();
  dBeneficiary->refresh();

  entity->ic_id = basicData.ic_id;
  entity->read();
  entity->refresh();

  PaymentDetails->ic_id = basicData.ic_id;
  PaymentDetails->read();
  PaymentDetails->refresh();
 }

 if (applicationInspection.ic_id > 0)
 {
     cbUnlimited_ep->setChecked(applicationInspection.unlimited_ep);
     cbDisagreements->setChecked(applicationInspection.disagreements); 
     cbExit_ad->setChecked(applicationInspection.exit_ad);
     cbRegion_circulation->setValue(applicationInspection.region_circulation);
     deDt_application->setDate(applicationInspection.dt_application.date());
     teDt_application->setTime(applicationInspection.dt_application.time());
     cbWho_applied->setValue(applicationInspection.who_applied);
   //  cbScheme_number->setValue(applicationInspection.scheme_number);
     cbApplicants->setValue(applicationInspection.applicants);
   //  cbInspection_type->setValue(applicationInspection.inspection_type); // �� BasicData
     cbRegion_inspection->setValue(applicationInspection.region_inspection);
   //  eSubdivision->setText(Wt::WString::fromUTF8(applicationInspection.subdivision));
     eResponsible->setText(Wt::WString::fromUTF8(applicationInspection.responsible));
     eInspection_address->setText(Wt::WString::fromUTF8(applicationInspection.inspection_address));
     deDt_inspection->setDate(applicationInspection.dt_inspection.date());
     teDt_inspection->setTime(applicationInspection.dt_inspection.time());
     cbOn_move->setChecked(applicationInspection.on_move);
     cbFlip->setChecked(applicationInspection.flip);
     cbFrontal->setChecked(applicationInspection.frontal);
     cbAirbags->setChecked(applicationInspection.airbags);
//     eDamage->setText(Wt::WString::fromUTF8(applicationInspection.damage));
     deRereferral->setDate(applicationInspection.rereferral);
     deReceiving->setDate(applicationInspection.receiving);
     eRepair_cost->setText(applicationInspection.repair_cost.toString());
     eWithout_wear->setText(applicationInspection.without_wear.toString());
     eAgreed_cost->setText(applicationInspection.agreed_cost.toString());
     eIncluding_wear->setText(applicationInspection.including_wear.toString());
     eExpertise_cost->setText(applicationInspection.expertise_cost.toString());
     cbTerms_payment->setValue(applicationInspection.terms_payment);
     cbSearch_defects->setChecked(applicationInspection.search_defects);
     cbDetermination->setChecked(applicationInspection.determination);
     cbResidual_value->setChecked(applicationInspection.residual_value);
     cbLoss_value->setChecked(applicationInspection.loss_value);
     cbTrace_evidence->setChecked(applicationInspection.trace_evidence);
     cbIndependent->setChecked(applicationInspection.independent);
     cbPhoto->setChecked(applicationInspection.photo);
 }

 if (culprit.ic_id > 0)
 {
     eInsurance_company->setText(Wt::WString::fromUTF8(culprit.insurance_company));
     ePolis_series->setText(Wt::WString::fromUTF8(culprit.polis_series));
     ePolicy_number->setText(Wt::WString::fromUTF8(culprit.policy_number));
     deDate_conclusion->setDate(culprit.date_conclusion);

     vCulprit->refresh();
     dCulprit->refresh();

     if (dCulprit->driver_id > 0 && dCulpritOwner->driver_id == dCulprit->driver_id)
     {cbCulpritDriverIsOwner->setChecked(true); onCulpritDriverIsOwnerChanged();}
     else dCulpritOwner->refresh();
 }

 if (victim.ic_id > 0)
 {
     eInsurance_companyV->setText(Wt::WString::fromUTF8(victim.insurance_company));
     ePolicy_seriesV->setText(Wt::WString::fromUTF8(victim.policy_series));
     ePolicy_numberV->setText(Wt::WString::fromUTF8(victim.policy_number));
   
     vVictim->refresh();
     dVictim->refresh();
     if (dOwner->driver_id > 0 && dOwner->driver_id == dVictim->driver_id)
     {cbVictimDriverIsOwner->setChecked(true); onVictimDriverIsOwnerChanged();}
     else dOwner->refresh();
     Damage->refresh();
 }
 if (basicData.ic_id > 0)
 {
     executor->ic_id = basicData.ic_id;
     executor->read();
     executor->refresh();
 }

 if (dBeneficiary->driver_id > 0 && dOwner->driver_id == dBeneficiary->driver_id)
 {
     cbOwnerIsBeneficiary->setChecked(true);
     onOwnerIsBeneficiaryChanged();
 }

 //dBeneficiary
 fileRefresh();
}
//--------------------------------------------------------------------------
//--------------------------------------------------------------------------

bool TinsuranceCaseUpd::save()
{
    if(/*basicData.number.empty() ||*/ basicData.dt_ta.isNull() || !basicData.dt_ta.isValid())
    {
        ShowMessage(0, Wt::WString(L"���������� ��������� ����/����� ���"), "");
        return (false);
    }

    if (!basicData.isValid()) return (false);

    if (dVictim->save() < 0)
    {
        ShowMessage(0, Wt::WString::fromUTF8(dl->errorstr), "");
        return (false);
    }

    if (isVictimDriverIsOwner()) dOwner->driver_id = dVictim->driver_id;
    else 
    {    
       if(dOwner->save() < 0)
       {
           ShowMessage(0, Wt::WString::fromUTF8(dl->errorstr), "");
           return (false);
       }
    }

    if (isOwnerIsBeneficiary()) dBeneficiary->driver_id = dOwner->driver_id;
    else
    {    
       if(dBeneficiary->save() < 0)
       {
           ShowMessage(0, Wt::WString::fromUTF8(dl->errorstr), "");
           return (false);
       }
    }
    if (dCulprit->save() < 0)
    {
        if(dBeneficiary->save() < 0)
        {
            ShowMessage(0, Wt::WString::fromUTF8(dl->errorstr), "");
            return (false);
        }
    }

    if (isCulpritDriverIsOwner()) dCulpritOwner->driver_id = dCulprit->driver_id;
    else
    {    
        if(dCulpritOwner->save() < 0)
        {
            ShowMessage(0, Wt::WString::fromUTF8(dl->errorstr), "");
            return (false);
        }
    }

    basicData.beneficiary = dBeneficiary->driver_id;

    if (basicData.save() > 0)
    {
        applicationInspection.ic_id = basicData.ic_id;
        if (applicationInspection.save() < 0)
        {
            ShowMessage(0, Wt::WString::fromUTF8(dl->errorstr), "");
            return (false);
        }

        culprit.ic_id = basicData.ic_id;
        if (vCulprit->save() < 0)
        {
            ShowMessage(0, Wt::WString::fromUTF8(dl->errorstr), "");
            return (false);
        }

        culprit.vehicle_id = vCulprit->vehicle_id;
        culprit.driver_id = dCulprit->driver_id;
        culprit.vehicle_owner = dCulpritOwner->driver_id;
        if (culprit.save() < 0)
        {
            {
                ShowMessage(0, Wt::WString::fromUTF8(dl->errorstr), "");
                return (false);
            }
        }
        if (vVictim->save() < 0)
        {
            ShowMessage(0, Wt::WString::fromUTF8(dl->errorstr), "");
            return (false);
        }

        victim.ic_id = basicData.ic_id;
        victim.vehicle_id = vVictim->vehicle_id;
        victim.driver_id = dVictim->driver_id;
        if (isVictimDriverIsOwner()) victim.vehicle_owner = dVictim->driver_id;
        else victim.vehicle_owner = dOwner->driver_id;

        if (victim.save() < 0)
        {
            ShowMessage(0, Wt::WString::fromUTF8(dl->errorstr), "");
            return (false);
        }

        Damage->ic_id = basicData.ic_id;

        if (Damage->save() < 0)
        {
            ShowMessage(0, Wt::WString::fromUTF8(dl->errorstr), "");
            return (false);
        }

        fileSave(basicData.ic_id); // TODO : ��������� ������


        executor->ic_id = basicData.ic_id;
        if (executor->save() < 0)
        {
           ShowMessage(0, Wt::WString::fromUTF8(dl->errorstr), "");
           return (false);
        }

        entity->ic_id = basicData.ic_id;

        if (entity->save() < 0)
        {
            ShowMessage(0, Wt::WString::fromUTF8(dl->errorstr), "");
            return (false);
        }
        PaymentDetails->ic_id = basicData.ic_id;
        if (PaymentDetails->save() < 0)
        {
            ShowMessage(0, Wt::WString::fromUTF8(dl->errorstr), "");
            return (false);
        }
        return (true);
    }
    else
    {
        ShowMessage(0, Wt::WString::fromUTF8(str2html(dl->errorstr)), "DB Error");
    }
    return (false);
}

void TinsuranceCaseUpd::onbOk()
{
  state.clear();
  state["_"] = __PRETTY_FUNCTION__;
  if (Wt::WApplication::instance() != NULL)
      state["sessionId"] = Wt::WApplication::instance()->sessionId();
  state["ic_id"] = std::to_string(basicData.ic_id);
  logtxt(&state);

  if(save()) onCloseInt(basicData.ic_id);
}
//--------------------------------------------------------------------------
void TinsuranceCaseUpd::onbCancel()
{
 onCloseInt(0);
}
//--------------------------------------------------------------------------
void TinsuranceCaseUpd::onbReport()
{
    Wt::WApplication *app = Wt::WApplication::instance();
    if (app == NULL) return;

    state.clear();
    state["_"] = __PRETTY_FUNCTION__;
    if (Wt::WApplication::instance() != NULL)
        state["sessionId"] = Wt::WApplication::instance()->sessionId();
    state["ic_id"] = std::to_string(basicData.ic_id);
    logtxt(&state);

    if (save())
    {
       refresh();
       TPDFStatement * pdf;
       pdf = new TPDFStatement(dl, this);
       pdf->basicData = &basicData;
       pdf->dBeneficiary = dynamic_cast<TDriver *>(dBeneficiary);
       pdf->dOwner = dynamic_cast<TDriver *>(dOwner);
       pdf->vVictim = dynamic_cast<TVehicle *>(vVictim);
       pdf->dCulprit = dynamic_cast<TDriver *>(dCulprit);
       pdf->entity = dynamic_cast<TEntity *>(entity);
       pdf->culprit = &culprit;
       pdf->paymentDetails = dynamic_cast<TPaymentDetails *>(PaymentDetails);

       app->redirect(pdf->url());
    }
}

void TinsuranceCaseUpd::onbOrder()
{
    std::map<std::string, std::string> state;
    Wt::WApplication *app = Wt::WApplication::instance();
    if (app == NULL) return;

    state["_"] = __PRETTY_FUNCTION__;
    if (Wt::WApplication::instance() != NULL)
        state["sessionId"] = Wt::WApplication::instance()->sessionId();
    state["ic_id"] = std::to_string(basicData.ic_id);
    logtxt(&state);

    if (save())
    {
        refresh();
        TPDFOrder * pdf = new TPDFOrder(dl, this);
        pdf->basicData = &basicData;
     //   pdf->dBeneficiary = dynamic_cast<TDriver *>(dBeneficiary);
        pdf->dOwner = dynamic_cast<TDriver *>(dOwner);
        pdf->vVictim = dynamic_cast<TVehicle *>(vVictim);
     //   pdf->entity = dynamic_cast<TEntity *>(entity);
     //   pdf->culprit = &culprit;
        pdf->victim = &victim;
        pdf->applicationInspection = &applicationInspection;
     //   pdf->paymentDetails = dynamic_cast<TPaymentDetails *>(PaymentDetails);

        app->redirect(pdf->url());
    }
}
//--------------------------------------------------------------------------
void TinsuranceCaseUpd::onTextInput()
{
 Wt::WObject * w = sender();
 if (w == NULL || w->objectName().empty()) return;

 if (w->objectName() == "cbSettlement")
 {
     basicData.settlement = cbSettlement->isChecked();
 }
 else if (w->objectName() == "cbReimbursement_lmv")
 {
     basicData.reimbursement_lmv = cbReimbursement_lmv->isChecked();
 }
 else if (w->objectName() == "cbCalculation_lmv_possible")
 {
     basicData.calculation_lmv_possible = cbCalculation_lmv_possible->isChecked();
 }
 else if (w->objectName() == "eNumber")
 {
  basicData.number = eNumber->text().trim().toUTF8();
 }
 else if (w->objectName() == "cbInsuranceType")
 {
     basicData.insurance_type = cbInsuranceType->value();
 }
 else if (w->objectName() == "deTa" || w->objectName() == "teTa")
 {
     basicData.dt_ta = Wt::WDateTime(deTa->date(), teTa->time());
 }
 else if (w->objectName() == "eInspection")
 {
     basicData.inspection = eInspection->text().trim().toUTF8();
 }
 else if (w->objectName() == "sbParticipants")
 {
     basicData.participants = sbParticipants->value();
 }
 else if (w->objectName() == "spCulprits")
 {
     basicData.culprits = spCulprits->value();
 }
 else if (w->objectName() == "cbTa_certificate")
 {
     basicData.ta_certificate = cbTa_certificate->value();
 }
 else if (w->objectName() == "cbOptional_equipment")
 {
     basicData.optional_equipment = cbOptional_equipment->isChecked(); 
 }
 else if (w->objectName() == "cbInspection_type")
 {
     basicData.inspection_type = cbInspection_type->value();
 }
 else if (w->objectName() == "cbApplication")
 {
     basicData.application = cbApplication->isChecked();
 }
 else if (w->objectName() == "cbIncident_type")
 {
     basicData.incident_type = cbIncident_type->value();
 }
 else if (w->objectName() == "cbCause_damage")
 {
     basicData.cause_damage = cbCause_damage->value();
 }
 else if (w->objectName() == "cbTa_location_type")
 {
     basicData.ta_location_type = cbTa_location_type->value();
 }
 else if (w->objectName() == "cbFranchise")
 {
     basicData.franchise = cbFranchise->isChecked();
 }
 else if (w->objectName() == "ePost_code")
 {
     basicData.postcode = ePost_code->text().trim().toUTF8();
 }
 else if (w->objectName() == "eAddr_field1")
 {
     basicData.addr_field1 = eAddr_field1->text().trim().toUTF8().substr(0, 128);
 }
 else if (w->objectName() == "cbNight")
 {
     basicData.night = cbNight->isChecked();
 }
 else if (w->objectName() == "cbMinimal_contact")
 {
     basicData.minimal_contact = cbMinimal_contact->isChecked();
 }
 else if (w->objectName() == "cbCasualties")
 {
     basicData.casualties = cbCasualties->isChecked();
 }
 else if (w->objectName() == "cbTransfer")
 {
  basicData.transfer = cbTransfer->isChecked();
 }
 else if (w->objectName() == "cbMmobile_app")
 {
  basicData.mobile_app = cbMmobile_app->isChecked();
 }
 else if (w->objectName() == "cbContract_sale")
 {
  basicData.contract_sale = cbContract_sale->isChecked();
 }
 else if (w->objectName() == "cbUnlimited_ep")
 {
 applicationInspection.unlimited_ep = cbUnlimited_ep->isChecked();
 }
 else if (w->objectName() == "cbDisagreements")
 {
 applicationInspection.disagreements = cbDisagreements->isChecked();
 }
 else if (w->objectName() == "cbExit_ad")
 {
 applicationInspection.exit_ad = cbExit_ad->isChecked();
 }
 else if (w->objectName() == "cbRegion_circulation")
 {
 applicationInspection.region_circulation = cbRegion_circulation->value();
 }
 else if (w->objectName() == "deDt_application" || w->objectName() == "teDt_application")
 {
 applicationInspection.dt_application = Wt::WDateTime(deDt_application->date(), teDt_application->time());
 }
 else if (w->objectName() == "cbWho_applied")
 {
 applicationInspection.who_applied = cbWho_applied->value();
 }
 /*else if (w->objectName() == "cbScheme_number")
 {
 applicationInspection.scheme_number = cbScheme_number->value();
 }*/
 else if (w->objectName() == "cbApplicants")
 {
 applicationInspection.applicants = cbApplicants->value();
 }
 else if (w->objectName() == "cbInspection_type")
 {
 applicationInspection.inspection_type = cbInspection_type->value();
 }
 else if (w->objectName() == "cbRegion_inspection")
 {
 applicationInspection.region_inspection = cbRegion_inspection->value();
 }
 /*else if (w->objectName() == "eSubdivision")
 {
 applicationInspection.subdivision = eSubdivision->text().trim().toUTF8();
 }*/
 else if (w->objectName() == "eResponsible")
 {
 applicationInspection.responsible = eResponsible->text().trim().toUTF8();
 }
 else if (w->objectName() == "eInspection_address")
 {
 applicationInspection.inspection_address = eInspection_address->text().trim().toUTF8().substr(0, 128);
 }
 else if (w->objectName() == "deDt_inspection" || w->objectName() == "teDt_inspection")
 {
 applicationInspection.dt_inspection = Wt::WDateTime(deDt_inspection->date(), teDt_inspection->time());
 }
 else if (w->objectName() == "cbOn_move")
 {
 applicationInspection.on_move = cbOn_move->isChecked();
 }
 else if (w->objectName() == "cbFlip")
 {
 applicationInspection.flip = cbFlip->isChecked();
 }
 else if (w->objectName() == "cbFrontal")
 {
 applicationInspection.frontal = cbFrontal->isChecked();
 }
 else if (w->objectName() == "cbAirbags")
 {
 applicationInspection.airbags = cbAirbags->isChecked();
 }
 else if (w->objectName() == "deRereferral")
 {
 applicationInspection.rereferral = deRereferral->date();
 }
 else if (w->objectName() == "deReceiving")
 {
 applicationInspection.receiving = deReceiving->date();
 }
 else if (w->objectName() == "eRepair_cost")
 {
 applicationInspection.repair_cost.fromString(eRepair_cost->text().toUTF8());
 }
 else if (w->objectName() == "eWithout_wear")
 {
 applicationInspection.without_wear.fromString(eWithout_wear->text().toUTF8());
 }
 else if (w->objectName() == "eAgreed_cost")
 {
 applicationInspection.agreed_cost.fromString(eAgreed_cost->text().toUTF8());
 }
 else if (w->objectName() == "eIncluding_wear")
 {
 applicationInspection.including_wear.fromString(eIncluding_wear->text().toUTF8());
 }
 else if (w->objectName() == "eExpertise_cost")
 {
 applicationInspection.expertise_cost.fromString(eExpertise_cost->text().toUTF8());
 }
 else if (w->objectName() == "cbTerms_payment")
 {
 applicationInspection.terms_payment = cbTerms_payment->value();
 }
 else if (w->objectName() == "cbSearch_defects")
 {
 applicationInspection.search_defects = cbSearch_defects->isChecked();
 }
 else if (w->objectName() == "cbDetermination")
 {
 applicationInspection.determination = cbDetermination->isChecked();
 }
 else if (w->objectName() == "cbResidual_value")
 {
 applicationInspection.residual_value = cbResidual_value->isChecked();
 }
 else if (w->objectName() == "cbLoss_value")
 {
 applicationInspection.loss_value = cbLoss_value->isChecked();
 }
 else if (w->objectName() == "cbTrace_evidence")
 {
 applicationInspection.trace_evidence = cbTrace_evidence->isChecked();
 }
 else if (w->objectName() == "cbIndependent")
 {
 applicationInspection.independent = cbIndependent->isChecked();
 }
 else if (w->objectName() == "cbPhoto")
 {
 applicationInspection.photo = cbPhoto->isChecked();
 }

 bOk->setEnabled(basicData.isValid());
}

void TinsuranceCaseUpd::onTextInputCulprit()
{
    Wt::WObject * w = sender();
    if (w == NULL || w->objectName().empty()) return;

    if (w->objectName() == "eInsurance_company")
    {
        culprit.insurance_company = eInsurance_company->text().trim().toUTF8();
    }
    else if (w->objectName() == "ePolis_series")
    {
        culprit.polis_series = ePolis_series->text().trim().toUTF8();
    }
    else if (w->objectName() == "ePolicy_number")
    {
        culprit.policy_number = ePolicy_number->text().trim().toUTF8();
    }
    else if (w->objectName() == "deDate_conclusion")
    {
        culprit.date_conclusion = deDate_conclusion->date();
    }
    bOk->setEnabled(basicData.isValid());
}

void TinsuranceCaseUpd::onCulpritDriverIsOwnerChanged()
{
    pCulpritOwner->setHidden(isCulpritDriverIsOwner());
    if (!isCulpritDriverIsOwner() && dCulpritOwner->driver_id > 0 && dCulprit->driver_id == dCulpritOwner->driver_id)
    {
        dCulpritOwner->driver_id = 0;
        dynamic_cast<TDriver *>(dCulpritOwner)->clear();
    }
}

bool TinsuranceCaseUpd::isCulpritDriverIsOwner()
{
    return(cbCulpritDriverIsOwner->isChecked());
}

void TinsuranceCaseUpd::onTextInputVictim()
{
    Wt::WObject * w = sender();
    if (w == NULL || w->objectName().empty()) return;

    if (w->objectName() == "eInsurance_companyV")
    {
        victim.insurance_company = eInsurance_companyV->text().trim().toUTF8();
    }
    else if (w->objectName() == "ePolicy_seriesV")
    {
        victim.policy_series = ePolicy_seriesV->text().trim().toUTF8();
    }
    else if (w->objectName() == "ePolicy_numberV")
    {
        victim.policy_number = ePolicy_numberV->text().trim().toUTF8();
    }
  
    bOk->setEnabled(basicData.isValid());
}

void TinsuranceCaseUpd::onVictimDriverIsOwnerChanged()
{
    pOwner->setHidden(isVictimDriverIsOwner());
    if (!isVictimDriverIsOwner() && dOwner->driver_id > 0 && dVictim->driver_id == dOwner->driver_id)
    {
        dOwner->driver_id = 0;
        dynamic_cast<TDriver *>(dOwner)->clear();
    }
}
bool TinsuranceCaseUpd::isVictimDriverIsOwner()
{
  return(cbVictimDriverIsOwner->isChecked());
}

void TinsuranceCaseUpd::onOwnerIsBeneficiaryChanged()
{
    dBeneficiary->setHidden(isOwnerIsBeneficiary());
    if (!isOwnerIsBeneficiary() && dBeneficiary->driver_id > 0 && dBeneficiary->driver_id == dOwner->driver_id)
    {
        dBeneficiary->driver_id = 0;
        dynamic_cast<TDriver *>(dBeneficiary)->clear();
    }
}
bool TinsuranceCaseUpd::isOwnerIsBeneficiary()
{
    return(cbOwnerIsBeneficiary->isChecked());
}

Wt::WWidget * TinsuranceCaseUpd::setupUiBeneficiary() 
{
    Wt::WVBoxLayout * lmain;
    lmain = new Wt::WVBoxLayout();

    Wt::WContainerWidget * lBeneficiary = new Wt::WContainerWidget();
    lBeneficiary->setLayout(lmain);

    entity = new TEntityWidget(0, dl);
    lmain->addWidget(entity);

    cbOwnerIsBeneficiary = new Wt::WCheckBox(); cbOwnerIsBeneficiary->setObjectName("cbOwnerIsBeneficiary");
    cbOwnerIsBeneficiary->setText(L"����������� ������������� �� �������� ��������������������");
    cbOwnerIsBeneficiary->changed().connect(this, &TinsuranceCaseUpd::onOwnerIsBeneficiaryChanged);
    lmain->addWidget(cbOwnerIsBeneficiary);

    dBeneficiary = new TDriverWidget(0, dl); dBeneficiary->setObjectName("dBeneficiary");
    dBeneficiary->hideDriversLicense();
    dBeneficiary->changed.connect(this, &TinsuranceCaseUpd::onTextInput);

    lmain->addWidget(dBeneficiary);


    Wt::WPanel * p0 = new Wt::WPanel(); p0->setObjectName("p0");
    p0->setTitle(Wt::WString(L"����������� ����������� �������� �� ��������� ����������"));
    p0->setTitleBar(true);
    p0->titleBarWidget()->addStyleClass("nav-header");
    lmain->addWidget(p0);

    PaymentDetails = new TPaymentDetailsWidget(0, dl); PaymentDetails->setObjectName("PaymentDetails");
    PaymentDetails->changed.connect(this, &TinsuranceCaseUpd::onTextInput);

    p0->setCentralWidget(PaymentDetails);

    return(lBeneficiary);
}
//--------------------------------------------------------------------------
//--------------------------------------------------------------------------
