#include <Wt/WCalendar>

#include "carlifterUpdWindow.h"

//--------------------------------------------------------------------------
//--------------------------------------------------------------------------
TcarlifterUpd::TcarlifterUpd(Wt::WContainerWidget *parent, TDataLayer * p_dl, const int p_city_id, const int p_carlifter_id) : Twindow(p_dl, "TcarlifterUpd", parent), 
city_id(p_city_id), carlifter_id(p_carlifter_id)
{
 refresh();
}
//--------------------------------------------------------------------------
//TcarlifterUpd::~TcarlifterUpd()
//{
//}
//--------------------------------------------------------------------------
void TcarlifterUpd::setupUi()
{
 Wt::WTable * tMain = new Wt::WTable(this); tMain->setObjectName("tMain");

 int r(0);

 spNumber = new Wt::WSpinBox(tMain->elementAt(r, 1)); spNumber->setObjectName("spNumber");
 spNumber->textInput().connect(this, &TcarlifterUpd::onTextInput);
 spNumber->valueChanged().connect(this, &TcarlifterUpd::onTextInput);
 Wt::WText * l = new Wt::WText(L"�����:", tMain->elementAt(r, 0));
 r++;

 eDescription = new Wt::WLineEdit(tMain->elementAt(r, 1)); eDescription->setObjectName("eDescription");
 eDescription->setEnabled(true);
 eDescription->textInput().connect(this, &TcarlifterUpd::onTextInput);
 l = new Wt::WText(L"��������:", tMain->elementAt(r, 0));
 r++;

 TOkFooter * footer = new TOkFooter(tMain->elementAt(r, 0));
 footer->bCancel->clicked().connect(this, &TcarlifterUpd::onbCancel);
 bOk = footer->bOk;
 bOk->clicked().connect(this, &TcarlifterUpd::onbOk);
 tMain->elementAt(r, 0)->setColumnSpan(tMain->columnCount());

 setTablePadding(tMain, 2);
}
//--------------------------------------------------------------------------
void TcarlifterUpd::refresh()
{
 Twindow::refresh();

 if (carlifter_id > 0)
 {
     Wt::WStandardItemModel lmt(this);
     if (dl->CarlifterS(&lmt, city_id) > 0)
     {         
      for(int i = 0; i < lmt.rowCount(); i++)
      { 
       if (to_int(lmt.data(i, fieldByName(&lmt, "carlifter_id"))) != carlifter_id) continue;
       spNumber->setValue(to_int(lmt.data(i, fieldByName(&lmt, "number"))));
       eDescription->setText(to_wstring(lmt.data(i, fieldByName(&lmt, "description"))));
       break;
      }
     }
 }
}
//--------------------------------------------------------------------------
void TcarlifterUpd::onTextInput()
{
 bOk->setEnabled(spNumber->value() > 0);
}
//--------------------------------------------------------------------------
void TcarlifterUpd::onbOk()
{
    int number;
    std::string description;

    number = spNumber->value();
    description = eDescription->text().trim().toUTF8();

    int id = dl->CarlifterIU(carlifter_id, city_id, number, description);
    if (id > 0)
    {
        onCloseInt(id);
    }
    else
    {
        ShowMessage(0, Wt::WString::fromUTF8(str2html(dl->errorstr)), "DB Error");
    }
}
//--------------------------------------------------------------------------
void TcarlifterUpd::onbCancel()
{
 onCloseInt(0);
}
//--------------------------------------------------------------------------