#ifndef CARLIFTERPLANWINDOW_H
#define CARLIFTERPLANWINDOW_H

#include <vector>

#include <Wt/WWidget>
#include <Wt/WContainerWidget>
#include <Wt/WVBoxLayout>
#include <Wt/WHBoxLayout>
#include <Wt/WDialog>
#include <Wt/WLineEdit>
#include <Wt/WCheckBox>
#include <Wt/WTableView>
#include <Wt/WItemDelegate>
#include <Wt/WComboBox>
#include <Wt/WSpinBox>

#include "uDataLayer.h"
#include "widget.h"

class TcarlifterPlanBrw : public Twindow
{
 public:
    TcarlifterPlanBrw(const int city, Wt::WContainerWidget *parent, TDataLayer * dl);
    //virtual ~TcarlifterPlanBrw();

    void setupUi();
    //void refresh();

    void onCityChanged(const int);
private:
    Wt::WPushButton *bNew, *bEdit, *bDelete, *bNewReq, *bEditReq, *bDeleteReq;
    Wt::WSpinBox *sbYear;
    Wt::WTableView * tableView; Wt::WStandardItemModel * mt;
    Wt::WComboBox * sbMonth;
    TComboBox * cbCarLifter;

    int currentYear, currentMonth, currentCarLifter;
    std::vector<TCarlifterPlan> vplan;

    int city;

    void refresh(const int);
    void redraw(const int);
    void onEdit();
    void onNew();
    void onDelete();
    void onEditReq();
    void onNewReq();
    void onDeleteReq();
    void onSelectionChanged();
    void refreshPlan();
    void saveStatement();
    int currentRequisitionId();
};

#endif