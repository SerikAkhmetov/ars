#ifndef INSURANCECASEUPDWINDOW_H
#define INSURANCECASEUPDWINDOW_H

#include <Wt/WWidget>
#include <Wt/WContainerWidget>
#include <Wt/WVBoxLayout>
#include <Wt/WHBoxLayout>
#include <Wt/WDialog>
#include <Wt/WLineEdit>
#include <Wt/WDoubleSpinBox>
#include <Wt/WTextEdit>
#include <Wt/WTextArea>
#include <Wt/WTable>
#include <Wt/WTabWidget>
#include <Wt/WTextArea>
#include <Wt/WDateEdit>
#include <Wt/WTimeEdit>
#include <Wt/WTabWidget>
#include <Wt/WCheckBox>
#include <Wt/WRadioButton>
//#include <Wt/WFileDropWidget>
#include <Wt/WFileUpload>

#include "uDataLayer.h"
#include "widget.h"
#include "taskBrwWindow.h"

//--------------------------------------------------------------------------
class TDriverWidget : public Wt::WContainerWidget, public TDriver
{
public:
  TDriverWidget(Wt::WContainerWidget *parent, TDataLayer * p_dl);
  void setupUi();
  void refresh();
  void hideDriversLicense();
  void hideCitizenship(); 

  Wt::Signal<> changed;

private:
    Wt::WTable * t1;
    //Wt::WCheckBox * cbResident;
    Wt::WButtonGroup * bgResident;
    Wt::WRadioButton * rbResident1, * rbResident2;
    TComboBoxDirLine * cbCitizenship;
    int rowCitizenship;
    TDateEdit * deDate_birth;
    Wt::WButtonGroup * bgGender;
    Wt::WRadioButton * rbGender1, * rbGender2;
    Wt::WLineEdit * eLast_name, * eFirst_name, * eMiddle_name;
    Wt::WCheckBox * cbWithout_mname;
    Wt::WLineEdit * ePhone, *eDl_series, *eDl_number;
    Wt::WCheckBox * cbDl_no_data;
    int rowDl;

    Wt::WLineEdit *eP_series, *eP_number, *eIssued;
    TDateEdit *deIssue;
    Wt::WLineEdit *ePostcode, /**eAddr_field1,*/ *eAddr_field2, *eAddr_field3, *eAddr_field4, *eAddr_field5;
    Wt::WTextArea *eAddr_field1;
    Wt::WSpinBox *cbHouse, *cbApartment;

    void onTextInput();
    void onCheckedChanged(Wt::WRadioButton *);
};
//--------------------------------------------------------------------------
class TVehicleWidget : public Wt::WContainerWidget, public TVehicle
{
public:
    TVehicleWidget(Wt::WContainerWidget *parent, TDataLayer * p_dl);
    void setupUi();
    void refresh();

    Wt::Signal<> changed;

    Wt::WPanel * p0, * p1, * p2;

private:
    TComboBoxDirLine * cbVehicle_type;
    Wt::WLineEdit * eBrand, * eModel, * eVin;
    Wt::WCheckBox * cbRight_hand_drive;
    Wt::WSpinBox * sbCar_year;
    Wt::WLineEdit * eRegistration_number;
    TComboBoxDirLine * cbVehicle_doc;
    Wt::WLineEdit * eSeries, *eNumberV;
    TDateEdit * deDate_doc, *deDate_start;
    Wt::WCheckBox * cbTd_passport;
    TDateEdit * deDate_sale, * deLast_sale;
    Wt::WCheckBox * cbVehicle_warranty;

    void onTextInput();
};
//--------------------------------------------------------------------------
class TDamageWidget : public Wt::WPanel, public TDamage
{
public:
    TDamageWidget(Wt::WContainerWidget *parent, TDataLayer * p_dl);
    void setupUi();
    void refresh();

private:

    Wt::WTable * t0;
    Wt::WString getString();
    void onUnCollapsed();
    void onKindChanged(int element_id);
};
//--------------------------------------------------------------------------
class TExecutorItemWidget : public Wt::WContainerWidget, public TBasic
{
public:
    TExecutorItemWidget(const int p_job, const int p_partner_id, TDataLayer * dl);
    void setupUi();
    void refresh();

    void clear();
    bool read();
    //  bool read(Wt::WStandardItemModel * mt, const int i);
    int save();
    bool remove(){return (true);};

    void setPartner(const int partner_id);

    TComboBox * cbExecutor;
    TTask task;
    int partner_id;
private:
    Wt::WPushButton * bTask;

    TComboBoxDirLine * cbTypeTask;
    Wt::WDateEdit * deExecuteBefore;
    Wt::WTextArea * teTask;

    void onTask();
    void onExecutorChanged();
    void onTaskDialogDone(Wt::WDialog::DialogCode code);
};
//--------------------------------------------------------------------------
class TExecutorWidget : public Wt::WPanel, public TBasic
{
public:
    TExecutorWidget(Wt::WContainerWidget *parent, TDataLayer * p_dl);
    void setupUi();
    void refresh();

    void clear();
    bool read();
  //  bool read(Wt::WStandardItemModel * mt, const int i);
    int save();
    bool remove();

    TBasicData * bd;
    TApplicationInspection * ai;
    Wt::WLineEdit * eResponsible;

private:
    TComboBox * cbPartner;
    TComboBoxDirLine * cbFirm, * cbStatus0, * cbStatus1;

    TICState state0, state1;
    std::list<TExecutorItemWidget *> lstItem;
    Wt::WStandardItemModel mtJob, mtUser, mtUser0, mtUserPartner;

    void initmtUser(Wt::WStandardItemModel * mt, const int partner_id);
    void onPartnerChanged();
    void onFirmChanged();
};
//--------------------------------------------------------------------------
class TEntityWidget : public Wt::WContainerWidget, public TEntity
{
public:
    TEntityWidget(Wt::WContainerWidget *parent, TDataLayer * p_dl);
    void setupUi();
    void refresh();

    Wt::Signal<> changed;
private:
    Wt::WTable * t0;
    Wt::WLineEdit *eName, *eITN;
    void onTextInput();
};
//--------------------------------------------------------------------------
class TPaymentDetailsWidget : public Wt::WContainerWidget, public TPaymentDetails
{
public:
    TPaymentDetailsWidget(Wt::WContainerWidget *parent, TDataLayer * p_dl);
    void setupUi();
    void refresh();

    Wt::Signal<> changed;
private:
    Wt::WTable * t0;
    Wt::WLineEdit *eCheckingAccount, *eBank, *eDepartment, *eCorrespondentAccount, *eBic, *eITN, *eCard, *eRecipient;
    void onTextInput();
};
//--------------------------------------------------------------------------
class TinsuranceCaseUpd : public Twindow
{
public:
    TinsuranceCaseUpd(Wt::WContainerWidget *parent, TDataLayer * p_dl, const int ic_id = 0);
    //virtual ~TinsuranceCaseUpd();

    void setupUi();
    void refresh();

private:    
    Wt::WTabWidget * tab;

    Wt::WWidget * setupUiBasic();

    TBasicData basicData;

    Wt::WCheckBox * cbSettlement, *cbReimbursement_lmv, * cbCalculation_lmv_possible;
    Wt::WLineEdit * eNumber;
    TComboBoxDirLine * cbInsuranceType;
    TDateEdit * deTa; TTimeEdit * teTa;
    Wt::WLineEdit * eInspection;
    Wt::WSpinBox * sbParticipants, * spCulprits;
    TComboBoxDirLine * cbTa_certificate, * cbInspection_type;
    Wt::WCheckBox * cbOptional_equipment, * cbApplication, * cbContract_sale;
    TComboBoxDirLine * cbIncident_type, *cbCause_damage, * cbTa_location_type;
    Wt::WCheckBox * cbFranchise;
    Wt::WLineEdit * ePost_code;//, * eAddr_field1;
    Wt::WTextArea * eAddr_field1;
    Wt::WCheckBox * cbNight, * cbMinimal_contact, * cbCasualties, * cbTransfer, * cbMmobile_app;

    Wt::WWidget * setupUiApplicationInspection();

    TApplicationInspection applicationInspection;

    Wt::WCheckBox * cbUnlimited_ep, * cbDisagreements, * cbExit_ad;
    TComboBox * cbRegion_circulation;
    TDateEdit * deDt_application;
    TTimeEdit * teDt_application; 
    TComboBoxDirLine * cbWho_applied, /* *cbScheme_number,*/ * cbApplicants;
    TComboBox * cbRegion_inspection;
    Wt::WLineEdit /* eSubdivision,*/ * eResponsible;//, * eInspection_address;
    Wt::WTextArea * eInspection_address;
    TDateEdit * deDt_inspection;
    TTimeEdit * teDt_inspection;
    Wt::WCheckBox * cbOn_move, * cbFlip, * cbFrontal, * cbAirbags;
    TDateEdit * deRereferral, * deReceiving;
    Wt::WDoubleSpinBox * eRepair_cost, * eWithout_wear, * eAgreed_cost, * eIncluding_wear, * eExpertise_cost;
    TComboBoxDirLine * cbTerms_payment;
    Wt::WCheckBox * cbSearch_defects, * cbDetermination, * cbResidual_value, * cbLoss_value, * cbTrace_evidence, * cbIndependent, * cbPhoto;
    TDamageWidget * Damage;

    Wt::WWidget * setupUiCulprit();

    TCulprit culprit;

    Wt::WLineEdit * eInsurance_company, * ePolis_series, * ePolicy_number;
    TDateEdit * deDate_conclusion;
    TVehicleWidget * vCulprit;
    TDriverWidget * dCulprit, *dCulpritOwner;
    Wt::WPanel * pCulpritOwner;
    Wt::WCheckBox * cbCulpritDriverIsOwner;

    void onTextInputCulprit();
    void onCulpritDriverIsOwnerChanged();
    bool isCulpritDriverIsOwner();

    Wt::WWidget * setupUiVictim();

    TVictim victim;

    Wt::WLineEdit * eInsurance_companyV, * ePolicy_seriesV, *ePolicy_numberV;

    TVehicleWidget * vVictim;

    TDriverWidget * dVictim, * dOwner;
    Wt::WPanel * pOwner;
    Wt::WCheckBox * cbVictimDriverIsOwner;

    void onTextInputVictim();
    void onVictimDriverIsOwnerChanged();
    bool isVictimDriverIsOwner();

    Wt::WWidget * setupUiBeneficiary(); 

    TDriverWidget * dBeneficiary;
    TEntityWidget * entity;
    TPaymentDetailsWidget * PaymentDetails;

    Wt::WCheckBox * cbOwnerIsBeneficiary;
    void onOwnerIsBeneficiaryChanged();
    bool isOwnerIsBeneficiary();

    Wt::WWidget * setupUiFile();
    //Wt::WFileDropWidget * fileDrop;

    TFileDoc * fileDoc1, * fileDoc2, *fileDoc3;

  //  Wt::WFileUpload * fileUpload;
  //  Wt::WTable * fView;
  //  Wt::WStandardItemModel mtFile;

    void fileRead(const int id);
    void fileRefresh();
    void fileSave(const int id);
  //  void fileDelete(int);
 //   void startUpload();
 //   void onUploaded();
 //   void fileTooLarge(int64_t filesize);

    Wt::WWidget * setupUiTask();
    TtaskBrw * taskBrw;

    Wt::WWidget * setupUiExecutor();
    TExecutorWidget * executor;

    Wt::WPushButton * bOk, * bCancel, * bReport, *bOrder;

    void onTextInput();
    void onbOk();
    void onbCancel();
    void onbReport();
    void onbOrder();

    bool save();
};

#endif // INSURANCECASEUPDWINDOW_H
