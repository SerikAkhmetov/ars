######################################################################
# Automatically generated by qmake (2.01a) ?? 22. ??? 13:47:25 2015
######################################################################
CONFIG   -= qt
CONFIG   += console
QT       -= gui qt# core

TEMPLATE = app
TARGET = wars
DEPENDPATH += .
INCLUDEPATH += .

WT_ROOT = $$system(echo %WT_ROOT%)
IBPP_ROOT = $$system(echo %IBPP_ROOT%)
JPEG_ROOT = $$system(echo %JPEG_ROOT%)
ZLIB_ROOT = $$system(echo %ZLIB_ROOT%)
LIBZIP_ROOT = $$system(echo %LIBZIP_ROOT%)

INCLUDEPATH += $${WT_ROOT}/include

INCLUDEPATH += $${IBPP_ROOT}/core
DEPENDPATH += $${IBPP_ROOT}/core
VPATH += $${IBPP_ROOT}/core
include($${IBPP_ROOT}/ibpp.pri)

INCLUDEPATH += sqlite
DEPENDPATH += sqlite
VPATH += sqlite
include(sqlite/sqlite.pri)

INCLUDEPATH += $${JPEG_ROOT}
LIBS += -L'$${JPEG_ROOT}'
LIBS += -L'$${JPEG_ROOT}'/Release/Win32

INCLUDEPATH += $${ZLIB_ROOT}
INCLUDEPATH += $${ZLIB_ROOT}/build
LIBS += -L'$${ZLIB_ROOT}'/build/release

INCLUDEPATH += $${LIBZIP_ROOT}/lib
INCLUDEPATH += $${LIBZIP_ROOT}/build
LIBS += -L'$${LIBZIP_ROOT}'/build/lib/Release

# Input
SOURCES += carlifterPlanBrwWindow.cpp carlifterPlanUpdWindow.cpp dirLineBrwWindow.cpp dirLineUpdWindow.cpp ibppquery.cpp \
           insuranceCaseBrwWindow.cpp insuranceCaseUpdWindow.cpp mainWindow.cpp parameter.cpp partnerBrwWindow.cpp partnerUpdWindow.cpp showLogWindow.cpp \
           uDataLayer.cpp userBrwWindow.cpp userUpdWindow.cpp wars.cpp widget.cpp requsitionUpdWindow.cpp requisitionBrwWindow.cpp \
           taskBrwWindow.cpp taskUpdWindow.cpp carlifterBrwWindow.cpp carlifterUpdWindow.cpp

HEADERS += carlifterPlanBrwWindow.h carlifterPlanUpdWindow.h dirLineBrwWindow.h dirLineUpdWindow.h ibppquery.h \
           insuranceCaseBrwWindow.h insuranceCaseUpdWindow.h mainWindow.h parameter.h partnerBrwWindow.h partnerUpdWindow.h showLogWindow.h \
           uDataLayer.h userBrwWindow.h userUpdWindow.h requsitionUpdWindow.h requisitionBrwWindow.h \
           wars.h widget.h \
           taskBrwWindow.cpp taskUpdWindow.cpp carlifterBrwWindow.h carlifterUpdWindow.h

win32{
DEFINES += IBPP_WINDOWS HPDF_DLL ZIP_STATIC
DEFINES += __PRETTY_FUNCTION__=__FUNCSIG__
DEFINES += __func__=__FUNCTION__
LIBS += -ladvapi32 user32.lib advapi32.lib

LIBS += -L'$${WT_ROOT}'/lib/
CONFIG(debug, debug|release){
DEFINES += DEBUG
LIBS += -lwtd -lwthttpd -llibjpeg -lzlibstatic -lzip #-llibcurl_debug -llibhpdfd #-llibxml2_a
#LIBS += -LC:\Programs\libxml2-2.9.4.msvc2010e.debug\lib\ -llibxml2
}
CONFIG(release, debug|release){
LIBS += -lwt -lwthttp -llibjpeg -lzlibstatic -lzip #-llibcurl -llibhpdf
#LIBS += -LC:\Programs\libxml2-2.9.4.msvc2010e\lib\ -llibxml2_a
}

}

QMAKE_CFLAGS_RELEASE    = -MD
#QMAKE_CFLAGS_RELEASE_WITH_DEBUGINFO += -O2 -MT -Zi
QMAKE_CFLAGS_DEBUG      = -MDd

MOC_DIR = ../tmp
OBJECTS_DIR = ../tmp



