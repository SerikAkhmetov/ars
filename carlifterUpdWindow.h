#ifndef CARLIFTERUPDWINDOW_H
#define CARLIFTERUPDWINDOW_H

#include <Wt/WWidget>
#include <Wt/WContainerWidget>
#include <Wt/WVBoxLayout>
#include <Wt/WHBoxLayout>
#include <Wt/WDialog>
#include <Wt/WLineEdit>
#include <Wt/WTable>
#include <Wt/WTabWidget>
#include <Wt/WTextArea>
#include <Wt/WDateEdit>

#include "uDataLayer.h"
#include "widget.h"

//--------------------------------------------------------------------------
//--------------------------------------------------------------------------
class TcarlifterUpd : public Twindow
{
public:
    TcarlifterUpd(Wt::WContainerWidget *parent, TDataLayer * p_dl, const int city_id, const int carlifter_id = 0);
    //virtual ~TcarlifterUpd();

    void setupUi();
    void refresh();

private:
    int carlifter_id, city_id;

    Wt::WSpinBox * spNumber;
    Wt::WLineEdit * eDescription;

    Wt::WPushButton * bOk, * bCancel;

    void onTextInput();
    void onbOk();
    void onbCancel();
};

#endif
