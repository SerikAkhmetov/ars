#ifndef TASKUPDWINDOW_H
#define TASKUPDWINDOW_H

#include <Wt/WWidget>
#include <Wt/WContainerWidget>
#include <Wt/WVBoxLayout>
#include <Wt/WHBoxLayout>
#include <Wt/WDialog>
#include <Wt/WLineEdit>
#include <Wt/WTable>
#include <Wt/WTabWidget>
#include <Wt/WTextArea>
#include <Wt/WDateEdit>

#include "uDataLayer.h"
#include "widget.h"

//--------------------------------------------------------------------------
//--------------------------------------------------------------------------
class TtaskUpd : public Twindow, public TTask
{
public:
    TtaskUpd(Wt::WContainerWidget *parent, TDataLayer * p_dl, const int task_id = 0);
    //virtual ~TtaskUpd();

    void setupUi();
    void refresh();

private:
    TDataLayer * dl;

    Wt::WTable * tMain;
    TLineEdit * eExecutor;
    Wt::WLineEdit * eNumber, * eInitiator;
    Wt::WDateEdit * deExecuteBefore;
    Wt::WTextArea * teTask;
    TComboBoxDirLine * cbTypeTask;
    Wt::WPushButton * bOk, * bState3, * bState4;//, * bCancel;
  //  Wt::WTable * tState;
    Wt::WGridLayout * lmain;

    Wt::WTextArea * taComment;
    Wt::WPushButton * bNewComment;

    void createCommentWriter(Wt::WContainerWidget *);
    Wt::WWidget * showComment(TTaskComment * comment);
    int startComment;

    void onCommentTextInput();
    void onbNewComment();
    void onExecutorSelect();
    void onTextInput();
    void onbOk();
    void onbCancel();
    void onbState3();
    void onbState4();
    void refreshState();
    void refreshComment();
};

#endif
