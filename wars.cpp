#include <Wt/WApplication>
#include <Wt/WEnvironment>
#include <Wt/WBootstrapTheme>
#include <Wt/WServer>
#include <Wt/WSslInfo>
#include <Wt/WLocalDateTime>
#include <signal.h>

#include "uDataLayer.h"
#include "loginWindow.h"
#include "mainWindow.h"
#include "parameter.h"
#include "wArs.h"

#if defined(WIN32)
#else
#include <execinfo.h>
#include <dlfcn.h>
#include <cxxabi.h>
#include <pwd.h>
#include "daemon.h"
#endif

TQueryResourceList qrList;
TParameterList params;
TSQLayer sl;

//---------------------------------------------------------------------------
WArsApplication::WArsApplication(const Wt::WEnvironment& env)
  : WApplication(env), locale(env.locale())
{
 Wt::WBootstrapTheme * bootstrap = new Wt::WBootstrapTheme();
 bootstrap->setVersion(Wt::WBootstrapTheme::Version::Version2);
 setTheme(bootstrap);

 //locale.setDateFormat("dd/MM/yyyy");

  locale.setDateFormat("dd/MM/yyyy");
  locale.setTimeFormat("HH:mm");
  setLocale(locale);

  setTitle(Wt::WString::fromUTF8("АРССервис"));                // application title

  enableUpdates(true);
  bool a = environment().ajax();

//  setConfirmCloseMessage(Wt::WString::fromUTF8("Go away ?"));

//  dl = DataLayerFactory();

  std::map<std::string, std::string> state;
  state["_"] = std::string("Create app");
  state["hostName"] = environment().hostName();
  state["clientAddress"] = environment().clientAddress();
  state["User-Agent"] = env.userAgent();
  //std::stringstream s1; s1 << env.screenWidth() << ":" << env.screenHeight();
  state["screen"] = std::to_string((long long)env.screenWidth()) + std::string(":") + std::to_string((long long)env.screenHeight());
  state["timeZoneOffset"] = env.timeZoneOffset();
  state["locale"] = env.locale().name();
  state["timeZone"] = env.locale().timeZone();
  state["dateFormat"] = env.locale().dateFormat().toUTF8();
  state["timeFormat"] = env.locale().timeFormat().toUTF8();
  state["decimalPoint"] = env.locale().decimalPoint();
  state["groupSeparator"] = env.locale().groupSeparator();
  state["referer"] = env.referer();
  state["urlScheme"] = env.urlScheme();
  state["sessionId"] = sessionId();

  Wt::WSslInfo *sslInfo = env.sslInfo();
  if (sslInfo)
  {
   state["SSL"] = "yes";
  }
  else
  {
   state["SSL"] = "no";
  }

  logtxt(&state);

//  startLogin();
//  wLogin->loggedIn();

  return;
}
//---------------------------------------------------------------------------
WArsApplication::~WArsApplication()
{
 std::map<std::string, std::string> state;
 state["_"] = std::string("Close app");
 if (Wt::WApplication::instance() != NULL)
    state["sessionId"] = Wt::WApplication::instance()->sessionId();
// state["sessionId"] = sessionId();

// root()->clear();

 logtxt(&state);
}
//---------------------------------------------------------------------------
void WArsApplication::initialize()
{
 Wt::WApplication::initialize();

 startMain();
 //startLogin();
 //wLogin->loggedIn();
}
void WArsApplication::finalize()
{
 root()->clear();

// dl->close();
// delete(dl);

 Wt::WApplication::finalize();
}
//---------------------------------------------------------------------------
void WArsApplication::startMain()
{
 root()->clear();
 TmainWindow * frm = new TmainWindow(root());
 frm->close.connect(this, &WArsApplication::startMain);
}
void WArsApplication::startLogin()
{
  processEvents();
  root()->clear();
 // wLogin = new TloginWindow(dl, root());
 // wLogin->close.connect(this, &WArsApplication::startMain);
}
Wt::WApplication *createApplication(const Wt::WEnvironment& env)
{
  return new WArsApplication(env);
}
//---------------------------------------------------------------------------
Wt::WApplication *createQueryResource(const Wt::WEnvironment& env)
{
  /*
   * You could read information from the environment to decide whether
   * the user has permission to start a new application
   */
  return new WArsApplication(env);
}
//---------------------------------------------------------------------------
void init()
{
 params.add("docroot",      0, 0, ".");
 params.add("approot",      0, 0, ".");
 params.add("config",       0, 0, "../wt-3.3.11/build/wt_config.xml");
 params.add("http-port",    0, 0, "8080");
 params.add("http-address", 0, 0, "0.0.0.0");
// params.add("%*",           0, 0, "");

 if (Wt::WApplication::instance() != NULL) params.set("approot", Wt::WApplication::instance()->appRoot());
}
//---------------------------------------------------------------------------
#if defined(WIN32)
#else
void signal_handler(int signum)
{
 std::map<std::string, std::string> state;
 std::stringstream msg;
 std::stringstream corestm;

 corestm << "/var/lib/wArs/coreDump" << params.get("http-port")->v_str;
{
 void *callstack[128];
 const int maxFrames = sizeof (callstack) / sizeof (callstack[0]);
 char buf[1024];
 int frames = backtrace (callstack, maxFrames);
 char **symbols = backtrace_symbols (callstack, frames);

 FILE * crashFile = fopen(corestm.str().c_str(), "w");
 if (crashFile)
 {
  fprintf (crashFile, "=== %s === %d\n", Wt::WLocalDateTime::currentDateTime().toString("dd-MM-yyyy hh:mm:ss").toUTF8().c_str(), signum);

  for (int i = 0; i < frames; i++)
  {
   Dl_info info;
   if (dladdr (callstack[i], &info) && info.dli_sname)
   {
    char *demangled = NULL;
    int status = -1;
    if (info.dli_sname[0] == '_')
      demangled = abi::__cxa_demangle (info.dli_sname, NULL, NULL, &status);
    snprintf (buf, sizeof (buf), "%-3d %*p %s + %zd",
              i, int (2 + sizeof (void *) * 2), callstack[i],
              status == 0 ? demangled : info.dli_sname == 0 ? symbols[i] : info.dli_sname,
              (char *)callstack[i] - (char *)info.dli_saddr);
    free (demangled);
   }
   else
   {
    snprintf (buf, sizeof (buf), "%-3d %*p %s", i, int (2 + sizeof (void *) * 2), callstack[i], symbols[i]);
   }
   fprintf (crashFile, "%s\n", buf);
   msg << buf << std::endl;
  }

  fclose(crashFile);
 }
 free (symbols);
}

 state["callstack"] = msg.str();
 state["signum"] = std::to_string(static_cast<long long>(signum));
 state["_"] = __PRETTY_FUNCTION__;
 if (Wt::WApplication::instance() != NULL)
   state["sessionId"] = Wt::WApplication::instance()->sessionId();

 logtxt(&state);
}
#endif
//---------------------------------------------------------------------------
int main(int argc, char **argv)
{
  int ret(0);

  init();
  params.init_from_arg(argc, argv);

std::cout << "wArs" << std::endl;
std::cout <<  params.print() << std::endl;

#if defined(WIN32)
#else
  int pid;

  Tpid p(params.get("http-port")->v_str);

  if (!p.check())
  {
   printf("Error: Daemon on port %s are already started\n", params.get("http-port")->v_str.c_str());
   return -1;
  }

  pid = fork();
  if (pid == -1) // если не удалось запустить потомка
  {
   // выведем на экран ошибку и её описание
   printf("Error: Start Daemon failed (%s)\n", strerror(errno));
   return -1;
  }
  else if (!pid) // если это потомок
  {
   p.write();
  // закрываем дескрипторы ввода/вывода/ошибок, так как нам они больше не понадобятся
  close(STDIN_FILENO);
  close(STDOUT_FILENO);
  close(STDERR_FILENO);

  if (getuid() == 0) // root -> wArs
  {
   struct passwd * p;
   p = getpwnam("wArs");
   if (p != NULL)
   {
    setreuid(p->pw_uid, p->pw_uid);
   }
  }
 {
  struct sigaction sa;
  memset (&sa, 0, sizeof(sa));
  sa.sa_handler = signal_handler;
  sigaction(SIGUSR1, &sa, NULL);      // reconnect DB
  sigaction(SIGUSR2, &sa, NULL);      // reload cache
  sigaction(SIGHUP,  &sa, NULL);
  sigaction(SIGINT,  &sa, NULL);      /* interrupt */
  sigaction(SIGILL,  &sa, NULL);      /* illegal instruction - invalid function image */
  sigaction(SIGFPE,  &sa, NULL);      /* floating point exception */
  sigaction(SIGSEGV, &sa, NULL);      /* segment violation */
  sigaction(SIGTERM, &sa, NULL);      /* Software termination signal from kill */
//  sigaction(SIGBREAK,&sa, NULL);      /* Ctrl-Break sequence */
  sigaction(SIGABRT, &sa, NULL);      /* abnormal termination triggered by abort call */
 }

#endif
  qrList.start();
  sl.start();

#ifdef _MSC_VER
  sl.dbname = /*"file:///" +*/ params.get("approot")->v_str + "/wArs.db";
   while(sl.dbname.find("\\") != std::string::npos) {sl.dbname.replace(sl.dbname.find("\\"), 1, "/");}
#else
  sl.dbname = /*"file:" +*/ params.get("approot")->v_str + "/wArs.db";
#endif
 if (sl.open() != SQLITE_OK)
 {
  Wt::log("SQLite") << sl.errmsg() << " " << sl.dbname;
 }

  const int vsize(params.db.size() * 2 + 1);
  char **v = new char *[vsize + 1]();
//  char *v[vsize];

//  int t = strlen(argv[0]);

  v[0] = argv[0]; //new char [strlen(argv[0]) + 1];
  //strncpy(v[0], argv[0], strlen(argv[0]));
  int i(1), l(0);
  for(auto itr = params.db.begin(); itr != params.db.end(); ++itr)
  { if (l < (itr->first.length() + 2)) l = itr->first.length() + 2;
   if (l < itr->second.v_str.length()) l = itr->second.v_str.length(); }
  for(auto itr = params.db.begin(); itr != params.db.end() && i < vsize; ++itr)
  {
   std::string p = "--" + itr->first;
   v[i] = new char[/*p.length()*/l + 1]();
   strncpy(v[i], p.c_str(), p.length());
   v[i][p.length()] = 0;
   i++;
   v[i] = new char[/*itr->second.v_str.length()*/l + 1]();
   strncpy(v[i], itr->second.v_str.c_str(), itr->second.v_str.length());
   v[i][itr->second.v_str.length()] = 0;
   i++;
  }
  v[vsize] = NULL;

   std::map<std::string, std::string> state;
   state["_"] = std::string("Server start");
   if (Wt::WApplication::instance() != NULL)
     state["sessionId"] = Wt::WApplication::instance()->sessionId();

   for(auto itr = params.db.begin(); itr != params.db.end(); ++itr)
   { state[itr->first] = itr->second.v_str; }
   logtxt(&state);

  ret = start_wt(argc, argv);

  for(i = 1; i < vsize; i++)
  { delete [] v[i];}
  delete [] v;

#if defined(WIN32)
#else
  p.remove();
 }
#endif
 return (ret);
}
//---------------------------------------------------------------------------
int start_wt(int argc, char **argv)
{
  int ret(0);

  setlocale(LC_ALL, "ru_RU.utf8");

  try {
    Wt::WServer server(argv[0]);
    qrList.server = &server;
    // WTHTTP_CONFIGURATION is e.g. "/etc/wt/wthttpd"
    server.setServerConfiguration(argc, argv/*, WTHTTP_CONFIGURATION*/);
  //  server.setServerConfiguration(vsize, v, WTHTTP_CONFIGURATION);
    server.addEntryPoint(Wt::Application, createApplication);

    qrList.append(new TQueryResource(), "/query.xml");
//    qrList.append(new TWebDAV(), "/webdav");
//    server.addResource(new TWebDAV(), "/webdav");
    if (server.start()) {
      //qrList.start();
      int sig = Wt::WServer::waitForShutdown();
      qrList.stop();
      std::stringstream stm;
      stm << "Shutdown (signal = " << sig << ")";
      std::cout << stm.str() << std::endl;
      logtxt(stm.str());
      server.stop();
      sl.close();
      //if (sig == SIGHUP)
      //  Wt::WServer::restart(argc, argv, environ);
    }
    else
    {    
     std::cout << "Server start error" << std::endl;
     logtxt("Server start error");
    }
  } catch (Wt::WServer::Exception& e) {    
    std::cerr << e.what() << std::endl;
    logtxt(e.what());
    ret = 1;
  } catch (std::exception& e) {
    std::cerr << e.what() << std::endl;
    logtxt(e.what());
    ret = 1;
  }
   return (ret);
}
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
