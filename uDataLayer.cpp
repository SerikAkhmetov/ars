//---------------------------------------------------------------------------
#include <algorithm>
#include <iostream>
#include <boost/algorithm/string.hpp>
#include <boost/archive/iterators/base64_from_binary.hpp>
#include <boost/archive/iterators/insert_linebreaks.hpp>
#include <boost/archive/iterators/transform_width.hpp>
#if defined(WIN32)
#include <boost/uuid/detail/sha1.hpp>
#else
#include <boost/uuid/sha1.hpp>
#endif
#include <Wt/WBoostAny>
#include <Wt/WEnvironment>
#include <Wt/Utils>
#include <boost/regex.hpp>
#include <boost/filesystem.hpp>

#include <ibase.h>
#include <ibpp.h>
#include <iberror.h>
#include "ibppquery.h"

#include "uDataLayer.h"
#include "widget.h"
//#include "docUpdWindow.h"
#include "parameter.h"
//#include "service.h"
class TProcedure
{
 public:
  std::string name;
  char type;
  std::vector<std::string> in_param;
  std::list<std::string> out_param;
};
//---------------------------------------------------------------------------
TRequisition::TRequisition(TDataLayer * p_dl)
{
 dl = p_dl;
 clear();
}
void TRequisition::clear()
{
 requisition_id = 0;
 date_ta = Wt::WDate();
 phone_number.clear();
 customer.clear();
 carlifter_plan_id = 0;

 carlifter_id = 0;
 date_plan = Wt::WDate();
}
bool TRequisition::read()
{
 Wt::WStandardItemModel mt;
 if (dl->RequisitionS(&mt, requisition_id, 0) > 0)
 {
  requisition_id = to_int(mt.data(0, fieldByName(&mt, "requisition_id")));
  date_ta = to_wdate(mt.data(0, fieldByName(&mt, "date_ta")));
  phone_number = to_string(mt.data(0, fieldByName(&mt, "phone")));
  customer = to_string(mt.data(0, fieldByName(&mt, "customer")));
  carlifter_plan_id = to_int(mt.data(0, fieldByName(&mt, "carlifter_plan_id")));

  carlifter_id = to_int(mt.data(0, fieldByName(&mt, "carlifter_id")));
  date_plan = to_wdate(mt.data(0, fieldByName(&mt, "DT_START")));
  return(true);
 }
 return(false);
}
int TRequisition::save()
{
 int id;
 id = dl->RequisitionIU(requisition_id, date_ta, phone_number, customer, carlifter_plan_id);
 if (id > 0) requisition_id = id;
 return(id);
}

bool TRequisition::remove()
{
    return(dl->RequisitionD(requisition_id));
}
//---------------------------------------------------------------------------
TCarlifterPlan::TCarlifterPlan(TDataLayer * p_dl)
{
  dl = p_dl;
  clear();
}

void TCarlifterPlan::clear()
{
 carlifter_plan_id = 0;
 carlifter_id = 0;
 number = 0;
 requisition_id = 0;
 dt_start = Wt::WDateTime();
 dt_finish = Wt::WDateTime();
}

bool TCarlifterPlan::read()
{
//  CarlifterPlanS(Wt::WAbstractItemModel * mt, const int city, const Wt::WDateTime& start, const Wt::WDateTime& finish, const int carlifter_plan_id)
  Wt::WStandardItemModel mt;
  if (carlifter_plan_id > 0 && dl->CarlifterPlanS(&mt, 0, Wt::WDateTime(), Wt::WDateTime(), carlifter_plan_id) > 0)
  {
   carlifter_plan_id = to_int(mt.data(0, fieldByName(&mt, "carlifter_plan_id")));
   carlifter_id = to_int(mt.data(0, fieldByName(&mt, "carlifter_id")));
   number = to_int(mt.data(0, fieldByName(&mt, "number")));
   requisition_id = to_int(mt.data(0, fieldByName(&mt, "requisition_id")));
   dt_start = to_wdatetime(mt.data(0, fieldByName(&mt, "dt_start")));
   dt_finish = to_wdatetime(mt.data(0, fieldByName(&mt, "dt_finish")));
   return(true);
  }
  return(false);
}
int TCarlifterPlan::save()
{
 int id;
 id = dl->CarlifterPlanIU(carlifter_plan_id, carlifter_id, dt_start, dt_finish);
 if (id > 0) carlifter_plan_id = id;
 return(id);
}
bool TCarlifterPlan::remove()
{
 return(dl->CarlifterPlanD(carlifter_plan_id));
}
std::string TCarlifterPlan::label() const
{
 std::stringstream stm;
 if (dt_start.isValid() && !dt_start.isNull())
   stm << dt_start.time().hour() << ":" <<  std::setw(2) << std::setfill ('0') << std::right << dt_start.time().minute();
 stm << "-" << std::endl;
 if (dt_finish.isValid() && !dt_finish.isNull())
   stm << dt_finish.time().hour() << ":" << std::setw(2) << std::setfill ('0') << std::right << dt_finish.time().minute();
 return(stm.str());
}
//---------------------------------------------------------------------------
TDriver::TDriver(TDataLayer * p_dl)
{
    dl = p_dl;
    clear();
}
void TDriver::clear()
{
    driver_id = 0;
    resident = false;
    citizenship = 0;
    date_birth = Wt::WDate();
    gender = 0;
    last_name = first_name = middle_name = fio = std::string();
    without_mname = false;
    phone = dl_series = dl_number = std::string();
    dl_no_data = false;
    p_series = p_number = issued = postcode = addr_field1 = addr_field2 = 
        addr_field3 = addr_field4 = addr_field5 = std::string();
    date_issue = Wt::WDate();
    house = apartment = 0;
}
bool TDriver::read()
{
    Wt::WStandardItemModel mt;
    if (driver_id > 0 && dl->DriverS(&mt, driver_id) > 0)
    {
        resident = to_bool(mt.data(0, fieldByName(&mt, "resident")));
        citizenship = to_int(mt.data(0, fieldByName(&mt, "citizenship")));
        date_birth = to_wdate(mt.data(0, fieldByName(&mt, "date_birth")));
        gender = to_int(mt.data(0, fieldByName(&mt, "gender")));
        last_name = to_string(mt.data(0, fieldByName(&mt, "last_name")));
        first_name = to_string(mt.data(0, fieldByName(&mt, "first_name")));
        middle_name = to_string(mt.data(0, fieldByName(&mt, "middle_name")));
        fio = to_string(mt.data(0, fieldByName(&mt, "fio")));
        without_mname = to_bool(mt.data(0, fieldByName(&mt, "without_mname")));
        phone = to_string(mt.data(0, fieldByName(&mt, "phone")));
        dl_series = to_string(mt.data(0, fieldByName(&mt, "dl_series")));
        dl_number = to_string(mt.data(0, fieldByName(&mt, "dl_number")));
        dl_no_data = to_bool(mt.data(0, fieldByName(&mt, "dl_no_data")));
        p_series = to_string(mt.data(0, fieldByName(&mt, "p_series")));
        p_number = to_string(mt.data(0, fieldByName(&mt, "p_number")));
        issued = to_string(mt.data(0, fieldByName(&mt, "issued")));
        date_issue = to_wdate(mt.data(0, fieldByName(&mt, "date_issue")));
        postcode = to_string(mt.data(0, fieldByName(&mt, "postcode")));
        addr_field1 = to_string(mt.data(0, fieldByName(&mt, "addr")));
        house       = to_int(mt.data(0, fieldByName(&mt, "house")));
        apartment   = to_int(mt.data(0, fieldByName(&mt, "apartment")));
        return(true);
    }
    return(false);
}
int TDriver::save()
{
    int id;
    id = dl->DriverIU(this);
    if (id > 0) driver_id = id;
    return(id);
}
bool TDriver::remove()
{
    if (driver_id > 0)
        return(dl->DriverD(driver_id));
    else
        return(true);
}

/*bool TDriver::isEmpty()
{

}*/
//---------------------------------------------------------------------------
TVehicle::TVehicle(TDataLayer * p_dl)
{
    dl = p_dl;
    clear();
}
void TVehicle::clear()
{
    vehicle_id = 0;
    vehicle_type = 0;
    brand = model = vin = std::string();
    right_hand_drive = false;
    car_year = 0;
    registration_number = std::string();;
    vehicle_doc = 0;
    series = number = std::string();
    date_doc = date_start = Wt::WDate();
    td_passport = false;
    date_sale = last_sale = Wt::WDate();
    vehicle_warranty = 0;
}
bool TVehicle::read()
{
    Wt::WStandardItemModel mt;
    if (vehicle_id > 0 && dl->VehicleS(&mt, vehicle_id) > 0)
    {
        vehicle_type = to_int(mt.data(0, fieldByName(&mt, "vehicle_type")));
        brand = to_string(mt.data(0, fieldByName(&mt, "brand")));
        model = to_string(mt.data(0, fieldByName(&mt, "model")));
        vin = to_string(mt.data(0, fieldByName(&mt, "vin")));
        right_hand_drive = to_bool(mt.data(0, fieldByName(&mt, "right_hand_drive")));
        car_year = to_int(mt.data(0, fieldByName(&mt, "car_year")));
        registration_number = to_string(mt.data(0, fieldByName(&mt, "registration_number")));
        vehicle_doc = to_int(mt.data(0, fieldByName(&mt, "vehicle_doc")));
        series = to_string(mt.data(0, fieldByName(&mt, "series")));
        number = to_string(mt.data(0, fieldByName(&mt, "number")));
        date_doc = to_wdate(mt.data(0, fieldByName(&mt, "date_doc")));
        date_start = to_wdate(mt.data(0, fieldByName(&mt, "date_start")));
        td_passport = to_bool(mt.data(0, fieldByName(&mt, "td_passport")));
        date_sale = to_wdate(mt.data(0, fieldByName(&mt, "date_sale")));
        vehicle_warranty = to_bool(mt.data(0, fieldByName(&mt, "vehicle_warranty")));
        last_sale = to_wdate(mt.data(0, fieldByName(&mt, "last_sale")));
        return(true);
    }
    return(false);
}
int TVehicle::save()
{
    int id;
    id = dl->VehicleIU(this);
    if (id > 0) vehicle_id = id;
    return(id);
}
bool TVehicle::remove()
{
    if (vehicle_id > 0)
        return(dl->TableD("vehicle", vehicle_id));
    else
        return(true);
}
//---------------------------------------------------------------------------
TUser::TUser(TDataLayer * p_dl)
{
    dl = p_dl;
    clear();
}
void TUser::clear()
{
    user_id = 0;
    user_name = first_name = middle_name = last_name = pass = email = fio = std::string();
    partner_id = can_create_user = job = 0;

    isActive = false;
}
bool TUser::read()
{
    Wt::WStandardItemModel mt;
    if (user_id > 0 && dl->TableS("sys_users", &mt, user_id) > 0)
    {
        user_name = to_string(mt.data(0, fieldByName(&mt, "user_name")));
        first_name = to_string(mt.data(0, fieldByName(&mt, "first_name")));
        middle_name = to_string(mt.data(0, fieldByName(&mt, "middle_name")));
        last_name = to_string(mt.data(0, fieldByName(&mt, "last_name")));
        pass = to_string(mt.data(0, fieldByName(&mt, "pass")));
        email = to_string(mt.data(0, fieldByName(&mt, "email")));
        fio = to_string(mt.data(0, fieldByName(&mt, "fio")));
        partner_id = to_int(mt.data(0, fieldByName(&mt, "partner_id")));
        can_create_user = to_int(mt.data(0, fieldByName(&mt, "can_create_user")));
        job = to_int(mt.data(0, fieldByName(&mt, "job")));
        return(true);
    }
    return(false);
}
int TUser::save()
{
    int id; bool dbEnabled = isInDBActive();
    id = dl->UsersIU(this);
    if (id > 0) user_id = id;
    if (isActive && !dbEnabled)
    {
      if (!createUser())
      {
          ShowMessage(0, Wt::WString::fromUTF8(str2html(dl->errorstr)), "DB Error");
      }
    }
    if (!isActive && dbEnabled)
    {
        if (!removeUser())
        {
            ShowMessage(0, Wt::WString::fromUTF8(str2html(dl->errorstr)), "DB Error");
        }
    }
    return(id);
}
bool TUser::remove()
{
    return(false);
}
bool TUser::isValid() const
{
    return(user_name.length() > 0);
}
bool TUser::isInDBActive() const
{
    bool res(false);
     IBPP::Service service = IBPP::ServiceFactory(dl->hostName(), "SYSDBA", "masterke"); // TODO : remove const

     service->Connect();
     if (service->Connected())
     {
         try{
             IBPP::User user; std::string uname(Upper(user_name)); user.username = uname;
             service->GetUser(user);
             res = user.username == uname;
         }
         catch(...)
         {
             res = false;
         }
         service->Disconnect();
     }
  return (res);
}

bool TUser::createUser() const
{
    bool res(false);
    IBPP::Service service = IBPP::ServiceFactory(dl->hostName(), "SYSDBA", "masterke"); // TODO : remove const

    service->Connect();
    if (service->Connected())
    {
        try{
            IBPP::User user; std::string uname(Upper(user_name));
            user.username = uname;
            user.password = pass;
            service->AddUser(user);
            res = true;
        }
        catch(IBPP::Exception& e)
        {
            dl->errorstr = e.what();
            Wt::log("notice") << "TUser::createUser " << e.what();
            res = false;
        }
        catch(...)
        {
            res = false;
        }
        service->Disconnect();
    }

    if (res)
    {
        TDataLayer * dla = DataLayerFactory();
        dla->setHostName(dl->hostName());
        dla->setPort(dl->port());
        dla->setDatabaseName(dl->dbName());
        dla->setUserName(dl->userName());
        dla->setRole("RDB$ADMIN");
        dla->setPassword(dl->password());
        dla->open();
        if (dla->isOpen())
        {
         if (dla->GrandRole("MASTER", user_name) < 0)
         {res = false; dl->errorstr = dla->errorstr;}
         if (res && can_create_user > 0)
         {    if (dla->GrandRole("RDB$ADMIN", user_name) < 0)
             {res = false; dl->errorstr = dla->errorstr;}
         }
        }
        delete(dla);
    }

    return (res);
}

bool TUser::removeUser() const
{
    bool res(false);
    IBPP::Service service = IBPP::ServiceFactory(dl->hostName(), "SYSDBA", "masterke"); // TODO : remove const

    service->Connect();
    if (service->Connected())
    {
        try{
            std::string uname(Upper(user_name));
            service->RemoveUser(uname);
            res = true;
        }
        catch(IBPP::Exception& e)
        {
            dl->errorstr = e.what();
            Wt::log("notice") << "TUser::createUser " << e.what();
            res = false;
        }
        catch(...)
        {
            res = false;
        }
        service->Disconnect();
    }

    return (res);
}
//---------------------------------------------------------------------------
TTaskComment::TTaskComment()
{
 clear();
}
void TTaskComment::clear()
{
    task_comment_id = task_id = 0;
    comment = std::string();
    author = 0;
    dt = Wt::WDateTime();
    fio = std::string();
}
//---------------------------------------------------------------------------
TTask::TTask(TDataLayer * p_dl)
{
    dl = p_dl;
    clear();
}
void TTask::clear()
{
    task_id = ic_id = initiator = executor = type_task = job = 0; 
    task.clear();
    execute_before = Wt::WDate();
    lState.clear();
    lComment.clear();
}
bool TTask::read()
{
    Wt::WStandardItemModel mt, mts, mtc;
    if (task_id > 0 && dl->TableS("task", &mt, task_id) > 0)
    {
        clear();
        task_id = to_int(mt.data(0, fieldByName(&mt, "task_id")));
        ic_id = to_int(mt.data(0, fieldByName(&mt, "ic_id")));
        initiator = to_int(mt.data(0, fieldByName(&mt, "initiator")));
        executor = to_int(mt.data(0, fieldByName(&mt, "executor")));
        task = to_string(mt.data(0, fieldByName(&mt, "task")));
        execute_before = to_wdate(mt.data(0, fieldByName(&mt, "execute_before")));
        type_task = to_int(mt.data(0, fieldByName(&mt, "type_task")));
        job = to_int(mt.data(0, fieldByName(&mt, "job")));

        if (dl->TaskStateS(&mts, task_id) > 0)
        {
            for(int i = 0; i < mts.rowCount(); ++i)
            {
               // int p0 = to_int(mt.data(i, fieldByName(&mts, "code")));
                lState.push_back(TTaskState(to_int(mts.data(i, fieldByName(&mts, "code"))),
                                            to_string(mts.data(i, fieldByName(&mts, "name"))),
                                            to_wdatetime(mts.data(i, fieldByName(&mts, "dt_state")))));
            }
        }

        if (dl->TableS("task_comment", &mtc, task_id) > 0)
        {
            for(int i = 0; i < mtc.rowCount(); ++i)
            {
                TTaskComment cm;
                cm.task_comment_id = to_int(mtc.data(i, fieldByName(&mtc, "task_comment_id")));
                cm.task_id = to_int(mtc.data(i, fieldByName(&mtc, "task_id")));
                cm.comment = to_string(mtc.data(i, fieldByName(&mtc, "comment")));
                cm.author = to_int(mtc.data(i, fieldByName(&mtc, "author")));
                cm.dt = to_wdatetime(mtc.data(i, fieldByName(&mtc, "dt")));
                cm.fio = to_string(mtc.data(i, fieldByName(&mtc, "fio")));
                lComment.push_back(cm);
            }
        }
        return(true);
    }
    return(false);
}
int TTask::save()
{
    int id; 
    id = dl->TaskIU(this);
    if (id > 0 && task_id == 0)
    {
        dl->TaskStateIU(0, id, dl->TaskStateID(1));
    }
    if (id > 0) task_id = id;
    return(id);
}
bool TTask::remove()
{
    return(false);
}

bool TTask::isValid() const
{
 return(ic_id > 0 && initiator > 0 && executor > 0 && task.length() > 0 && execute_before.isValid()); 
}

int TTask::currentState() const
{
    int res(0);
    if (!lState.empty())
    {
        auto itr = lState.rbegin();
        res = itr->code;
    }
    return(res);
}

void TTask::setState(const int v)
{
  dl->TaskStateIU(0, task_id, dl->TaskStateID(v));
}

void TTask::appendComment(const std::string & msg)
{
  dl->TaskCommentIU(0, task_id, str2html(msg));
}
//---------------------------------------------------------------------------

TICState::TICState(const int p_partner_id, TDataLayer * dl): TBasic(dl), partner_id(partner_id)
{
 clear();
}

void TICState::clear()
{
    TBasic::clear();
    state_id = state = 0;
    dt_state = Wt::WDateTime(); 
}

bool TICState::read()
{
  Wt::WStandardItemModel mt;
  if (ic_id > 0 && dl->TableS("STATE", &mt, ic_id) > 0)
  {
    return(read(&mt));
  }
  return(false);
}

bool TICState::read(Wt::WStandardItemModel * mt)
{
    for(int i = 0; i < mt->rowCount(); i++)
    {
        if (partner_id != to_int(mt->data(i, fieldByName(mt, "partner_id")))) continue;
        if (dt_state == Wt::WDateTime() || dt_state < to_wdatetime(mt->data(i, fieldByName(mt, "dt_state"))))
        {
            state_id = to_int(mt->data(i, fieldByName(mt, "state_id")));
            state = to_int(mt->data(i, fieldByName(mt, "state")));
            dt_state = to_wdatetime(mt->data(i, fieldByName(mt, "dt_state")));
        }
    }
    return(state_id != 0);
}

int TICState::save()
{
    int id;
    id = dl->StateIU(state_id, ic_id, partner_id, state);
    if (id > 0) state_id = id;
    return(id);
}

bool TICState::remove()
{
    return(false);
}

//---------------------------------------------------------------------------
TBasic::TBasic(TDataLayer * p_dl)
{
 dl = p_dl;
 clear();
}

void TBasic::clear()
{
 ic_id = 0; // insurance_case_id;
}

bool TBasic::isValid() const
{
 return (true);
}

TBasicData::TBasicData(TDataLayer * p_dl) : TBasic(p_dl)
{
    clear();
}
void TBasicData::clear()
{
 TBasic::clear();
 settlement = false;
 reimbursement_lmv = false;
 calculation_lmv_possible = false;
 number.clear();
 insurance_type = 0;
 dt_ta = Wt::WDateTime();
 inspection.clear();
 participants = 0; 
 culprits = 0;
 ta_certificate = 0;
 optional_equipment = false;
 inspection_type = 0;
 application = false; 
 incident_type = 0;
 cause_damage = 0;
 ta_location_type = 0;
 franchise = false;
 postcode = addr_field1 = std::string();
 partner_id = firm = beneficiary = 0;
 night = minimal_contact = casualties = transfer = mobile_app = contract_sale = false;
}
bool TBasicData::read()
{
 Wt::WStandardItemModel mt;
 if (ic_id > 0 && dl->BasicDataS(&mt, ic_id) > 0)
 {
  settlement = to_bool(mt.data(0, fieldByName(&mt, "settlement")));
  reimbursement_lmv = to_bool(mt.data(0, fieldByName(&mt, "reimbursement_lmv")));
  calculation_lmv_possible = to_bool(mt.data(0, fieldByName(&mt, "calculation_lmv_possible")));
  number = to_string(mt.data(0, fieldByName(&mt, "number")));
  insurance_type = to_int(mt.data(0, fieldByName(&mt, "insurance_type")));
  dt_ta = to_wdatetime(mt.data(0, fieldByName(&mt, "dt_ta")));
  inspection = to_string(mt.data(0, fieldByName(&mt, "inspection")));
  participants  = to_int(mt.data(0, fieldByName(&mt, "participants")));
  culprits = to_int(mt.data(0, fieldByName(&mt, "culprits")));
  ta_certificate = to_int(mt.data(0, fieldByName(&mt, "ta_certificate")));
  optional_equipment = to_bool(mt.data(0, fieldByName(&mt, "optional_equipment")));
  inspection_type = to_int(mt.data(0, fieldByName(&mt, "inspection_type")));
  application  = to_bool(mt.data(0, fieldByName(&mt, "application")));
  incident_type = to_int(mt.data(0, fieldByName(&mt, "incident_type")));
  cause_damage = to_int(mt.data(0, fieldByName(&mt, "cause_damage")));
  ta_location_type = to_int(mt.data(0, fieldByName(&mt, "ta_location_type")));
  franchise = to_bool(mt.data(0, fieldByName(&mt, "franchise")));
  postcode = to_string(mt.data(0, fieldByName(&mt, "postcode")));
  addr_field1 = to_string(mt.data(0, fieldByName(&mt, "addr_field1")));
  night = to_bool(mt.data(0, fieldByName(&mt, "night")));
  minimal_contact = to_bool(mt.data(0, fieldByName(&mt, "minimal_contact")));
  casualties = to_bool(mt.data(0, fieldByName(&mt, "casualties")));
  transfer = to_bool(mt.data(0, fieldByName(&mt, "transfer")));
  mobile_app = to_bool(mt.data(0, fieldByName(&mt, "mobile_app")));
  contract_sale = to_bool(mt.data(0, fieldByName(&mt, "contract_sale")));
  partner_id = to_int(mt.data(0, fieldByName(&mt, "partner_id")));
  firm = to_int(mt.data(0, fieldByName(&mt, "firm")));
  beneficiary = to_int(mt.data(0, fieldByName(&mt, "beneficiary")));
  return(true);
 }
 return(false);
}
int TBasicData::save()
{
 int id;
// if (number.empty()) {number = Wt::WDateTime::currentDateTime().toString().toUTF8();}
 id = dl->BasicDataIU(this);
 if (id > 0) ic_id = id;
 return(id);
}
bool TBasicData::remove()
{
  if (ic_id > 0)
    return(dl->BasicDataD(ic_id));
  else
    return(true);
}

bool TBasicData::isValid() const
{
    return (true);//(!number.empty() && !dt_ta.isNull() && dt_ta.isValid());
}

TApplicationInspection::TApplicationInspection(TDataLayer * dl) : TBasic(dl)
{
    clear();
}

void TApplicationInspection::clear()
{
    TBasic::clear();
    unlimited_ep = disagreements = exit_ad = false;
    region_circulation = 0;
    dt_application = Wt::WDateTime();
    who_applied = scheme_number = applicants = inspection_type = region_inspection = 0;
    /*subdivision =*/ responsible = inspection_address = std::string();
    dt_inspection = Wt::WDateTime();
    on_move =  flip = frontal = airbags = false;
    rereferral = receiving = Wt::WDate();
    repair_cost = without_wear = agreed_cost = including_wear = 0;
    terms_payment = 0;
    expertise_cost = search_defects = determination = residual_value = loss_value = trace_evidence = independent = photo = false;
}
bool TApplicationInspection::read()
{
    Wt::WStandardItemModel mt;
    if (ic_id > 0 && dl->ApplicationInspectionS(&mt, ic_id) > 0)
    {
        unlimited_ep = to_bool(mt.data(0, fieldByName(&mt, "unlimited_ep")));
        disagreements = to_bool(mt.data(0, fieldByName(&mt, "disagreements")));
        exit_ad = to_bool(mt.data(0, fieldByName(&mt, "exit_ad")));
        region_circulation = to_int(mt.data(0, fieldByName(&mt, "region_circulation")));
        dt_application = to_wdatetime(mt.data(0, fieldByName(&mt, "dt_application")));
        who_applied = to_int(mt.data(0, fieldByName(&mt, "who_applied")));
        scheme_number = to_int(mt.data(0, fieldByName(&mt, "scheme_number")));
        applicants = to_int(mt.data(0, fieldByName(&mt, "applicants")));
        inspection_type = to_int(mt.data(0, fieldByName(&mt, "inspection_type")));
        region_inspection = to_int(mt.data(0, fieldByName(&mt, "region_inspection")));
      //  subdivision = to_string(mt.data(0, fieldByName(&mt, "subdivision")));
        responsible = to_string(mt.data(0, fieldByName(&mt, "responsible")));
        inspection_address = to_string(mt.data(0, fieldByName(&mt, "inspection_address")));
        dt_inspection = to_wdatetime(mt.data(0, fieldByName(&mt, "dt_inspection")));
        on_move = to_bool(mt.data(0, fieldByName(&mt, "on_move")));
        flip = to_bool(mt.data(0, fieldByName(&mt, "flip")));
        frontal = to_bool(mt.data(0, fieldByName(&mt, "frontal")));
        airbags = to_bool(mt.data(0, fieldByName(&mt, "airbags")));
        rereferral = to_wdate(mt.data(0, fieldByName(&mt, "rereferral")));
        receiving = to_wdate(mt.data(0, fieldByName(&mt, "receiving")));
        repair_cost = to_numeric(mt.data(0, fieldByName(&mt, "repair_cost")));
        without_wear = to_numeric(mt.data(0, fieldByName(&mt, "without_wear")));
        agreed_cost = to_numeric(mt.data(0, fieldByName(&mt, "agreed_cost")));
        including_wear = to_numeric(mt.data(0, fieldByName(&mt, "including_wear")));
        terms_payment = to_int(mt.data(0, fieldByName(&mt, "terms_payment")));
        expertise_cost = to_numeric(mt.data(0, fieldByName(&mt, "expertise_cost")));
        search_defects = to_bool(mt.data(0, fieldByName(&mt, "search_defects")));
        determination = to_bool(mt.data(0, fieldByName(&mt, "determination")));
        residual_value = to_bool(mt.data(0, fieldByName(&mt, "residual_value")));
        loss_value = to_bool(mt.data(0, fieldByName(&mt, "loss_value")));
        trace_evidence = to_bool(mt.data(0, fieldByName(&mt, "trace_evidence")));
        independent = to_bool(mt.data(0, fieldByName(&mt, "independent")));
        photo = to_bool(mt.data(0, fieldByName(&mt, "photo")));

        return(true);
    }
    return(false);
}
int TApplicationInspection::save()
{
    int id;
    id = dl->ApplicationInspectionIU(this);
    if (id > 0) ic_id = id;
    return(id);
}
bool TApplicationInspection::remove()
{
    if (ic_id > 0)
        return(dl->ApplicationInspectionD(ic_id));
    else
        return(true);
}

TCulprit::TCulprit(TDataLayer * dl) : TBasic(dl)
{
    clear();
}
void TCulprit::clear()
{
    insurance_company = polis_series = policy_number = std::string();
    date_conclusion = Wt::WDate();
    vehicle_type = brand_model = vin = body_number = registration_number = std::string();
    inspection_culprit = false;
    vehicle_id = driver_id = vehicle_owner = 0;
}
bool TCulprit::read()
{
    Wt::WStandardItemModel mt;
    if (ic_id > 0 && dl->CulpritS(&mt, ic_id) > 0)
    {
        insurance_company = to_string(mt.data(0, fieldByName(&mt, "insurance_company")));
        polis_series = to_string(mt.data(0, fieldByName(&mt, "polis_series")));
        policy_number = to_string(mt.data(0, fieldByName(&mt, "policy_number")));
        date_conclusion = to_wdate(mt.data(0, fieldByName(&mt, "date_conclusion")));
        vehicle_type = to_string(mt.data(0, fieldByName(&mt, "vehicle_type")));
        brand_model = to_string(mt.data(0, fieldByName(&mt, "brand_model")));
        vin = to_string(mt.data(0, fieldByName(&mt, "vin")));
        body_number = to_string(mt.data(0, fieldByName(&mt, "body_number")));
        registration_number = to_string(mt.data(0, fieldByName(&mt, "registration_number")));
        inspection_culprit = to_bool(mt.data(0, fieldByName(&mt, "inspection_culprit")));
        driver_id = to_int(mt.data(0, fieldByName(&mt, "driver_id")));
        vehicle_id = to_int(mt.data(0, fieldByName(&mt, "vehicle_id")));
        vehicle_owner = to_int(mt.data(0, fieldByName(&mt, "vehicle_owner")));
        return(true);
    }
    return(false);
}
int TCulprit::save()
{
    int id;
    id = dl->CulpritIU(this);
    if (id > 0) ic_id = id;
    return(id);
}
bool TCulprit::remove()
{
    if (ic_id > 0)
        return(dl->CulpritD(ic_id));
    else
        return(true);
}

TVictim::TVictim(TDataLayer * dl) : TBasic(dl)
{
    clear();
}
void TVictim::clear()
{
    insurance_company =  policy_series = policy_number = std::string();
    vehicle_id = 0;
    driver_id = vehicle_owner = 0;
}
bool TVictim::read()
{
    Wt::WStandardItemModel mt;
    if (ic_id > 0 && dl->VictimS(&mt, ic_id) > 0)
    {
        insurance_company = to_string(mt.data(0, fieldByName(&mt, "insurance_company")));
        policy_series = to_string(mt.data(0, fieldByName(&mt, "policy_series")));
        policy_number = to_string(mt.data(0, fieldByName(&mt, "policy_number")));
        vehicle_id = to_int(mt.data(0, fieldByName(&mt, "vehicle_id")));
        driver_id = to_int(mt.data(0, fieldByName(&mt, "driver_id")));
        vehicle_owner = to_int(mt.data(0, fieldByName(&mt, "vehicle_owner")));
        return(true);
    }
    return(false);
}
int TVictim::save()
{
    int id;
    id = dl->VictimIU(this);
    if (id > 0) ic_id = id;
    return(id);
}
bool TVictim::remove()
{
    if (ic_id > 0)
        return(dl->VictimD(ic_id));
    else
        return(true);
}
//---------------------------------------------------------------------------

TDamage::TDamage(TDataLayer * dl) : TBasic(dl)
{
    clear();
    dl->DIRLineS(&mtElement, 18);
    dl->DIRLineS(&mtKind, 19);
}
void TDamage::clear()
{
    ic_id = 0;
    mtDamage.clear();
}
bool TDamage::read()
{
    return(dl->DamageS(&mtDamage, ic_id) > 0);
}
int TDamage::save()
{
    for(int i = 0; i < mtDamage.rowCount(); i++)
    {
        int damage_id = to_int(mtDamage.data(i, fieldByName(&mtDamage, "damage_id")));
        int element   = to_int(mtDamage.data(i, fieldByName(&mtDamage, "element")));
        int kind      = to_int(mtDamage.data(i, fieldByName(&mtDamage, "kind")));
        int res       = dl->DamageIU(damage_id, ic_id, element, kind);
        if (res > 0)
        {mtDamage.setData(i, fieldByName(&mtDamage, "damage_id"), res);}
    }
    return(0);
}
bool TDamage::remove()
{ 
    mtDamage.clear();
    dl->DamageS(&mtDamage, ic_id);
    for(int i = 0; i < mtDamage.rowCount(); i++)
    {
        int damage_id = to_int(mtDamage.data(i, fieldByName(&mtDamage, "damage_id")));
        if (damage_id > 0)
        {dl->TableD("damage", damage_id);}
    }
    return(false);
}
int TDamage::getElementIdx(const int element_id)
{
    int element(fieldByName(&mtDamage, "element"));

    for(int i = 0; i < mtDamage.rowCount(); i++)
    {
        if (to_int(mtDamage.data(i, element)) == element_id)
        {
            return(i);
        }
    }

    return(-1);
}
void TDamage::setKind(const int element_id, const int kind_id)
{
    // mtDamage, mtElement, mtKind
    int idx = getElementIdx(element_id);
    if (idx < 0)
    {
        idx = mtDamage.rowCount();
        mtDamage.insertRow(idx, 0);
        mtDamage.setData(idx, fieldByName(&mtDamage, "element"), element_id);
    }
    mtDamage.setData(idx, fieldByName(&mtDamage, "kind"), kind_id);
}

int TDamage::getKind(const int element_id)
{
    int idx = getElementIdx(element_id);
    if (idx < 0)
    {
     return(0);
    }
    return(to_int(mtDamage.data(idx, fieldByName(&mtDamage, "kind"))));
}
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------

TEntity::TEntity(TDataLayer * dl): TBasic(dl)
{
    clear();
}
void TEntity::clear()
{
    ic_id = 0;
    name = itn = std::string();
}
bool TEntity::read()
{
    Wt::WStandardItemModel mt;
    if (ic_id > 0 && dl->TableS("ENTITY", &mt, ic_id) > 0)
    {
        ic_id = to_int(mt.data(0, fieldByName(&mt, "ic_id")));
        name = to_string(mt.data(0, fieldByName(&mt, "name")));
        itn = to_string(mt.data(0, fieldByName(&mt, "itn")));

        return(true);
    }
    return(false);
}
int TEntity::save()
{
    int id;
    id = dl->EntityIU(ic_id, name, itn);
    if (id > 0) ic_id = id;
    return(id);
}
bool TEntity::remove()
{
    if (ic_id > 0)
        return(dl->TableD("ENTITY", ic_id));
    else
        return(true);
}

//---------------------------------------------------------------------------
//---------------------------------------------------------------------------

TPaymentDetails::TPaymentDetails(TDataLayer * dl): TBasic(dl)
{
    clear();
}
void TPaymentDetails::clear()
{
    TBasic::clear();
    checking_account = bank = department = correspondent_account = bic = itn = card = recipient = std::string();
}
bool TPaymentDetails::read()
{
    Wt::WStandardItemModel mt;
    if (ic_id > 0 && dl->TableS("PAYMENT_DETAILS", &mt, ic_id) > 0)
    {
        ic_id = to_int(mt.data(0, fieldByName(&mt, "ic_id")));
        checking_account = to_string(mt.data(0, fieldByName(&mt, "checking_account")));
        bank = to_string(mt.data(0, fieldByName(&mt, "bank")));
        department = to_string(mt.data(0, fieldByName(&mt, "department")));
        correspondent_account = to_string(mt.data(0, fieldByName(&mt, "correspondent_account")));
        bic = to_string(mt.data(0, fieldByName(&mt, "bic")));
        itn = to_string(mt.data(0, fieldByName(&mt, "itn")));
        card = to_string(mt.data(0, fieldByName(&mt, "card")));
        recipient = to_string(mt.data(0, fieldByName(&mt, "recipient")));
        return(true);
    }
    return(false);
}
int TPaymentDetails::save()
{
    int id;
    id = dl->PaymentDetailsIU(ic_id, checking_account, bank, department, correspondent_account, bic, itn, card, recipient);
    if (id > 0) ic_id = id;
    return(id);
}
bool TPaymentDetails::remove()
{
    if (ic_id > 0)
        return(dl->TableD("Payment_Details", ic_id));
    else
        return(true);
}

//---------------------------------------------------------------------------
//---------------------------------------------------------------------------


TNode::TNode(TDataLayer * p_dl)
{
 extern TParameterList params;

    dl = p_dl;
    clear();

    if(! Wt::WApplication::instance()->readConfigurationProperty("files", fdb))
        fdb = params.get("approot")->v_str;
#ifdef _MSC_VER
    delimiter = "\\";
#else
    delimiter = "/";
#endif
}
void TNode::clear()
{
    node_id = 0;
    p3 = p2 = p1 = name_server = std::string();
    node_size = 0;
    crc = std::string();
}
bool TNode::read()
{
    Wt::WStandardItemModel mt;
    if (node_id > 0 && dl->TableS("fdb_node", &mt, node_id) > 0)
    {
        p3 = to_string(mt.data(0, fieldByName(&mt, "p3")));
        p2 = to_string(mt.data(0, fieldByName(&mt, "p2")));
        p1 = to_string(mt.data(0, fieldByName(&mt, "p1")));
        name_server = to_string(mt.data(0, fieldByName(&mt, "name_server")));
        node_size = to_int64(mt.data(0, fieldByName(&mt, "node_size")));
        crc  = to_string(mt.data(0, fieldByName(&mt, "crc")));
        return(true);
    }
    return(false);
}
int TNode::save()
{
    int id;
    id = dl->NodeIU(this);
    if (id > 0) node_id = id;
    return(id);
}

int TNode::save(const std::string& spoolFile)
{
    std::stringstream stm;

    node_id = dl->GetNext("gen_fdb_node_id");

    stm.width(8);
    stm.fill('0');
    stm << node_id;

    name_server = stm.str();
    p3 = "";
    p2 = name_server.substr(0, 2);
    p1 = name_server.substr(2, 3);

    bool r = copy(spoolFile, fullPath() + delimiter + name_server);

    if (r) return(save());
    else return(false);
}

bool TNode::copy(const std::string& src, const std::string& dst)
{
 boost::uuids::detail::sha1 sha1;
 std::ifstream fin;  /* исходный файл */
 std::ofstream fout; /* целевой файл  */
 bool ret(false);

 const int BLOCK_SIZE = 64 * 1024;
 char sbuff[BLOCK_SIZE];

 if(boost::filesystem::exists(dst)) return(false); // не перетираем
 sha1.reset();

 fin.open(src.c_str(), std::ios::binary);
 if (fin.fail())
 {return(false);}

 fout.open(dst.c_str(), std::ios::binary);
 if (fout.fail())
 { fin.close(); return(false);}

 node_size = 0;
 while(!fin.eof())
 {
     fin.read(sbuff, BLOCK_SIZE);
     fout.write(sbuff, fin.gcount());
     if (fout.fail())
     {
        ret = false; break;
     }
     node_size += fin.gcount();
     sha1.process_bytes(sbuff, fin.gcount());
     ret = true;
 } // while

 fin.close();
 fout.close();

 if (ret)
 {
         std::stringstream stm;
         unsigned hash[5] = {0};
         sha1.get_digest(hash);
         for(int i = 0; i < 5; i++)
         {
             stm << std::right << std::hex << std::noshowbase << std::setw(8) << std::setfill('0') << hash[i];
         }
         crc = stm.str();      
 }
 return(ret);
}

std::string TNode::fullPath() const
{
    std::stringstream stm;
    if (!fdb.empty()) {stm << fdb; if (!boost::filesystem::exists(stm.str())) boost::filesystem::create_directory(stm.str());}
    if (!p3.empty()) {stm << delimiter << p3; if (!boost::filesystem::exists(stm.str())) boost::filesystem::create_directory(stm.str());}
    if (!p2.empty()) {stm << delimiter << p2; if (!boost::filesystem::exists(stm.str())) boost::filesystem::create_directory(stm.str());}
    if (!p1.empty()) {stm << delimiter << p1; if (!boost::filesystem::exists(stm.str())) boost::filesystem::create_directory(stm.str());}

return(stm.str());
}

std::string TNode::fullName() const
{
    std::stringstream stm;
    stm << fullPath();
    stm << delimiter << name_server;
    return(stm.str());
}
//---------------------------------------------------------------------------

TFile::TFile(TDataLayer * p_dl)
{
    dl = p_dl;
    clear();
}

void TFile::clear()
{
    file_id = 0;
    name_user = std::string();
    file_size = 0;
    node_id = preview = 0;
}

bool TFile::read()
{
    Wt::WStandardItemModel mt;
    if (file_id > 0 && dl->TableS("fdb_file", &mt, file_id) > 0)
    {
        name_user = to_string(mt.data(0, fieldByName(&mt, "name_user")));
        file_size = to_int64(mt.data(0, fieldByName(&mt, "file_size")));
        node_id  = to_int(mt.data(0, fieldByName(&mt, "node_id")));
        preview  = to_int(mt.data(0, fieldByName(&mt, "preview")));
        return(true);
    }
    return(false);
}

int TFile::save()
{
    int id;
    id = dl->FileIU(this);
    if (id > 0) file_id = id;
    return(id);
}

int64_t TFile::fileSize(const std::string & file)
{
    int64_t ret(0);
    std::ifstream f;
    f.open(file.c_str(), ios::binary);
    if (f.fail())
    {f.close(); return(-1);}

    f.seekg(0, std::ios::end);        // ╨Т linux ╨╜╨╡ ╤А╨░╨▒╨╛╤В╨░╨╡╤В
    ret = f.tellg();
    f.seekg(0, std::ios::beg);
    f.close();
    return(ret);
}

//---------------------------------------------------------------------------
class TDataLayerImpl : public TDataLayer
{
 private:
   IBPP::Database    db;
   IBPP::Transaction tr;
   IBPP::Statement   st;
//        TStatementEx st;
   TIBPPQuery      * q;
//   pthread_mutex_t   mutex;

   std::string dbhost, /*dbport,*/ dbname, dbrole, dbuser, dbpassword;
   int dbport;

 public:
//   std::string errorstr;
//   int SQLException_SqlCode;
//   int SQLException_EngineCode;

 TDataLayerImpl();
 ~TDataLayerImpl();

 void setHostName(const std::string& s)    {dbhost = s;};
 void setPort(const int p)                {dbport = p;};
 void setDatabaseName(const std::string& s){dbname = s;};
 void setUserName(const std::string& s)    {dbuser = s;};
 void setRole(const std::string& s)        {dbrole = s;};
 void setPassword(const std::string& s)    {dbpassword = s;};

 const std::string userName() const {return(dbuser);}
 const std::string hostName() const {return(dbhost);}
// const std::string port() const {std::stringstream stm("");stm<<std::dec<<p; return(stm.str().c_str());}
 const int port() const {return (dbport);}
 const std::string role() const {return(dbrole);}
 const std::string dbName() const {return(dbname);}
 const std::string password() const {return(dbpassword);}

 bool open();
 void close();

 bool isOpen();

   bool GlbVariablesS(const std::string& vname, int &vint, std::string &vstr, float &vnum);
   int DIRLineIU(const int line_id, const int directory_id, const int code, const std::string& name, const std::string& comment = std::string());
   int DIRLineS(Wt::WAbstractItemModel * mt, const int directory_id = 0);
   int DIRLineOne(Wt::WAbstractItemModel * mt, const int line_id = 0);
   std::string DIRLineName(const int line_id);
   int DIRLineD(const int line_id);
   int SysProcParams(Wt::WAbstractItemModel * mt, const std::string& sql_proc, const int in_param = 0);
   int RdbProcedureS(Wt::WAbstractItemModel * mt, const std::string& sql_proc);
   int ExecuteProcedure(Wt::WAbstractItemModel * mt, const std::string& sql_proc,
                              const Wt::Http::ParameterMap & parameter);
   int ExecuteSQL(Wt::WAbstractItemModel * mt, const std::string& sql,
                              const Wt::Http::ParameterMap & parameter);
   int SecDescriptorUsersIU(const int sid, const int user_id, const int rights, const int meta_rights);
   int SecDescriptorGroupsIU(const int sid, const int group_id, const int rights, const int meta_rights);
   int Begin();
   int Commit();
   int Rollback();
   int TransactionStatus();

   int SYSGetUser();
   int SYSCurrentUserFirm();

   int RequisitionS(Wt::WAbstractItemModel * mt, const int requsition_id, const int city);
   int RequisitionIU(const int requisition_id, const Wt::WDate& date_ta, const std::string & phone_number, const std::string& customer, const int carlifter_plan_i);
   bool RequisitionD(const int requisition_id);
   int CarlifterS(Wt::WAbstractItemModel * mt, const int city);
   int CarlifterIU(const int carlifter_id, const int city, const int number, const std::string& description);
   int CarlifterPlanS(Wt::WAbstractItemModel * mt, const int city, const Wt::WDateTime& start, const Wt::WDateTime& finish, const int carlifter_plan_id = 0);
   int CarlifterPlanSFree(Wt::WAbstractItemModel * mt, const int city, const Wt::WDateTime& start, const Wt::WDateTime& finish, const int carlifter, const int carlifter_plan_id);
   int CarlifterPlanIU(const int carlifter_plan_id, const int carlifter_id, const Wt::WDateTime& start, const Wt::WDateTime& finish);
   bool CarlifterPlanD(const int carlifter_plan_id);
//   void test(void);

   int BasicDataS(Wt::WAbstractItemModel * mt, const int ic_id, const int status0 = 0, const int status1 = 0, const Wt::WDate deApplication = Wt::WDate());
   int BasicDataIU(const TBasicData * bd);
   bool BasicDataD(const int ic_id);

   int ApplicationInspectionS(Wt::WAbstractItemModel * mt, const int ic_id);
   int ApplicationInspectionIU(const TApplicationInspection * bd);
   bool ApplicationInspectionD(const int ic_id);

   int CulpritS(Wt::WAbstractItemModel * mt, const int ic_id);
   int CulpritIU(const TCulprit * bd);
   bool CulpritD(const int ic_id);

   int VictimS(Wt::WAbstractItemModel * mt, const int ic_id);
   int VictimIU(const TVictim * bd);
   bool VictimD(const int ic_id);

   int DriverS(Wt::WAbstractItemModel * mt, const int driver_id);
   int DriverIU(const TDriver * bd);
   bool DriverD(const int driver_id);

   int TableS(const std::string& table, Wt::WAbstractItemModel * mt, const int id = 0);
   bool TableD(const std::string& table, const int id);

   int NodeIU(const TNode * bd);
   int FileIU(const TFile * bd);

   int FileDocS(Wt::WAbstractItemModel * mt, const int type, const int id);
   int FileDocIU(const int file_doc_id, const int file_id, const int type, const int id);

   int GetNext(const std::string& generator);

   int VehicleS(Wt::WAbstractItemModel * mt, const int vehicle_id);
   int VehicleIU(const TVehicle * bd);

   int DamageS(Wt::WAbstractItemModel * mt, const int ic_id);
   int DamageIU(const int damage_id, const int ic_id, const int element, const int kind);

   int PartnerIU(const int partner_id, const std::string& name);

   int UsersS(Wt::WAbstractItemModel * mt, const int user_id, const int partner_id = 0);
   int UsersIU(const TUser * db);

   int TaskS(Wt::WAbstractItemModel * mt, const int ic_id = 0, const int executor = 0, const int state = 0, const int type = 0);
   int TaskIU(const TTask * db);

   int TaskStateID(const int code);
   int TaskStateIU(const int task_state_id, const int task_id, const int state);
   int TaskStateS(Wt::WAbstractItemModel * mt, const int task_id);

   int TaskCommentIU(const int task_comment_id, const int task_id, const std::string & comment);

   int StateIU(const int state_id, const int ic_id, const int partner_id, const int state);
 
   int EntityIU(const int ic_id, const std::string& name, const std::string& itn);

   int PaymentDetailsIU(const int ic_id, const std::string& checking_account, const std::string& bank, const std::string& department, 
       const std::string& correspondent_account, const std::string& bic, const std::string& itn, const std::string& card, const std::string& recipient);

   int GrandRole(const std::string &role, const std::string &user);

   int CarS(Wt::WAbstractItemModel * mt, const int line_id);

   private:
     std::map<std::string, std::string> cookie; // key, value
     std::string cookieKey(const std::string& key);
//     void setParam(const std::string name, const std::string value, const int type);

     boost::mutex mtxProc;
     std::map<std::string, TProcedure *> proc; // proc_name, TProcedure
     Wt::WDateTime dtProc;
     void clearProc();
     void checkProc();
     void refreshProc();
     TProcedure * getProc(const std::string& proc_name);

     int current_user;

   public:
     std::string getCookie(const std::string& key, const std::string& def=std::string());
     void setCookie(const std::string& key, const std::string& value);
     void saveColumnWidth(const Wt::WAbstractItemView * view, const std::string& pref);
     void setColumnWidth(Wt::WAbstractItemView * view, const std::string& pref);

  std::string cp2koi(const std::string& src, int direction = 1) const {return src;};

  std::map <int, int> mTaskState; // code, line_id
};

//---------------------------------------------------------------------------
// ---------------------  TDataLayer ----------------------------------------
//--------------------------------------------------------------------------
TDataLayerImpl::TDataLayerImpl() : TDataLayer(), //db_is_created(false), //dberror("")
dbhost(""), /*dbport(""),*/ dbname(""), dbrole(""), dbuser(""), dbpassword(""), dbport(0)//,
//errorstr("")
,current_user(-1)
{
 q = NULL;
 cookie.clear();
}
//--------------------------------------------------------------------------
TDataLayerImpl::~TDataLayerImpl()
{
// if (q != NULL)
 {
 // delete(q);
  close();
 }
 clearProc();
}
//---------------------------------------------------------------------------
bool TDataLayerImpl::open()
{
 current_user = -1;
 close();
 try
 {
   std::string hostport (dbhost);
   if (!hostport.empty() && dbport != 0) hostport += ("/" + std::to_string((long long)dbport));

   db = IBPP::DatabaseFactory(hostport.c_str(),
                              dbname.c_str(),
                              dbuser.c_str(),
                              dbpassword.c_str(),
                              dbrole.c_str(),
                              "UTF8", "");
                             // "WIN1251", "");
//   db_is_created = true;
   db->Connect();
//   if ((db)->Connected())
//   {
//    log("Connect successfull !");
//    ret = 1;
//   }
 }
 catch(IBPP::Exception& e)
 {
  errorstr = e.what();
  Wt::log("notice") << "TDataLayerImpl::open " << e.what();
  return(false);
 }

 catch(...)
 {
  errorstr = "Unknown error";
  Wt::log("notice") << "TDataLayerImpl::open " << errorstr;
  return(false);
 }

 if (/*db_is_created &&*/ db->Connected())
 {
  try
  {
   q = new TIBPPQuery(db, "cp1251", "utf8");
  }
  catch(...)
  {errorstr = "No memory ibppquery.";}

 }

 return(/*db_is_created &&*/ db->Connected());
}
//--------------------------------------------------------------------------
void TDataLayerImpl::close()
{
//logtxt(__PRETTY_FUNCTION__);
// if (db_is_created)
// {
//  try{chtr->Commit();}
//  catch(...) {/*return;*/}
//  try{db->Inactivate();} catch(...){}
  if (db.intf() != 0)
    if (db->Connected()){ try{db->Disconnect();} catch(...){}}
  if (q != NULL) {delete(q); q = NULL;}
//  db_is_created = false;
// }
}
//--------------------------------------------------------------------------
bool TDataLayerImpl::isOpen()
{
 if (db.intf() == 0) return(false);
 return(db->Connected());
}
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
//--------------------------------------------------------------------------
//--------------------------------------------------------------------------
bool TDataLayerImpl::GlbVariablesS(const std::string& vname, int &vint, std::string &vstr, float &vnum)
{
 std::string sql;

 sql = "select INTS, STR, NUM from GLB_VARIABLES_S(:VAR_NAME) ";

 //pthread_mutex_lock(&mutex);
 q->SetNewQuery(sql, IBPP::amRead);
 q->SetParam("VAR_NAME", vname);

 if (q->exec(st))
 {
  try
  {
   if(st->Fetch())
   {
    if (st->Get("INTS", vint)) vint = 0;
    if (st->Get("STR", vstr)) vstr.clear();
    if (st->Get("NUM", vnum)) vnum = 0;
   }
  } // try
  catch (IBPP::Exception& e)
  {
   errorstr = cp2koi(e.what(), 1);
   logtxt("Error: TDataLayerImpl::GlbVariablesS - " + errorstr);
   //pthread_mutex_unlock(&mutex);
   return (false);
  }

  q->commit();
 }
 else
 {
  SQLException_SqlCode     = q->SQLException_SqlCode;
  SQLException_EngineCode  = q->SQLException_EngineCode;
  errorstr                 = q->errorstr;
  logtxt("Error: TDataLayerImpl::GlbVariablesS: " + errorstr);
  return (false);
 }

 //pthread_mutex_unlock(&mutex);
 return (true);
}
//--------------------------------------------------------------------------
//--------------------------------------------------------------------------
int TDataLayerImpl::DIRLineIU(const int line_id, const int directory_id, const int code, const std::string& name,
              const std::string& comment)
{
 int ret(0);
 std::string sql;

 sql = "select id from DIR_LINE_IU(:line_id, :directory_id, :code, :name, :comment) ";

 //pthread_mutex_lock(&mutex);
 q->SetNewQuery(sql, IBPP::amWrite);
 q->SetParamOrNull("line_id", line_id);
 q->SetParam("directory_id",directory_id);
 q->SetParam("code",    code);
 q->SetParam("name",    name);
 q->SetParam("comment", comment);

 if (q->exec(st))
 {
  try
  {
   while(st->Fetch())
   {
     st->Get("id", ret);
   }
  } // try
  catch (IBPP::Exception& e)
  {
   errorstr = cp2koi(e.what(), 1);
   ret      = -1;
   logtxt("Error: TDataLayerImpl::DIRLineIU - " + errorstr);
   //pthread_mutex_unlock(&mutex);
   return (ret);
  }

  q->commit();
 }
 else
 {
  ret                      = -1;
  SQLException_SqlCode     = q->SQLException_SqlCode;
  SQLException_EngineCode  = q->SQLException_EngineCode;
  errorstr                 = q->errorstr;
  logtxt("Error: TDataLayerImpl::DIRLineIU: " + errorstr);
 }

 //pthread_mutex_unlock(&mutex);
 return (ret);
}
//--------------------------------------------------------------------------
int TDataLayerImpl::DIRLineS(Wt::WAbstractItemModel * mt, const int directory_id)
{
 int ret (-1);
 std::string sql;

 errorstr.clear();

 sql = "select t.line_id, t.directory_id, t.code, t.name, t.comment, t.code || ' ' || t.name as codename ";
 sql +="from DIR_LINE_S(:directory_id) t order by t.code";

 //pthread_mutex_lock(&mutex);

 q->SetNewQuery(sql, IBPP::amRead);
 q->SetParam("directory_id", directory_id);

 if (!q->exec())
 {
  ret      = -1;
  errorstr = q->errorstr;
  logtxt("Error: " + errorstr);
 }
 else
 {
  ret = q->st_to_mt(mt);
  if (q->errorstr.length() > 0)
  {
   errorstr = q->errorstr;
   logtxt("Error: " + errorstr);
   ret = -1;
  }
  q->commit();
 } // else

 //pthread_mutex_unlock(&mutex);

 return (ret);
}

int TDataLayerImpl::DIRLineOne(Wt::WAbstractItemModel * mt, const int line_id)
{
    int ret (-1);
    std::string sql;

    errorstr.clear();

    sql = "select t.line_id, t.directory_id, t.code, t.name, t.comment, t.code || ' ' || t.name as codename ";
    sql +="from DIR_LINE_ONE(:line_id) t order by t.code";

    //pthread_mutex_lock(&mutex);

    q->SetNewQuery(sql, IBPP::amRead);
    q->SetParamOrNull("line_id", line_id);

    if (!q->exec())
    {
        ret      = -1;
        errorstr = q->errorstr;
        logtxt("Error: " + errorstr);
    }
    else
    {
        ret = q->st_to_mt(mt);
        if (q->errorstr.length() > 0)
        {
            errorstr = q->errorstr;
            logtxt("Error: " + errorstr);
            ret = -1;
        }
        q->commit();
    } // else

      //pthread_mutex_unlock(&mutex);

    return (ret);
}

std::string TDataLayerImpl::DIRLineName(const int line_id)
{
    Wt::WStandardItemModel mt;
    std::string ret;
    if(line_id > 0 && DIRLineOne(&mt, line_id) > 0)
    {
        ret = to_string(mt.data(0, fieldByName(&mt, "name")));
    }
    return(ret);
}
//--------------------------------------------------------------------------
int TDataLayerImpl::DIRLineD(const int line_id)
{
    bool ret(true);
    std::string sql;

    sql = "execute procedure DIR_LINE_D(:line_id) ";

    //pthread_mutex_lock(&mutex);
    q->SetNewQuery(sql, IBPP::amWrite);
    q->SetParam("line_id", line_id);

    if (q->exec())
    {
        q->commit();
    }
    else
    {
        ret                      = false;
        SQLException_SqlCode     = q->SQLException_SqlCode;
        SQLException_EngineCode  = q->SQLException_EngineCode;
        errorstr                 = q->errorstr;
        logtxt("Error: TDataLayerImpl::DIRLineD: " + errorstr);
    }

    //pthread_mutex_unlock(&mutex);
    return (ret);
}
//--------------------------------------------------------------------------
//--------------------------------------------------------------------------
int TDataLayerImpl::SysProcParams(Wt::WAbstractItemModel * mt, const std::string& sql_proc, const int in_param)
{
 int ret(-1);
 std::string sql;

 errorstr.clear();

 sql = "select * from SYS$PROC_PARAMS (:PROC_NAME) WHERE IN_PARAM = :IN_PARAM ";

 //pthread_mutex_lock(&mutex);

 q->SetNewQuery(sql, IBPP::amRead);
 q->SetParam("PROC_NAME", sql_proc);
 q->SetParam("IN_PARAM", in_param);

 if (!q->exec())
 {
  ret      = -1;
  errorstr = q->errorstr;
  logtxt("Error: " + errorstr);
 }
 else
 {
  ret = q->st_to_mt(mt);
  if (q->errorstr.length() > 0)
  {
   errorstr = q->errorstr;
   logtxt("Error: " + errorstr);
   ret = -1;
  }
  q->commit();
 } // else

 //pthread_mutex_unlock(&mutex);

 return (ret);
}
//--------------------------------------------------------------------------
int TDataLayerImpl::RdbProcedureS(Wt::WAbstractItemModel * mt, const std::string& sql_proc)
{
 int ret(-1);
 std::string sql, sql2;

 errorstr.clear();

 sql = "select * from RDB$PROCEDURES";
 sql2 = " RDB$PROCEDURE_NAME = :PROC_NAME ";

 if (!sql_proc.empty()) sql += " where " + sql2;

 //pthread_mutex_lock(&mutex);

 q->SetNewQuery(sql, IBPP::amRead);
 if (!sql_proc.empty()) q->SetParam("PROC_NAME", Upper(sql_proc));

 if (!q->exec())
 {
  ret      = -1;
  errorstr = q->errorstr;
  logtxt("Error: " + errorstr);
 }
 else
 {
  ret = q->st_to_mt(mt);
  if (q->errorstr.length() > 0)
  {
   errorstr = q->errorstr;
   logtxt("Error: " + errorstr);
   ret = -1;
  }
  q->commit();
 } // else

 //pthread_mutex_unlock(&mutex);

 return (ret);
}
//--------------------------------------------------------------------------
//--------------------------------------------------------------------------
int TDataLayerImpl::ExecuteProcedure(Wt::WAbstractItemModel * mt, const std::string& sql_proc,
                              const Wt::Http::ParameterMap & param)
{
 int ret(-1);
 std::string sql("select * from ");
 int proc_type(1); // Procedure type: 1 - selectable stored procedure (contains a SUSPEND statement) 2 - executable stored procedure

  TProcedure * pr = getProc(sql_proc);
  if (pr != NULL) proc_type = pr->type;
  else
  {
   errorstr = "Unknown procedure " + sql_proc;
   return (-3);
  }

 if (proc_type == 2) sql = "execute procedure ";

 sql += sql_proc;

 if (!pr->in_param.empty())
 {
  sql += "(";
  for(auto itr = pr->in_param.begin(); itr != pr->in_param.end(); ++itr)
  {
   if (itr->empty()) continue;

   if (param.find(std::string("_") + *itr) != param.end() && !param.find(std::string("_") + *itr)->second.empty())
   {
    sql += " :" + *itr + ",";
   }
   else
    sql += " NULL/*"  + *itr +  "*/,";
  }
  sql.erase(sql.length() - 1, 1);
  sql += ")";
 }

 q->SetNewQuery(sql, IBPP::amWrite);
 for(auto itr = pr->in_param.begin(); itr != pr->in_param.end(); ++itr)
 {
  std::string v;// int t;
  bool y(false);
  if (itr->empty()) continue;

  if (param.find(std::string("_") + *itr) != param.end() && !param.find(std::string("_") + *itr)->second.empty())
  {
    v = Wt::Utils::urlDecode(param.find(std::string("_") + *itr)->second[0]);
    q->SetParamAsString(*itr, v);
  }
//  else
//    q->SetParamNull(*itr);
 }

 if (!q->exec())
 {
  ret      = -1;
  errorstr = q->errorstr;
  logtxt("Error: " + errorstr);
 }
 else
 {
  if (!pr->out_param.empty())
    ret = q->st_to_mt(mt);
  else
    ret = 0;
  if (q->errorstr.length() > 0)
  {
   errorstr = q->errorstr;
   logtxt("Error: " + errorstr);
   ret = -1;
  }
  q->commit();
 } // else

 return(ret);
}
//--------------------------------------------------------------------------
int TDataLayerImpl::ExecuteSQL(Wt::WAbstractItemModel * mt, const std::string& sql,
                              const Wt::Http::ParameterMap & param)
{
 int ret(-1);
// std::string sql("select * from ");
// Wt::WStandardItemModel mtParam;

// SysProcParams(&mtParam, sql_proc, 0);

// sql += sql_proc;
// if (mtParam.rowCount() > 0)
// {
//  std::string f, v;
//  int t(fieldByName(&mtParam, "PARAM_NAME"));
//  sql += "(";
//  for(int i = 0; i < mtParam.rowCount(); i++)
//  {
//   f = to_string(mtParam.data(i, t));
//   boost::trim(f);
//   if (f.empty()) continue;
//   sql += " :" + f + ",";
//  }
//  sql.erase(sql.length() - 1, 1);
//  sql += ")";
// }

 q->SetNewQuery(sql, IBPP::amWrite);
 for(auto itr = param.begin(); itr != param.end(); ++itr)
 {
  std::string f, v;
  f = Wt::Utils::urlDecode(itr->first);
  v = Wt::Utils::urlDecode(itr->second[0]);
  f.erase(0, 1);
  q->SetParamAsString(f, v);
 }

 if (!q->exec())
 {
  ret      = -1;
  errorstr = q->errorstr;
  logtxt("Error: " + errorstr);
 }
 else
 {
  ret = q->st_to_mt(mt);
  if (q->errorstr.length() > 0)
  {
   errorstr = q->errorstr;
   logtxt("Error: " + errorstr);
   ret = -1;
  }
  q->commit();
 } // else

 return(ret);
}
//--------------------------------------------------------------------------
int TDataLayerImpl::SecDescriptorUsersIU(const int sid, const int user_id, const int rights, const int meta_rights)
{
 int ret;
 std::string sql;

 sql = "execute procedure SEC_DESCRIPTOR_USERS_IU(:sid, :user_id, :rights, :meta_rights) ";

 //pthread_mutex_lock(&mutex);
 q->SetNewQuery(sql, IBPP::amWrite);
 q->SetParam("sid", sid);
 q->SetParam("user_id", user_id);
 q->SetParam("rights", rights);
 q->SetParam("meta_rights", meta_rights);

 if (!q->exec())
 {
  ret      = -1;
  errorstr = q->errorstr;
  logtxt("Error: " + errorstr);
 }
 else
 {
  ret = 1;
  q->commit();
 } // else

 //pthread_mutex_unlock(&mutex);

 return (ret);
}
//--------------------------------------------------------------------------
int TDataLayerImpl::SecDescriptorGroupsIU(const int sid, const int group_id, const int rights, const int meta_rights)
{
 int ret;
 std::string sql;

 sql = "execute procedure SEC_DESCRIPTOR_GROUPS_IU(:sid, :group_id, :rights, :meta_rights) ";

 //pthread_mutex_lock(&mutex);
 q->SetNewQuery(sql, IBPP::amWrite);
 q->SetParam("sid", sid);
 q->SetParam("group_id", group_id);
 q->SetParam("rights", rights);
 q->SetParam("meta_rights", meta_rights);

 if (!q->exec())
 {
  ret      = -1;
  errorstr = q->errorstr;
  logtxt("Error: " + errorstr);
 }
 else
 {
  ret = 1;
  q->commit();
 } // else

 //pthread_mutex_unlock(&mutex);

 return (ret);
}
//--------------------------------------------------------------------------
int TDataLayerImpl::Begin()
{
 int ret(q->begin(IBPP::amWrite));
 if (ret < 0) errorstr = q->errorstr;
 else errorstr.clear();
 return(ret);
}
int TDataLayerImpl::Commit()
{
 int ret(q->commit());
 if (ret < 0) errorstr = q->errorstr;
 else errorstr.clear();
 return(ret);
}
int TDataLayerImpl::Rollback()
{
 errorstr = "Rollback is not implement.";
 return(-1);
}
int TDataLayerImpl::TransactionStatus()
{
 errorstr.clear();
 return(q->transactionStatus());
}
//--------------------------------------------------------------------------
int TDataLayerImpl::SYSGetUser()
{
 int ret(-1);
 std::string sql;

 if (current_user > 0) return (current_user);

 sql = "select USER_ID from SYS_GET_USER ";

 //pthread_mutex_lock(&mutex);
 q->SetNewQuery(sql, IBPP::amRead);

 if (q->exec(st))
 {
  try
  {
   while(st->Fetch())
   {
     st->Get("USER_ID", ret);
   }
  } // try
  catch (IBPP::Exception& e)
  {
   errorstr = cp2koi(e.what(), 1);
   ret      = -1;
   logtxt("Error: TDataLayerImpl::SYSGetUser - " + errorstr);
   //pthread_mutex_unlock(&mutex);
   return (ret);
  }

  q->commit();
 }
 else
 {
  ret                      = -1;
  SQLException_SqlCode     = q->SQLException_SqlCode;
  SQLException_EngineCode  = q->SQLException_EngineCode;
  errorstr                 = q->errorstr;
  logtxt("Error: TDataLayerImpl::SYSGetUser: " + errorstr);
 }

 //pthread_mutex_unlock(&mutex);
 current_user = ret;
 return (ret);
}
//--------------------------------------------------------------------------
int TDataLayerImpl::SYSCurrentUserFirm()
{
 int ret(-1);
 std::string sql;

 sql = "select e.firm_id ";
 sql+= "from SYS_GET_USER u ";
 sql+= "left join SYS_USERS_S(NULL, 0) su on (su.user_id = u.user_id) ";
 sql+= "left join employees_s(NULL, NULL, '', NULL, NULL) e on (e.id = su.employee_id) ";

 //pthread_mutex_lock(&mutex);
 q->SetNewQuery(sql, IBPP::amRead);

 if (q->exec(st))
 {
  try
  {
   while(st->Fetch())
   {
     st->Get("firm_id", ret);
   }
  } // try
  catch (IBPP::Exception& e)
  {
   errorstr = cp2koi(e.what(), 1);
   ret      = -1;
   logtxt("Error: TDataLayerImpl::SYSCurrentUserFirm - " + errorstr);
   //pthread_mutex_unlock(&mutex);
   return (ret);
  }

  q->commit();
 }
 else
 {
  ret                      = -1;
  SQLException_SqlCode     = q->SQLException_SqlCode;
  SQLException_EngineCode  = q->SQLException_EngineCode;
  errorstr                 = q->errorstr;
  logtxt("Error: TDataLayerImpl::SYSCurrentUserFirm: " + errorstr);
 }

 //pthread_mutex_unlock(&mutex);
 return (ret);
}
//--------------------------------------------------------------------------
int TDataLayerImpl::RequisitionS(Wt::WAbstractItemModel * mt, const int requsition_id, const int city)
{
    int ret (-1);
    std::string sql;

    errorstr.clear();

    sql = "select r.* from REQUISITION_S(:requsition_id, :city) r ";

    //pthread_mutex_lock(&mutex);

    q->SetNewQuery(sql, IBPP::amRead);
    if (requsition_id > 0) q->SetParam("requsition_id", requsition_id); else q->SetParamNull("requsition_id");
    if (city > 0) q->SetParam("city", city); else q->SetParamNull("city");

    if (!q->exec())
    {
        ret      = -1;
        errorstr = q->errorstr;
        logtxt("Error: " + errorstr);
    }
    else
    {
        ret = q->st_to_mt(mt);
        if (q->errorstr.length() > 0)
        {
            errorstr = q->errorstr;
            logtxt("Error: " + errorstr);
            ret = -1;
        }
        q->commit();
    } // else

      //pthread_mutex_unlock(&mutex);

    return (ret);
}
//--------------------------------------------------------------------------
int TDataLayerImpl::RequisitionIU(const int requisition_id, const Wt::WDate& date_ta, const std::string & phone_number, const std::string& customer, const int carlifter_plan_id)
{
int ret(0);
std::string sql;

sql = "select ID from REQUISITION_IU(:requisition_id, :date_ta, :phone_number, :customer, :carlifter_plan_id) ";

//pthread_mutex_lock(&mutex);
q->SetNewQuery(sql, IBPP::amWrite);
if (requisition_id > 0) q->SetParam("requisition_id", requisition_id); else q->SetParamNull("requisition_id");
q->SetParam("date_ta", wd2d(date_ta));
q->SetParam("phone_number", phone_number);
q->SetParam("customer", customer);
if (carlifter_plan_id > 0) q->SetParam("carlifter_plan_id", carlifter_plan_id); else q->SetParamNull("carlifter_plan_id");

if (q->exec(st))
{
    try
    {
        if(st->Fetch())
        {
            st->Get("ID", ret);
        }
    } // try
    catch (IBPP::Exception& e)
    {
        errorstr = cp2koi(e.what(), 1);
        ret      = -1;
        logtxt("Error: TDataLayerImpl::RequisitionIU - " + errorstr);
        //pthread_mutex_unlock(&mutex);
        return (ret);
    }
    q->commit();
}
else
{
    ret      = -1;
    errorstr = q->errorstr;
    logtxt("Error: " + errorstr);
} // else

  //pthread_mutex_unlock(&mutex);

 return (ret);
}
//--------------------------------------------------------------------------
bool TDataLayerImpl::RequisitionD(const int requisition_id)
{
    bool ret(true);
    std::string sql;

    sql = "execute procedure REQUISITION_D(:requisition_id) ";

    //pthread_mutex_lock(&mutex);
    q->SetNewQuery(sql, IBPP::amWrite);
    q->SetParam("requisition_id", requisition_id);

    if (q->exec())
    {
        q->commit();
    }
    else
    {
        ret                      = false;
        SQLException_SqlCode     = q->SQLException_SqlCode;
        SQLException_EngineCode  = q->SQLException_EngineCode;
        errorstr                 = q->errorstr;
        logtxt("Error: TDataLayerImpl::FDBTagD: " + errorstr);
    }

    //pthread_mutex_unlock(&mutex);
    return (ret);
}
//--------------------------------------------------------------------------
int TDataLayerImpl::CarlifterS(Wt::WAbstractItemModel * mt, const int city)
{
    int ret (-1);
    std::string sql;

    errorstr.clear();

    sql  = "select c.* ";
    sql += "from carlifter_s(:city) c ";
    sql += "order by c.number ";

    //pthread_mutex_lock(&mutex);

    q->SetNewQuery(sql, IBPP::amRead);
    q->SetParam("city", city);

    if (!q->exec())
    {
        ret      = -1;
        errorstr = q->errorstr;
        logtxt("Error: " + errorstr);
    }
    else
    {
        ret = q->st_to_mt(mt);
        if (q->errorstr.length() > 0)
        {
            errorstr = q->errorstr;
            logtxt("Error: " + errorstr);
            ret = -1;
        }
        q->commit();
    } // else

      //pthread_mutex_unlock(&mutex);

    return (ret);
}

int TDataLayerImpl::CarlifterIU(const int carlifter_id, const int city, const int number, const std::string& description)
{
    int ret(0);
    std::string sql;

    sql = "select ID from carlifter_IU(:carlifter_id, :city, :number, :description) ";

    //pthread_mutex_lock(&mutex);
    q->SetNewQuery(sql, IBPP::amWrite);
    q->SetParamOrNull("carlifter_id", carlifter_id);
    q->SetParam("city", city);
    q->SetParam("number", number);
    q->SetParam("description", description);

    if (q->exec(st))
    {
        try
        {
            if(st->Fetch())
            {
                st->Get("ID", ret);
            }
        } // try
        catch (IBPP::Exception& e)
        {
            errorstr = cp2koi(e.what(), 1);
            ret      = -1;
            logtxt("Error: TDataLayerImpl::CarlifterIU - " + errorstr);
            //pthread_mutex_unlock(&mutex);
            return (ret);
        }
        q->commit();
    }
    else
    {
        ret      = -1;
        errorstr = q->errorstr;
        logtxt("Error: " + errorstr);
    } // else

      //pthread_mutex_unlock(&mutex);

    return (ret);
}

//--------------------------------------------------------------------------
int TDataLayerImpl::CarlifterPlanS(Wt::WAbstractItemModel * mt, const int city, const Wt::WDateTime& start, const Wt::WDateTime& finish, const int carlifter_plan_id)
{
 int ret (-1);
 std::string sql;

 errorstr.clear();

 sql  = "select p.* ";
 sql += "from carlifter_plan_s(:city, :start, :finish, :carlifter_plan_id) p ";
 sql += "order by p.number, p.DT_START ";

    //pthread_mutex_lock(&mutex);

    q->SetNewQuery(sql, IBPP::amRead);
    q->SetParam("start", wdt2ts(start));
    q->SetParam("finish", wdt2ts(finish));
    q->SetParam("city", city);
    if (carlifter_plan_id > 0) q->SetParam("carlifter_plan_id", carlifter_plan_id); else q->SetParamNull("carlifter_plan_id");

    if (!q->exec())
    {
        ret      = -1;
        errorstr = q->errorstr;
        logtxt("Error: " + errorstr);
    }
    else
    {
        ret = q->st_to_mt(mt);
        if (q->errorstr.length() > 0)
        {
            errorstr = q->errorstr;
            logtxt("Error: " + errorstr);
            ret = -1;
        }
        q->commit();
    } // else

      //pthread_mutex_unlock(&mutex);

    return (ret);
}
//--------------------------------------------------------------------------
int TDataLayerImpl::CarlifterPlanSFree(Wt::WAbstractItemModel * mt, const int city, const Wt::WDateTime& start, const Wt::WDateTime& finish, const int carlifter_id, const int carlifter_plan_id)
{
    int ret (-1);
    std::string sql, where;

    errorstr.clear();

    where = "p.carlifter_id = :carlifter_id and (p.requisition_id is NULL ";
    if (carlifter_plan_id > 0)
    {where += "or p.CARLIFTER_PLAN_ID = :carlifter_plan_id ";}
    where +=") ";

    sql  = "select p.*, l.number, l.description,  ";
    sql += "extract(hour from p.DT_START) || ':' || substring(100+extract(minute from p.DT_START) from 2 for 2) || '-' || ";
    sql += "extract(hour from p.DT_FINISH) || ':' || substring(100+extract(minute from p.DT_FINISH) from 2 for 2) as label ";
    sql += "from carlifter_plan_s(:city, :start, :finish, NULL) p ";
    sql += "inner join carlifter_s(:city0) l on (l.carlifter_id = p.CARLIFTER_ID) ";
    sql += "where " + where;
    sql += "order by l.number, p.DT_START ";

    //pthread_mutex_lock(&mutex);

    q->SetNewQuery(sql, IBPP::amRead);
    q->SetParam("start", wdt2ts(start));
    q->SetParam("finish", wdt2ts(finish));
    q->SetParam("city", city);
    q->SetParam("city0", city);
    q->SetParam("carlifter_id", carlifter_id);
    if (carlifter_plan_id > 0)
        q->SetParam("carlifter_plan_id", carlifter_plan_id);
    if (!q->exec())
    {
        ret      = -1;
        errorstr = q->errorstr;
        logtxt("Error: " + errorstr);
    }
    else
    {
        ret = q->st_to_mt(mt);
        if (q->errorstr.length() > 0)
        {
            errorstr = q->errorstr;
            logtxt("Error: " + errorstr);
            ret = -1;
        }
        q->commit();
    } // else

      //pthread_mutex_unlock(&mutex);

    return (ret);
}
//--------------------------------------------------------------------------
int TDataLayerImpl::CarlifterPlanIU(const int carlifter_plan_id, const int carlifter_id, const Wt::WDateTime& start, const Wt::WDateTime& finish)
{
    int ret(0);
    std::string sql;

    sql = "select ID from CARLIFTER_PLAN_IU(:carlifter_plan_id, :carlifter_id, :start, :finish) ";

    //pthread_mutex_lock(&mutex);
    q->SetNewQuery(sql, IBPP::amWrite);
    if (carlifter_plan_id > 0) q->SetParam("carlifter_plan_id", carlifter_plan_id); else q->SetParamNull("carlifter_plan_id");
    if (carlifter_id > 0) q->SetParam("carlifter_id", carlifter_id); else q->SetParamNull("carlifter_id");
    q->SetParam("start", wdt2ts(start));
    q->SetParam("finish", wdt2ts(finish));
    if (q->exec(st))
    {
        try
        {
            if(st->Fetch())
            {
                st->Get("ID", ret);
            }
        } // try
        catch (IBPP::Exception& e)
        {
            errorstr = cp2koi(e.what(), 1);
            ret      = -1;
            logtxt("Error: TDataLayerImpl::CarlifterPlanIU - " + errorstr);
            //pthread_mutex_unlock(&mutex);
            return (ret);
        }
        q->commit();
    }
    else
    {
        ret      = -1;
        errorstr = q->errorstr;
        logtxt("Error: " + errorstr);
    } // else

      //pthread_mutex_unlock(&mutex);

    return (ret);
}
//--------------------------------------------------------------------------
bool TDataLayerImpl::CarlifterPlanD(const int carlifter_plan_id)
{
    bool ret(true);
    std::string sql;

    sql = "execute procedure CARLIFTER_PLAN_D(:carlifter_plan_id) ";

    //pthread_mutex_lock(&mutex);
    q->SetNewQuery(sql, IBPP::amWrite);
    q->SetParam("carlifter_plan_id", carlifter_plan_id);

    if (q->exec())
    {
        q->commit();
    }
    else
    {
        ret                      = false;
        SQLException_SqlCode     = q->SQLException_SqlCode;
        SQLException_EngineCode  = q->SQLException_EngineCode;
        errorstr                 = q->errorstr;
        logtxt("Error: TDataLayerImpl::carlifter_plan_d: " + errorstr);
    }

    //pthread_mutex_unlock(&mutex);
    return (ret);
}
//--------------------------------------------------------------------------
int TDataLayerImpl::BasicDataS(Wt::WAbstractItemModel * mt, const int ic_id, const int status0, const int status1, const Wt::WDate deApplication)
{
    int ret (-1);
    std::string sql, where("");

    errorstr.clear();

    sql  = "select p.* ";
    sql += "from basic_data_s(:ic_id) p ";
    if (status0 > 0) {where += "p.STATE0_ID = :status0 "; }    
    if (status1 > 0) {if (!where.empty()) {  where += " and "; } where += "p.STATE1_ID = :status1 "; }
    if (!deApplication.isNull() && deApplication.isValid()) {if (!where.empty()) {  where += " and "; } where += "cast(p.DT_APPLICATION as date) = :deApplication "; }

    if (!where.empty()) sql += " where " + where;

    //pthread_mutex_lock(&mutex);

    q->SetNewQuery(sql, IBPP::amRead);
    q->SetParam("ic_id", ic_id);
    if (status0 > 0){q->SetParam("status0", status0);}
    if (status1 > 0){q->SetParam("status1", status1);}
    if (!deApplication.isNull() && deApplication.isValid()){q->SetParam("deApplication", wd2d(deApplication));}

    if (!q->exec())
    {
        ret      = -1;
        errorstr = q->errorstr;
        logtxt("Error: " + errorstr);
    }
    else
    {
        ret = q->st_to_mt(mt);
        if (q->errorstr.length() > 0)
        {
            errorstr = q->errorstr;
            logtxt("Error: " + errorstr);
            ret = -1;
        }
        q->commit();
    } // else

      //pthread_mutex_unlock(&mutex);

    return (ret);
}
//--------------------------------------------------------------------------
int TDataLayerImpl::BasicDataIU(const TBasicData * bd)
{
    int ret(0);
    std::string sql;

    sql = "select ID from BASIC_DATA_IU(:ic_id, :settlement, :reimbursement_lmv, :calculation_lmv_possible, :number, :insurance_type, :dt_ta, :inspection, :participants, :culprits, ";
    sql +=":ta_certificate, :optional_equipment, :inspection_type, :application, :incident_type, ";
    sql +=":cause_damage, :ta_location_type, :franchise, ";
    sql +=":postcode, :addr_field1, :night, :minimal_contact, :casualties, ";
    sql +=":transfer, :mobile_app, :contract_sale, :partner_id, :firm, :beneficiary) ";

    //pthread_mutex_lock(&mutex);
    q->SetNewQuery(sql, IBPP::amWrite);
    q->SetParamOrNull("ic_id", bd->ic_id);
    q->SetParam("settlement", bd->settlement );
    q->SetParam("reimbursement_lmv", bd->reimbursement_lmv);
    q->SetParam("calculation_lmv_possible", bd->calculation_lmv_possible);
    q->SetParam("number", bd->number);
    q->SetParamOrNull("insurance_type", bd->insurance_type);
    q->SetParam("dt_ta", wdt2ts(bd->dt_ta));
    q->SetParam("inspection", bd->inspection);
    q->SetParam("participants", bd->participants);
    q->SetParam("culprits", bd->culprits);
    q->SetParamOrNull("ta_certificate", bd->ta_certificate);
    q->SetParam("optional_equipment", bd->optional_equipment);
    q->SetParamOrNull("inspection_type", bd->inspection_type);
    q->SetParam("application", bd->application);
    q->SetParamOrNull("incident_type", bd->incident_type);
    q->SetParamOrNull("cause_damage", bd->cause_damage);
    q->SetParamOrNull("ta_location_type", bd->ta_location_type);
    q->SetParam("franchise", bd->franchise);
    q->SetParam("postcode", bd->postcode);
    q->SetParam("addr_field1", bd->addr_field1);
    q->SetParam("night", bd->night);
    q->SetParam("minimal_contact", bd->minimal_contact);
    q->SetParam("casualties", bd->casualties);
    q->SetParam("transfer", bd->transfer);
    q->SetParam("mobile_app", bd->mobile_app);
    q->SetParam("contract_sale", bd->contract_sale);
    q->SetParamOrNull("partner_id", bd->partner_id);
    q->SetParamOrNull("firm", bd->firm);
    q->SetParamOrNull("beneficiary", bd->beneficiary);

    if (q->exec(st))
    {
        try
        {
            if(st->Fetch())
            {
                st->Get("ID", ret);
            }
        } // try
        catch (IBPP::Exception& e)
        {
            errorstr = cp2koi(e.what(), 1);
            ret      = -1;
            logtxt("Error: TDataLayerImpl::BasicDataIU - " + errorstr);
            //pthread_mutex_unlock(&mutex);
            return (ret);
        }
        q->commit();
    }
    else
    {
        ret      = -1;
        errorstr = q->errorstr;
        logtxt("Error: " + errorstr);
    } // else

      //pthread_mutex_unlock(&mutex);

    return (ret);
}
//--------------------------------------------------------------------------
bool TDataLayerImpl::BasicDataD(const int ic_id)
{
    bool ret(true);
    std::string sql;

    sql = "execute procedure BASIC_DATA_D(:ic_id) ";

    //pthread_mutex_lock(&mutex);
    q->SetNewQuery(sql, IBPP::amWrite);
    q->SetParam("ic_id", ic_id);

    if (q->exec())
    {
        q->commit();
    }
    else
    {
        ret                      = false;
        SQLException_SqlCode     = q->SQLException_SqlCode;
        SQLException_EngineCode  = q->SQLException_EngineCode;
        errorstr                 = q->errorstr;
        logtxt("Error: TDataLayerImpl::BasicDataD: " + errorstr);
    }

    //pthread_mutex_unlock(&mutex);
    return (ret);
}

int TDataLayerImpl::ApplicationInspectionS(Wt::WAbstractItemModel * mt, const int ic_id)
{
    int ret (-1);
    std::string sql;

    errorstr.clear();

    sql  = "select p.* ";
    sql += "from APPLICATION_INSPECTION_S(:ic_id) p ";

    //pthread_mutex_lock(&mutex);

    q->SetNewQuery(sql, IBPP::amRead);
    q->SetParam("ic_id", ic_id);

    if (!q->exec())
    {
        ret      = -1;
        errorstr = q->errorstr;
        logtxt("Error: " + errorstr);
    }
    else
    {
        ret = q->st_to_mt(mt);
        if (q->errorstr.length() > 0)
        {
            errorstr = q->errorstr;
            logtxt("Error: " + errorstr);
            ret = -1;
        }
        q->commit();
    } // else

      //pthread_mutex_unlock(&mutex);

    return (ret);
}

int TDataLayerImpl::ApplicationInspectionIU(const TApplicationInspection * bd)
{
    int ret(0);
    std::string sql;

    sql = "select ID from APPLICATION_INSPECTION_IU(:ic_id, :unlimited_ep, :disagreements, :exit_ad, ";
    sql+= ":region_circulation, ";
    sql+= ":dt_application, :who_applied, :scheme_number, :applicants, ";
    sql+= ":inspection_type, :region_inspection, :responsible, :inspection_address, ";
    sql+= ":dt_inspection, :on_move, :flip, :frontal, :airbags, :rereferral, ";
    sql+= ":receiving, :repair_cost, :without_wear, :agreed_cost, :including_wear, ";
    sql+= ":terms_payment, :expertise_cost, :search_defects, :determination, :residual_value, ";
    sql+= ":loss_value, :trace_evidence, :independent, :photo) ";

    //pthread_mutex_lock(&mutex);
    q->SetNewQuery(sql, IBPP::amWrite);
    q->SetParamOrNull("ic_id", bd->ic_id);
    q->SetParam("unlimited_ep", bd->unlimited_ep);
    q->SetParam("disagreements", bd->disagreements);
    q->SetParam("exit_ad", bd->exit_ad);
    q->SetParam("region_circulation", bd->region_circulation);
    q->SetParam("dt_application", wdt2ts(bd->dt_application));
    q->SetParam("who_applied", bd->who_applied);
    q->SetParam("scheme_number", bd->scheme_number);
    q->SetParam("applicants", bd->applicants);
    q->SetParam("inspection_type", bd->inspection_type);
    q->SetParam("region_inspection", bd->region_inspection);
//    q->SetParam("subdivision", bd->subdivision);
    q->SetParam("responsible", bd->responsible);
    q->SetParam("inspection_address", bd->inspection_address);
    q->SetParam("dt_inspection", wdt2ts(bd->dt_inspection));
    q->SetParam("on_move", bd->on_move);
    q->SetParam("flip", bd->flip);
    q->SetParam("frontal", bd->frontal);
    q->SetParam("airbags", bd->airbags);
 //   q->SetParam("damage", bd->damage);
    q->SetParam("rereferral", wd2d(bd->rereferral));
    q->SetParam("receiving", wd2d(bd->receiving));
    q->SetParam("repair_cost", bd->repair_cost);
    q->SetParam("without_wear", bd->without_wear);
    q->SetParam("agreed_cost", bd->agreed_cost);
    q->SetParam("including_wear", bd->including_wear);
    q->SetParam("terms_payment", bd->terms_payment);
    q->SetParam("expertise_cost", bd->expertise_cost);
    q->SetParam("search_defects", bd->search_defects);
    q->SetParam("determination", bd->determination);
    q->SetParam("residual_value", bd->residual_value);
    q->SetParam("loss_value", bd->loss_value);
    q->SetParam("trace_evidence", bd->trace_evidence);
    q->SetParam("independent", bd->independent);
    q->SetParam("photo", bd->photo);

    if (q->exec(st))
    {
        try
        {
            if(st->Fetch())
            {
                st->Get("ID", ret);
            }
        } // try
        catch (IBPP::Exception& e)
        {
            errorstr = cp2koi(e.what(), 1);
            ret      = -1;
            logtxt("Error: TDataLayerImpl::ApplicationInspectionIU - " + errorstr);
            //pthread_mutex_unlock(&mutex);
            return (ret);
        }
        q->commit();
    }
    else
    {
        ret      = -1;
        errorstr = q->errorstr;
        logtxt("Error: " + errorstr);
    } // else

      //pthread_mutex_unlock(&mutex);

    return (ret);
}
bool TDataLayerImpl::ApplicationInspectionD(const int ic_id)
{
    bool ret(true);
    std::string sql;

    sql = "execute procedure APPLICATION_INSPECTION_D(:ic_id) ";

    //pthread_mutex_lock(&mutex);
    q->SetNewQuery(sql, IBPP::amWrite);
    q->SetParam("ic_id", ic_id);

    if (q->exec())
    {
        q->commit();
    }
    else
    {
        ret                      = false;
        SQLException_SqlCode     = q->SQLException_SqlCode;
        SQLException_EngineCode  = q->SQLException_EngineCode;
        errorstr                 = q->errorstr;
        logtxt("Error: TDataLayerImpl::BasicDataD: " + errorstr);
    }

    //pthread_mutex_unlock(&mutex);
    return (ret);
}
//--------------------------------------------------------------------------

int TDataLayerImpl::CulpritS(Wt::WAbstractItemModel * mt, const int ic_id)
{
    int ret (-1);
    std::string sql;

    errorstr.clear();

    sql  = "select p.* ";
    sql += "from CULPRIT_S(:ic_id) p ";

    //pthread_mutex_lock(&mutex);

    q->SetNewQuery(sql, IBPP::amRead);
    q->SetParam("ic_id", ic_id);

    if (!q->exec())
    {
        ret      = -1;
        errorstr = q->errorstr;
        logtxt("Error: " + errorstr);
    }
    else
    {
        ret = q->st_to_mt(mt);
        if (q->errorstr.length() > 0)
        {
            errorstr = q->errorstr;
            logtxt("Error: " + errorstr);
            ret = -1;
        }
        q->commit();
    } // else

      //pthread_mutex_unlock(&mutex);

    return (ret);
}
int TDataLayerImpl::CulpritIU(const TCulprit * bd)
{
    int ret(0);
    std::string sql;

    sql = "select ID from CULPRIT_IU(:ic_id, :insurance_company, :polis_series, :policy_number, :date_conclusion, ";
    sql+= ":vehicle_type, :brand_model, :vin, :body_number, :registration_number, :inspection_culprit, ";
    sql+= ":vehicle_id, :driver_id, :vehicle_owner) ";

    //pthread_mutex_lock(&mutex);
    q->SetNewQuery(sql, IBPP::amWrite);
    q->SetParamOrNull("ic_id", bd->ic_id);
    q->SetParam("insurance_company", bd->insurance_company);
    q->SetParam("polis_series", bd->polis_series);
    q->SetParam("policy_number", bd->policy_number);
    q->SetParam("date_conclusion", wd2d(bd->date_conclusion));
    q->SetParam("vehicle_type", bd->vehicle_type);
    q->SetParam("brand_model", bd->brand_model);
    q->SetParam("vin", bd->vin);
    q->SetParam("body_number", bd->body_number);
    q->SetParam("registration_number", bd->registration_number);
    q->SetParam("inspection_culprit", bd->inspection_culprit);
    q->SetParamOrNull("driver_id", bd->driver_id);
    q->SetParamOrNull("vehicle_id", bd->vehicle_id);
    q->SetParamOrNull("vehicle_owner", bd->vehicle_owner);
    if (q->exec(st))
    {
        try
        {
            if(st->Fetch())
            {
                st->Get("ID", ret);
            }
        } // try
        catch (IBPP::Exception& e)
        {
            errorstr = cp2koi(e.what(), 1);
            ret      = -1;
            logtxt("Error: TDataLayerImpl::ApplicationInspectionIU - " + errorstr);
            //pthread_mutex_unlock(&mutex);
            return (ret);
        }
        q->commit();
    }
    else
    {
        ret      = -1;
        errorstr = q->errorstr;
        logtxt("Error: " + errorstr);
    } // else

      //pthread_mutex_unlock(&mutex);

    return (ret);
}
bool TDataLayerImpl::CulpritD(const int ic_id)
{
    bool ret(true);
    std::string sql;

    sql = "execute procedure CULPRIT_D(:ic_id) ";

    //pthread_mutex_lock(&mutex);
    q->SetNewQuery(sql, IBPP::amWrite);
    q->SetParam("ic_id", ic_id);

    if (q->exec())
    {
        q->commit();
    }
    else
    {
        ret                      = false;
        SQLException_SqlCode     = q->SQLException_SqlCode;
        SQLException_EngineCode  = q->SQLException_EngineCode;
        errorstr                 = q->errorstr;
        logtxt("Error: TDataLayerImpl::BasicDataD: " + errorstr);
    }

    //pthread_mutex_unlock(&mutex);
    return (ret);
}
//--------------------------------------------------------------------------
int TDataLayerImpl::VictimS(Wt::WAbstractItemModel * mt, const int ic_id)
{
    int ret (-1);
    std::string sql;

    errorstr.clear();

    sql  = "select p.* ";
    sql += "from VICTIM_S(:ic_id) p ";

    //pthread_mutex_lock(&mutex);

    q->SetNewQuery(sql, IBPP::amRead);
    q->SetParam("ic_id", ic_id);

    if (!q->exec())
    {
        ret      = -1;
        errorstr = q->errorstr;
        logtxt("Error: " + errorstr);
    }
    else
    {
        ret = q->st_to_mt(mt);
        if (q->errorstr.length() > 0)
        {
            errorstr = q->errorstr;
            logtxt("Error: " + errorstr);
            ret = -1;
        }
        q->commit();
    } // else

      //pthread_mutex_unlock(&mutex);

    return (ret);
}
int TDataLayerImpl::VictimIU(const TVictim * bd)
{
    int ret(0);
    std::string sql;

    sql = "select ID from VICTIM_IU(:ic_id, :insurance_company, :policy_series, :policy_number, :vehicle_id, :DRIVER_ID, :vehicle_owner) ";

    //pthread_mutex_lock(&mutex);
    q->SetNewQuery(sql, IBPP::amWrite);
    q->SetParamOrNull("ic_id", bd->ic_id);
    q->SetParam("insurance_company", bd->insurance_company);
    q->SetParam("policy_series", bd->policy_series);
    q->SetParam("policy_number", bd->policy_number);
    q->SetParam("vehicle_id", bd->vehicle_id);
    q->SetParam("driver_id", bd->driver_id);
    q->SetParam("vehicle_owner", bd->vehicle_owner);

    if (q->exec(st))
    {
        try
        {
            if(st->Fetch())
            {
                st->Get("ID", ret);
            }
        } // try
        catch (IBPP::Exception& e)
        {
            errorstr = cp2koi(e.what(), 1);
            ret      = -1;
            logtxt("Error: TDataLayerImpl::VictimIU - " + errorstr);
            //pthread_mutex_unlock(&mutex);
            return (ret);
        }
        q->commit();
    }
    else
    {
        ret      = -1;
        errorstr = q->errorstr;
        logtxt("Error: " + errorstr);
    } // else

      //pthread_mutex_unlock(&mutex);

    return (ret);
}

bool TDataLayerImpl::VictimD(const int ic_id)
{
    bool ret(true);
    std::string sql;

    sql = "execute procedure VICTIM_D(:ic_id) ";

    //pthread_mutex_lock(&mutex);
    q->SetNewQuery(sql, IBPP::amWrite);
    q->SetParam("ic_id", ic_id);

    if (q->exec())
    {
        q->commit();
    }
    else
    {
        ret                      = false;
        SQLException_SqlCode     = q->SQLException_SqlCode;
        SQLException_EngineCode  = q->SQLException_EngineCode;
        errorstr                 = q->errorstr;
        logtxt("Error: TDataLayerImpl::VictimD: " + errorstr);
    }

    //pthread_mutex_unlock(&mutex);
    return (ret);
}
 //--------------------------------------------------------------------------
int TDataLayerImpl::DriverS(Wt::WAbstractItemModel * mt, const int driver_id)
{
    int ret (-1);
    std::string sql;

    errorstr.clear();

    sql  = "select p.* ";
    sql += "from DRIVER_S(:driver_id) p ";

    //pthread_mutex_lock(&mutex);

    q->SetNewQuery(sql, IBPP::amRead);
    q->SetParam("driver_id", driver_id);

    if (!q->exec())
    {
        ret      = -1;
        errorstr = q->errorstr;
        logtxt("Error: " + errorstr);
    }
    else
    {
        ret = q->st_to_mt(mt);
        if (q->errorstr.length() > 0)
        {
            errorstr = q->errorstr;
            logtxt("Error: " + errorstr);
            ret = -1;
        }
        q->commit();
    } // else

      //pthread_mutex_unlock(&mutex);

    return (ret);
}
int TDataLayerImpl::DriverIU(const TDriver * bd)
{
    int ret(0);
    std::string sql;

    sql = "select ID from DRIVER_IU(:driver_id, ";
    sql+= ":resident, :citizenship, :date_birth, :gender, :first_name, :middle_name, :last_name, ";
    sql+= ":without_mname, :phone, :dl_series, :dl_number, :dl_no_data, ";
    sql+= ":p_series, :p_number, :issued, :date_issue, :postcode, :addr, :house, :apartment) ";

    //pthread_mutex_lock(&mutex);
    q->SetNewQuery(sql, IBPP::amWrite);
    q->SetParamOrNull("driver_id", bd->driver_id);
    q->SetParam("resident", bd->resident);
    q->SetParamOrNull("citizenship", bd->citizenship);
    q->SetParam("date_birth", wd2d(bd->date_birth));
    q->SetParamOrNull("gender", bd->gender);
    q->SetParam("last_name", bd->last_name);
    q->SetParam("first_name", bd->first_name);
    q->SetParam("middle_name", bd->middle_name);
    q->SetParam("without_mname", bd->without_mname);
    q->SetParam("phone", bd->phone);
    q->SetParam("dl_series", bd->dl_series);
    q->SetParam("dl_number", bd->dl_number);
    q->SetParam("dl_no_data", bd->dl_no_data);
    q->SetParam("p_series", bd->p_series);
    q->SetParam("p_number", bd->p_number);
    q->SetParam("issued", bd->issued);
    q->SetParam("date_issue", wd2d(bd->date_issue));
    q->SetParam("postcode", bd->postcode);
    q->SetParam("addr", bd->addr_field1);
    q->SetParam("house", bd->house);
    q->SetParam("apartment", bd->apartment);

    if (q->exec(st))
    {
        try
        {
            if(st->Fetch())
            {
                st->Get("ID", ret);
            }
        } // try
        catch (IBPP::Exception& e)
        {
            errorstr = cp2koi(e.what(), 1);
            ret      = -1;
            logtxt("Error: TDataLayerImpl::DriverIU - " + errorstr);
            //pthread_mutex_unlock(&mutex);
            return (ret);
        }
        q->commit();
    }
    else
    {
        ret      = -1;
        errorstr = q->errorstr;
        logtxt("Error: " + errorstr);
    } // else

      //pthread_mutex_unlock(&mutex);

    return (ret);
}
bool TDataLayerImpl::DriverD(const int driver_id)
{
    bool ret(true);
    std::string sql;

    sql = "execute procedure DRIVER_D(:ic_id) ";

    //pthread_mutex_lock(&mutex);
    q->SetNewQuery(sql, IBPP::amWrite);
    q->SetParam("ic_id", driver_id);

    if (q->exec())
    {
        q->commit();
    }
    else
    {
        ret                      = false;
        SQLException_SqlCode     = q->SQLException_SqlCode;
        SQLException_EngineCode  = q->SQLException_EngineCode;
        errorstr                 = q->errorstr;
        logtxt("Error: TDataLayerImpl::Driver_D: " + errorstr);
    }

    //pthread_mutex_unlock(&mutex);
    return (ret);
}

int TDataLayerImpl::TableS(const std::string& table, Wt::WAbstractItemModel * mt, const int id)
{
    int ret (-1);
    std::string sql;

    errorstr.clear();

    sql  = "select p.* from ";
    sql += table;
    sql += "_S(:id) p ";

    //pthread_mutex_lock(&mutex);

    q->SetNewQuery(sql, IBPP::amRead);
    q->SetParamOrNull("id", id);

    if (!q->exec())
    {
        ret      = -1;
        errorstr = q->errorstr;
        logtxt("Error: " + errorstr);
    }
    else
    {
        ret = q->st_to_mt(mt);
        if (q->errorstr.length() > 0)
        {
            errorstr = q->errorstr;
            logtxt("Error: " + errorstr);
            ret = -1;
        }
        q->commit();
    } // else

      //pthread_mutex_unlock(&mutex);

    return (ret);
}

bool TDataLayerImpl::TableD(const std::string& table, const int id)
{
    bool ret(true);
    std::string sql;

    sql = "execute procedure " + table + "_D(:id) ";

    //pthread_mutex_lock(&mutex);
    q->SetNewQuery(sql, IBPP::amWrite);
    q->SetParam("id", id);

    if (q->exec())
    {
        q->commit();
    }
    else
    {
        ret                      = false;
        SQLException_SqlCode     = q->SQLException_SqlCode;
        SQLException_EngineCode  = q->SQLException_EngineCode;
        errorstr                 = q->errorstr;
        logtxt("Error: TDataLayerImpl::TableD: " + errorstr);
    }

    //pthread_mutex_unlock(&mutex);
    return (ret);
}

int TDataLayerImpl::NodeIU(const TNode * bd)
{
    int ret(0);
    std::string sql;

    sql = "select ID from FDB_NODE_IU(:node_id, :p3, :p2, :p1, :name_server, :node_size, :crc) ";

    //pthread_mutex_lock(&mutex);
    q->SetNewQuery(sql, IBPP::amWrite);
    q->SetParamOrNull("node_id", bd->node_id);
    q->SetParam("p3", bd->p3);
    q->SetParam("p2", bd->p2);
    q->SetParam("p1", bd->p1);
    q->SetParam("name_server", bd->name_server);
    q->SetParam("node_size", bd->node_size);
    q->SetParam("crc", bd->crc);

    if (q->exec(st))
    {
        try
        {
            if(st->Fetch())
            {
                st->Get("ID", ret);
            }
        } // try
        catch (IBPP::Exception& e)
        {
            errorstr = cp2koi(e.what(), 1);
            ret      = -1;
            logtxt("Error: TDataLayerImpl::NodeIU - " + errorstr);
            //pthread_mutex_unlock(&mutex);
            return (ret);
        }
        q->commit();
    }
    else
    {
        ret      = -1;
        errorstr = q->errorstr;
        logtxt("Error: " + errorstr);
    } // else

      //pthread_mutex_unlock(&mutex);

    return (ret);
}

int TDataLayerImpl::FileIU(const TFile * bd)
{
    int ret(0);
    std::string sql;

    sql = "select ID from FDB_FILE_IU(:file_id, :name_user, :file_size, :node_id, :preview) ";

    //pthread_mutex_lock(&mutex);
    q->SetNewQuery(sql, IBPP::amWrite);
    q->SetParamOrNull("file_id", bd->file_id);
    q->SetParam("name_user", bd->name_user);
    q->SetParam("file_size", bd->file_size);
    q->SetParamOrNull("node_id", bd->node_id);
    q->SetParamOrNull("preview", bd->preview);

    if (q->exec(st))
    {
        try
        {
            if(st->Fetch())
            {
                st->Get("ID", ret);
            }
        } // try
        catch (IBPP::Exception& e)
        {
            errorstr = cp2koi(e.what(), 1);
            ret      = -1;
            logtxt("Error: TDataLayerImpl::FileIU - " + errorstr);
            //pthread_mutex_unlock(&mutex);
            return (ret);
        }
        q->commit();
    }
    else
    {
        ret      = -1;
        errorstr = q->errorstr;
        logtxt("Error: " + errorstr);
    } // else

      //pthread_mutex_unlock(&mutex);

    return (ret);
}

int TDataLayerImpl::FileDocS(Wt::WAbstractItemModel * mt, const int type, const int id)
{
    int ret (-1);
    std::string sql;

    errorstr.clear();

    sql  = "select d.file_doc_id, d.rec_type, d.rec_id, f.file_id, f.name_user, f.file_size, f.node_id, ";
    sql += "n.p3, n.p2, n.p1, n.name_server, ";
    sql += "v.p3 as v_p3, v.p2 as v_p2, v.p1 as v_p1, v.name_server as v_name_server ";
    sql += "from fdb_file_doc_s(NULL, :type, :id) d ";
    sql += "left join fdb_file_s(d.file_id) f on (1=1) ";
    sql += "left join fdb_node_s(f.node_id) n on (1=1) ";
    sql += "left join fdb_node_s(f.preview) v on (1=1) ";

    //pthread_mutex_lock(&mutex);

    q->SetNewQuery(sql, IBPP::amRead);
    q->SetParam("type", type);
    q->SetParam("id", id);

    if (!q->exec())
    {
        ret      = -1;
        errorstr = q->errorstr;
        logtxt("Error: " + errorstr);
    }
    else
    {
        ret = q->st_to_mt(mt);
        if (q->errorstr.length() > 0)
        {
            errorstr = q->errorstr;
            logtxt("Error: " + errorstr);
            ret = -1;
        }
        q->commit();
    } // else

      //pthread_mutex_unlock(&mutex);

    return (ret);
}

int TDataLayerImpl::FileDocIU(const int file_doc_id, const int file_id, const int type, const int id)
{
    int ret(0);
    std::string sql;

    sql = "select ID from fdb_file_doc_iu(:file_doc_id, :file_id, :rec_type, :rec_id) ";

    //pthread_mutex_lock(&mutex);
    q->SetNewQuery(sql, IBPP::amWrite);
    q->SetParamOrNull("file_id", file_id);
    q->SetParam("file_doc_id", file_doc_id);
    q->SetParam("rec_type", type);
    q->SetParam("rec_id", id);

    if (q->exec(st))
    {
        try
        {
            if(st->Fetch())
            {
                st->Get("ID", ret);
            }
        } // try
        catch (IBPP::Exception& e)
        {
            errorstr = cp2koi(e.what(), 1);
            ret      = -1;
            logtxt("Error: TDataLayerImpl::FileDocIU - " + errorstr);
            //pthread_mutex_unlock(&mutex);
            return (ret);
        }
        q->commit();
    }
    else
    {
        ret      = -1;
        errorstr = q->errorstr;
        logtxt("Error: " + errorstr);
    } // else

      //pthread_mutex_unlock(&mutex);

    return (ret);
}

//--------------------------------------------------------------------------
int TDataLayerImpl::GetNext(const std::string& generator)
{
    int ret (-1);
    std::string sql;

    errorstr.clear();

    sql  = "select gen_id(";
    sql += generator;
    sql += ", 1) as id from rdb$database; ";

    //pthread_mutex_lock(&mutex);

    q->SetNewQuery(sql, IBPP::amRead);

    if (q->exec(st))
    {
        try
        {
            if(st->Fetch())
            {
                st->Get("id", ret);
            }
        } // try
        catch (IBPP::Exception& e)
        {
            errorstr = cp2koi(e.what(), 1);
            ret      = -1;
            logtxt("Error: TDataLayerImpl::GetNext - " + errorstr);
            //pthread_mutex_unlock(&mutex);
            return (ret);
        }

        q->commit();
    }
    else
    {
        ret      = -1;
        errorstr = q->errorstr;
        logtxt("Error: " + errorstr);
    }

      //pthread_mutex_unlock(&mutex);

    return (ret);
}
//--------------------------------------------------------------------------
int TDataLayerImpl::VehicleS(Wt::WAbstractItemModel * mt, const int vehicle_id)
{
    int ret (-1);
    std::string sql;

    errorstr.clear();

    sql  = "select * from vehicle_s(:vehicle_id) ";

    //pthread_mutex_lock(&mutex);

    q->SetNewQuery(sql, IBPP::amRead);
    q->SetParam("vehicle_id", vehicle_id);

    if (!q->exec())
    {
        ret      = -1;
        errorstr = q->errorstr;
        logtxt("Error: " + errorstr);
    }
    else
    {
        ret = q->st_to_mt(mt);
        if (q->errorstr.length() > 0)
        {
            errorstr = q->errorstr;
            logtxt("Error: " + errorstr);
            ret = -1;
        }
        q->commit();
    } // else

      //pthread_mutex_unlock(&mutex);

    return (ret);
}

int TDataLayerImpl::VehicleIU(const TVehicle * bd)
{
    int ret(0);
    std::string sql;

    sql = "select ID from vehicle_iu(:vehicle_id, :vehicle_type, :brand, :model, :vin, :right_hand_drive, :car_year, ";
    sql+= ":registration_number, :vehicle_doc, :series, :number, :date_doc, :date_start, ";
    sql+= ":td_passport, :date_sale, :vehicle_warranty, :last_sale) ";

    //pthread_mutex_lock(&mutex);
    q->SetNewQuery(sql, IBPP::amWrite);
    q->SetParamOrNull("vehicle_id", bd->vehicle_id);
    q->SetParam("vehicle_type", bd->vehicle_type);
    q->SetParam("brand", bd->brand);
    q->SetParam("model", bd->model);
    q->SetParam("vin", bd->vin);
    q->SetParam("right_hand_drive", bd->right_hand_drive);
    q->SetParam("car_year", bd->car_year);
    q->SetParam("registration_number", bd->registration_number);
    q->SetParam("vehicle_doc", bd->vehicle_doc);
    q->SetParam("series", bd->series);
    q->SetParam("number", bd->number);
    q->SetParam("date_doc", wd2d(bd->date_doc));
    q->SetParam("date_start", wd2d(bd->date_start));
    q->SetParam("td_passport", bd->td_passport);
    q->SetParam("date_sale", wd2d(bd->date_sale));
    q->SetParam("vehicle_warranty", bd->vehicle_warranty);
    q->SetParam("last_sale", wd2d(bd->last_sale));

    if (q->exec(st))
    {
        try
        {
            if(st->Fetch())
            {
                st->Get("ID", ret);
            }
        } // try
        catch (IBPP::Exception& e)
        {
            errorstr = cp2koi(e.what(), 1);
            ret      = -1;
            logtxt("Error: TDataLayerImpl::VehicleIU - " + errorstr);
            //pthread_mutex_unlock(&mutex);
            return (ret);
        }
        q->commit();
    }
    else
    {
        ret      = -1;
        errorstr = q->errorstr;
        logtxt("Error: " + errorstr);
    } // else

      //pthread_mutex_unlock(&mutex);

    return (ret);
}
//--------------------------------------------------------------------------
int TDataLayerImpl::DamageS(Wt::WAbstractItemModel * mt, const int ic_id)
{
    int ret (-1);
    std::string sql;

    errorstr.clear();

    sql  = "select * from damage_s(:ic_id) ";

    //pthread_mutex_lock(&mutex);

    q->SetNewQuery(sql, IBPP::amRead);
    q->SetParam("ic_id", ic_id);

    if (!q->exec())
    {
        ret      = -1;
        errorstr = q->errorstr;
        logtxt("Error: " + errorstr);
    }
    else
    {
        ret = q->st_to_mt(mt);
        if (q->errorstr.length() > 0)
        {
            errorstr = q->errorstr;
            logtxt("Error: " + errorstr);
            ret = -1;
        }
        q->commit();
    } // else

      //pthread_mutex_unlock(&mutex);

    return (ret);
}

int TDataLayerImpl::DamageIU(const int damage_id, const int ic_id, const int element, const int kind)
{
    int ret(0);
    std::string sql;

    sql = "select ID from damage_iu(:damage_id, :ic_id, :element, :kind) ";

    //pthread_mutex_lock(&mutex);
    q->SetNewQuery(sql, IBPP::amWrite);
    q->SetParamOrNull("damage_id", damage_id);
    q->SetParam("ic_id", ic_id);
    q->SetParam("element", element);
    q->SetParam("kind", kind);

    if (q->exec(st))
    {
        try
        {
            if(st->Fetch())
            {
                st->Get("ID", ret);
            }
        } // try
        catch (IBPP::Exception& e)
        {
            errorstr = cp2koi(e.what(), 1);
            ret      = -1;
            logtxt("Error: TDataLayerImpl::DamageIU - " + errorstr);
            //pthread_mutex_unlock(&mutex);
            return (ret);
        }
        q->commit();
    }
    else
    {
        ret      = -1;
        errorstr = q->errorstr;
        logtxt("Error: " + errorstr);
    } // else

      //pthread_mutex_unlock(&mutex);
    return (ret);
}
//--------------------------------------------------------------------------
int TDataLayerImpl::PartnerIU(const int partner_id, const std::string& name)
{
    int ret(0);
    std::string sql;

    sql = "select ID from partner_iu(:partner_id, :name) ";

    //pthread_mutex_lock(&mutex);
    q->SetNewQuery(sql, IBPP::amWrite);
    q->SetParamOrNull("partner_id", partner_id);
    q->SetParam("name", name);

    if (q->exec(st))
    {
        try
        {
            if(st->Fetch())
            {
                st->Get("ID", ret);
            }
        } // try
        catch (IBPP::Exception& e)
        {
            errorstr = cp2koi(e.what(), 1);
            ret      = -1;
            logtxt("Error: TDataLayerImpl::PartnerIU - " + errorstr);
            //pthread_mutex_unlock(&mutex);
            return (ret);
        }
        q->commit();
    }
    else
    {
        ret      = -1;
        errorstr = q->errorstr;
        logtxt("Error: " + errorstr);
    } // else

      //pthread_mutex_unlock(&mutex);
    return (ret);
}
//--------------------------------------------------------------------------
int TDataLayerImpl::UsersS(Wt::WAbstractItemModel * mt, const int user_id, const int partner_id)
{
    int ret (-1);
    std::string sql;

    errorstr.clear();

    sql  = "select u.* from sys_users_s(:user_id) u ";

    if (partner_id > 0)
        sql += "where u.partner_id = :partner_id ";

    //pthread_mutex_lock(&mutex);

    q->SetNewQuery(sql, IBPP::amRead);
    q->SetParamOrNull("user_id", user_id);
    if (partner_id > 0) q->SetParam("partner_id", partner_id);

    if (!q->exec())
    {
        ret      = -1;
        errorstr = q->errorstr;
        logtxt("Error: " + errorstr);
    }
    else
    {
        ret = q->st_to_mt(mt);
        if (q->errorstr.length() > 0)
        {
            errorstr = q->errorstr;
            logtxt("Error: " + errorstr);
            ret = -1;
        }
        q->commit();
    } // else

      //pthread_mutex_unlock(&mutex);

    return (ret);
}

int TDataLayerImpl::UsersIU(const TUser * db)
{
    int ret(0);
    std::string sql;

    sql = "select ID from SYS_USERS_IU(:user_id, :user_name, :first_name, :middle_name, :last_name, ";
    sql+= ":pass, :email, :partner_id, :can_create_user, :job) ";

    //pthread_mutex_lock(&mutex);
    q->SetNewQuery(sql, IBPP::amWrite);
    q->SetParamOrNull("user_id", db->user_id);
    q->SetParam("user_name", db->user_name);
    q->SetParam("first_name", db->first_name);
    q->SetParam("middle_name", db->middle_name);
    q->SetParam("last_name", db->last_name);
    q->SetParam("pass", db->pass);
    q->SetParam("email", db->email);
    q->SetParamOrNull("partner_id", db->partner_id);
    q->SetParam("can_create_user", db->can_create_user);
    q->SetParamOrNull("job", db->job);
    if (q->exec(st))
    {
        try
        {
            if(st->Fetch())
            {
                st->Get("ID", ret);
            }
        } // try
        catch (IBPP::Exception& e)
        {
            errorstr = cp2koi(e.what(), 1);
            ret      = -1;
            logtxt("Error: TDataLayerImpl::UsersIU - " + errorstr);
            //pthread_mutex_unlock(&mutex);
            return (ret);
        }
        q->commit();
    }
    else
    {
        ret      = -1;
        errorstr = q->errorstr;
        logtxt("Error: " + errorstr);
    } // else

      //pthread_mutex_unlock(&mutex);
    return (ret);
}
//--------------------------------------------------------------------------
int TDataLayerImpl::TaskS(Wt::WAbstractItemModel * mt, const int ic_id, const int executor, const int state, const int type)
{
    int ret (-1);
    std::string sql, sql2;

    errorstr.clear();

    sql  = "select t.*, i.fio as initiator_fio, e.fio as executor_fio, tt.name as type_task_name, l21.name as state_name ";
    sql += "from task_s(NULL) t ";
    sql += "left join sys_users_s(t.initiator) i on (1=1) ";
    sql += "left join sys_users_s(t.executor) e on (1=1) ";
    sql += "left join dir_line_s(22) tt on (t.TYPE_TASK = tt.line_id) "; // 22 - Тип задачи
    sql += "left join dir_line_s(21) l21 on (t.STATE = l21.line_id) "; // 21 - Состояние задачи

    if (ic_id > 0)
    {       
        sql2 += " t.ic_id = :ic_id ";
    }
    if (executor > 0)
    {
        if (!sql2.empty()) sql2 += " and "; 
        sql2 += " t.executor = :executor ";
    }
    if (state > 0)
    {
        if (!sql2.empty()) sql2 += " and "; 
        sql2 += " t.STATE = :state ";
    }
    if (type > 0)
    {
        if (!sql2.empty()) sql2 += " and "; 
        sql2 += " t.TYPE_TASK = :type ";
    }
    if (!sql2.empty()) sql += " where " + sql2; 

    //pthread_mutex_lock(&mutex);

    q->SetNewQuery(sql, IBPP::amRead);
//    q->SetParamOrNull("user_id", user_id);
    if (ic_id > 0) q->SetParam("ic_id", ic_id);
    if (executor > 0) q->SetParam("executor", executor);
    if (state > 0) q->SetParam("state", state);
    if (type > 0) q->SetParam("type", type);

    if (!q->exec())
    {
        ret      = -1;
        errorstr = q->errorstr;
        logtxt("Error: " + errorstr);
    }
    else
    {
        ret = q->st_to_mt(mt);
        if (q->errorstr.length() > 0)
        {
            errorstr = q->errorstr;
            logtxt("Error: " + errorstr);
            ret = -1;
        }
        q->commit();
    } // else

      //pthread_mutex_unlock(&mutex);

    return (ret);
}
int TDataLayerImpl::TaskIU(const TTask * db)
{
    int ret(0);
    std::string sql;

    sql = "select ID from TASK_IU(:task_id, :ic_id, :initiator, :executor, :_task, :execute_before, :type_task, :job) ";

    //pthread_mutex_lock(&mutex);
    q->SetNewQuery(sql, IBPP::amWrite);
    q->SetParamOrNull("task_id", db->task_id);
    q->SetParam("ic_id", db->ic_id);
    q->SetParamOrNull("initiator", db->initiator);
    q->SetParam("executor", db->executor);
    q->SetParam("_task", db->task);
    q->SetParam("execute_before", wd2d(db->execute_before));
    q->SetParamOrNull("type_task", db->type_task);
    q->SetParamOrNull("job", db->job);
    if (q->exec(st))
    {
        try
        {
            if(st->Fetch())
            {
                st->Get("ID", ret);
            }
        } // try
        catch (IBPP::Exception& e)
        {
            errorstr = cp2koi(e.what(), 1);
            ret      = -1;
            logtxt("Error: TDataLayerImpl::TaskIU - " + errorstr);
            //pthread_mutex_unlock(&mutex);
            return (ret);
        }
        q->commit();
    }
    else
    {
        ret      = -1;
        errorstr = q->errorstr;
        logtxt("Error: " + errorstr);
    } // else

      //pthread_mutex_unlock(&mutex);
    return (ret);
}
//--------------------------------------------------------------------------
int TDataLayerImpl::TaskStateIU(const int task_state_id, const int task_id, const int state/*, Wt::WDateTime dt_state*/)
{
    int ret(0);
    std::string sql;

    sql = "select ID from TASK_STATE_IU(:task_state_id, :task_id, :state, NULL) ";

    //pthread_mutex_lock(&mutex);
    q->SetNewQuery(sql, IBPP::amWrite);
    q->SetParamOrNull("task_state_id", task_state_id);
    q->SetParam("task_id", task_id);
    q->SetParam("state", state);
  //  if (!dt_state.isNull() && dt_state.isValid()) q->SetParam("dt_state", wdt2ts(dt_state)); else q->SetParamNull("dt_state");
    if (q->exec(st))
    {
        try
        {
            if(st->Fetch())
            {
                st->Get("ID", ret);
            }
        } // try
        catch (IBPP::Exception& e)
        {
            errorstr = cp2koi(e.what(), 1);
            ret      = -1;
            logtxt("Error: TDataLayerImpl::TaskStateIU - " + errorstr);
            //pthread_mutex_unlock(&mutex);
            return (ret);
        }
        q->commit();
    }
    else
    {
        ret      = -1;
        errorstr = q->errorstr;
        logtxt("Error: " + errorstr);
    } // else

      //pthread_mutex_unlock(&mutex);
    return (ret);
}

int TDataLayerImpl::TaskStateS(Wt::WAbstractItemModel * mt, const int task_id)
{
    int ret (-1);
    std::string sql;

    errorstr.clear();

    sql  = "select t.* ";
    sql += "from task_state_s(:task_id) t ";
    sql += "order by t.DT_STATE ";

    //pthread_mutex_lock(&mutex);

    q->SetNewQuery(sql, IBPP::amRead);
    q->SetParam("task_id", task_id);
    if (!q->exec())
    {
        ret      = -1;
        errorstr = q->errorstr;
        logtxt("Error: " + errorstr);
    }
    else
    {
        ret = q->st_to_mt(mt);
        if (q->errorstr.length() > 0)
        {
            errorstr = q->errorstr;
            logtxt("Error: " + errorstr);
            ret = -1;
        }
        q->commit();
    } // else
   //pthread_mutex_unlock(&mutex);
    return (ret);
}
//--------------------------------------------------------------------------
int TDataLayerImpl::TaskCommentIU(const int task_comment_id, const int task_id, const std::string & comment)
{
    int ret(0);
    std::string sql;

    sql = "select ID from TASK_COMMENT_IU(:task_comment_id, :task_id, :comment) ";

    //pthread_mutex_lock(&mutex);
    q->SetNewQuery(sql, IBPP::amWrite);
    q->SetParamOrNull("task_comment_id", task_comment_id);
    q->SetParam("task_id", task_id);
    q->SetParam("comment", comment);
    //  if (!dt_state.isNull() && dt_state.isValid()) q->SetParam("dt_state", wdt2ts(dt_state)); else q->SetParamNull("dt_state");
    if (q->exec(st))
    {
        try
        {
            if(st->Fetch())
            {
                st->Get("ID", ret);
            }
        } // try
        catch (IBPP::Exception& e)
        {
            errorstr = cp2koi(e.what(), 1);
            ret      = -1;
            logtxt("Error: TDataLayerImpl::TaskCommentIU - " + errorstr);
            //pthread_mutex_unlock(&mutex);
            return (ret);
        }
        q->commit();
    }
    else
    {
        ret      = -1;
        errorstr = q->errorstr;
        logtxt("Error: " + errorstr);
    } // else

      //pthread_mutex_unlock(&mutex);
    return (ret);
}
//--------------------------------------------------------------------------
int TDataLayerImpl::StateIU(const int state_id, const int ic_id, const int partner_id, const int state)
{
    int ret(0);
    std::string sql;

    sql = "select ID from STATE_IU(:state_id, :ic_id, :partner_id, :v) ";

    //pthread_mutex_lock(&mutex);
    q->SetNewQuery(sql, IBPP::amWrite);
    q->SetParamOrNull("state_id", state_id);
    q->SetParam("ic_id", ic_id);
    q->SetParamOrNull("partner_id", partner_id);
    q->SetParamOrNull("v", state);

    //  if (!dt_state.isNull() && dt_state.isValid()) q->SetParam("dt_state", wdt2ts(dt_state)); else q->SetParamNull("dt_state");
    if (q->exec(st))
    {
        try
        {
            if(st->Fetch())
            {
                st->Get("ID", ret);
            }
        } // try
        catch (IBPP::Exception& e)
        {
            errorstr = cp2koi(e.what(), 1);
            ret      = -1;
            logtxt("Error: TDataLayerImpl::StateIU - " + errorstr);
            //pthread_mutex_unlock(&mutex);
            return (ret);
        }
        q->commit();
    }
    else
    {
        ret      = -1;
        errorstr = q->errorstr;
        logtxt("Error: " + errorstr);
    } // else

      //pthread_mutex_unlock(&mutex);
    return (ret);
}
//--------------------------------------------------------------------------

int TDataLayerImpl::EntityIU(const int ic_id, const std::string& name, const std::string& itn)
{
    int ret(0);
    std::string sql;

    sql = "select ID from ENTITY_IU(:ic_id, :name, :itn) ";

    //pthread_mutex_lock(&mutex);
    q->SetNewQuery(sql, IBPP::amWrite);
    q->SetParamOrNull("ic_id", ic_id);
    q->SetParam("name", name);
    q->SetParam("itn", itn);

    if (q->exec(st))
    {
        try
        {
            if(st->Fetch())
            {
                st->Get("ID", ret);
            }
        } // try
        catch (IBPP::Exception& e)
        {
            errorstr = cp2koi(e.what(), 1);
            ret      = -1;
            logtxt("Error: TDataLayerImpl::EntityIU - " + errorstr);
            //pthread_mutex_unlock(&mutex);
            return (ret);
        }
        q->commit();
    }
    else
    {
        ret      = -1;
        errorstr = q->errorstr;
        logtxt("Error: " + errorstr);
    } // else

      //pthread_mutex_unlock(&mutex);
    return (ret);
}

int TDataLayerImpl::PaymentDetailsIU(const int ic_id, const std::string& checking_account, const std::string& bank, const std::string& department, 
    const std::string& correspondent_account, const std::string& bic, const std::string& itn, const std::string& card, const std::string& recipient)
{
    int ret(0);
    std::string sql;

    sql = "select ID from PAYMENT_DETAILS_IU(:ic_id, :checking_account, :bank, :department, :correspondent_account, :bic, :itn, :card, :recipient) ";

    //pthread_mutex_lock(&mutex);
    q->SetNewQuery(sql, IBPP::amWrite);
    q->SetParamOrNull("ic_id", ic_id);
    q->SetParam("checking_account", checking_account);
    q->SetParam("bank", bank);
    q->SetParam("department", department);
    q->SetParam("correspondent_account", correspondent_account);
    q->SetParam("bic", bic);
    q->SetParam("itn", itn);
    q->SetParam("card", card);
    q->SetParam("recipient", recipient);

    if (q->exec(st))
    {
        try
        {
            if(st->Fetch())
            {
                st->Get("ID", ret);
            }
        } // try
        catch (IBPP::Exception& e)
        {
            errorstr = cp2koi(e.what(), 1);
            ret      = -1;
            logtxt("Error: TDataLayerImpl::PaymentDetailsIU - " + errorstr);
            //pthread_mutex_unlock(&mutex);
            return (ret);
        }
        q->commit();
    }
    else
    {
        ret      = -1;
        errorstr = q->errorstr;
        logtxt("Error: " + errorstr);
    } // else

      //pthread_mutex_unlock(&mutex);
    return (ret);
}

//--------------------------------------------------------------------------
int TDataLayerImpl::GrandRole(const std::string &role, const std::string &user)
{
    int ret(0);
    std::string sql;

    sql = "GRANT " + role + " to " + user + " ; ";
    //sql = "GRANT :role to :user ; ";
    //q->SetParam("role", role);
    //q->SetParam("user", user);
    //pthread_mutex_lock(&mutex);
    q->SetNewQuery(sql, IBPP::amWrite);

    if (q->exec())
    {
        q->commit();
        ret = 1;
    }
    else
    {
        ret                      = -1;
        SQLException_SqlCode     = q->SQLException_SqlCode;
        SQLException_EngineCode  = q->SQLException_EngineCode;
        errorstr                 = q->errorstr;
        logtxt("Error: TDataLayerImpl::GrandRole: " + errorstr);
    }

    //pthread_mutex_unlock(&mutex);
    return (ret);
}
//--------------------------------------------------------------------------
int TDataLayerImpl::CarS(Wt::WAbstractItemModel * mt, const int line_id)
{
    int ret (-1);
    std::string sql;

    errorstr.clear();

    sql  = "select * ";
    sql += "from car_s(:line_id) ";

    //pthread_mutex_lock(&mutex);

    q->SetNewQuery(sql, IBPP::amRead);
    q->SetParam("line_id", line_id);

    if (!q->exec())
    {
        ret      = -1;
        errorstr = q->errorstr;
        logtxt("Error: " + errorstr);
    }
    else
    {
        ret = q->st_to_mt(mt);
        if (q->errorstr.length() > 0)
        {
            errorstr = q->errorstr;
            logtxt("Error: " + errorstr);
            ret = -1;
        }
        q->commit();
    } // else

      //pthread_mutex_unlock(&mutex);

    return (ret);
}
//--------------------------------------------------------------------------
std::string TDataLayerImpl::cookieKey(const std::string& key)
{
 std::string ret(key);
 size_t p;
 while((p = ret.find(".")) != std::string::npos) {ret.replace(p, 1, "_");}
 while((p = ret.find(" ")) != std::string::npos) {ret.replace(p, 1, "_");}
 while((p = ret.find(":")) != std::string::npos) {ret.replace(p, 1, "_");}
 while((p = ret.find("*")) != std::string::npos) {ret.replace(p, 1, "_");}

 return(ret);
}
//--------------------------------------------------------------------------
/*void TDataLayerImpl::setParam(const std::string name, const std::string value, const int type)
{
 switch (type)
 {
  case 7: // SMALLINT
  case 8: // INTEGER
  {
   int v(0);
   try{v = std::stoi(value);}
   catch(...){}
   q->SetParam(name, v);
   break;
  }
  case 16: // BIGINT
  {
   int64_t v(0);
   try{v = std::stol(value);}
   catch(...){}
   q->SetParam(name, v);
   break;
  }

  case 35: // TIMESTAMP
  default:
    q->SetParam(name, value);
 }
}*/
//--------------------------------------------------------------------------
void TDataLayerImpl::clearProc()
{
 for(auto itr = proc.begin(); itr != proc.end(); ++itr)
 {
  delete(itr->second);
 }
 proc.clear();
 dtProc = Wt::WDateTime();
}
void TDataLayerImpl::checkProc()
{
 if (proc.empty() || dtProc.addSecs(300) < Wt::WDateTime::currentDateTime())
   refreshProc();
}
void TDataLayerImpl::refreshProc()
{
 clearProc();

 std::string sql;

 sql = "select upper(p.RDB$PROCEDURE_NAME) as PROCEDURE_NAME, p.RDB$PROCEDURE_TYPE, lower(pp.rdb$parameter_name) as PARAM_NAME, ";
 sql += "pp.rdb$parameter_number as PARAM_NUMBER, pp.rdb$parameter_type as PARAM_TYPE ";
 sql += "from RDB$PROCEDURES p ";
 sql += "LEFT join rdb$procedure_parameters pp on (pp.rdb$procedure_name = p.RDB$PROCEDURE_NAME /* and pp.rdb$parameter_type = 0*/) ";
 sql += "left join rdb$fields d on (pp.rdb$field_source = d.rdb$field_name) ";
 sql += "order by p.RDB$PROCEDURE_NAME, PARAM_NUMBER asc ";

 //pthread_mutex_lock(&mutex);

 q->SetNewQuery(sql, IBPP::amRead);

 if (q->exec())
 {
  while (q->Fetch())
  {
   TProcedure * pr; std::string proc_name(""), par_name(""); int type(1), par_type(0);
   q->GetValue("PROCEDURE_NAME", proc_name);
   q->GetValue("RDB$PROCEDURE_TYPE", type);
   q->GetValue("PARAM_NAME", par_name);
   q->GetValue("PARAM_TYPE", par_type);
   boost::trim(proc_name);
   boost::trim(par_name);
   auto itr = proc.find(proc_name);
   if (itr == proc.end())
   {
    pr = new TProcedure();
    pr->name = proc_name;
    pr->type = type;
    proc.insert(std::make_pair(pr->name, pr));
   }
   else pr = itr->second;
   if (!par_name.empty())
   {
    if (par_type == 0)
     pr->in_param.push_back(par_name);
    else
     pr->out_param.push_back(par_name);
   }
  } // while
  errorstr = q->errorstr;
  q->commit();
 }
}
TProcedure * TDataLayerImpl::getProc(const std::string& proc_name)
{
 std::string name(Upper(proc_name));
 boost::lock_guard<boost::mutex> guard(mtxProc);
 checkProc();
 auto itr = proc.find(name);
 if (itr != proc.end() && itr->first == name)
   return(itr->second);
 else
   return(NULL);
}
//--------------------------------------------------------------------------
std::string TDataLayerImpl::getCookie(const std::string& key, const std::string& def)
{
 std::string name(cookieKey(key));
 auto itr = cookie.find(name);
 if (itr != cookie.end() && itr->first == name)
 {
  return (itr->second);
 }

 try{
  const std::string * s = Wt::WApplication::instance()->environment().getCookieValue(name);
  if (s != NULL && !s->empty())
  {
   cookie[name] = *s;
   return(*s);
  }
 }
 catch(...){}

 return(def);
}
//--------------------------------------------------------------------------
void TDataLayerImpl::setCookie(const std::string& key, const std::string& value)
{
 std::string name(cookieKey(key));
 auto itr = cookie.find(name);
 if (itr != cookie.end() && itr->first == name && itr->second == value)
 {
  return;
 }

 cookie[name] = value;

 Wt::WApplication::instance()->setCookie(name, value, 3000000);
}
//--------------------------------------------------------------------------
void TDataLayerImpl::saveColumnWidth(const Wt::WAbstractItemView * view, const std::string& pref)
{
 std::string param(pref + "_" + view->objectName());

 if (view == 0 || view->model() == 0) return;

 for(int i = 0; i < view->model()->columnCount(); i++)
 {
  std::string column("");
  double width = view->columnWidth(i).value();
  std::stringstream stm;
  stm << int(width);
  column = to_string(view->model()->headerData(i));
  if (column.empty())
  {std::stringstream stm; stm << "column_" << i; column = stm.str();}

  setCookie(param + "_" + column, stm.str());
 }
}
void TDataLayerImpl::setColumnWidth(Wt::WAbstractItemView * view, const std::string& pref)
{
 std::string param(pref + "_" + view->objectName());

 if (view == 0 || view->model() == 0) return;

 for(int i = 0; i < view->model()->columnCount(); i++)
 {
  std::string column;
  int width(0);
  column = to_string(view->model()->headerData(i));
  if (column.empty())
  {std::stringstream stm; stm << "column_" << i; column = stm.str();}

  try
  {
   std::string s(getCookie(param + "_" + column));
   if (!s.empty()) width = std::stoi(s);
  }
  catch(...){continue;}

  if (width > 0) {view->setColumnWidth(i, Wt::WLength(width));}
 }
}

int TDataLayerImpl::TaskStateID(const int code)
{
 if (mTaskState.size() == 0)
 {
     Wt::WStandardItemModel mt;
     DIRLineS(&mt, 21);
     for(int i = 0; i < mt.rowCount(); ++i)
     {
         int code = to_int(mt.data(i, fieldByName(&mt, "CODE")));
         int line_id = to_int(mt.data(i, fieldByName(&mt, "LINE_ID")));
         mTaskState.insert(std::pair<int, int>(code, line_id));
     }
 }
 auto itr = mTaskState.find(code);
 return itr != mTaskState.end() && itr->first == code ? itr->second : 0;
}
//--------------------------------------------------------------------------
/*
bool TDocOneItem::init(const Wt::WAbstractItemModel * mt, const int row)
{
 int f;
 bool ret(true);

 if (mt->rowCount() < row) return(false);

#define CPFIELD( variable, name , variable_type) \
 f = fieldByName(mt, name); \
 if (f > -1 && typeid(variable_type) == mt->data(row, f).type()) \
 {variable = boost::any_cast<variable_type>(mt->data(row, f));} \
 else {ret = false;}
#define CPFIELDFLOAT( variable, name) \
 f = fieldByName(mt, name); \
 if (f > -1 && typeid(TNumeric) == mt->data(row, f).type()) \
 {Currency c ( boost::any_cast<TNumeric>(mt->data(row, f)) ); \
 variable = float(c); } \
 else {ret = false;}
#define CPFIELDSTR( variable, name ) \
 f = fieldByName(mt, name); \
 if (f > -1 && typeid(Wt::WString) == mt->data(row, f).type()) \
 {variable = boost::any_cast<Wt::WString>(mt->data(row, f)).toUTF8();} \
 else {ret = false;}
#define CPFIELDDATE( variable, name ) \
 f = fieldByName(mt, name); \
 if (f > -1 && typeid(Wt::WDate) == mt->data(row, f).type()) \
 {variable = boost::any_cast<Wt::WDate>(mt->data(row, f));} \
 else {ret = false;}

 CPFIELD( doc_id, "doc_id", int );
 CPFIELD( item_id, "item_id", int );
 CPFIELDSTR( number, "number" );
 CPFIELDSTR( obj_catalog_name, "obj_catalog_name" );
 CPFIELD( nom_id, "nom_id", int );
 CPFIELD( pack_id, "pack_id", int );
 CPFIELDSTR( nom_name, "nom_name" );
 CPFIELDSTR( nom_sheet, "nom_sheet" );
 CPFIELD( manufacturer, "manufacturer", int );
 CPFIELDDATE( man_date, "man_date" );
 CPFIELDSTR( man_name, "man_name" );
 CPFIELDSTR( sn_text, "sn_text" );
 CPFIELD( firm_owner, "firm_owner", int );
 CPFIELD( firm_in, "firm_in", int );
 CPFIELDSTR( firm_in_name, "firm_in_name" );
 CPFIELD( firm_out, "firm_out", int );
 CPFIELDSTR( firm_out_name, "firm_out_name" );
 CPFIELDSTR( firm_out_email, "firm_out_email" );
 CPFIELD( storage_in, "storage_in", int );
 CPFIELD( storage_out, "storage_out", int );
 CPFIELDDATE( date_doc, "date_doc" );
 CPFIELDSTR( descript, "descript" );
 CPFIELD( repair, "repair", int );
 CPFIELD( calibr, "calibr", int );
 CPFIELD( test, "test", int );
 CPFIELDFLOAT( in_cnt, "in_cnt" );
// CPFIELDFLOAT( str_cnt, "str_cnt" );

  f = fieldByName(mt, "str_cnt");
 if (f > -1 && typeid(TNumeric) == mt->data(row, f).type())
 {Currency c ( boost::any_cast<TNumeric>(mt->data(row, f)) );
 str_cnt = float(c); }
 else {ret = false;}

 CPFIELDFLOAT( all_cnt, "all_cnt" );
 CPFIELDSTR( doc_user_name, "doc_user_name" );
 CPFIELDSTR( very_user_name, "very_user_name" );
 CPFIELDDATE( date_verify, "date_verify" );
 CPFIELDDATE( date_verify_next, "date_verify_next" );
 CPFIELDSTR( verify_description, "verify_description" );
 CPFIELD( nom_pack_verify_id, "nom_pack_verify_id", int );
 CPFIELD( eqp_list_id, "eqp_list_id", int );

#undef CPFIELD
#undef CPFIELDFLOAT
#undef CPFIELDSTR
#undef CPFIELDDATE

 return(ret);
}
*/
//--------------------------------------------------------------------------
TSQLayer::TSQLayer()
{
 db = NULL;
 zErrMsg = 0;
 nameid.clear();
 first_value_length = 0; second_value_length = 0;
 trN = 0;
}
TSQLayer::~TSQLayer()
{
 stop();
 close();
}
//--------------------------------------------------------------------------
int TSQLayer::open()
{
 int ret(0);

#ifdef _MSC_VER
 char tmppath[MAX_PATH];
 memset(tmppath, 0, sizeof(tmppath));
 if (GetTempPathA(MAX_PATH, tmppath) > 0)
 {
  sqlite3_temp_directory = sqlite3_mprintf("%s", tmppath);
 }
 else
 {
  ShowMessage(0, Wt::WString::fromUTF8("Ошибка получения пути к папке временных файлов.\nОбратитесь с системному администратору."));
 }
#else
 sqlite3_temp_directory = sqlite3_mprintf("/tmp");
#endif

 boost::lock_guard<boost::mutex> guard(mtx);
 ret = sqlite3_open_v2(dbname.c_str(), &db, SQLITE_OPEN_READWRITE, NULL);

 if (ret == SQLITE_CANTOPEN)
 {
  ret = sqlite3_open_v2(dbname.c_str(), &db, SQLITE_OPEN_READWRITE | SQLITE_OPEN_CREATE, NULL);
  if (ret == SQLITE_OK) ret = create_struct();
 }

 if (ret == SQLITE_OK) ret = check_version();

// init_length();

 return(ret);
}
bool TSQLayer::isOpen()
{
 boost::lock_guard<boost::mutex> guard(mtx);
 return (db != NULL);
}
//--------------------------------------------------------------------------
void TSQLayer::close()
{
 boost::lock_guard<boost::mutex> guard(mtx);
 sqlite3_close(db);
}
//--------------------------------------------------------------------------
int TSQLayer::setQuery(const std::string& sql)
{
 int ret;
 sqlquery = sql;

 ret = sqlite3_prepare_v2(db, sqlquery.c_str(), sqlquery.length(), &st, NULL);
 return (ret);
}
//--------------------------------------------------------------------------
int TSQLayer::setParam(const std::string& name, const std::string& v)
{
 int ret;
 int parameter_idx;
 std::string param(std::string(":") + name);

 parameter_idx = sqlite3_bind_parameter_index(st, param.c_str());
 if (parameter_idx == 0)
 {
  return(-1);
 }
 ret = sqlite3_bind_text(st, parameter_idx, v.c_str(), v.length(), SQLITE_TRANSIENT);
 return(ret);
}
//--------------------------------------------------------------------------
int TSQLayer::setParam(const std::string& name, const int v)
{
 int ret;
 int parameter_idx;
 std::string param(std::string(":") + name);
 parameter_idx = sqlite3_bind_parameter_index(st, param.c_str());
 if (parameter_idx == 0)
 {
  return(-1);
 }
 ret = sqlite3_bind_int(st, parameter_idx, v);
 return(ret);
}
//--------------------------------------------------------------------------
int TSQLayer::exec(Wt::WAbstractItemModel * mt)
{
 int ret(0);

 while(sqlite3_step(st) == SQLITE_ROW)
 {
  st_to_mt(mt, st);
  ret++;
 }

 sqlite3_finalize(st);

 return(ret);
}
//--------------------------------------------------------------------------
bool TSQLayer::exec()
{
 bool ret(false);

 if(sqlite3_step(st) == SQLITE_DONE)
 {
  ret = true;
 }

 sqlite3_finalize(st);

 return(ret);
}
int TSQLayer::begin(void)
{
 if (trN == 0)
 {
  std::string sql("BEGIN TRANSACTION;");
  int ret = sqlite3_exec(db, sql.c_str(), NULL, NULL, NULL);
 }
 trN++;
 return(trN);
}
int TSQLayer::commit(void)
{
 trN--;
 if (trN == 0)
 {
   std::string sql("COMMIT;");
   int ret = sqlite3_exec(db, sql.c_str(), NULL, NULL, NULL);
 }
 if (trN < 0)
 {
  trN = 0;
 }
 return (trN);
}
void TSQLayer::rollback(void)
{
 std::string sql("ROLLBACK;");
 int ret = sqlite3_exec(db, sql.c_str(), NULL, NULL, NULL);
 trN = 0;
}
//--------------------------------------------------------------------------
std::string TSQLayer::errmsg()
{
 return(sqlite3_errmsg(db));
}
//--------------------------------------------------------------------------
int TSQLayer::st_to_mt(Wt::WAbstractItemModel * mt, sqlite3_stmt * st) const
{
 int ret(0);
 int cols, row;

 if (mt == NULL) return (-1);

 cols = sqlite3_column_count(st);

 if (mt->columnCount() == 0)
 {
  //  Заголовки
  for(int i = 0; i < cols; i++)
  {
   std::string name(std::string(sqlite3_column_name(st, i)));
   mt->insertColumn(i);
   mt->setHeaderData(i, Wt::Orientation::Horizontal, name);
  } // for
 }

 row = mt->rowCount();
 mt->insertRow(row);
// Данные
 for(int i = 0; i < cols; i++)
 {
  switch(sqlite3_column_type(st, i))
  {
   case SQLITE_INTEGER:
   { int v(sqlite3_column_int(st, i));
     mt->setData(row, i, v); break;}
   case SQLITE_FLOAT:
   { double v(sqlite3_column_double(st, i));
     mt->setData(row, i, v); break;}
   case SQLITE_TEXT:
   { std::string s((const char *)sqlite3_column_text(st, i), sqlite3_column_bytes(st, i));
     mt->setData(row, i, s); break;}
   case SQLITE_BLOB:
   default /*SQLITE_NULL*/:;
  }
 }

 return(ret);
}
//--------------------------------------------------------------------------
int TSQLayer::create_struct()
{
 extern TParameterList params;
 int ret(0);
 std::string sql;
 std::string sqlfile; // docroot
 if (Wt::WApplication::instance() != NULL) sqlfile = (Wt::WApplication::instance()->docRoot());
 else sqlfile = params.get("docroot")->v_str + "/";

 sqlfile += "sql/sqlayer.sql";

 sql = read_file(sqlfile);

 if (!sql.empty())
 {
 //  ret = setQuery(sql);
 //  ret = exec();
   ret = sqlite3_exec(db, sql.c_str(), NULL, NULL, NULL);
 }

 return(ret);
}
//--------------------------------------------------------------------------
int TSQLayer::check_version()
{
 int ret(0);
 return(ret);
}
//--------------------------------------------------------------------------
std::string TSQLayer::read_file(const std::string& filename)
{
 std::stringstream stm;
 std::ifstream src;
 char buff[4096];

 src.open(filename.c_str());
 if (!src.is_open()) {return (std::string()); }

// src.seekg (0, src.beg);
 while(!src.eof())
 {
  memset(buff, 0, 4096);
  src.read(buff, 4096);           // чтение блока
  if(src.gcount() > 0) stm << buff;
 }

 src.close();

 return(stm.str());
}
//--------------------------------------------------------------------------
void TSQLayer::init_length()
{
 int l;

 l = table_columnt_length("first", "value");
 if (l > 0) first_value_length = l;

 l = table_columnt_length("second", "value");
 if (l > 0) second_value_length = l;
}
//--------------------------------------------------------------------------
//int TSQLayer::profile_group_id()
//{
// boost::lock_guard<boost::mutex> guard(mtx);
// if (fprofile_group_id < 0)
// {
//  std::string sql;
//  sql = "select max(profile_group_id) from profile_group ";
//  setQuery(sql);
//  if(sqlite3_step(st) == SQLITE_ROW)
//  {
//   fprofile_group_id = sqlite3_column_int(st, 0);
//  }
//  else fprofile_group_id = 0;
//  sqlite3_finalize(st);
// }
// return(++fprofile_group_id);
//}
//--------------------------------------------------------------------------
int TSQLayer::profile_group_s(Wt::WAbstractItemModel * mt, const std::string& name)
{
 int ret(-1);
 if (!isOpen()) return(ret);
 std::string sql;

 sql = "select profile_group_id, name from profile_group ";
 if (!name.empty())
   sql += "where name = :name ";

 boost::lock_guard<boost::mutex> guard(mtx);

 setQuery(sql);
 if (!name.empty())
   if (setParam("name", name) != SQLITE_OK) return(ret);
 ret = exec(mt);

 return(ret);
}
//--------------------------------------------------------------------------
int TSQLayer::profile_group_iu(const int profile_group_id, const std::string& name)
{
 int ret(-1);
 if (!isOpen()) return(ret);
 std::string sql;

 int id(profile_group_id);

 if (id <= 0)
 {
  id = get_id("profile_group");
  sql = "insert into profile_group(profile_group_id, name) ";
  sql+= "values(:profile_group_id, :name) ";
  }
 else
 { sql = "update profile_group set name = :name where profile_group_id = :profile_group_id "; }

 boost::lock_guard<boost::mutex> guard(mtx);

 setQuery(sql);
 setParam("profile_group_id", id);
 if (setParam("name", name) != SQLITE_OK) return(ret);
 if (exec()) ret = id;

 return(ret);
}
//--------------------------------------------------------------------------
int TSQLayer::get_id(const std::string& pname)
{
 std::string name(Upper(pname));

 if (!isOpen()){return (-1);}

 boost::lock_guard<boost::mutex> guard(mtx);

 if (nameid.find(name) == nameid.end())
 {
  nameid[name] = 0;
  std::string sql;
  sql = "select max(" + name +"_id) from " + name;
  setQuery(sql);
  if(sqlite3_step(st) == SQLITE_ROW)
  {
   nameid[name] = sqlite3_column_int(st, 0);
  }
  sqlite3_finalize(st);
 }
 return(++nameid[name]);
}
//--------------------------------------------------------------------------
int TSQLayer::first_s(Wt::WAbstractItemModel * mt, const std::string& value)
{
 int ret(-1);
 if (!isOpen()) return(ret);
 std::string sql;

 sql = "select first_id, value from first ";
 if (!value.empty())
   sql += "where value = :value ";

 boost::lock_guard<boost::mutex> guard(mtx);

 setQuery(sql);
 if (!value.empty())
 {
  ret = setParam("value", value);
 }
 if (ret == SQLITE_OK)
 {
  ret = exec(mt);
 }

 return(ret);
}
//--------------------------------------------------------------------------
int TSQLayer::first_s(Wt::WAbstractItemModel * mt, const std::list<std::string>& value)
{
 int ret(-1);
 if (!isOpen()) return(ret);
 std::string sql;

 sql = "select first_id, value from first ";
 if (!value.empty())
 {
  std::stringstream stm;
  sql += "where value in (";
  for (int i = 0; i < value.size(); i++)
  {
   if (i > 0) stm << ",";
   stm << ":" << i << "value";
  }
  stm << ")";
  sql += stm.str();
 }

 boost::lock_guard<boost::mutex> guard(mtx);

 setQuery(sql);
 if (!value.empty())
 {
  int i(0);
  for(auto itr = value.begin(); itr != value.end(); ++itr, i++)
  {
   std::stringstream stm;
   stm << i << "value";
   ret = setParam(stm.str(), *itr);
  }
 }
 if (ret == SQLITE_OK)
 {
  ret = exec(mt);
 }

 return(ret);
}
//--------------------------------------------------------------------------
int TSQLayer::first_iu(const int first_id, const std::string& value)
{
 std::string sql;
 if (!isOpen()) return(-1);
 int id(first_id);

 if (id <= 0)
 {
  id = get_id("first");
  sql = "insert into first(first_id, value) values(:first_id, :value) ";
 }
 else
 { sql = "update first set value = :value where first_id = :first_id "; }

 boost::lock_guard<boost::mutex> guard(mtx);

 setQuery(sql);
 setParam("first_id", id);
 if (setParam("value", value) != SQLITE_OK) return(-1);
 if (exec()) return(id);
 else return(-1);
}
//--------------------------------------------------------------------------
int TSQLayer::second_s(Wt::WAbstractItemModel * mt, const std::string& value)
{
 int ret(-1);
 if (!isOpen()) return(ret);
 std::string sql;

 sql = "select second_id, value from second ";
// if (!value.empty())
 sql += "where value = :value ";

 boost::lock_guard<boost::mutex> guard(mtx);

 setQuery(sql);
 //if (!value.empty())
 if (setParam("value", value) == SQLITE_OK)
 {
  ret = exec(mt);
 }

 return(ret);
}
//--------------------------------------------------------------------------
int TSQLayer::second_s(Wt::WAbstractItemModel * mt, const std::list<std::string>& value)
{
 int ret(-1);
 if (!isOpen()) return(ret);
 std::string sql;

 sql = "select second_id, value from second ";
 if (!value.empty())
 {
  std::stringstream stm;
  sql += "where value in (";
  for (int i = 0; i < value.size(); i++)
  {
   if (i > 0) stm << ",";
   stm << ":" << i << "value";
  }
  stm << ")";
  sql += stm.str();
 }

 boost::lock_guard<boost::mutex> guard(mtx);

 setQuery(sql);
 if (!value.empty())
 {
  int i(0);
  for(auto itr = value.begin(); itr != value.end(); ++itr, i++)
  {
   std::stringstream stm;
   stm << i << "value";
   ret = setParam(stm.str(), *itr);
  }
 }
 if (ret == SQLITE_OK)
 {
  ret = exec(mt);
 }

 return(ret);
}
//--------------------------------------------------------------------------
int TSQLayer::second_iu(const int first_id, const std::string& value)
{
 std::string sql;
 if (!isOpen()) return(-1);
 int id(first_id);

 if (id <= 0)
 {
  id = get_id("second");
  sql = "insert into second(second_id, value) values(:second_id, :value) ";
 }
 else
 { sql = "update second set value = :value where second_id = :second_id "; }

 boost::lock_guard<boost::mutex> guard(mtx);

 setQuery(sql);
 setParam("second_id", id);
 if (setParam("value", value) != SQLITE_OK) return(-1);
 if (exec()) return(id);
 else return(-1);
}
//--------------------------------------------------------------------------
int TSQLayer::profile_item_s(Wt::WAbstractItemModel * mt, const int group_id, const int first_id, const int second_id)
{
 int ret(-1);
 if (!isOpen()) return(ret);
 std::string sql, sql2("");

 sql = "select pi.profile_item_id, pi.profile_group_id, pg.name, pi.first_id, f.value as first, ";
 sql+= "pi.second_id, s.value as second ";
 sql+= "from profile_item pi ";
 sql+= "left join profile_group pg on (pg.profile_group_id = pi.profile_group_id) ";
 sql+= "left join first f on (f.first_id = pi.first_id) ";
 sql+= "left join second s on (s.second_id = pi.second_id) ";

 sql2 += "pi.profile_group_id = :group_id ";
 if (first_id > 0)
 {
  if (!sql2.empty()) sql2 += " and ";
  sql2 += "pi.first_id = :first_id ";
 }
 if (second_id > 0)
 {
  if (!sql2.empty()) sql2 += " and ";
  sql2 += "pi.second_id = :second_id ";
 }

 if (!sql2.empty())
   sql += " where " + sql2;

 boost::lock_guard<boost::mutex> guard(mtx);

 setQuery(sql);
 setParam("group_id", group_id);
 if (first_id > 0) setParam("first_id", first_id);
 if (second_id > 0) setParam("second_id", second_id);
 ret = exec(mt);

 return(ret);
}
//--------------------------------------------------------------------------
int TSQLayer::profile_item_iu(const int profile_item_id, const int profile_group_id,
                              const int first_id, const int second_id)
{
 if (!isOpen()) return(-1);
 std::string sql;
 int id(profile_item_id);

 if (id <= 0)
 {
  id = get_id("profile_item");
  sql = "insert into profile_item(profile_item_id, profile_group_id, first_id, second_id) ";
  sql +="values(:profile_item_id, :profile_group_id, :first_id, :second_id) ";
 }
 else
 {
  sql = "update second set profile_group_id = :profile_group_id, first_id = :first_id, ";
  sql +="second_id = :second_id where profile_item_id = :profile_item_id ";
 }

 boost::lock_guard<boost::mutex> guard(mtx);

 setQuery(sql);
 setParam("profile_item_id", id);
 setParam("profile_group_id", profile_group_id);
 setParam("first_id", first_id);
 setParam("second_id", second_id);
 if (exec()) return(id);
 else return(-1);
}
//--------------------------------------------------------------------------
bool TSQLayer::profileSaveValue(const std::string& value, const std::string& param,
                                const std::string& group, const std::string& pref)
{
 bool ret(false);
 if (!isOpen()) return(ret);
 Wt::WStandardItemModel mt_profile_group; int profile_group_id(0);
 Wt::WStandardItemModel mt_first; int first_id(0); std::string first(pref + "_" + param);
 Wt::WStandardItemModel mt_second; int second_id(0);
 Wt::WStandardItemModel mt_profile_item; int profile_item_id(0);

 if (profile_group_s(&mt_profile_group, group) > 0)
 {
  profile_group_id = to_int(mt_profile_group.data(0, fieldByName(&mt_profile_group, "profile_group_id")));
 }
 else
 {
  profile_group_id = profile_group_iu(0, group);
 }

 if (first_s(&mt_first, first) > 0)
 {
  first_id = to_int(mt_first.data(0, fieldByName(&mt_first, "first_id")));
 }
 else
 {
  first_id = first_iu(0, first);
 }

 if (second_s(&mt_second, value) > 0)
 {
  second_id = to_int(mt_second.data(0, fieldByName(&mt_second, "second_id")));
 }
 else
 {
  second_id = second_iu(0, value);
 }

 if (profile_item_s(&mt_profile_item, profile_group_id, first_id) > 0)
 {
  profile_item_id = to_int(mt_profile_item.data(0, fieldByName(&mt_profile_item, "profile_item_id")));
 }

 if (profile_group_id >0 && first_id < 0 && second_id > 0)
 {
  ret = profile_item_iu(profile_item_id, profile_group_id, first_id, second_id);
 }

 return(ret);
}
//--------------------------------------------------------------------------
int TSQLayer::event_i()
{
 if (!isOpen()) return(-1);
 std::string sql;
 int id(0);

 id = get_id("event");
 sql = "insert into event(event_id) ";
 sql +="values(:event_id) ";

 boost::lock_guard<boost::mutex> guard(mtx);

 setQuery(sql);
 setParam("event_id", id);
 if (exec()) return(id);
 else return(-1);
}
//--------------------------------------------------------------------------
int TSQLayer::event_item_i(const int event_id, const int first_id, const int second_id)
{
 if (!isOpen()) return(-1);
 std::string sql;
 int id(get_id("event_item"));

 sql = "insert into event_item(event_item_id, event_id, first_id, second_id) ";
 sql +="values(:event_item_id, :event_id, :first_id, :second_id) ";

 boost::lock_guard<boost::mutex> guard(mtx);

 setQuery(sql);
 setParam("event_item_id", id);
 setParam("event_id", event_id);
 setParam("first_id", first_id);
 setParam("second_id", second_id);
 if (exec()) return(id);
 else return(-1);
}
//--------------------------------------------------------------------------
/*select e.event_id, e.dt, f.value as first, s.value as second
from event e
left join event_item ei on (ei.event_id = e.event_id)
left join first f on (f.first_id = ei.first_id)
left join second s on (s.second_id = ei.second_id)
order by e.event_id DESC*/
int TSQLayer::stateSave(const TState * state, int event_id)
{
 int i, f1, f2;
 int first_id; std::string first;
 int second_id; std::string second;
 Wt::WStandardItemModel mt_first, mt_second;

 std::list<std::string> lst_first, lst_second;
 if (!isOpen()) return(-1);

 if (state->empty()) return(event_id);

 for(auto itr = state->begin(); itr != state->end(); ++itr)
 {
  lst_first.push_back(itr->first);
  lst_second.push_back(itr->second);
 }

 begin();

 if (event_id <= 0)
   event_id = event_i();
 if (event_id < 0) {rollback(); return(event_id);}

 first_s(&mt_first, lst_first);
 second_s(&mt_second, lst_second);

 f1 = fieldByName(&mt_first, "value");
 f2 = fieldByName(&mt_second, "value");

 for(auto itr = state->begin(); itr != state->end(); ++itr)
 {
  first_id = -1; first = itr->first;
  second_id = -1; second = itr->second;

  for(i = 0; i < mt_first.rowCount(); i++)
  {
   if (to_string(mt_first.data(i, f1)) == first)
   {
    first_id = to_int(mt_first.data(i, fieldByName(&mt_first, "first_id")));
    break;
   }
  }
  if (first_id == -1) first_id = first_iu(0, first);

  for(i = 0; i <mt_second.rowCount(); i++)
  {
   if (to_string(mt_second.data(i, f2)) == second)
   {
    second_id = to_int(mt_second.data(i, fieldByName(&mt_second, "second_id")));
    break;
   }
  }
  if (second_id == -1) second_id = second_iu(0, second);

  if (event_id > 0 && first_id > 0 && second_id > 0)
  {
   if (event_item_i(event_id, first_id, second_id) < 0) {rollback(); return(-1);}
  }
 }

 commit();
 return(event_id);
}
//--------------------------------------------------------------------------
int TSQLayer::stateSave_a(const TState * state, int event_id)
{
 if (event_id <= 0)
  event_id = event_i();

 push_state(state, event_id);

 return(event_id);
}
//--------------------------------------------------------------------------
int TSQLayer::event_count(const std::string& second_value)
{
 int ret(-1);
 if (!isOpen()) return(ret);

 std::string sql;

 sql = "select count(*) from event ";

  if (!second_value.empty())
 {
 sql +="left join event_item ei on (ei.event_id = e.event_id) ";
 sql +="left join second s on (ei.second_id = s.second_id) ";
 sql +="where s.value like :second_value ";
 }

 if (setQuery(sql) != SQLITE_OK) { return(ret); }

 if (!second_value.empty())
 {
  setParam("second_value", '%' + second_value + '%');
 }

 if(sqlite3_step(st) == SQLITE_ROW)
 {
  if (sqlite3_column_type(st, 0) == SQLITE_INTEGER)
  { ret = sqlite3_column_int(st, 0); }
 }
 else
 {ret = 0;}

 sqlite3_finalize(st);

 return(ret);
}
//--------------------------------------------------------------------------
int TSQLayer::event_s(Wt::WAbstractItemModel * mt, const int offset, const int limit, const std::string& second_value)
{
 int ret(-1);
 if (!isOpen()) return(ret);
 std::string sql, s;

// boost::posix_time::ptime t0(boost::posix_time::microsec_clock::local_time());
// TState state;
// state["_"] = __PRETTY_FUNCTION__;

 sql = "select e.event_id, e.dt, 0 as pair ";
 sql +="from event e ";

 if (!second_value.empty())
 {
 sql +="left join event_item ei on (ei.event_id = e.event_id) ";
 sql +="left join second s on (ei.second_id = s.second_id) ";
 sql +="where s.value like :second_value ";
 }

 sql +="order by e.event_id DESC ";

 if (limit > 0)
 {
  sql += "limit :limit ";
  if (offset > 0)
  {
   sql += "offset :offset ";
  }
 }

 if (setQuery(sql) != SQLITE_OK) { return(ret); }

 if (limit > 0)
 { setParam("limit", limit);
 if (offset > 0) setParam("offset", offset);}

 if (!second_value.empty())
 {
  setParam("second_value", '%' + second_value + '%');
 }

 ret = exec(mt);

 Wt::WStandardItemModel * m2;

 sql = "select f.value as first, s.value as second ";
 sql +="from event_item ei  ";
 sql +="left join first f on (f.first_id = ei.first_id) ";
 sql +="left join second s on (s.second_id = ei.second_id) ";
 sql +="where ei.event_id = :event_id ";

 for(int i = 0; i < mt->rowCount(); i++)
 {
  if (setQuery(sql) != SQLITE_OK) { return(-1); }
  setParam("event_id", to_int(mt->data(i, 0)));
  m2 = new Wt::WStandardItemModel();
  if (exec(m2) > 0)
  {
   s.clear();
   for(int i2 = 0 ; i2 < m2->rowCount(); i2++)
   {
    if (!s.empty()) s += " ";
    s += to_string(m2->data(i2, 0)) + "=" +  to_string(m2->data(i2, 1));
   }
   mt->setData(i, 2, s);
  }
  delete(m2);
 }

//  boost::posix_time::time_duration td;
//  td = boost::posix_time::microsec_clock::local_time() - t0;
//  std::stringstream stm;
//  stm << td.total_milliseconds();
//  state["exec_time"] = stm.str();
//  logtxt(&state);
/*CREATE INDEX idx_event_item_second_id ON event_item (
    second_id
);
*/
 return(ret);
}
//--------------------------------------------------------------------------
int TSQLayer::table_info(Wt::WAbstractItemModel * mt, const std::string& table)
{
 if (table.empty() || !isOpen()) return(-1);
 std::string sql;

 sql = "PRAGMA table_info('" + table + "')";

 setQuery(sql);
// setParam("table", table);

 return(exec(mt));
}
//--------------------------------------------------------------------------
int TSQLayer::table_columnt_length(const std::string& table, const std::string& column)
{
 int ret(-1);
 Wt::WStandardItemModel first;
 if (table_info(&first, table) > 0)
 {
  int fname(fieldByName(&first, "name"));
  for(int i = 0; i < first.rowCount() && fname != -1; i++)
  {
   if (Upper(to_string(first.data(first.index(i, fname)))) == Upper(column))
   {
    std::string s = Upper(to_string(first.data(first.index(i, fieldByName(&first, "type")))));
    if (s.find("CHAR") != std::string::npos && s.find("(") != std::string::npos && s.find(")") != std::string::npos)
    {
     std::string v = s.substr(s.find("(") + 1, s.find(")") - s.find("(") - 1);
     if (!v.empty())
     {
      try {ret = std::stoi(v);} catch(...){return(ret);}
     }
    }
   }
  }
 }
 return(ret);
}
//--------------------------------------------------------------------------
int TSQLayer::logged_in_iu(const int logged_in_id, const std::string& session,
                 const std::string& db_server, const int db_port, const std::string& db_name,
                 const std::string& db_user, const std::string& db_role, const std::string& db_password)
{
 std::string sql;
 if (!isOpen()) return(-1);
 int id(logged_in_id);

 if (id <= 0)
 {
  id = get_id("logged_in");
  sql = "insert into logged_in(logged_in_id, session, db_server, db_port, db_name, db_user, db_role, db_password) ";
  sql += "values(:logged_in_id, :session, :db_server, :db_port, :db_name, :db_user, :db_role, :db_password) ";
 }
 else
 {
  sql = "update logged_in set session=:session, db_server=:db_server, db_port=:db_port, db_name=:db_name, ";
  sql += "db_user=:db_user, db_role=:db_role, db_password=:db_password, dt=datetime(CURRENT_TIMESTAMP, 'localtime') ";
  sql += "where logged_in_id = :logged_in_id ";
 }

 boost::lock_guard<boost::mutex> guard(mtx);

 setQuery(sql);
 if (setParam("logged_in_id", id) != SQLITE_OK) return(-1);
 if (setParam("session", session) != SQLITE_OK) return(-1);
 if (setParam("db_server", db_server) != SQLITE_OK) return(-1);
 if (setParam("db_port", db_port) != SQLITE_OK) return(-1);
 if (setParam("db_name", db_name) != SQLITE_OK) return(-1);
 if (setParam("db_user", db_user) != SQLITE_OK) return(-1);
 if (setParam("db_role", db_role) != SQLITE_OK) return(-1);
 if (setParam("db_password", db_password) != SQLITE_OK) return(-1);

 if (exec()) return(id);
 else return(-1);
}
//--------------------------------------------------------------------------
int TSQLayer::logged_in_s(Wt::WAbstractItemModel * mt, const std::string& session)
{
 int ret(-1);
 if (!isOpen()) return(ret);
 std::string sql, sql2("");

 sql = "select logged_in_id, session, db_server, db_port, db_name, db_user, db_role, db_password ";
 sql +="from logged_in ";

 if (!session.empty())
 {
  if (!sql2.empty()) sql2 += " and ";
  sql2 += "session = :session ";
 }

 if (!sql2.empty())
   sql += " where " + sql2;

 boost::lock_guard<boost::mutex> guard(mtx);

 setQuery(sql);
 if (!session.empty()) setParam("session", session);
 ret = exec(mt);

 return(ret);
}
//--------------------------------------------------------------------------
void TSQLayer::thread()
{
 TState t; int event_id;
 int pause(5);
 boost::unique_lock<boost::mutex> lock(state_mtx);
 running = true;
 lock.unlock();

 while(true)
 {
  boost::this_thread::yield();
  if (pause > 0) boost::this_thread::sleep(boost::posix_time::milliseconds(pause));
  if (!lock.try_lock()) continue;
  if (!running && state_lst.empty()) {lock.unlock(); break;}
  if (state_lst.empty()) {lock.unlock(); continue;}

  auto itr = state_lst.begin();
  event_id = itr->first;
  t.insert(itr->second.begin(), itr->second.end());
  state_lst.erase(itr);
  pause = state_lst.empty() ? 5 : 0;
  lock.unlock();

  stateSave(&t, event_id);
  t.clear();
 }
}
//--------------------------------------------------------------------------
void TSQLayer::start()
{
 {
  boost::lock_guard<boost::mutex> guard(mtx);
  if (running) return;
 }

 thr = boost::thread(&TSQLayer::thread, this);
}
//--------------------------------------------------------------------------
void TSQLayer::stop()
{
 boost::unique_lock<boost::mutex> lock(state_mtx);
 running = false;
 lock.unlock();
 thr.join();
}
//--------------------------------------------------------------------------
void TSQLayer::push_state(const TState * state, const int event_id)
{
  TState t;
  t.insert(state->begin(), state->end());

  boost::lock_guard<boost::mutex> guard(state_mtx);
  state_lst.insert(std::pair<int, TState>(event_id, t));
}
//--------------------------------------------------------------------------
TDataLayer * DataLayerFactory()
{
 TDataLayer * dl = new TDataLayerImpl();
 /*dl->setHostName("localhost");
 dl->setDatabaseName("ARS");
 dl->setUserName("SYSDBA");
 dl->setPassword("masterke");
 dl->open(); */
 return(dl);
}
//---------------------------------------------------------------------------
TBytea::TBytea():
 size(0), position(0)
{
 data     = 0;
}
//---------------------------------------------------------------------------
TBytea::TBytea(const int64_t new_size):size(0)
{
 set_size(new_size);
}
//---------------------------------------------------------------------------
TBytea::TBytea(const TBytea& orig):
 size(0),
 position(orig.position)
{
 set_size(orig.get_size());
 memcpy(data, orig.data, size);
}
//---------------------------------------------------------------------------
TBytea::~TBytea()
{
 if (data != 0)
 {
  delete [] data;
 }
}
//---------------------------------------------------------------------------
int64_t TBytea::set_size(const int64_t new_size)
{
 unsigned char * new_data, * old_data;

 paddedCharacters = new_size % 3;

 if (new_size == 0)
 {
  free();
  return (0);
 }

 if (size == 0 && new_size > 0)
 {
  data = new unsigned char [new_size + paddedCharacters];
  size = new_size;
 }
 else
 {
  new_data = new unsigned char [new_size + paddedCharacters];
  old_data = data;
  memcpy(new_data, old_data, std::min(new_size, size));
  size = new_size;
  data = new_data;
  delete [] old_data;
 }

 if (paddedCharacters > 0)
 {
  memset(data + size, 0, paddedCharacters);
 }

 return (size);
}
//---------------------------------------------------------------------------
const int64_t TBytea::get_size(void) const
{
 return (size);
}
//---------------------------------------------------------------------------
int64_t TBytea::set_position(const int64_t new_pos)
{
 if (new_pos <= size)
  return (position = new_pos);
 else
  return (position = size);
}
//---------------------------------------------------------------------------
const int64_t TBytea::get_position(void) const
{
 return (position);
}
//---------------------------------------------------------------------------
void TBytea::clear(void)
{
 memset(data, 0, size);
 return ;
}
//---------------------------------------------------------------------------
void TBytea::free(void)
{
 if (data != NULL)
   delete [] data;
 data     = NULL;
 size     = 0;
 position = 0;
 return;
}
//---------------------------------------------------------------------------
// read from stream
int64_t TBytea::read(void * dst, const int64_t buf_size)
{
 int64_t ret(buf_size);

 if ((position + buf_size) > size) ret = size - position;

 memcpy(dst, &data[position], ret);
 position +=ret;

 return (ret);
}
//---------------------------------------------------------------------------
// write to stream
int64_t TBytea::write(void * src, const int64_t buf_size)
{
 int64_t ret(buf_size);

 if (buf_size > (size - position)) set_size(position + buf_size);

 memcpy(&data[position], src, ret);
 position += ret;

 return (ret);
}
//---------------------------------------------------------------------------
int64_t TBytea::load_from_file(const std::string& file_name)
{
 int64_t ret(0), new_size(0);
 std::ifstream fin;

 fin.open(file_name.c_str(), std::ios::binary);
 if (fin.fail())
 {
  std::cout << "Error open file " << file_name << std::endl;
  return(-1);
 }

 if (fin.seekg(0, std::ios::end))
 {
  new_size = fin.tellg();
 }
 else
 {
  // TODO: Error
 }

 set_size(new_size);

 fin.seekg(0);
 fin.read((char *)data, size);
 if (fin.fail())
 {
  std::cout << "Error read file " << file_name << std::endl;
  return(-1);
 }

 fin.close();

 position = 0;
 ret      = size;

 return (ret);
}
//---------------------------------------------------------------------------
int64_t TBytea::save_to_file(const std::string& file_name) const
{
 int64_t ret(0);
 std::ofstream fout;

 fout.open(file_name.c_str(), std::ios::binary);
 if (fout.fail())
 {
  std::cout << "Error open file " << file_name << std::endl;
  return (-1);
 }

 fout.write((char *)data, size);

 fout.close();

 ret = size;
 return (ret);
}
//---------------------------------------------------------------------------
const std::string TBytea::base64(void) const
{
 typedef boost::archive::iterators::base64_from_binary
         <boost::archive::iterators::transform_width<unsigned char *, 6, 8> > base64Iterator;

 std::string encodedString(
      base64Iterator(data),
      base64Iterator(data + size));

 // Add '=' for each padded character used
 for(unsigned int i = 0; i < paddedCharacters; i++)
 {
  encodedString.push_back('=');
 }
 return encodedString;
}
//---------------------------------------------------------------------------
const std::string TBytea::sha1(void) const
{
 boost::uuids::detail::sha1 sha1;
 sha1.reset();
 sha1.process_bytes(data, size);
 return(sha12str(sha1));
}
//---------------------------------------------------------------------------
unsigned char * TBytea::operator[](const int64_t idx) const
{
 unsigned char * ret = NULL;

 if (idx > (size-1) || idx < 0)
 {
  // TODO : Error
 }
 else
 {
  ret = data + idx;
 }

 return(ret);
}
//---------------------------------------------------------------------------
bool TBytea::operator == (const TBytea n)
{
 bool ret = false;

 if (n.size != size) return(ret);

 if (size == 0) return(true);

 for (int64_t i = 0; i < size; i++)
 {
  if (data[i] != n.data[i]) break;
 }
 return (ret);
}
//---------------------------------------------------------------------------
TBytea& TBytea::operator = (const TBytea& r)
{
 set_size(r.get_size());
 memcpy(data, &r.data[0], size);
 position = 0;
 return (*this);
}
//---------------------------------------------------------------------------
TBytea& TBytea::operator = (const std::string& r)
{
 set_size(r.length());
 memcpy(data, r.c_str(), size);
 position = 0;
 return (*this);
}
//---------------------------------------------------------------------------
std::string Upper(const std::string& p)
{
  std::string ret(p);
  transform(ret.begin(), ret.end(), ret.begin(), ::toupper);
  return (ret);
}
std::wstring Upper(const std::wstring p)
{
  std::wstring ret(p);
  transform(ret.begin(), ret.end(), ret.begin(), ::towupper);
  return (ret);
}
std::string Lower(const std::string& p)
{
  std::string ret(p);
  transform(ret.begin(), ret.end(), ret.begin(), ::tolower);
  return (ret);
}
//---------------------------------------------------------------------------

//--------------------------------------------------------------------------
int fieldByName(const Wt::WAbstractItemModel * mt, const std::string& name)
{
 int ret(-1);

 std::string s(name);
 std::transform(s.begin(), s.end(), s.begin(), ::toupper);

 for(int i = 0; i < mt->columnCount(); i++)
 {
//  if (typeid(std::string) == mt->headerData(i).type())
//  {
   std::string data(to_string(mt->headerData(i)));
   std::transform(data.begin(), data.end(), data.begin(), ::toupper);

   if (data == s)
   {
    ret = i;
    break;
   }
//  }

//  if (typeid(Wt::WString) == mt->headerData(i).type())
//  {
//   std::string data(boost::any_cast<Wt::WString>(mt->headerData(i)).toUTF8());
//   std::transform(data.begin(), data.end(), data.begin(), ::toupper);

//   if (data == s)
//   {
//    ret = i;
//    break;
//   }
//  }

//  if (typeid(char *) == mt->headerData(i).type())
//  {
//   std::string data(boost::any_cast<char *>(mt->headerData(i)));
//   std::transform(data.begin(), data.end(), data.begin(), ::toupper);

//   if (data == s)
//   {
//    ret = i;
//    break;
//   }
//  }

 }

 return(ret);
}
//--------------------------------------------------------------------------
bool modelCopy(const Wt::WAbstractItemModel * src, Wt::WAbstractItemModel * dst,
               const std::vector<std::string> * field, const int offset, const int count_line)
{
 int newline;

 if (count_line > 0)
 {
  newline = count_line;
  if (src->rowCount() - offset < count_line) newline = src->rowCount() - offset;
 }
 else newline = src->rowCount();

 dst->insertRows(0, newline - dst->rowCount());
 dst->insertColumns(0, field->size() / 2 - dst->columnCount());

 for(unsigned int i = 0; i < field->size(); i += 2)
 {
  int s = fieldByName(src, field->at(i));
  if (s != -1)
  {
   for(int y = offset; y < src->rowCount() && (y < offset + count_line || count_line == 0); y++)
   {
    dst->setData(y - offset, i / 2, src->data(y, s));
   }
  }
  dst->setHeaderData(i / 2, Wt::Orientation::Horizontal, field->at(i + 1));
 }

 return (true);
}
bool modelCopy(const Wt::WAbstractItemModel * src, Wt::WAbstractItemModel * dst)
{
 std::vector<std::string> field;
 field.clear();
 for(int i = 0; i < src->columnCount(); i++)
 {
  std::string s(to_string(src->headerData(i, Wt::Orientation::Horizontal)));
  field.push_back(s);
  field.push_back(s);
 }
 return(modelCopy(src, dst, &field));
}
bool modelFilter(Wt::WAbstractItemModel * mt,
                 const std::list<std::string> * filter, const std::string& value)
{
 std::list<int> field;
 int f, i(0);
 std::string xStr;
 bool check;
 boost::regex xRegEx;

 try{xRegEx = boost::regex(value, boost::regex::icase); }
 catch(...){ return(false);}

 for(auto itr = filter->begin(); itr != filter->end(); ++itr)
 {
  f = fieldByName(mt, *itr);
  if (f >= 0) field.push_back(f);
 }

 while(i < mt->rowCount())
 {
  xStr.clear();
  for(auto itr = field.begin(); itr != field.end(); ++itr) xStr += to_string(mt->data(i, *itr));

  try {check = boost::regex_search(xStr, xRegEx);}
  catch(...){ check = true; }

  if (check)
  {
   i++;
  }
  else
  {
   mt->removeRow(i);
  }
 }
 return(true);
}
/*
bool modelCopy(const Wt::WAbstractItemModel * src, Wt::WAbstractItemModel * dst,
               const std::vector<std::string> * field, const int offset, const int count_line)
{
 int newline;

 int r(dst->rowCount()), c(dst->columnCount());

 if (dst->rowCount() > 0) dst->removeRows(0, dst->rowCount());
 if (dst->columnCount() > 0) dst->removeColumns(0, dst->columnCount());

 if (count_line > 0)
 {
  newline = count_line;
  if (src->rowCount() - offset < count_line) newline = src->rowCount() - offset;
 }
 else newline = src->rowCount();

 dst->insertRows(0, newline);
 dst->insertColumns(0, (!field->empty()) ? field->size() / 2 : src->columnCount());

 for(unsigned int i = 0; i < (!field->empty()) ? field->size() : src->columnCount(); i += (!field->empty()) ? 2 : 1)
 {
  int s = (!field->empty()) ? fieldByName(src, field->at(i)) : i;
  if (s != -1)
  {
   for(int y = offset; y < src->rowCount() && (y < offset + count_line || count_line == 0); y++)
   {
    dst->setData(y - offset, i / 2, src->data(y, s));
   }
  }
  if ((!field->empty()))
    dst->setHeaderData(i / 2, Wt::Orientation::Horizontal, field->at(i + 1));
  else
    dst->setHeaderData(s, Wt::Orientation::Horizontal, src->headerData(s));
 }

 return (true);
}
bool modelCopy(const Wt::WAbstractItemModel * src, Wt::WAbstractItemModel * dst)
{
 std::vector<std::string> field;
 field.clear();
 return(modelCopy(src, dst, &field));
}
*/

//--------------------------------------------------------------------------
// заменяет пробелы и звездочки на знак процента.
std::string MatchForSQL(std::string str)
{
  if (str.length() == 0) return str;
    for(unsigned int i = 0; i < str.length(); i++)
    {
        if ((str[i] == ' ') || (str[i] == '*'))
        if (i > 1 && str[i-1] != '%')	str[i] = '%';
    }
    if (str[str.length() - 1] != '%')
        str = str + "%";
    if (str[0] != '%')
        str = "%" + str;

    return str;
}
//--------------------------------------------------------------------------
void logtxt(const std::string& file, const std::string& s, const bool showDateTime)
{
 if (file.empty() || s.empty()) return;

 FILE * f = fopen(file.c_str(), "a");
 if (f == NULL)
 {
  std::cerr << "File not open: " << file << std::endl
            << s;
  return;
 }

 if (showDateTime)
 {
  //Wt::WLocalDateTime ldt;
  //stm << ldt.currentDateTime().toString(Wt::WString::fromUTF8("dd-MM-yyyy hh:mm:ss")).toUTF8() << " ";
  IBPP::Timestamp ts; ts.Now();
  fprintf(f,"%02d-%02d-%04d %02d:%02d:%02d %s\n", ts.Day(), ts.Month(), ts.Year(),
                                                  ts.Hours(), ts.Minutes(), ts.Seconds(),
                                                  s.c_str() );
 }
 else
 {
  fprintf(f, "%s\n", s.c_str());
 }
 fclose(f);
}

void logtxt(const std::string& s, const bool showDateTime)
{
 bool res(false);
 extern TSQLayer sl;
 if (sl.isOpen())
 {
  std::map<std::string, std::string> state;
  state["msg"] = s;
  if (Wt::WApplication::instance() != NULL)
    state["sessionId"] = Wt::WApplication::instance()->sessionId();
  res = sl.stateSave_a(&state);
 }
 if (!res)
 {
//#ifdef DEBUG
 extern TParameterList params;
 std::string fname;

 if (Wt::WApplication::instance() != NULL) fname = (Wt::WApplication::instance()->appRoot());
 else fname = params.get("approot")->v_str + "/";
 fname += "log.txt";

 if (s.empty()) return;

 logtxt(fname, s, showDateTime);
//#endif
 }
}

int logtxt(const TState * state, const int event_id)
{
 int res(0);
 extern TSQLayer sl;

 if (state->empty()) return (event_id);

 if (sl.isOpen())
 {
  res = sl.stateSave(state, event_id);
 }
 if (res <= 0)
 {
//#ifdef DEBUG
 extern TParameterList params;
 std::string fname;
 std::stringstream stm;

 if (Wt::WApplication::instance() != NULL) fname = (Wt::WApplication::instance()->appRoot());
 else fname = params.get("approot")->v_str + "/";
 fname += "log.txt";

 for(auto itr = state->begin(); itr != state->end(); ++itr)
 {if (!stm.str().empty()) stm << "; "; stm << itr->first << "=" << itr->second;}

 logtxt(fname, stm.str(), true);
//#endif
 }
 return(res);
}
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
