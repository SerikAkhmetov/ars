#include <Wt/WApplication>
#include <Wt/WEnvironment>
#include <Wt/WHBoxLayout>
#include <Wt/WVBoxLayout>
#include <Wt/WPushButton>
#include <Wt/WApplication>
#include <Wt/WMessageBox>
#include <Wt/WCssDecorationStyle>
#include <Wt/WImage>
#include <Wt/WProgressBar>
#include <Wt/Http/Request>
#include <Wt/WTemplate>
#include <Wt/WTheme>
#include <Wt/WDateValidator>
#include <Wt/WValidator>
#include <Wt/Utils>
#include <Wt/WServer>
#include <Wt/WStandardItem>

#include <basetsd.h>
#include <boost/gil.hpp>
#include <boost/gil/extension/io/jpeg.hpp>
#include <boost/gil/extension/numeric/sampler.hpp>
#include <boost/gil/extension/numeric/resample.hpp>

#include <cstdio>

#include <zip.h>

#include "widget.h"
//#include "firmBrwWindow.h"
//#include "productUsageWindow.h"
//#include "cellBrwWindow.h"

//std::string date_format = "ddd MMM d yyyy" ;//"MMMM d, yyyy";
//--------------------------------------------------------------------------
Twindow::Twindow(Wt::WContainerWidget *parent) : WContainerWidget(parent), close(this), ui(false)
{
 setObjectName("Twindow");
 father = parent;
 timerRefresh = new Wt::WTimer(this);
 timerRefresh->setInterval(1000);
 timerRefresh->setSingleShot(true);
 timerRefresh->timeout().connect(this, &Twindow::refresh);

 TState state; extern TSQLayer sl;
 state["_"] = __PRETTY_FUNCTION__;
 if (Wt::WApplication::instance() != NULL)
   state["sessionId"] = Wt::WApplication::instance()->sessionId();
 state["name"] = name();
 sl.stateSave(&state);
}
Twindow::Twindow(TDataLayer * pdl, const std::string& pname, Wt::WContainerWidget *parent) :
 WContainerWidget(parent), close(this), ui(false)
{
 if (pname.empty()) setObjectName("Twindow");
 else setObjectName(pname);
 father = parent;
 dl = pdl;
 timerRefresh = new Wt::WTimer(this);
 timerRefresh->setInterval(1000);
 timerRefresh->setSingleShot(true);
 timerRefresh->timeout().connect(this, &Twindow::refresh);

 TState state; extern TSQLayer sl;
 state["_"] = __PRETTY_FUNCTION__;
 if (Wt::WApplication::instance() != NULL)
   state["sessionId"] = Wt::WApplication::instance()->sessionId();
 state["name"] = name();
 sl.stateSave(&state);
}
//--------------------------------------------------------------------------
Twindow::~Twindow()
{
// stateSave();
 TState state; extern TSQLayer sl;
 state["_"] = __PRETTY_FUNCTION__;
 if (Wt::WApplication::instance() != NULL)
   state["sessionId"] = Wt::WApplication::instance()->sessionId();
 state["name"] = name();
 sl.stateSave(&state);
}
//--------------------------------------------------------------------------
void Twindow::refresh()
{
 Wt::WContainerWidget::clear();
 setupUi();
 ui = true;
}
//--------------------------------------------------------------------------
std::string Twindow::name(const Wt::WObject * o)
{
 std::string ret(o->objectName());

 if (!ret.empty()) return(ret);

 while(o != 0)
 {
  if (!ret.empty()) ret = "_" + ret;
  if (!o->objectName().empty())
  {
   ret = o->objectName() + ret;
   break;
  }
  ret = typeid(o).name() + ret;
  o = o->parent();
 }
 return(ret);
}
//--------------------------------------------------------------------------
void Twindow::setChild(Twindow * child)
{
  child->close.connect(this, &Twindow::onChildClose);
}
//--------------------------------------------------------------------------
void Twindow::onChildClose()
{
  Wt::WObject * w = sender();
  if (w != 0) delete(w);
  refresh();
}
//--------------------------------------------------------------------------
void Twindow::onClose()
{
 extern TSQLayer sl;
 TState state;
 state["_"] = __PRETTY_FUNCTION__;
 if (Wt::WApplication::instance() != NULL)
   state["sessionId"] = Wt::WApplication::instance()->sessionId();
 state["name"] = name();
 sl.stateSave(&state);

 clear();
 close.emit();
}
//--------------------------------------------------------------------------
void Twindow::onCloseInt(const int v)
{
    onClose();
    closeInt.emit(v);
}
//--------------------------------------------------------------------------
void Twindow::onColumnResized(int column, Wt::WLength width)
{
 Wt::WObject * w = sender();
 Wt::WAbstractItemView * view = dynamic_cast<Wt::WAbstractItemView *>(w);
 if (view != 0)
 {
  dl->saveColumnWidth(view, name(view));
 }
}
//--------------------------------------------------------------------------
void Twindow::delayedRefresh()
{
 if (timerRefresh->isActive()) timerRefresh->stop();
 timerRefresh->start();
}
//--------------------------------------------------------------------------
void Twindow::stateSave()
{
 extern TSQLayer sl;
 //state.clear();
 state["name"] = name();
 if (Wt::WApplication::instance() != NULL)
   state["sessionId"] = Wt::WApplication::instance()->sessionId();

 sl.stateSave(&state);
}
void Twindow::stateSave(Wt::WObject * obj)
{
 std::string name(obj->objectName());
 if (dynamic_cast<Wt::WLineEdit *>(obj) != NULL)
 {
  if (!name.empty())
  {
   Wt::WLineEdit * e = dynamic_cast<Wt::WLineEdit *>(obj);
   state[name] = e->text().toUTF8();
  }
 }
 if (static_cast<Wt::WTable *>(obj) != NULL)
 {
  Wt::WTable * t = static_cast<Wt::WTable *>(obj);
  for(int column = 0; column <= t->columnCount() - 1; column++)
   for(int row = 0; row <= t->rowCount() - 1; row++)
    stateSave(t->elementAt(row, column));
 }
 if (static_cast<Wt::WContainerWidget *>(obj) != NULL)
 {
  Wt::WContainerWidget * t = static_cast<Wt::WContainerWidget *>(obj);
  //if (t->layout() != NULL)
  //{
  // auto layout = t->layout();
  // for(int i = 0; i < layout->count(); i++)
  // {stateSave(layout->itemAt(i)->widget());}
  //}
  //auto children = obj->children();
  //for(auto itr = children.begin(); itr != children.end(); ++itr) stateSave(*itr);
 }
 else
 {
  auto children = obj->children();
  for(auto itr = children.begin(); itr != children.end(); ++itr) stateSave(*itr);
 }
}
void Twindow::stateRestore()
{}
Twindow * Twindow::createWindow(const std::string& name, Wt::WContainerWidget *parent, TDataLayer * dl)
{
 Twindow * ret = NULL;

// if (name == "TfileBrw") {ret = new TfileBrw(parent, dl); }

 return(ret);
}

void Twindow::clear()
{
 stateSave();
 Wt::WContainerWidget::clear();
 ui = false;
// if (parent() != NULL && dynamic_cast<Wt::WContainerWidget *>(parent()) != NULL)
// {
//  Wt::WContainerWidget * container = dynamic_cast<Wt::WContainerWidget *>(parent());
//  container->clear();
// }
// else clear();
}

void Twindow::onbOk()
{
 onCloseInt(1);
}

void Twindow::onbCancel()
{
 onCloseInt(0);
}

void Twindow::setTablePadding(Wt::WTable * tMain, const Wt::WLength& padding, Wt::WFlags<Wt::Side> sides)
{
    for(int column = 0; column <= tMain->columnCount() - 1; column++)
        for(int row = 0; row <= tMain->rowCount() - 1; row++)
            tMain->elementAt(row, column)->setPadding(padding, sides);
}

//--------------------------------------------------------------------------
//--------------------------------------------------------------------------
WTableViewPageControl::WTableViewPageControl(TDataLayer * p_dl, Wt::WContainerWidget * parent) : Wt::WContainerWidget(parent),
 /* currentPage(1),*/ line(20), curentPageChanged(this), filterChanged(this)
{
 dl = p_dl;
 xRegEx = std::string("");
 setupUi();
}
//--------------------------------------------------------------------------
void WTableViewPageControl::setupUi()
{
 layout = new Wt::WHBoxLayout();
 layout->setSpacing(0);

 bFirst = new Wt::WPushButton("First"); bFirst->setObjectName("bFirst");
 bFirst->setEnabled(false);
 layout->addWidget(bFirst, 0, Wt::AlignmentFlag::AlignTop);
 bPrev = new Wt::WPushButton("Prev"); bPrev->setObjectName("bPrev");
 bPrev->setEnabled(false);
 layout->addWidget(bPrev, 0, Wt::AlignmentFlag::AlignTop);

 sbPage = new Wt::WSpinBox();  sbPage->setObjectName("sbPage");
 sbPage->setWidth(Wt::WLength(50));
 sbPage->setMinimumSize(Wt::WLength(50),sbPage->height());
 sbPage->setMinimum(1); sbPage->setValue(1);

 sbPage->setMargin(Wt::WLength(3), Wt::Left);
 layout->addWidget(sbPage, 0, Wt::AlignmentFlag::AlignTop);

 lCount = new Wt::WLabel(); lCount->setObjectName("lCount");
 lCount->setMargin(Wt::WLength(3));
 layout->addWidget(lCount, 0, Wt::AlignmentFlag::AlignLeft);

 bNext = new Wt::WPushButton("Next"); bNext->setObjectName("bNext");
 bNext->setEnabled(false);
 layout->addWidget(bNext, 0, Wt::AlignmentFlag::AlignTop);
 bLast = new Wt::WPushButton("Last"); bLast->setObjectName("bLast");
 bLast->setEnabled(false);
 layout->addWidget(bLast, 0, Wt::AlignmentFlag::AlignTop);

 lFilter = new Wt::WLabel(Wt::WString::fromUTF8("F:"), this); lFilter->setObjectName("lFilter");
 eFilter = new Wt::WLineEdit(this); eFilter->setObjectName("eFilter");
 eFilter->setToolTip(Wt::WString::fromUTF8("Filter"));
 eFilter->addStyleClass("input-medium search-query");
}
//--------------------------------------------------------------------------
void WTableViewPageControl::selectRow(const int row)
{
 if (row > mt->rowCount())
 {
  refresh(lastPage);
  return;
 }
 if (row < 0)
 {
  refresh(1);
  return;
 }

 refresh(row / line + 1);
 tableView->select(tableView->model()->index(row - offset(), 0), Wt::SelectionFlag::ClearAndSelect);
}
//--------------------------------------------------------------------------
bool WTableViewPageControl::findRow(const std::string& key, const int value)
{
 int f = fieldByName(mt, key);
 if (value > 0 && f != -1)
 {
  for(int i = 0; i < mt->rowCount(); i++)
  {
   if(typeid(int) == mt->data(i, f).type())
   {
    if (boost::any_cast<int>(mt->data(i, f)) == value)
    {
     selectRow(i);
     return(true);
    }
   }
  }
 }

 return(false);
}
//--------------------------------------------------------------------------
int WTableViewPageControl::offset() const
{
 return ((sbPage->value() - 1) * line);
}
//--------------------------------------------------------------------------
void WTableViewPageControl::refresh(int page)
{
 bool pageChanged(false);

 line = (tableView->height().toPixels() - tableView->headerHeight().toPixels()) /
         tableView->rowHeight().toPixels();

 if (line < 1) line = 10;

 updateFilterIndex();

 lastPage = f.size() > 0 ? (f.size() / line + ((f.size() % line > 0) ? 1 : 0) ): 1;
 std::stringstream stm;
 stm << "/" << lastPage;
 lCount->setText(Wt::WString::fromUTF8(stm.str()));

 sbPage->setMinimum(1); sbPage->setMaximum(lastPage);

 if (page < 1) page = 1;
 if (page > lastPage) page = lastPage;

 if (sbPage->value() != page)
 {
  sbPage->setValue(page);
  pageChanged = true;
 }

 Wt::WStandardItemModel * mt2 = new Wt::WStandardItemModel();

 modelCopy(mt2, &field, (page - 1) * line, line);

 for(auto itr = emptyTitleColumn.begin(); itr != emptyTitleColumn.end(); ++itr)
 {
  int f(fieldByName(mt2, *itr));
  if (f >= 0)
  {
   mt2->setHeaderData(f, Wt::Orientation::Horizontal, Wt::WString());
  }
 }

 mt2->dataChanged().connect(this, &WTableViewPageControl::onDataChanged);

 dl->saveColumnWidth(tableView, Twindow::name(tableView));
 tableView->setModel(mt2);
 dl->setColumnWidth(tableView, Twindow::name(tableView));
 for(int i = 0; i < tableView->model()->columnCount(); i++)
   {tableView->setHeaderAlignment(i, Wt::AlignmentFlag::AlignCenter);}

 bFirst->setEnabled(page > 1);
 bPrev->setEnabled(page > 1);
 bNext->setEnabled(page != lastPage);
 bLast->setEnabled(page != lastPage);

 if (pageChanged) curentPageChanged.emit(currentPage());
}
//--------------------------------------------------------------------------
//--------------------------------------------------------------------------
int WTableViewPageControl::rowIndex(const int tableViewIndex) const
{
 if (tableViewIndex + offset() >= f.size()) return(-1);
 return(f[tableViewIndex + offset()]);
}
//--------------------------------------------------------------------------
int WTableViewPageControl::fieldColumn(const std::string& fieldName) const
{
 int ret(-1), f0(fieldByName(mt, fieldName));
 if (f0 >= 0)
 {
  auto itr = src2dst.find(f0);
  if (itr != src2dst.end()) ret = itr->second;
 }
 return(ret);
}
//--------------------------------------------------------------------------
bool WTableViewPageControl::modelCopy(Wt::WAbstractItemModel * dst,
               const std::vector<Wt::WString> * field, const int offset, const int count_line)
{
 int newline;

 updateXNField();
 src2dst.clear();
 dst2src.clear();

 if (count_line > 0)
 {
  newline = count_line;
  if (f.size() - offset < count_line) newline = f.size() - offset;
 }
 else newline = f.size();

 dst->insertRows(0, newline - dst->rowCount());
 dst->insertColumns(0, field->size() / 2 - dst->columnCount());

 for(unsigned int i = 0; i < field->size(); i += 2)
 {
  int s = fieldByName(mt, field->at(i).toUTF8());
  if (s != -1)
  {
   for(int y = offset; y < f.size() && (y - offset < count_line || count_line == 0); y++)
   {
    dst->setData(y - offset, i / 2, mt->data(f[y], s));
   }
  }
  dst->setHeaderData(i / 2, Wt::Orientation::Horizontal, field->at(i + 1));
  src2dst[s] = i/2;
  dst2src[i/2] = s;
 }

 return (true);
}
//--------------------------------------------------------------------------
void WTableViewPageControl::updateXNField()
{
 xNField.clear();
 for(auto itr = xField.begin(); itr != xField.end(); ++itr)
 {
  int n = fieldByName(mt, *itr);
  if (n > 0)
  {
   xNField.push_back(n);
  }
 }
}
//--------------------------------------------------------------------------
bool WTableViewPageControl::xCheckLine(const int i) const
{
 bool ret(true);
 std::string xStr("");

 if (xNField.empty() || xRegEx.empty()) return(ret);

 for(auto itr = xNField.begin(); itr != xNField.end(); ++itr)
 {
  xStr += to_string( mt->data(i, *itr) );
 }

 try
 {
  ret = boost::regex_search(xStr, xRegEx);
 }
 catch(...)
 {
  ret = true;
 }

 return(ret);
}
//--------------------------------------------------------------------------
void WTableViewPageControl::updateFilterIndex()
{
 r.clear();
 f.clear();

 for(int rowIndex = 0; rowIndex < mt->rowCount(); rowIndex++)
 {
  if (xCheckLine(rowIndex))
  {
   r.push_back(f.size());
   f.push_back(rowIndex);
  }
  else
  {
   r.push_back(-1);
  }
 }
}
//--------------------------------------------------------------------------
void WTableViewPageControl::onDataChanged(Wt::WModelIndex i1, Wt::WModelIndex i2)
{
 int row(rowIndex(i1.row()));
 Wt::WAbstractItemModel * m = dynamic_cast<Wt::WAbstractItemModel *>(sender());

 if(m == NULL) return;

 mt->setData(row, dst2src[i1.column()], i1.data());
}
//--------------------------------------------------------------------------
//--------------------------------------------------------------------------
WTableViewPage::WTableViewPage(TDataLayer * dl, Wt::WContainerWidget * parent) :
   WTableViewPageControl(dl, parent)
{
 bFirst->clicked().connect(this, &WTableViewPage::onbClicked);
 bPrev->clicked().connect(this, &WTableViewPage::onbClicked);
 sbPage->valueChanged().connect(this, &WTableViewPage::onbClicked);
 sbPage->textInput().connect(this, &WTableViewPage::onbClicked);
 bNext->clicked().connect(this, &WTableViewPage::onbClicked);
 bLast->clicked().connect(this, &WTableViewPage::onbClicked);
 eFilter->textInput().connect(this, &WTableViewPage::onTextInputFilter);
}
//--------------------------------------------------------------------------
void WTableViewPage::onbClicked()
{
 Wt::WObject * w = sender();
 if (w == 0) return;

 if (w->objectName() == "sbPage")
 {
  refresh(sbPage->value());
 }
 if (w->objectName() == "bFirst")
 {
  refresh(1);
 }
 if (w->objectName() == "bLast")
 {
  refresh(lastPage);
 }
 if (w->objectName() == "bNext")
 {
  refresh(sbPage->value() + 1);
 }
 if (w->objectName() == "bPrev")
 {
  refresh(sbPage->value() - 1);
 }
}
//--------------------------------------------------------------------------
void WTableViewPage::onTextInputFilter()
{
 xRegEx = std::string("");
 try
 {
  xRegEx = boost::regex(eFilter->text().toUTF8(), boost::regex::icase);
 }
 catch(...)
 {
 }
 //delayedRefresh();
 filterChanged.emit();
 return;
}
//--------------------------------------------------------------------------
//--------------------------------------------------------------------------
TComboBox::TComboBox(Wt::WContainerWidget *parent) : Wt::WContainerWidget(parent),
  activated(this), valueChanged(this)
{
 key = -1; column = -1;
 setupUi();
}
//--------------------------------------------------------------------------
void TComboBox::setupUi()
{
 clear();

 setPadding(Wt::WLength(0));
 setMargin(0);

 Wt::WHBoxLayout * lmain;
 lmain = new Wt::WHBoxLayout();
 lmain->setContentsMargins(0, 0, 0, 0);

 setLayout(lmain);

 label = new Wt::WText();
 label->setMargin(0);
 //label->setMargin(Wt::WLength(5), Wt::Side::Right);
 lmain->addWidget(label, 0, Wt::AlignRight);

 comboBox = new Wt::WComboBox();
// comboBox->setMargin(Wt::WLength(0), Wt::Side::Left);
 comboBox->activated().connect(this, &TComboBox::onActivated);
 //comboBox->addStyleClass("input-medium");
 comboBox->setMargin(0);
 lmain->addWidget(comboBox, 0, Wt::AlignLeft);
 lmain->addStretch(1);
 mt = new Wt::WStandardItemModel(this);
 comboBox->setModel(mt);
}
//--------------------------------------------------------------------------
void TComboBox::setLabel(const Wt::WString& l)
{
    label->setText(l);
    label->setMargin(Wt::WLength( l.empty()? 0 : 5), Wt::Side::Right);

    if (l.empty()) label->setMargin(Wt::WLength(0));
    label->setHidden(l.empty());
}

bool TComboBox::setLabelKeyColumn(const std::string& l, const std::string& k, const std::string& c)
{
 setLabel(l);

 key = fieldByName(mt, k);
 column = fieldByName(mt, c);

 if (key >= 0 && column >= 0)
 {
  comboBox->setModelColumn(column);
  return(true);
 }
 else
 return(false);
}
//--------------------------------------------------------------------------
bool TComboBox::setValue(const int v)
{
 if (key < 0) return(false);

 for(int i = 0; i < mt->rowCount(); i++)
 {
  if (typeid(int) == mt->data(i, key).type() && v == boost::any_cast<int>(mt->data(i, key)) )
  {
   comboBox->setCurrentIndex(i);
   onActivated(i);
   return(true);
  }
 }
 return(false);
}
//--------------------------------------------------------------------------
void TComboBox::setModel(Wt::WAbstractItemModel * p)
{
 mt = dynamic_cast<Wt::WStandardItemModel *>(p);
 comboBox->setModel(mt);
}
//--------------------------------------------------------------------------
int TComboBox::value() const
{
 if (!valid()) return(-1);

 if (typeid(int) == mt->data(comboBox->currentIndex(), key).type())
 {
  return (boost::any_cast<int>(mt->data(comboBox->currentIndex(), key)));
 }

 return(-1);
}
//--------------------------------------------------------------------------
void TComboBox::setNoSelectionEnabled(const bool enabled)
{
 // comboBox->setNoSelectionEnabled(enabled);
  if (enabled)
  {
      mt->insertRow(0, new Wt::WStandardItem());
      mt->setData(0, key, 0);
      mt->setData(0, column, Wt::WString(""));
  }
}

void TComboBox::setEnabled(const bool enabled)
{
  comboBox->setEnabled(enabled);
}
//--------------------------------------------------------------------------
bool TComboBox::valid() const
{
 return(comboBox != NULL &&        
        comboBox->model() != NULL &&
        comboBox->model()->rowCount() > 0 &&
        comboBox->currentIndex() != -1 &&
        key >= 0 && 
        key <= comboBox->model()->columnCount() - 1);
}
//--------------------------------------------------------------------------
void TComboBox::onActivated(int v)
{
 activated.emit(v);
 valueChanged.emit(value());
}
//--------------------------------------------------------------------------
//--------------------------------------------------------------------------
TComboBoxDirLine::TComboBoxDirLine(TDataLayer * dl, const int directory_id, Wt::WContainerWidget *parent) : TComboBox(parent)
{
 dl->DIRLineS(mt, directory_id);
 setLabelKeyColumn(""/*"�����:"*/, "line_id", "name");
}

/*void TComboBoxDirLine::setNoSelectionEnabled(const bool enabled)
{
    if (enabled)
    {
      mt->insertRow(0, new Wt::WStandardItem());
      mt->setData(0, fieldByName(mt, "line_id"), 0);
      mt->setData(0, fieldByName(mt, "name"), Wt::WString(""));
    }
}*/
//--------------------------------------------------------------------------
//--------------------------------------------------------------------------

TPanel::TPanel(Wt::WContainerWidget * parent) : Wt::WPanel(parent)
{
 setupUi();
}
//--------------------------------------------------------------------------
void TPanel::setupUi()
{
 container = new Wt::WContainerWidget();
 setCentralWidget(container);
}
//--------------------------------------------------------------------------
void TPanel::setLayout(Wt::WLayout * layout)
{
 container->setLayout(layout);
}
//--------------------------------------------------------------------------
//--------------------------------------------------------------------------
TOkFooter::TOkFooter(Wt::WContainerWidget *parent) : Wt::WContainerWidget(parent)
{
  setupUi();
}
void TOkFooter::setupUi()
{
    Wt::WHBoxLayout * lh1 = new Wt::WHBoxLayout();
    bOk = new Wt::WPushButton(Wt::WString(L"��")); bOk->setObjectName("bOk");
    bOk->setEnabled(false);
    //bOk->clicked().connect(this, &TinsuranceCaseUpd::onbOk);
    bOk->setIcon(Wt::WLink("image/Apply.png"));

    bCancel = new Wt::WPushButton(Wt::WString(L"������"));
    bCancel->setObjectName("bCancel");
    //bCancel->clicked().connect(this, &TinsuranceCaseUpd::onbCancel);
    bCancel->setIcon(Wt::WLink("image/Cancel.png"));

    lh1->addStretch(1);
    lh1->addWidget(bOk);
    lh1->addWidget(bCancel);

    addStyleClass("modal-footer");
    setLayout(lh1);
}
//--------------------------------------------------------------------------
//--------------------------------------------------------------------------
TTagLabel::TTagLabel(Wt::WString label, int id, WContainerWidget * parent, bool preadOnly) :
 Wt::WContainerWidget(parent), line_id(id), readOnly(preadOnly)
{
 setMargin(Wt::WLength(0));

 Wt::WHBoxLayout * lh;
 lh = new Wt::WHBoxLayout();
 lh->setContentsMargins(3, 0, 0, 0);

 tLabel = new Wt::WText(label); tLabel->setMargin(Wt::WLength(0));
 tLabel->addStyleClass("badge badge-info"/*"label label-info"*/);
 lh->addWidget(tLabel, 0, Wt::AlignRight | Wt::AlignTop);

 if (!readOnly)
 {
  bClose = new Wt::WPushButton(Wt::WString("X")); bClose->setMargin(Wt::WLength(0));
  bClose->setMaximumSize(Wt::WLength(1, Wt::WLength::Unit::FontEm), height());
  bClose->clicked().connect(this, &TTagLabel::onbClose);
  lh->addWidget(bClose, 0, Wt::AlignLeft | Wt::AlignTop);
 }

 setLayout(lh);
}
//--------------------------------------------------------------------------
void TTagLabel::onbClose()
{
 remove.emit(line_id);
}
//--------------------------------------------------------------------------
//--------------------------------------------------------------------------
//--------------------------------------------------------------------------
//--------------------------------------------------------------------------
//--------------------------------------------------------------------------
//--------------------------------------------------------------------------
TFileUploadWidget::TFileUploadWidget(TPFSFile * p_pfs, Wt::WContainerWidget * parent) :
 Wt::WContainerWidget(parent), fileChanged(this), fileUploaded(this), fileTooLarge(this), remove_pfs(false)
{
/* pfs = p_pfs;
 dl = pfs->dl;*/
 setupUi();
}
TFileUploadWidget::TFileUploadWidget(int parent_id, TDataLayer * p_dl, Wt::WContainerWidget * parent) :
 Wt::WContainerWidget(parent), fileChanged(this), fileUploaded(this), fileTooLarge(this), remove_pfs(true)
{
/* pfs = new TPFSFile(p_dl);
 pfs->parent = this;
 pfs->ffdb_file_parent_id = parent_id;
 dl = pfs->dl;*/
 setupUi();
}
//--------------------------------------------------------------------------
TFileUploadWidget::~TFileUploadWidget()
{
// if (remove_pfs) delete(pfs);
}
//--------------------------------------------------------------------------
void TFileUploadWidget::setupUi()
{
 clear();
 setPadding(Wt::WLength(0));

 Wt::WHBoxLayout * lh = new Wt::WHBoxLayout();
 lh->setContentsMargins(3, 0, 0, 0);
 setLayout(lh);

 fu = new Wt::WFileUpload(this); //fu->setObjectName("TFileUploadWidget::fu");
 fu->setMultiple(true);
 fu->uploaded().connect(this, &TFileUploadWidget::onUploaded);
 fu->fileTooLarge().connect(this, &TFileUploadWidget::onFileTooLarge);
 fu->changed().connect(this, &TFileUploadWidget::onChanged);

 lh->addWidget(fu, 0);

 progressBar = new Wt::WProgressBar(this); //progressBar->setObjectName("TFileUploadWidget::progressBar");
 progressBar->hide();
 lh->addWidget(progressBar, 0);

 lv = new Wt::WVBoxLayout();
 lh->addItem(lv);

/* lTag = new Wt::WText(Wt::WString::fromUTF8("Tag:"));
 lh->addWidget(lTag, 0);

 tagShow = new TTagShowWidget(dl, this);
 lh->addItem(tagShow->lhTag);
 lh->addWidget(tagShow->cbTag);
 lh->addWidget(tagShow->bNewTag);*/

 fu->setProgressBar(progressBar);
}
//--------------------------------------------------------------------------
bool TFileUploadWidget::canUpload() const
{
 return(/*fu->canUpload()*/!uploadedFile.empty());
}
//--------------------------------------------------------------------------
void TFileUploadWidget::upload()
{
 copy2DB();
}
//--------------------------------------------------------------------------
void TFileUploadWidget::onChanged()
{
 if (fu->canUpload())
 {
  fu->hide();
  progressBar->show();
  fu->upload();
 }
}
//--------------------------------------------------------------------------
class TFileUploadWidgetStealFile : public TCommand
{
 public:
  TFileUploadWidgetStealFile(TFileUploadWidget * parent = 0) : TCommand(static_cast<Wt::WObject *>(parent))
  {fileUploadWidget = parent;};

 std::string spoolFileName;

 void exec()
 {
  fileUploadWidget->remove(spoolFileName);
 };

 TFileUploadWidget * fileUploadWidget;
};

void TFileUploadWidget::onUploaded()
{
 uploadedFile.clear();
 for(auto itr = fu->uploadedFiles().begin(); itr != fu->uploadedFiles().end(); ++itr)
 {
  itr->stealSpoolFile();
  uploadedFile.push_back(TuploadedFile(itr->spoolFileName(), itr->clientFileName()));
 }
 progressBar->hide();
 refreshLV();
 fileChanged.emit();
}
void TFileUploadWidget::refreshLV()
{
 lv->clear();
 for(auto itr = uploadedFile.begin(); itr != uploadedFile.end(); ++itr)
 {
  Wt::WHBoxLayout * lh = new Wt::WHBoxLayout();
  lv->addItem(lh);
  lh->addWidget(new Wt::WText(Wt::WString::fromUTF8(itr->clientFileName)));

  Wt::WPushButton * bClose = new Wt::WPushButton(Wt::WString("X")); bClose->setMargin(Wt::WLength(0));
  bClose->setMaximumSize(Wt::WLength(1, Wt::WLength::Unit::FontEm), height());
  TFileUploadWidgetStealFile * command = new TFileUploadWidgetStealFile(this);
  command->spoolFileName =  itr->spoolFileName;
  bClose->clicked().connect(command, &TFileUploadWidgetStealFile::exec);
  lh->addWidget(bClose, 0, Wt::AlignLeft | Wt::AlignTop);
  lh->addStretch(1);
 }

 refresh();
}
//--------------------------------------------------------------------------
void TFileUploadWidget::remove(const std::string spoolFileName)
{
  for (auto itr = uploadedFile.begin(); itr != uploadedFile.end(); ++itr)
  {
   if (itr->spoolFileName == spoolFileName)
   {    
    std::remove(itr->spoolFileName.c_str());
    uploadedFile.erase(itr);
    break;
   }
  }
 refreshLV();
 fileChanged.emit();
}
//void TFileUploadWidget::copy_file(const std::string src, const std::string dst)
//{
// std::ifstream srce;
// std::ofstream dest;

// Bytef * sbuff;

// dest.open(dst, std::ios::binary | std::ios::trunc);
// if (dest.fail()) {return;}

// srce.open(src, std::ios::binary);
// if (srce.fail()) {dest.close(); return;}

// sbuff = new Bytef[4096];  // src �����

// while(!srce.eof())
// {
//  srce.read((char *)sbuff, 4096);
//  dest.write((char *)sbuff, srce.gcount());
//  if (dest.fail()) break;
// } // while

// delete [] sbuff;

// srce.close();
// dest.close();
//}
//--------------------------------------------------------------------------
void TFileUploadWidget::copy2DB()
{
 int ret(-1);
 /*for(auto itr = uploadedFile.begin(); itr != uploadedFile.end(); ++itr)
 {
  pfs->fname_user = itr->clientFileName;
  if (pfs->spool2DB(itr->spoolFileName)) ret = pfs->ffdb_file_id;
 }
 if (ret > 0) fileUploaded.emit(ret);*/
}
//--------------------------------------------------------------------------
void TFileUploadWidget::onFileTooLarge(int64_t t)
{
 Wt::WString msg;
 WObject * w = sender();
 if (w != NULL)
 {
  Wt::WFileUpload * fu = dynamic_cast<Wt::WFileUpload *>(w);
  msg = Wt::WString::fromUTF8("File ") + fu->clientFileName() + Wt::WString::fromUTF8(" too large.");
 }
 else
  msg = Wt::WString::fromUTF8("File too large.");

 ShowMessage(0, msg, "Error");

 fileTooLarge.emit(t);
}
//--------------------------------------------------------------------------
//--------------------------------------------------------------------------
TFileGetWidget::TFileGetWidget(int pfs_file_id, TDataLayer * dl, Wt::WContainerWidget * parent) :
 Wt::WContainerWidget(parent)
{
 setPadding(Wt::WLength(0));

 /*res = new TPFSResource(dl, this);

 Wt::WHBoxLayout * lh = new Wt::WHBoxLayout();
 lh->setContentsMargins(3, 0, 0, 0);

// bGet->setMaximumSize(Wt::WLength(1, Wt::WLength::Unit::FontEm), height());
// bGet->setMinimumSize(Wt::WLength(1, Wt::WLength::Unit::FontEm), height());

 if (res->pfsfile->read_meta(pfs_file_id))
 {
  tLabel = new Wt::WText(Wt::WString::fromUTF8(res->pfsfile->fname_user)); tLabel->setMargin(Wt::WLength(0));
  bGet = new Wt::WPushButton(Wt::WString("Get")); bGet->setMargin(Wt::WLength(0));
  res->suggestFileName(Wt::WString::fromUTF8(res->pfsfile->fname_user));
  bGet->setResource(res);
 }

 lh->addWidget(tLabel, 0, Wt::AlignRight);
 lh->addWidget(bGet, 0, Wt::AlignLeft);

 setLayout(lh);*/
}
/*TFileGetWidget::~TFileGetWidget()
{
 delete(res);
}*/
//--------------------------------------------------------------------------
//--------------------------------------------------------------------------
TFileGetMenuItem::TFileGetMenuItem(int pfs_file_id, TDataLayer * p_dl) :
 Wt::WMenuItem(Wt:: WString())
{
/*res = new TPFSResource(p_dl, this);
 if (res->pfsfile->read_meta(pfs_file_id))
 {
  res->suggestFileName(Wt::WString::fromUTF8(res->pfsfile->fname_user));
  setText(Wt::WString::fromUTF8(res->pfsfile->fname_user));
  setLink(Wt::WLink(res));
 }*/
}
//--------------------------------------------------------------------------
//--------------------------------------------------------------------------
//--------------------------------------------------------------------------
void TThreadCommandPreview::exec()
{
    dl->open();
    if (!dl->isOpen()) return;

    TFile file(dl);
    TNode node(dl), view(dl);

    file.file_id = file_id;
    file.read();
    node.node_id = file.node_id;
    node.read();

    std::string tmp(std::tmpnam(nullptr));
    jpeg_resize(node.fullName(), tmp, 64, 64); 
    view.save(tmp);
    std::remove(tmp.c_str());

    file.preview = view.node_id;
    file.save();

    dl->close();
  //  finished.emit();
}
//--------------------------------------------------------------------------
//--------------------------------------------------------------------------
void TCommandFileDocD::exec()
{
 if (Wt::StandardButton::Yes != Wt::WMessageBox::show("Confirm", "Remove file ?",
                                                      Wt::StandardButton::Yes | Wt::StandardButton::No))
 {
  return;
 }

 if(dl->TableD("FDB_FILE_DOC", file_doc_id) > 0)
  {changed.emit();}
 else
  {ShowMessage(0, Wt::WString::fromUTF8(str2html(dl->errorstr)), "DB Error");}
};

void TCommandParam::exec()
{
    fire.emit(parameter);
    changed.emit();
};

void TCommandParamParam::exec()
{
    fire.emit(parameter, parameter2);
    changed.emit();
};
//--------------------------------------------------------------------------
//--------------------------------------------------------------------------
//--------------------------------------------------------------------------
//--------------------------------------------------------------------------
//--------------------------------------------------------------------------
TFileWidget::TFileWidget(TDataLayer * p_dl, Wt::WContainerWidget *parent) : Wt::WContainerWidget(parent), file(p_dl), selected(false), changed(this)
{
 dl = p_dl;
 setupUi();
 timer = new Wt::WTimer(this);
 timer->setSingleShot(true);
 timer->setInterval(1500);
 timer->timeout().connect(this, &TFileWidget::refresh);
}

void TFileWidget::setupUi()
{
    Wt::WVBoxLayout * lv0 = new Wt::WVBoxLayout(); lv0->setObjectName("lv0");

    setObjectName("TFileWidget");
    setLayout(lv0);

    preview = new Wt::WFileResource();
    link.setResource(preview);
    img = new Wt::WImage(link);
    img->setMaximumSize(Wt::WLength(64), Wt::WLength(64));
    img->clicked().connect(this, &TFileWidget::onClicked);

    lv0->addWidget(img);

    res = new Wt::WFileResource();
    anchor = new Wt::WAnchor(res);

    lv0->addWidget(anchor);
}

void TFileWidget::refresh()
{
    TNode node(dl), view(dl);

    std::string view_full_name(Wt::WApplication::instance()->appRoot() + "image\\New.png"), view_suggest("New.png");
    file.read();

    node.node_id = file.node_id;
    node.read();
    if (file.preview == 0)
    {
      std::string src(file.name_user);
      std::string::size_type n = src.rfind(".");
      std::string ext = Upper(src.substr(n + 1));
      if (ext == std::string("JPG") || ext == std::string("JPEG"))
      {
          TThreadCommandPreview * thview = new TThreadCommandPreview(/*parent()*/); // TODO : memory leaks
          thview->dl->setHostName(dl->hostName());
          thview->dl->setPort(dl->port());
          thview->dl->setDatabaseName(dl->dbName());
          thview->dl->setRole(dl->role());
          thview->dl->setUserName(dl->userName());
          thview->dl->setPassword(dl->password());
          thview->file_id = file.file_id;
          thview->start();
          timer->start();
      }
    }
    else
    {
     view.node_id = file.preview;
     view.read();
     view_full_name = view.fullName();
     view_suggest = file.name_user;
    }

    res->setFileName(node.fullName());
    res->suggestFileName(Wt::WString::fromUTF8(file.name_user));
    anchor->setText(Wt::WString::fromUTF8(file.name_user));

    preview->setFileName(view_full_name);
    preview->suggestFileName(Wt::WString::fromUTF8(view_suggest));
}

void TFileWidget::onClicked(Wt::WMouseEvent event)
{
    if (event.button() == Wt::WMouseEvent::Button::LeftButton)
    {setSelected(!selected);}
}

void TFileWidget::setSelected(const bool v)
{
    selected = v;
    if (selected)
    {
        addStyleClass("btn-primary");
    }
    else
    {
        removeStyleClass("btn-primary");
    }
    changed.emit();
}
//--------------------------------------------------------------------------
//--------------------------------------------------------------------------
//--------------------------------------------------------------------------
//--------------------------------------------------------------------------
//--------------------------------------------------------------------------
//--------------------------------------------------------------------------
TFileDoc::TFileDoc(TDataLayer * p_dl, Wt::WContainerWidget *parent) : Wt::WContainerWidget(parent), changed(this)
{
    dl = p_dl;
    setupUi();
}
void TFileDoc::setupUi()
{
    Wt::WText * l;
    Wt::WVBoxLayout * lv0 = new Wt::WVBoxLayout(); lv0->setObjectName("lv0");

    setObjectName("TFileDoc");
    setLayout(lv0);

    fileLayout = new Wt::WHBoxLayout(); fileLayout->setObjectName("fileLayout");

    lv0->addLayout(fileLayout);

    fileProgress = new Wt::WProgressBar(); fileProgress->setObjectName("fileProgress");
    lv0->addWidget(fileProgress, 0);

    fView = new Wt::WTable(); fView->setObjectName("fView");
    lv0->addWidget(fView, 0, Wt::AlignmentFlag::AlignLeft);

    createFileUpload();
}
void TFileDoc::refresh()
{
    int max_width(Wt::WApplication::instance()->environment().screenWidth() / 120);
    int x(0), y(0);
    if (max_width == 0) max_width = 1;

    fView->clear();
    fwlst.clear();
 int c = fieldByName(&mtFile, "status");
 for(int i = 0; i < mtFile.rowCount(); i++)
 {
     if(to_int(mtFile.data(i, c)) == 2) continue; // ���������
     TFileWidget * fw = new TFileWidget(dl, fView->elementAt(y, x));
     fw->file.file_id = to_int(mtFile.data(i, fieldByName(&mtFile, "file_id")));
 //    fh->addWidget(fw, 0, Wt::AlignmentFlag::AlignTop);
     fw->refresh();
     fw->changed.connect(this, &TFileDoc::onFileChanged);
     fwlst.push_back(fw);
     x++;
     if (x == max_width) { x = 0; y++; }
 }
}

void TFileDoc::read(const int p_type, const int p_id)
{
    type = p_type;
    id = p_id;
    mtFile.clear();
    if (dl->FileDocS(&mtFile, type, id) < 0)
    {
        //ShowMessage(0, Wt::WString::fromUTF8(str2html(dl->errorstr)), "DB Error");
        return;
    }
    int c(mtFile.columnCount());
    mtFile.insertColumn(c - 1);
    mtFile.setHeaderData(c - 1, Wt::Orientation::Horizontal, "status");
    c = fieldByName(&mtFile, "status");
    for(int i = 0; i < mtFile.rowCount(); i++) {mtFile.setData(i, c, (int)0);} // 0 - �� ��, 1 - ��������/��������, 2 - �������
}
void TFileDoc::save(const int p_type, const int p_id)
{
    type = p_type;
    id = p_id;
    int c = fieldByName(&mtFile, "status");
    for(int i = 0; i < mtFile.rowCount(); i++)
    {
        int file_doc_id = to_int(mtFile.data(i, fieldByName(&mtFile, "file_doc_id")));
        int status = to_int(mtFile.data(i, c));
        if (status == 0) continue;
        if (status == 1)
        {            
            int file_id = to_int(mtFile.data(i, fieldByName(&mtFile, "file_id")));
            dl->FileDocIU(file_doc_id, file_id, type, id);
        }
        if (status == 2 && file_doc_id > 0) // ���������
        {            
            dl->TableD("FDB_FILE_DOC", file_doc_id);
        }
    }
}

void TFileDoc::createFileUpload()
{
    fileLayout->clear();

    fileUpload = new Wt::WFileUpload(); fileUpload->setObjectName("fileUpload");
    fileUpload->setMultiple(true);
    fileUpload->setProgressBar(fileProgress);
    fileUpload->changed().connect(this, &TFileDoc::startUpload);
    fileUpload->uploaded().connect(this, &TFileDoc::onUploaded);
    fileLayout->addWidget(fileUpload);

    fileLayout->addStretch(1);

    bSelectAll = new Wt::WPushButton(Wt::WString(L"�������� ��")); bSelectAll->setObjectName("bSelectAll");
    bSelectAll->clicked().connect(this, &TFileDoc::onSelectAll);
    bSelectAll->setEnabled(true);
    //bSelectAll->setIcon(Wt::WLink("image/Ghost.png"));
    fileLayout->addWidget(bSelectAll, 0, Wt::AlignmentFlag::AlignTop);
    
    bDeselectAll= new Wt::WPushButton(Wt::WString(L"����� ���������")); bDeselectAll->setObjectName("bDeselectAll");
    bDeselectAll->clicked().connect(this, &TFileDoc::onDeselectAll);
    bDeselectAll->setEnabled(true);
    //bDeselectAll->setIcon(Wt::WLink("image/Ghost.png"));
    fileLayout->addWidget(bDeselectAll, 0, Wt::AlignmentFlag::AlignTop);

    bPreview = new Wt::WPushButton(Wt::WString(L"��������")); bPreview->setObjectName("bPreview");
    bPreview->clicked().connect(this, &TFileDoc::onPreview);
    bPreview->setEnabled(false);
    bPreview->setIcon(Wt::WLink("image/Ghost.png"));
    fileLayout->addWidget(bPreview, 0, Wt::AlignmentFlag::AlignTop);

    bDownload = new Wt::WPushButton(Wt::WString(L"���������")); bDownload->setObjectName("bDownload");
    //bDownload->clicked().connect(this, &TFileDoc::onDownload);
    bDownload->setEnabled(false);
    bDownload->setIcon(Wt::WLink("image/LoadTo.png"));
    fileLayout->addWidget(bDownload, 0, Wt::AlignmentFlag::AlignTop);

    fileRes = new TMultiFileResourse(bDownload);
    fileRes->suggestFileName(Wt::WString("attachments.zip"));
    bDownload->setLink(Wt::WLink(fileRes));

    bDelete = new Wt::WPushButton(Wt::WString(L"�������")); bDelete->setObjectName("bDelete");
    bDelete->clicked().connect(this, &TFileDoc::onDelete);
    bDelete->setEnabled(false);
    bDelete->setIcon(Wt::WLink("image/Delete.png"));
    fileLayout->addWidget(bDelete, 0, Wt::AlignmentFlag::AlignTop);

    fileProgress->setHidden(true);
}

void TFileDoc::onDelete()
{
    if (Wt::StandardButton::Yes != Wt::WMessageBox::show("Confirm", Wt::WString(L"������� ���� ?"),
        Wt::StandardButton::Yes | Wt::StandardButton::No))
    {
        return;
    }

    for(auto itr = fwlst.begin(); itr != fwlst.end(); ++itr)
    {
        if (!(*itr)->selected) continue;
        for(int i = 0; i < mtFile.rowCount(); i++)
        {
            int file_id = to_int(mtFile.data(i, fieldByName(&mtFile, "file_id")));
            if (file_id != (*itr)->file.file_id) continue;
            mtFile.setData(i, fieldByName(&mtFile, "status"), int(2));
            break;
        }
    }

    changed.emit();
    refresh();
    bDelete->setEnabled(false);
    bPreview->setEnabled(false);
    bDownload->setEnabled(false);
}

void TFileDoc::onPreview()
{
    TNode node(dl); std::string filename("");

    for(auto itr = fwlst.begin(); itr != fwlst.end(); ++itr)
    {
        if (!(*itr)->selected) continue;
        std::string src((*itr)->file.name_user);
        std::string::size_type n = src.rfind(".");
        std::string ext = Upper(src.substr(n + 1));
        if(ext == std::string("JPG") || ext == std::string("JPEG"))
        {
            node.node_id = (*itr)->file.node_id;
            filename = src;
            break;
        }
    }

    if (node.node_id == 0) return;
    node.read();

    Wt::WDialog * dialog = new Wt::WDialog(/*dl,*/ filename, this);
    dialog->setObjectName("ImagePrviewDialog");
    dialog->setClosable(true);
    dialog->rejectWhenEscapePressed(true);
    dialog->setResizable(true);
   // dialog->setAutoFocus(true);
    dialog->setWidth(Wt::WLength(800));
    dialog->setHeight(Wt::WLength(600));

    Wt::WFileResource * preview = new Wt::WFileResource();

    Wt::WLink link;
    link.setResource(preview);
    Wt::WImage * img = new Wt::WImage(link);

    dialog->contents()->addWidget(img);

    dialog->finished().connect(this, &TFileDoc::onPreviewDone);

    preview->setFileName(node.fullName());

    dialog->show();
}

void TFileDoc::onPreviewDone(Wt::WDialog::DialogCode code)
{
    Wt::WDialog * dialog = dynamic_cast<Wt::WDialog *>(sender());
    if (dialog == NULL) return;

  //  dialog->width();
  //  dialog->height();

  //  delete(dialog); // 
}

void TFileDoc::onSelectAll()
{
    for(auto itr = fwlst.begin(); itr != fwlst.end(); ++itr)
    {
        (*itr)->setSelected(true);
    }
}

void TFileDoc::onDeselectAll()
{
    for(auto itr = fwlst.begin(); itr != fwlst.end(); ++itr)
    {
        (*itr)->setSelected(false);
    }
}

void TFileDoc::onFileChanged()
{
   bool selected(false), canPreview(false);
   for(auto itr = fwlst.begin(); itr != fwlst.end() && !selected; ++itr)
   {
       selected = (*itr)->selected;
   }
   bDelete->setEnabled(selected);
   bDownload->setEnabled(selected);

   TFileWidget * fw = dynamic_cast<TFileWidget *>(sender());
   if (fw != NULL)
   {
       TNode node(dl);
       std::string name, fullname;
       name = fw->file.name_user;
       node.node_id = fw->file.node_id;
       node.read();
       fullname = node.fullName();

       if (fw->selected) fileRes->append(name, fullname);
       else fileRes->remove(name, fullname);
   }

   for(auto itr = fwlst.begin(); itr != fwlst.end() && !canPreview; ++itr)
   {
       if (!(*itr)->selected) continue;
       std::string src((*itr)->file.name_user);
       std::string::size_type n = src.rfind(".");
       std::string ext = Upper(src.substr(n + 1));
       canPreview = (ext == std::string("JPG") || ext == std::string("JPEG"));
   }
   bPreview->setEnabled(canPreview);
}

void TFileDoc::startUpload()
{
    Wt::WObject * w = sender();
    if (w == NULL) return;
    Wt::WFileUpload * fileUpload =  dynamic_cast<Wt::WFileUpload *>(w);
    if (fileUpload == NULL) return;
    if (fileUpload->canUpload())
    {
        current = 0;
        fileProgress->setHidden(false);
        fileUpload->upload();
    }
}
void TFileDoc::onUploaded()
{
    TNode node(dl); TFile file(dl);
    Wt::WObject * w = sender();
    if (w == NULL) return;
    Wt::WFileUpload * fileUpload =  dynamic_cast<Wt::WFileUpload *>(w);
    if (fileUpload == NULL) return;

    auto uploadedFiles = fileUpload->uploadedFiles();
    for(auto itr = uploadedFiles.begin(); itr != uploadedFiles.end(); ++itr)
    {
        node.clear(); file.clear();
        if (node.save(itr->spoolFileName()))
        {
            std::string src(itr->clientFileName());
            std::string::size_type n = src.rfind(".");
            std::string ext = Upper(src.substr(n + 1));

            if (ext != std::string("JPG") && ext != std::string("JPEG") && ext != std::string("PDF")) continue;

            file.name_user = itr->clientFileName();
            file.node_id = node.node_id;
            file.file_size = TFile::fileSize(itr->spoolFileName());
            file.save();

            current += node.node_size;
            fileProgress->setValue(current);

            int row = mtFile.rowCount();
            mtFile.insertRow(row);
            mtFile.setData(row, fieldByName(&mtFile, "file_id"), file.file_id);
            mtFile.setData(row, fieldByName(&mtFile, "name_user"), file.name_user);
            mtFile.setData(row, fieldByName(&mtFile, "node_id"), file.node_id);
            mtFile.setData(row, fieldByName(&mtFile, "status"), 1);
            mtFile.setData(row, fieldByName(&mtFile, "p3"), node.p3);
            mtFile.setData(row, fieldByName(&mtFile, "p2"), node.p2);
            mtFile.setData(row, fieldByName(&mtFile, "p1"), node.p1);
            mtFile.setData(row, fieldByName(&mtFile, "name_server"), node.name_server);

            // preview
            /*if (ext == std::string("JPG") || ext == std::string("JPEG"))
            {
              jpeg_resize(itr->spoolFileName(), node.fullName() + std::string("p"), 64, 64);
            }
            current += node.node_size;*/
        }
    }
    createFileUpload();
    changed.emit();
    refresh();
}
void TFileDoc::fileTooLarge(int64_t filesize)
{
    ShowMessage(0, Wt::WString(L"������ ��������: �������� ���������� ������ �����"), "DB Error");
}
//--------------------------------------------------------------------------

TMultiFileResourse::TMultiFileResourse(Wt::WObject * parent) : Wt::WFileResource(parent)
{

}

TMultiFileResourse::~TMultiFileResourse()
{
    if (!fileName().empty())
    {
        std::remove(fileName().c_str());
    }
}

void TMultiFileResourse::handleRequest(const Wt::Http::Request &request, Wt::Http::Response &response)
{
    if (fileName().empty())
    {
        std::string name1 = std::tmpnam(nullptr);
        int errorp;
        zip_t * z = zip_open(name1.c_str(), ZIP_CREATE | ZIP_TRUNCATE, &errorp);
        if (z != NULL)
        {
            for(auto itr = lst.begin(); itr != lst.end(); ++itr)
            {
                zip_source_t *s = zip_source_file(z, itr->fullname.c_str(), 0, 0);
                int idx = zip_file_add(z, itr->name.c_str(), s, ZIP_FL_OVERWRITE);
                zip_set_file_compression(z, idx, ZIP_CM_STORE, 0);
            }
        }
        zip_close(z);
        setFileName(name1);
    }

    Wt::WFileResource::handleRequest(request, response);
}

void TMultiFileResourse::clear()
{
    lst.clear();
    if (!fileName().empty())
    {
      std::remove(fileName().c_str());
    }
    setFileName(std::string());
}

void TMultiFileResourse::append(const std::string &name, const std::string &fullname)
{
    for (auto itr = lst.begin(); itr != lst.end(); ++itr)
    {
        if (itr->name == name && itr->fullname == fullname) return;
    }

    lst.push_back(TNameFullName(name, fullname));
    setFileName(std::string());
}

void TMultiFileResourse::remove(const std::string &name, const std::string &fullname)
{
    for (auto itr = lst.begin(); itr != lst.end(); ++itr)
    {
        if (itr->name == name && itr->fullname == fullname)
        {
            lst.erase(itr);
            return;
        }
    }
}
//--------------------------------------------------------------------------
Wt::WWidget * TItemDelegate::update(Wt::WWidget *widget, const Wt::WModelIndex &index,
                                Wt::WFlags< Wt::ViewItemRenderFlag > flags)
{
 Wt::WWidget * w;

 if (typeid(TNumeric) != index.data().type() && typeid(Currency) != index.data().type())
  return (Wt::WItemDelegate::update(widget, index, flags));
 else
  w = Wt::WItemDelegate::update(widget, Wt::WModelIndex(), flags);

 Wt::WText * t = dynamic_cast<Wt::WText *>(w->find("t"));

 if (t == NULL) return(w);

 //t->setWordWrap(true);
 //t->setTextFormat(Wt::TextFormat::PlainText);
 t->setText(Wt::WString::fromUTF8(to_string(index.data())));

 return(w);
}
//--------------------------------------------------------------------------
Wt::WWidget * TItemDelegateMultiLine::update (Wt::WWidget *widget, const Wt::WModelIndex &index,
    Wt::WFlags< Wt::ViewItemRenderFlag > flags)
{
   /* if (!index.data().empty() && typeid(std::string) == index.data().type())
    {
     std::string text = boost::any_cast<std::string>(index.data());
     Wt::WLabel * w = new Wt::WLabel();
     w->setWordWrap(true);
     //w->setTextFormat(Wt::TextFormat::PlainText);
     w->setTextFormat(Wt::TextFormat::XHTMLText);
     w->setText(Wt::WString::fromUTF8(str2html(text)));
     //return(dynamic_cast<Wt::WWidget *>(w));
     Wt::WContainerWidget * c = new Wt::WContainerWidget();
     Wt::WVBoxLayout * lh0 = new Wt::WVBoxLayout();
     c->setLayout(lh0);
     lh0->addWidget(w, 1, Wt::AlignmentFlag::AlignTop);
     return(dynamic_cast<Wt::WWidget *>(c));
    }*/

     if( !widget )
     {
      widget = Wt::WItemDelegate::update( widget, index, flags );
      widget-> setAttributeValue("style", "line-height: 20px; white-space: normal; overflow-y: visible");
      if (!index.model()->data(index, Wt::ItemDataRole::StyleClassRole).empty())
        widget->addStyleClass(to_wstring(index.model()->data(index, Wt::ItemDataRole::StyleClassRole)));
      return widget;
     }

    return(Wt::WItemDelegate::update(widget, index, flags));
}
//--------------------------------------------------------------------------
Wt::WWidget * TItemDelegateColor::update (Wt::WWidget *widget, const Wt::WModelIndex &index,
                                Wt::WFlags< Wt::ViewItemRenderFlag > flags)
{
  Wt::WWidget * w; // RenderSelected

  w = TItemDelegate::update(widget, index, flags);

  if (color.find(index.row()) != color.end()
   && color[index.row()].find(index.column()) != color[index.row()].end())
  {
   Wt::WText * t = dynamic_cast<Wt::WText *>(w->find("t"));
   Wt::WColor c = color[index.row()][index.column()];
//   if (flags.testFlag(Wt::ViewItemRenderFlag::RenderSelected))
//   {c.setRgb(!c.red(), !c.green(), !c.blue(), c.alpha());}
   if (t != 0) t->decorationStyle().setForegroundColor(c);
  }

 return(w);
}
//--------------------------------------------------------------------------
Wt::WWidget * TItemDelegateTag::update (Wt::WWidget *widget, const Wt::WModelIndex &index,
                                Wt::WFlags< Wt::ViewItemRenderFlag > flags)
{
 /* if (!index.data().empty())
  {
   int fdb_file_id(to_int(mt->data(index.row(), fieldByName(mt, "fdb_file_id"))));
   if (fdb_file_id > 0)
   {
    TTagShowWidget * w = new TTagShowWidget(dl, NULL, true);
    w->pfs->ffdb_file_id = fdb_file_id;
    w->pfs->init_tag();
    w->refreshlTag();
    return(dynamic_cast<Wt::WWidget *>(w));
   }
  }*/
  return(Wt::WItemDelegate::update(widget, index, flags));
}
//--------------------------------------------------------------------------
Wt::WWidget * TItemDelegateIco::update (Wt::WWidget *widget, const Wt::WModelIndex &index,
                                Wt::WFlags< Wt::ViewItemRenderFlag > flags)
{
  if (!index.data().empty())
  {
   std::string iconame(to_string(index.data()));
//   if (!boost::filesystem::exists(iconame) &&
//       Wt::WApplication::instance() != NULL)
//   {iconame = Wt::WApplication::instance()->docRoot() + iconame;}
   if (!iconame.empty())
   {
    Wt::WContainerWidget * c = new Wt::WContainerWidget();
    Wt::WImage *image = new Wt::WImage(Wt::WLink(iconame));
    c->addWidget(image);
    return(dynamic_cast<Wt::WWidget *>(c));
   }
  }

  return(Wt::WItemDelegate::update(widget, index, flags));
}
//--------------------------------------------------------------------------
class TCommandItemDelegateCheckBoxChanged : public TCommand
{
 public:
  TCommandItemDelegateCheckBoxChanged(const Wt::WModelIndex idx, Wt::WObject * parent = 0) :
   TCommand(parent), index(idx), changedItem(this) {setObjectName("TCommandItemDelegateCheckBoxChanged"); };
 virtual void exec()
 {Wt::WCheckBox * box = dynamic_cast<Wt::WCheckBox *>(sender());
  if (box != NULL){int v((box->checkState()==Wt::Checked)?1:0);
                   changedItem.emit(index.row(), v);
                   changed.emit();
                   Wt::WAbstractItemModel * m = const_cast<Wt::WAbstractItemModel *>(index.model());
                   if (m != NULL)  m->setData(index, v);
                   };
 };
 Wt::WModelIndex index;
 Wt::Signal<int, int> changedItem;
};
Wt::WWidget * TItemDelegateCheckBox::update (Wt::WWidget *widget, const Wt::WModelIndex &index,
                              Wt::WFlags< Wt::ViewItemRenderFlag > flags)
{
 (void) widget; (void) flags;

 Wt::WCheckBox * box = new Wt::WCheckBox();
 TCommandItemDelegateCheckBoxChanged * cm = new TCommandItemDelegateCheckBoxChanged(index, box);
 Wt::Signals::connection con = cm->changedItem.connect(this, &TItemDelegateCheckBox::onChanged);
// bool ch = con.connected();
// box->changed().connect(this, &TItemDelegateCheckBox::onChanged);
 box->changed().connect(cm, &TCommandItemDelegateCheckBoxChanged::exec);
 box->setChecked(to_bool(index.data()));
 box->setOffsets(Wt::WLength(0));

 Wt::WContainerWidget * c = new Wt::WContainerWidget();
 c->setPadding(Wt::WLength(0));
 Wt::WHBoxLayout * lh1 = new Wt::WHBoxLayout();
 lh1->setContentsMargins(0, 0, 0, 0);
 lh1->setSpacing(0);
 lh1->addWidget(box, 0, Wt::AlignmentFlag::AlignTop);
 c->setLayout(lh1);

 return(dynamic_cast<Wt::WWidget *>(c));
}
void TItemDelegateCheckBox::onChanged(int row, int v)
{
 changed.emit(row, v);
}
//--------------------------------------------------------------------------
TDateEdit::TDateEdit(Wt::WContainerWidget *parent) : Wt::WDateEdit(parent)
{
 setToolTip(Wt::WString(L"���� � ������� ��/��/����"));
 setMaxLength(10);
 textInput().connect(this, &TDateEdit::onTextInput);
}

void TDateEdit::onTextInput()
{
   if (!date().isValid())
   {
       std::string s(""), src(text().trim().toUTF8()), validchar("0123456789/");
       int i(0);
       for(auto itr = src.begin(); itr != src.end(); ++itr, i++)
        {
           if ((i == 2 || i == 5) && (*itr != '/')) s += "/";
           if (validchar.find(*itr) != std::string::npos) s += *itr;
       }
       setText(Wt::WString::fromUTF8(s));
    }
}
//--------------------------------------------------------------------------
TTimeEdit::TTimeEdit(Wt::WContainerWidget *parent) : Wt::WTimeEdit(parent)
{
    setToolTip(Wt::WString(L"����� � ������� ��:��"));
    setMaxLength(5);
    textInput().connect(this, &TTimeEdit::onTextInput);
}

void TTimeEdit::onTextInput()
{
    if (!time().isValid())
    {
        std::string s(""), src(text().trim().toUTF8()), validchar("0123456789:");
        int i(0);
        for(auto itr = src.begin(); itr != src.end(); ++itr, i++)
        {
            if ((i == 2) && (*itr != ':')) s += ":";
            if (validchar.find(*itr) != std::string::npos) s += *itr;
        }
        setText(Wt::WString::fromUTF8(s));
    }
}
//--------------------------------------------------------------------------
//TLineEditButton::TLineEditButton(Wt::WContainerWidget *parent) : Wt::WLineEdit(parent)
//{
//}
//--------------------------------------------------------------------------
/*TMemoryTable::TMemoryTable(Wt::WObject * parent) : Wt::WAbstractTableModel(parent)
{
 mt = new TDynamicList();
}

TMemoryTable::~TMemoryTable()
{
 delete(mt);
}

// Returns the table row count (only for the invalid parent!)
int TMemoryTable::rowCount (const Wt::WModelIndex &parent) const
{
 return(mt->get_row_count());
}

// Returns the table column count (only for the invalid parent!)
int TMemoryTable::columnCount (const Wt::WModelIndex &parent) const
{
 return(mt->get_column_count());
}

// Returns the data for an item, and a particular role
boost::any TMemoryTable::data (const Wt::WModelIndex &index, int role) const
{
 return(data(index.column(), index.row(), role));
}

// Returns the data for an item, and a particular role
boost::any TMemoryTable::data (const int column, const int row, int role) const
{
 boost::any ret;
 if (role != Wt::DisplayRole && role != Wt::EditRole) return(ret);
 if (column < mt->get_column_count() && row < mt->get_row_count())
 {
//  ret = Wt::WString::fromUTF8(mt->get_column_name(index.column()));
// dlArray, dlBlob, dlDate, dlTime, dlTimestamp, dlString,
//           dlSmallint, dlInteger, dlLargeint, dlFloat, dlDouble, dlNumeric
  mt->move_by(row);
  switch (mt->get_column_type(column))
  {
   case dlSmallint:
   case dlInteger: ret = mt->field_by_number(column)->get_integer(); break;
   default: ret = Wt::WString::fromUTF8(mt->field_by_number(column)->get_string());
  };
 }
 return(ret);
}

// Returns the header data for a column
boost::any TMemoryTable::headerData (int section, Wt::Orientation orientation, int role) const
{
 boost::any ret;
 if (section < mt->get_column_count())
 {
  ret = Wt::WString::fromUTF8(mt->get_column_name(section));
 }
 return(ret);
}*/
//--------------------------------------------------------------------------
//--------------------------------------------------------------------------
const std::string sha12str(boost::uuids::detail::sha1 &sha1)
{
 std::stringstream stm;
 unsigned hash[5] = {0};
 sha1.get_digest(hash);
 for(int i = 0; i < 5; i++)
 {
  stm << std::right << std::hex << std::noshowbase << std::setw(8) << std::setfill('0') << hash[i];
 }
 return(stm.str());
}
//--------------------------------------------------------------------------
//--------------------------------------------------------------------------
TQueryResourceList::TQueryResourceList()// : timer(io, boost::posix_time::seconds(5))//, work(io)
 : running(false)//, thr(&TQueryResourceList::thread, this)
{
// timerClean = new Wt::WTimer(this);
// timer.async_wait(boost::bind(&TQueryResourceList::timerCleanTimeout, this));
// io.run();

// boost::thread t(boost::bind(&TQueryResourceList::thread, this));
 lqr.clear();
}
//--------------------------------------------------------------------------
TQueryResourceList::~TQueryResourceList()
{
 stop();
 lqr.clear();
}
//--------------------------------------------------------------------------
//void TQueryResourceList::start()
//{
//// if (Wt::WApplication::instance() != NULL)
////   setParent(Wt::WApplication::instance()->root());

//// if (!timerClean->isActive() && Wt::WApplication::instance() != NULL)
//// {
////  timerClean->setInterval(60 * 2 * 1000);
////  timerClean->setSingleShot(false);
////  timerClean->timeout().connect(this, &TQueryResourceList::timerCleanTimeout);
////  timerClean->start();
//// }
//}
//--------------------------------------------------------------------------
void TQueryResourceList::start()
{
 {
  boost::lock_guard<boost::mutex> guard(mtx);
  if (running) return;
 }

 thr = boost::thread(&TQueryResourceList::thread, this);
}
//--------------------------------------------------------------------------
void TQueryResourceList::stop()
{
 {
  boost::lock_guard<boost::mutex> guard(mtx);
  if (!running) return;
  running = false;
 }
 thr.join();
}
//--------------------------------------------------------------------------
void TQueryResourceList::append(TQueryResource * qr, std::string path)
{
 boost::lock_guard<boost::mutex> guard(mtx);
 lqr[path] = qr;
 server->addResource(qr, path);
}
//--------------------------------------------------------------------------
//void TQueryResourceList::timerCleanTimeout()
//{
// boost::lock_guard<boost::mutex> guard(mtx);

// for(auto itr = lqr.begin(); itr != lqr.end(); ++itr)
// {
//  if (itr->second->session_mode && !itr->second->isFree()
//     && itr->second->started < Wt::WDateTime::currentDateTime().addSecs(-2 * 60) )
//  {
//   itr->second->dl->close();
//  }
// }
//// timer.expires_at(timer.expires_at() + boost::posix_time::seconds(5));
//// timer.async_wait(boost::bind(&TQueryResourceList::timerCleanTimeout, this));
//// io.run();
//}
//--------------------------------------------------------------------------
void TQueryResourceList::thread()
{
 int c(0);
 boost::unique_lock<boost::mutex> lock(mtx);

 running = true;
 lock.unlock();

 for(;;)
 {
  for(int i(0); i < 100; i++)
   {boost::this_thread::sleep(boost::posix_time::milliseconds(10)); boost::this_thread::yield();}

  if (!lock.try_lock()) continue;
  if (!running) return;
  if (++c == 120)
  {
   for(auto itr = lqr.begin(); itr != lqr.end(); ++itr)
   {
    if (itr->second->session_mode && !itr->second->isFree()
       && itr->second->started < Wt::WDateTime::currentDateTime().addSecs(-2 * 60) )
    {
     itr->second->dl->close();
    }
   }
   c = 0;
  }
  lock.unlock();
 }
}
//--------------------------------------------------------------------------
//--------------------------------------------------------------------------
TQueryResource::TQueryResource(const int s_id, Wt::WObject *parent) :
  Wt::WResource(parent), started(Wt::WDateTime::currentDateTime()), session_id(s_id), session_mode(false),
  zest(""), timeout(0)
{
// suggestFileName("data.xml");
// Wt::log("notice") << __PRETTY_FUNCTION__;
}
TQueryResource::TQueryResource(const Wt::Http::ParameterMap & param, const int s_id, Wt::WObject *parent) :
  Wt::WResource(parent), started(Wt::WDateTime::currentDateTime()), session_id(s_id), session_mode(true),
  zest(""), timeout(120)
{
// Wt::Http::ParameterMap param;
// for(auto itr = request.getParameterMap().begin(); itr != request.getParameterMap().end(); ++itr)
//  param[Upper(itr->first)] = itr->second;
 init_db(param);
// timerClean = new Wt::WTimer(this);
// timerClean->setInterval(60 * 2 * 1000);
// timerClean->setSingleShot(false);
// timerClean->timeout().connect(this, &TQueryResource::ontimerCleanTimeout);
// timerClean->start();
}
TQueryResource::~TQueryResource()
{
 beingDeleted();
 if (session_mode)
 {
  delete(dl);
 }
}
const bool TQueryResource::isFree() const
{
 if (session_mode && !dl->isOpen()) return (true);
 else return (false);
}
void TQueryResource::handleRequest(const Wt::Http::Request& request, Wt::Http::Response& response)
{

}
//--------------------------------------------------------------------------
void TQueryResource::request2param(const Wt::Http::Request &request, Wt::Http::ParameterMap &param)
{
 for(auto itr = request.getParameterMap().begin(); itr != request.getParameterMap().end(); ++itr)
 {
  param[Lower(itr->first)].assign(itr->second.begin(), itr->second.end());
 }
}
//--------------------------------------------------------------------------
void TQueryResource::decodeFile(std::string &spool)
{

}
//--------------------------------------------------------------------------
void TQueryResource::decompressFile(std::string &spool)
{

}
//--------------------------------------------------------------------------
//--------------------------------------------------------------------------
//--------------------------------------------------------------------------
int TQueryResource::ExecuteProcedure(std::string & out, const Wt::Http::ParameterMap &param)
{
 std::string sql_proc("");

 out.clear();

 if (param.find("sql_proc") != param.end() && !param.find("sql_proc")->second.empty())
   sql_proc = Wt::Utils::urlDecode(param.find("sql_proc")->second[0]);

 Wt::WStandardItemModel mt(this);

 if (sql_proc.empty())
 {
  out = "empty parameter sql_proc";
  return(-1);
 }

 if (dl->ExecuteProcedure(&mt, sql_proc, param) >= 0)
 {
  out = mt2xml(&mt);
  return(1);
 }
 else
 {
  out = dl->errorstr;
  return(-1);
 }
}
//--------------------------------------------------------------------------
int TQueryResource::ExecuteSQL(std::string & out, const Wt::Http::ParameterMap &param)
{
 std::string sql("");

 out.clear();

 if (param.find("sql") != param.end() && !param.find("sql")->second.empty())
   sql = Wt::Utils::urlDecode(param.find("sql")->second[0]);

 Wt::WStandardItemModel mt(this);

 if (sql.empty())
 {
  out = "empty parameter sql";
  return(-1);
 }

 if (dl->ExecuteSQL(&mt, sql, param) >= 0)
 {
  out = mt2xml(&mt);
  return(1);
 }
 else
 {
  out = dl->errorstr;
  return(-1);
 }
}
//--------------------------------------------------------------------------
int TQueryResource::Begin(std::string & out)
{
 if (!session_mode)
 {
  out = "session_mode is FALSE";
  return(-1);
 }
 if (!dl->isOpen())
 {
  out = "Database is closed";
  return(-1);
 }

 int res = dl->Begin();

 if (res >= 0)
 {
  Wt::WStandardItemModel mt(1, 1, this);
  mt.setHeaderData(0, std::string("TransactionStatus"));
  mt.setHeaderData(0, Wt::Orientation::Horizontal, std::string("Integer"), Wt::UserRole);

  mt.setData(0, 0, res);

  out = mt2xml(&mt);
 }
 else
 {
  out = dl->errorstr;
 }
 return(res);
}
//--------------------------------------------------------------------------
int TQueryResource::Commit(std::string & out)
{
 if (!session_mode)
 {
  out = "session_mode is FALSE";
  return(-1);
 }
 if (!dl->isOpen())
 {
  out = "Database is closed";
  return(-1);
 }

 int res = dl->Commit();

 if (res >= 0)
 {
  Wt::WStandardItemModel mt(1, 1, this);
  mt.setHeaderData(0, std::string("TransactionStatus"));
  mt.setHeaderData(0, Wt::Orientation::Horizontal, std::string("Integer"), Wt::UserRole);

  mt.setData(0, 0, res);

  out = mt2xml(&mt);
 }
 else
 {
  out = dl->errorstr;
 }
 return(res);
}
//--------------------------------------------------------------------------
int TQueryResource::TransactionStatus(std::string & out)
{
 if (!session_mode)
 {
  out = "session_mode is FALSE";
  return(-1);
 }
 if (!dl->isOpen())
 {
  out = "Database is closed";
  return(-1);
 }

 int res = dl->TransactionStatus();

 if (res > 0)
 {
  Wt::WStandardItemModel mt(1, 1, this);
  mt.setHeaderData(0, std::string("TransactionStatus"));
  mt.setHeaderData(0, Wt::Orientation::Horizontal, std::string("Integer"), Wt::UserRole);

  mt.setData(0, 0, res);

  out = mt2xml(&mt);
 }
 else
 {
  out = dl->errorstr;
 }
 return(res);
}
//--------------------------------------------------------------------------
int TQueryResource::Session(std::string & out, const Wt::Http::ParameterMap &param)
{
 TQueryResource * qr = NULL;
 std::string path;
 extern TQueryResourceList qrList;

 for(auto itr = qrList.lqr.begin(); itr != qrList.lqr.end(); ++itr)
 {
  if (itr->second->isFree())
  {
   path = itr->first;
   qr = itr->second;
   break;
  }
 }

 if (qr == NULL)
 {
  std::stringstream stm;
  stm << Wt::WDateTime::currentDateTime().toString() << " " << session_id;
  path = "/" + Wt::Utils::base64Encode(stm.str());
  qr = new TQueryResource(param, session_id, this);
  qrList.append(qr, path);
 }

 qr->init_db(param);

 if (qr->dl->isOpen())
 {
  std::stringstream stm;
  stm << Wt::WDateTime::currentDateTime().toString() << " " << session_id;
  qr->zest = Wt::Utils::base64Encode(stm.str());

  Wt::WStandardItemModel mt(1, 3, this);
  mt.setHeaderData(0, std::string("session"));
  mt.setHeaderData(1, std::string("zest"));
  mt.setHeaderData(2, std::string("timeout"));

  qr->started = Wt::WDateTime::currentDateTime();
//  out = path;

  mt.setData(0, 0, path);
  mt.setData(0, 1, qr->zest);
  mt.setData(0, 2, qr->timeout);

  out = mt2xml(&mt);
  //qrList.start();
  return(1);
 }
 else
 {
  out = qr->dl->errorstr;
  return(-1);
 }

}
//--------------------------------------------------------------------------
int TQueryResource::Session_close(std::string & out, const Wt::Http::ParameterMap &param)
{
 extern TQueryResourceList qrList;
 std::string zest(""), session("");

 if(session_mode)
 {
  dl->close();
  out = "Ok";
  return (1);
 }

 out.clear();

 if (param.find("zest") != param.end() && !param.find("zest")->second.empty())
   zest = param.find("zest")->second[0];
 if (param.find("session") != param.end() && !param.find("session")->second.empty())
   session = param.find("session")->second[0];

 auto itr = qrList.lqr.find(session);
 if (itr != qrList.lqr.end() && itr->first == session && itr->second->zest == zest)
 {
  itr->second->dl->close();
  out = "Ok";
  return (1);
 }

 out = "session not found";
 return (-1);
}
//--------------------------------------------------------------------------
unsigned long long TQueryResource::mkResponse(const int out_type, const std::string & out, Wt::Http::Response &response)
{
 unsigned long long ret(0);
 std::stringstream o;
 std::string type("data");

 if (out_type < 0)
 {
  type = "error";
  response.setStatus(400);
 }
 else
 {
  response.setStatus(200);
 }

 o<< "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" << std::endl;
 o<< "<body>" << std::endl << "<" << type << ">";
 o<< ((out_type < 0) ? to_xml(out) : out);
 o<< "</" << type << ">" << std::endl << "</body>";

 ret = o.tellp();

 response.setMimeType("text/xml");

 if (out_type >= 0 && ret > 0 && compress)
 {
//  const int CHUNK_SIZE = 8096;
//  const int COMPRESSION_LEVEL =  Z_DEFAULT_COMPRESSION; //8;
//  unsigned int rsize(0);
//  unsigned char outbuff[CHUNK_SIZE], inbuff[CHUNK_SIZE];
//  z_stream stream = {0};

//  stream.zalloc = Z_NULL;
//  stream.zfree = Z_NULL;
//  stream.opaque = Z_NULL;

//  if(deflateInit(&stream, COMPRESSION_LEVEL) != Z_OK) { return (0); }

//  int flush;
//  int offset(0);
//  do {
//      stream.avail_in = ret > offset + CHUNK_SIZE ? CHUNK_SIZE : ret - offset;
//      flush = (ret <= offset + stream.avail_in) ? Z_FINISH : Z_NO_FLUSH;
//      std::string s = o.str().substr(offset, stream.avail_in);
//      memcpy(inbuff, s.c_str(), s.length());
//      stream.next_in = inbuff;
//      do {
//          stream.avail_out = CHUNK_SIZE;
//          stream.next_out = outbuff;
//          int rdef = deflate(&stream, flush);
//          uint32_t nbytes = CHUNK_SIZE - stream.avail_out;
//          response.out().write((char *)outbuff, nbytes);
//          rsize += nbytes;
//      } while (stream.avail_out == 0);
//      offset += ret > offset + CHUNK_SIZE ? CHUNK_SIZE : ret - offset;
//  } while (flush != Z_FINISH);
//  deflateEnd(&stream);
  TBytea src, dst; /*TZLib zlib;

  src = o.str();
  if (zlib.gzip(&src, &dst) > 0)
  {
   response.out().write((char *)dst[0], dst.get_size());
//  response.addHeader("Transfer-Encoding", "compress");
//   response.addHeader("Transfer-Encoding", "deflate");
   response.addHeader("Transfer-Encoding", "gzip");
   response.addHeader("adler32", std::to_string(static_cast<long long>(zlib.adler32)));
   ret = dst.get_size();
  }
  else
  {
   response.out() << o.str();
   ret = src.get_size();
  }*/

  std::stringstream stm; stm << src.get_size();
  response.addHeader("Data-Size", stm.str()); //std::to_string(static_cast<long long>(src.get_size())));

//  ret = rsize;
 }
 else
 {
  response.out() << o.str();
 }

 response.setContentLength(ret);

 return(ret);
}
//--------------------------------------------------------------------------
std::string TQueryResource::to_xml(const std::string& txt) const
{
 // Just five: &lt; (<), &amp; (&), &gt; (>), &quot; ("), and &apos; (')
 std::string ret(txt);
 for(size_t pos = ret.find("&"); pos != std::string::npos; pos = ret.find("&", pos + 1))
 {
  ret.replace(pos, 1, "&amp;");
 }
/* for(size_t pos = ret.find("<"); pos != std::string::npos; pos = ret.find("<", pos + 1))
 {
  ret.replace(pos, 1, "&lt;");
 }
 for(size_t pos = ret.find(">"); pos != std::string::npos; pos = ret.find(">", pos + 1))
 {
  ret.replace(pos, 1, "&gt;");
 }
 for(size_t pos = ret.find("\""); pos != std::string::npos; pos = ret.find("\"", pos + 1))
 {
  ret.replace(pos, 1, "&quot;");
 }
 for(size_t pos = ret.find("'"); pos != std::string::npos; pos = ret.find("'", pos + 1))
 {
  ret.replace(pos, 1, "&apos;");
 }*/
 return(ret);
}
//--------------------------------------------------------------------------
void TQueryResource::init_db(const Wt::Http::ParameterMap &param)
{
 std::string user(""), password(""), dbserver(""), dbname(""), role("");

 for(auto itr = param.begin(); itr != param.end(); ++itr)
 {
  if (Lower(itr->first) == "user" && !itr->second.empty() && user.empty())
  {
   user = itr->second.front();
  }
  if (Lower(itr->first) == "password" && !itr->second.empty() && password.empty())
  {
   password = itr->second.front();
  }
  if (Lower(itr->first) == "dbname" && !itr->second.empty())
  {
   dbname = itr->second.front();
  }
  if (Lower(itr->first) == "dbserver" && !itr->second.empty())
  {
   dbserver = itr->second.front();
  }
  if (Lower(itr->first) == "role" && !itr->second.empty())
  {
   role = itr->second.front();
  }
 }

 dl = DataLayerFactory();
 dl->setHostName(dbserver);
 dl->setDatabaseName(dbname);
 dl->setUserName(user);
 dl->setRole(role);
 dl->setPassword(password);

 dl->open();
}
//--------------------------------------------------------------------------

//--------------------------------------------------------------------------
//--------------------------------------------------------------------------
TEditWindow::TEditWindow(Wt::WObject *parent) : Wt::WDialog(parent)
{
 setupUi();
}
TEditWindow::TEditWindow(const Wt::WString &windowTitle, Wt::WObject *parent) :
 Wt::WDialog(windowTitle, parent)
{
 setupUi();
}
//--------------------------------------------------------------------------
TEditWindow::~TEditWindow()
{
}
//--------------------------------------------------------------------------
void TEditWindow::setupUi()
{
 setClosable(true);
 setResizable(true);

 Wt::WVBoxLayout * lv1 = new Wt::WVBoxLayout();
 contents()->setLayout(lv1);

 eStr = new Wt::WLineEdit();
 eStr->setObjectName("eStr");
 eStr->setEnabled(true);
 lv1->addWidget(eStr);

 Wt::WHBoxLayout * lh1 = new Wt::WHBoxLayout();
// lv1->addItem(lh1);

 Wt::WPushButton * bOk = new Wt::WPushButton("Ok"); bOk->setObjectName("bOk");
 bOk->clicked().connect(this, &TEditWindow::accept);
 bOk->setIcon(Wt::WLink("image/Apply.png"));

 Wt::WPushButton * bCancel = new Wt::WPushButton("Cancel");
 bCancel->setObjectName("bCancel");
 bCancel->clicked().connect(this, &TEditWindow::reject);
 bCancel->setIcon(Wt::WLink("image/Cancel.png"));

 lh1->addWidget(bOk);
 lh1->addWidget(bCancel);

 Wt::WContainerWidget * footer = new Wt::WContainerWidget();
 footer->addStyleClass("modal-footer");
 footer->setLayout(lh1);
 lv1->addWidget(footer);
}
//--------------------------------------------------------------------------
//--------------------------------------------------------------------------
TLineEdit::TLineEdit(Wt::WContainerWidget *parent) : Wt::WContainerWidget(parent), clicked(this)
{
    setupUi();
}

void TLineEdit::setupUi()
{
    setPadding(0, 0);

    lh = new Wt::WHBoxLayout();
    lh->setContentsMargins(3, 0, 0, 0);
    setLayout(lh);

    setMargin(0);

    eStr = new Wt::WLineEdit();
    eStr->setMargin(Wt::WLength(0));
    eStr->setReadOnly(true);
    lh->addWidget(eStr, 0/*, Wt::AlignmentFlag::AlignTop*/);

    btn = new Wt::WPushButton(Wt::WString("..."));
    btn->setMargin(Wt::WLength(0));
    //btn->setMaximumSize(Wt::WLength (16), Wt::WLength(16));
    //btn->setMinimumSize(Wt::WLength (16), Wt::WLength(16));
    //btn->addStyleClass("icon-th-list");
    btn->clicked().connect(this, &TLineEdit::onClicked);

    lh->addWidget(btn, 0/*, Wt::AlignmentFlag::AlignLeft*/);
    lh->addStretch(1);
}

void TLineEdit::setEnabled(const bool v)
{
    for(int i = 0; i < layout()->count(); ++i)
    {
        Wt::WWidget * w = layout()->itemAt(i)->widget();
        if(w != NULL && v) w->enable();
        if(w != NULL && !v) w->disable();
    }
}

void TLineEdit::setText(const Wt::WString& str)
{
    eStr->setText(str);
}

void TLineEdit::onClicked()
{
    clicked.emit();
}
//--------------------------------------------------------------------------
//--------------------------------------------------------------------------
TProgressWindow::TProgressWindow(Wt::WObject *parent) : /*Wt::WMessageBox */ Wt::WDialog(parent)
{
 setupUi();
}
TProgressWindow::TProgressWindow(const Wt::WString &windowTitle, Wt::WObject *parent) :
 /*Wt::WMessageBox */ Wt::WDialog(/*windowTitle,*/ parent)
{
 setupUi();
 setWindowTitle(windowTitle);
}
void TProgressWindow::setupUi()
{
// setClosable(false);
// setResizable(false);

 Wt::WVBoxLayout * lv1 = new Wt::WVBoxLayout();
 contents()->setLayout(lv1);

 label = new Wt::WText(); label->setObjectName("label");
 lv1->addWidget(label);

 progress = new Wt::WProgressBar; progress->setObjectName("progress");
 lv1->addWidget(progress);
 progress->setRange(0, 100);

// Wt::WHBoxLayout * lh1 = new Wt::WHBoxLayout();
// lv1->addItem(lh1);

// Wt::WPushButton * bOk = new Wt::WPushButton("Ok"); bOk->setObjectName("bOk");
// bOk->clicked().connect(this, &TEditWindow::accept);
// bOk->setIcon(Wt::WLink("image/Apply.png"));

// Wt::WPushButton * bCancel = new Wt::WPushButton("Cancel");
// bCancel->setObjectName("bCancel");
// bCancel->clicked().connect(this, &TEditWindow::reject);
// bCancel->setIcon(Wt::WLink("image/Cancel.png"));

// lh1->addWidget(bOk);
// lh1->addWidget(bCancel);

// Wt::WContainerWidget * footer = new Wt::WContainerWidget();
// footer->addStyleClass("modal-footer");
// footer->setLayout(lh1);
// lv1->addWidget(footer);
 setValue(0);
}
void TProgressWindow::setValue(const int value)
{
 progress->setValue(value);
 if(Wt::WApplication::instance() != NULL) Wt::WApplication::instance()->processEvents();
}
void TProgressWindow::appendValue(const int value)
{
 progress->setValue(progress->value() + value);
 if(Wt::WApplication::instance() != NULL) Wt::WApplication::instance()->processEvents();
}
void TProgressWindow::appendValue (const std::string& label, const int value)
{
 setLabel(label);
 appendValue(value);
}
void TProgressWindow::setLabel(const std::string& value)
{
 label->setText(Wt::WString::fromUTF8(value));
 if(Wt::WApplication::instance() != NULL) Wt::WApplication::instance()->processEvents();
}
void TProgressWindow::show()
{
 Wt::WDialog::show();
 if(Wt::WApplication::instance() != NULL) Wt::WApplication::instance()->processEvents();
}
//--------------------------------------------------------------------------
//--------------------------------------------------------------------------
//--------------------------------------------------------------------------
//--------------------------------------------------------------------------
//--------------------------------------------------------------------------
bool jpeg_resize(const std::string& src_file, const std::string& dst_file, const int width, const int height)
{
    int h, w;

    boost::gil::rgb8_image_t img;
    boost::gil::read_image(src_file, img, boost::gil::jpeg_tag{});

    if (img.width() > img.height())
    {
        w = width;
        h = img.height() * width/img.width();
    }
    else
    {
        w = img.width() * height/img.height();
        h = height;
    }

    // test resize_view
    // Scale the image to 100x100 pixels using bilinear resampling
    boost::gil::rgb8_image_t square100x100(w, h);
    boost::gil::resize_view(boost::gil::const_view(img), boost::gil::view(square100x100), boost::gil::bilinear_sampler{});
    boost::gil::write_view(dst_file, boost::gil::const_view(square100x100), boost::gil::jpeg_tag{});

    return true;
}

//--------------------------------------------------------------------------
void ShowMessage(Wt::WObject * parent, Wt::WString message, Wt::WString label)
{
 /* Wt::WDialog dialog(parent);
  dialog.setWindowTitle(label);

  new Wt::WText(message, dialog.contents());
  Wt::WPushButton ok("Ok", dialog.footer());
  ok.setDefault(true);
  ok.clicked().connect(&dialog, &Wt::WDialog::accept);
  dialog.setClosable(true);
  dialog.setResizable(true);
  dialog.rejectWhenEscapePressed(true);
  dialog.exec();*/
  Wt::WMessageBox::show(label, message, Wt::StandardButton::Ok);
}
//--------------------------------------------------------------------------
//--------------------------------------------------------------------------
//void saveValue(const std::string value, const std::string param, const std::string pgroup, const std::string pref)
//{
// boost::property_tree::ptree pt;

// extern TSQLayer sl;
//// sl.profileSaveValue(value, param, pgroup, pref);

// std::string group(pgroup);
// while(group.find(".") != std::string::npos) {group.replace(group.find("."), 1, "_");}
// while(group.find(" ") != std::string::npos) {group.replace(group.find(" "), 1, "_");}

// try
// {
//  boost::property_tree::read_ini(Wt::WApplication::instance()->appRoot() + ini_file, pt);
// }
// catch(boost::property_tree::info_parser::info_parser_error & e)
// {
//  Wt::log("notice") << __PRETTY_FUNCTION__ << e.what();
// }
// catch(...){}

// pt.put(group + "." + pref + "_" + param, value);

// try
// {
//  boost::property_tree::write_ini(Wt::WApplication::instance()->appRoot() + ini_file, pt);
// }
// catch(boost::property_tree::info_parser::info_parser_error & e)
// {
//  logtxt(e.message());
// }
//}
//void saveValue(const bool value, const std::string param, const std::string pgroup, const std::string pref)
//{
// boost::property_tree::ptree pt;

// std::string group(pgroup);
// while(group.find(".") != std::string::npos) {group.replace(group.find("."), 1, "_");}
// while(group.find(" ") != std::string::npos) {group.replace(group.find(" "), 1, "_");}

// try
// {
//  boost::property_tree::read_ini(Wt::WApplication::instance()->appRoot() + ini_file, pt);
// }
// catch(boost::property_tree::info_parser::info_parser_error & e)
// {
//  Wt::log("notice") << __PRETTY_FUNCTION__ << e.what();
// }
// catch(...){}
// pt.put(group + "." + pref + "_" + param, value);

// try
// {
//  boost::property_tree::write_ini(Wt::WApplication::instance()->appRoot() + ini_file, pt);
// }
// catch(boost::property_tree::info_parser::info_parser_error & e)
// {
//  logtxt(e.message());
// }
//}
//std::string getValue(const std::string param, const std::string pgroup, const std::string pref)
//{
// boost::property_tree::ptree pt;
// std::string group(pgroup);
// while(group.find(".") != std::string::npos) {group.replace(group.find("."), 1, "_");}
// while(group.find(" ") != std::string::npos) {group.replace(group.find(" "), 1, "_");}
// try
// {
//  boost::property_tree::read_ini(Wt::WApplication::instance()->appRoot() + ini_file, pt);
// }
// catch(boost::property_tree::info_parser::info_parser_error & e)
// {
//  Wt::log("notice") << __PRETTY_FUNCTION__ << e.what();
//  return(std::string());
// }
// catch(...){return(std::string());}
// return(pt.get(group + "." + pref + "_" + param, ""));
//}
//bool getValueBool(const std::string param, const std::string pgroup, const std::string pref)
//{
// boost::property_tree::ptree pt;
// std::string group(pgroup);
// while(group.find(".") != std::string::npos) {group.replace(group.find("."), 1, "_");}
// while(group.find(" ") != std::string::npos) {group.replace(group.find(" "), 1, "_");}
// try
// {
//  boost::property_tree::read_ini(Wt::WApplication::instance()->appRoot() + ini_file, pt);
// }
// catch(boost::property_tree::info_parser::info_parser_error & e)
// {
//  Wt::log("notice") << __PRETTY_FUNCTION__ << e.what();
//  return(false);
// }
//catch(...){return(false);}
// return(pt.get(group + "." + pref + "_" + param, false));
//}
/*void saveColumnWidth(const Wt::WAbstractItemView * view, const std::string pgroup, const std::string pref)
{
 boost::property_tree::ptree pt;
 std::string group(pgroup);
 while(group.find(".") != std::string::npos) {group.replace(group.find("."), 1, "_");}
 while(group.find(" ") != std::string::npos) {group.replace(group.find(" "), 1, "_");}
 try
 {
  boost::property_tree::read_ini(Wt::WApplication::instance()->appRoot() + ini_file, pt);
 }
 catch(boost::property_tree::info_parser::info_parser_error & e)
 {
  Wt::log("notice") << __PRETTY_FUNCTION__ << e.what();
 }
catch(...){}
 std::string param(group + "." + pref + "_" + view->objectName());

 if (view == 0 || view->model() == 0) return;

 for(int i = 0; i < view->model()->columnCount(); i++)
 {
  std::string column("");
  double width = view->columnWidth(i).value();
  if (typeid(std::string) == view->model()->headerData(i).type())
  {
   column = boost::any_cast<std::string>(view->model()->headerData(i));
   while(column.find(".") != std::string::npos) {column.replace(column.find("."), 1, "_");}
   while(column.find(" ") != std::string::npos) {column.replace(column.find(" "), 1, "_");}
  }
  else
  {std::stringstream stm; stm << "column_" << i; column = stm.str();}

  pt.put(param + "_" + column, width);
 }
 try
 {
  boost::property_tree::write_ini(Wt::WApplication::instance()->appRoot() + ini_file, pt);
 }
 catch(boost::property_tree::info_parser::info_parser_error & e)
 {
  logtxt(e.message());
 }
}*/
//--------------------------------------------------------------------------
/*void setColumnWidth(Wt::WAbstractItemView * view, const std::string pgroup, const std::string pref)
{
 boost::property_tree::ptree pt;
 std::string group(pgroup);
 while(group.find(".") != std::string::npos) {group.replace(group.find("."), 1, "_");}
 while(group.find(" ") != std::string::npos) {group.replace(group.find(" "), 1, "_");}
 try
 {
  boost::property_tree::read_ini(Wt::WApplication::instance()->appRoot() + ini_file, pt);
 }
 catch(boost::property_tree::info_parser::info_parser_error & e)
 {
  Wt::log("notice") << __PRETTY_FUNCTION__ << e.what();
 }
catch(...){}
 std::string param(group + "." + pref + "_" + view->objectName());

 if (view == 0 || view->model() == 0) return;

 for(int i = 0; i < view->model()->columnCount(); i++)
 {
  std::string column;
  double width;
  if (typeid(std::string) == view->model()->headerData(i).type())
  {
   column = boost::any_cast<std::string>(view->model()->headerData(i));
   while(column.find(".") != std::string::npos) {column.replace(column.find("."), 1, "_");}
   while(column.find(" ") != std::string::npos) {column.replace(column.find(" "), 1, "_");}
  }
  else
  {std::stringstream stm; stm << "column_" << i; column = stm.str();}

  width = pt.get(param + "_" + column, 0);

  if (width > 0) {view->setColumnWidth(i, Wt::WLength(width));}
 }
}*/
//--------------------------------------------------------------------------
std::string str2html(const std::string& s)
{
 std::string p("<br />");
 std::string ret(s);

 while(ret.find('\n') != std::string::npos)
 {
  size_t pos = ret.find('\n');
  ret.replace(pos, 1, p);
 }
 return(ret);
}
//--------------------------------------------------------------------------
//--------------------------------------------------------------------------
//--------------------------------------------------------------------------

//--------------------------------------------------------------------------
Wt::WString to_wstring(boost::any const& a)
{
 return(Wt::WString::fromUTF8(to_string(a)));
}
Wt::WDate to_wdate(boost::any const& value)
{
 Wt::WDate ret;
 if (typeid(Wt::WDate) == value.type()) {ret = boost::any_cast<Wt::WDate>(value);}
 else if (typeid(Wt::WDateTime) == value.type()) {ret = boost::any_cast<Wt::WDateTime>(value).date();}
 return(ret);
}
Wt::WDateTime to_wdatetime(boost::any const& value)
{
 Wt::WDateTime ret;
 if (typeid(Wt::WDateTime) == value.type()) {ret = boost::any_cast<Wt::WDateTime>(value);}
 else if (typeid(Wt::WDate) == value.type()) {ret = Wt::WDateTime(boost::any_cast<Wt::WDate>(value), Wt::WTime(0, 0));}
 return(ret);
}
std::string to_string(boost::any const& value)
{
    std::string ans("");
    if (typeid(std::string) == value.type()) {
        ans = boost::any_cast<std::string>(value);
    } else if (typeid(Wt::WString) == value.type()) {
        ans = boost::any_cast<Wt::WString>(value).toUTF8();
    } else if (typeid(const char *) == value.type()) {
        ans = std::string(boost::any_cast<const char *>(value));
    } else if (typeid(bool) == value.type()) {
        ans = boost::any_cast<bool>(value) ? "1" : "0";
    } else if (typeid(char) == value.type()) {
        ans = std::to_string(static_cast<long long>(boost::any_cast<char>(value)));
    } else if (typeid(unsigned short) == value.type()) {
        ans = std::to_string(static_cast<unsigned long long>(
                                 boost::any_cast<unsigned short>(value)));
    } else if (typeid(short) == value.type()) {
        ans = std::to_string(static_cast<long long>(boost::any_cast<short>(value)));
    } else if (typeid(unsigned short) == value.type()) {
        ans = std::to_string(static_cast<unsigned long long>(
                                 boost::any_cast<unsigned short>(value)));
    } else if (typeid(int) == value.type()) {
        ans = std::to_string(static_cast<long long>(
                                 boost::any_cast<int>(value)));
    } else if (typeid(long) == value.type()) {
        ans = std::to_string(static_cast<long long>(
                                 boost::any_cast<long>(value)));
    } else if (typeid(unsigned int) == value.type()) {
        ans = std::to_string(static_cast<unsigned long long>(
                                 boost::any_cast<unsigned int>(value)));
    } else if (typeid(unsigned long) == value.type()) {
        ans = std::to_string(static_cast<unsigned long long>(
                                         boost::any_cast<unsigned long>(value)));
    } else if (typeid(long long) == value.type()) {
        ans = std::to_string(boost::any_cast<long long>(value));
    } else if (typeid(unsigned long long) == value.type()) {
        ans = std::to_string(boost::any_cast<unsigned long long>(value));
    } else if (typeid(float) == value.type()) {
        ans = std::to_string(static_cast<long double>(
                                 boost::any_cast<float>(value)));
    } else if (typeid(double) == value.type()) {
        ans = std::to_string(static_cast<long double>(
                                 boost::any_cast<double>(value)));
    } else if (typeid(Wt::WDate) == value.type()) {
        ans = boost::any_cast<Wt::WDate>(value).toString(Wt::WString::fromUTF8("yyyy-MM-dd")).toUTF8();
    } else if (typeid(Wt::WTime) == value.type()) {
        ans = boost::any_cast<Wt::WTime>(value).toString(Wt::WString::fromUTF8("hh:mm:ss")).toUTF8();
    } else if (typeid(Wt::WDateTime) == value.type()) {
        ans = boost::any_cast<Wt::WDateTime>(value).toString(Wt::WString::fromUTF8("yyyy-MM-dd hh:mm:ss")).toUTF8();
    } else if (typeid(TNumeric) == value.type()) {
        ans = boost::any_cast<TNumeric>(value).toString();
    } else if (typeid(Currency) == value.type()) {
        ans = Wt::WString(boost::any_cast<Currency>(value).toString()).toUTF8();
    } else if (typeid(TBytea) == value.type()) {
        ans = boost::any_cast<TBytea>(value).base64();
    }
    return (ans);
}
int to_int(boost::any const& value)
{
 int ans(0);

 if (value.empty())
    return std::numeric_limits<int>::signaling_NaN();

    if (typeid(int) == value.type()) { ans = boost::any_cast<int>(value);
    } else if (typeid(long) == value.type()) { ans = boost::any_cast<long>(value);
    } else if (typeid(int64_t) == value.type()) { ans = boost::any_cast<int64_t>(value);
    } else if (typeid(unsigned int) == value.type()) { ans = boost::any_cast<unsigned int>(value);
    } else if (typeid(unsigned long) == value.type()) { ans = boost::any_cast<unsigned long>(value);
    } else if (typeid(long long) == value.type()) { ans = (boost::any_cast<long long>(value));
    } else if (typeid(unsigned long long) == value.type()) { ans = (boost::any_cast<unsigned long long>(value));
    } else if (typeid(char) == value.type()) { ans = boost::any_cast<char>(value);
    } else if (typeid(unsigned short) == value.type()) { ans = boost::any_cast<unsigned short>(value);
    } else if (typeid(short) == value.type()) { ans = boost::any_cast<short>(value);
    } else if (typeid(unsigned short) == value.type()) { ans = boost::any_cast<unsigned short>(value);
    } else if (typeid(float) == value.type()) { ans = boost::any_cast<float>(value);
    } else if (typeid(double) == value.type()) { ans = boost::any_cast<double>(value);
    } else if (typeid(bool) == value.type()) { ans = boost::any_cast<bool>(value) ? 1 : 0;
    } else if (typeid(std::string) == value.type()) {
        ans = atoi(boost::any_cast<std::string>(value).c_str());
    } else if (typeid(Wt::WString) == value.type()) {
        ans = atoi(boost::any_cast<Wt::WString>(value).toUTF8().c_str());
//    } else if (typeid(Wt::WDate) == value.type()) {
//        ans = boost::any_cast<Wt::WDate>(value).toString(Wt::WString::fromUTF8("yyyy-MM-dd")).toUTF8();
//    } else if (typeid(Wt::WTime) == value.type()) {
//        ans = boost::any_cast<Wt::WTime>(value).toString(Wt::WString::fromUTF8("hh:mm:ss")).toUTF8();
//    } else if (typeid(Wt::WDateTime) == value.type()) {
//        ans = boost::any_cast<Wt::WDateTime>(value).toString(Wt::WString::fromUTF8("yyyy-MM-dd hh:mm:ss")).toUTF8();
    } else if (typeid(TNumeric) == value.type()) {
        ans = boost::any_cast<TNumeric>(value);
    }
    return (ans);
}
int to_int64(boost::any const& value)
{
    int64_t ans(0);

    if (value.empty())
        return std::numeric_limits<int>::signaling_NaN();
    if (typeid(int64_t) == value.type()) { ans = boost::any_cast<int64_t>(value);
    } else if (typeid(bool) == value.type()) { ans = boost::any_cast<bool>(value) ? 1 : 0;
    } else if (typeid(char) == value.type()) { ans = boost::any_cast<char>(value);
    } else if (typeid(unsigned short) == value.type()) { ans = boost::any_cast<unsigned short>(value);
    } else if (typeid(short) == value.type()) { ans = boost::any_cast<short>(value);
    } else if (typeid(unsigned short) == value.type()) { ans = boost::any_cast<unsigned short>(value);
    } else if (typeid(int) == value.type()) { ans = boost::any_cast<int>(value);
    } else if (typeid(long) == value.type()) { ans = boost::any_cast<long>(value);
    } else if (typeid(unsigned int) == value.type()) { ans = boost::any_cast<unsigned int>(value);
    } else if (typeid(unsigned long) == value.type()) { ans = boost::any_cast<unsigned long>(value);
    } else if (typeid(long long) == value.type()) { ans = (boost::any_cast<long long>(value));
    } else if (typeid(unsigned long long) == value.type()) { ans = (boost::any_cast<unsigned long long>(value));
    } else if (typeid(float) == value.type()) { ans = boost::any_cast<float>(value);
    } else if (typeid(double) == value.type()) { ans = boost::any_cast<double>(value);
    } else if (typeid(std::string) == value.type()) {
        ans = atoi(boost::any_cast<std::string>(value).c_str());
    } else if (typeid(Wt::WString) == value.type()) {
        ans = atoi(boost::any_cast<Wt::WString>(value).toUTF8().c_str());
        //    } else if (typeid(Wt::WDate) == value.type()) {
        //        ans = boost::any_cast<Wt::WDate>(value).toString(Wt::WString::fromUTF8("yyyy-MM-dd")).toUTF8();
        //    } else if (typeid(Wt::WTime) == value.type()) {
        //        ans = boost::any_cast<Wt::WTime>(value).toString(Wt::WString::fromUTF8("hh:mm:ss")).toUTF8();
        //    } else if (typeid(Wt::WDateTime) == value.type()) {
        //        ans = boost::any_cast<Wt::WDateTime>(value).toString(Wt::WString::fromUTF8("yyyy-MM-dd hh:mm:ss")).toUTF8();
    } else if (typeid(TNumeric) == value.type()) {
        ans = boost::any_cast<TNumeric>(value);
    }
    return (ans);
}
TNumeric to_numeric(boost::any const& value)
{
 TNumeric ans;
// if (value.empty())
//    return std::numeric_limits<int>::signaling_NaN();

   if (typeid(bool) == value.type()) {
        ans = boost::any_cast<bool>(value) ? 1 : 0;
    } else if (typeid(char) == value.type()) { ans = boost::any_cast<char>(value);
    } else if (typeid(short) == value.type()) { ans = boost::any_cast<short>(value);
    } else if (typeid(unsigned short) == value.type()) { ans = boost::any_cast<unsigned short>(value);
    } else if (typeid(int) == value.type()) { ans = boost::any_cast<int>(value);
    } else if (typeid(long) == value.type()) { ans = boost::any_cast<long>(value);
    } else if (typeid(unsigned int) == value.type()) { ans = boost::any_cast<unsigned int>(value);
    } else if (typeid(unsigned long) == value.type()) { ans = boost::any_cast<unsigned long>(value);
    } else if (typeid(long long) == value.type()) { ans = (boost::any_cast<long long>(value));
//    } else if (typeid(unsigned long long) == value.type()) {
//        ans = (boost::any_cast<unsigned long long>(value));
    } else if (typeid(float) == value.type()) { ans = boost::any_cast<float>(value);
    } else if (typeid(double) == value.type()) { ans = (boost::any_cast<double>(value));
    } else if (typeid(std::string) == value.type()) {
        ans = atoi(boost::any_cast<std::string>(value).c_str());
    } else if (typeid(Wt::WString) == value.type()) {
        ans = atoi(boost::any_cast<Wt::WString>(value).toUTF8().c_str());
//    } else if (typeid(Wt::WDate) == value.type()) {
//        ans = boost::any_cast<Wt::WDate>(value).toString(Wt::WString::fromUTF8("yyyy-MM-dd")).toUTF8();
//    } else if (typeid(Wt::WTime) == value.type()) {
//        ans = boost::any_cast<Wt::WTime>(value).toString(Wt::WString::fromUTF8("hh:mm:ss")).toUTF8();
//    } else if (typeid(Wt::WDateTime) == value.type()) {
//        ans = boost::any_cast<Wt::WDateTime>(value).toString(Wt::WString::fromUTF8("yyyy-MM-dd hh:mm:ss")).toUTF8();
    } else if (typeid(TNumeric) == value.type()) {
        ans = boost::any_cast<TNumeric>(value);
    }
    return (ans);
}
bool to_bool(boost::any const& value)
{
 bool ans(false);

   if (typeid(bool) == value.type()) {
        ans = boost::any_cast<bool>(value);
    } else if (typeid(char) == value.type()) {
        ans = static_cast<long long>(boost::any_cast<char>(value)) == 1 ? true : false ;
//    } else if (typeid(short) == value.type()) {
//        ans = (static_cast<long long>(boost::any_cast<short>(value)));
//    } else if (typeid(unsigned short) == value.type()) {
//        ans = (static_cast<unsigned long>(boost::any_cast<unsigned short>(value)));
    } else if (typeid(int) == value.type()) {
        ans = (static_cast<long long>(boost::any_cast<int>(value)))  == 1 ? true : false;
    } else if (typeid(long) == value.type()) {
        ans = (static_cast<long long>(boost::any_cast<long>(value))) == 1 ? true : false;
//    } else if (typeid(unsigned int) == value.type()) {
//        ans = (static_cast<unsigned long>(boost::any_cast<unsigned int>(value)));
//    } else if (typeid(unsigned long) == value.type()) {
//        ans = (static_cast<unsigned long>(boost::any_cast<unsigned long>(value)));
//    } else if (typeid(long long) == value.type()) {
//        ans = (boost::any_cast<long long>(value));
//    } else if (typeid(unsigned long long) == value.type()) {
//        ans = (boost::any_cast<unsigned long long>(value));
//    } else if (typeid(float) == value.type()) {
//        ans = (static_cast<long double>(boost::any_cast<float>(value)));
//    } else if (typeid(double) == value.type()) {
//        ans = (static_cast<long double>(boost::any_cast<double>(value)));
//    } else if (typeid(std::string) == value.type()) {
//        ans = atoi(boost::any_cast<std::string>(value).c_str());
//    } else if (typeid(Wt::WString) == value.type()) {
//        ans = atoi(boost::any_cast<Wt::WString>(value).toUTF8().c_str());
//    } else if (typeid(Wt::WDate) == value.type()) {
//        ans = boost::any_cast<Wt::WDate>(value).toString(Wt::WString::fromUTF8("yyyy-MM-dd")).toUTF8();
//    } else if (typeid(Wt::WTime) == value.type()) {
//        ans = boost::any_cast<Wt::WTime>(value).toString(Wt::WString::fromUTF8("hh:mm:ss")).toUTF8();
//    } else if (typeid(Wt::WDateTime) == value.type()) {
//        ans = boost::any_cast<Wt::WDateTime>(value).toString(Wt::WString::fromUTF8("yyyy-MM-dd hh:mm:ss")).toUTF8();
    } else if (typeid(TNumeric) == value.type()) {
        ans = boost::any_cast<TNumeric>(value).toFloat() > 0 ? true : false;
    }


 return(ans);
}
//--------------------------------------------------------------------------
