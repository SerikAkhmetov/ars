#include <Wt/WCalendar>

#include "requsitionUpdWindow.h"

//--------------------------------------------------------------------------
TrequsitionUpdDefault::TrequsitionUpdDefault(Wt::WContainerWidget *parent, TDataLayer * p_dl) : Twindow(p_dl, "TrequsitionUpd", parent), requisition(p_dl), city(0)
{
}
//--------------------------------------------------------------------------
//TrequsitionUpdDefault::~TrequsitionUpdDefault()
//{
//}
//--------------------------------------------------------------------------
void TrequsitionUpdDefault::setupUi()
{
  Wt::WHBoxLayout * lh;
  lh = new Wt::WHBoxLayout();
  lh->setContentsMargins(3, 0, 0, 0);

  Wt::WVBoxLayout * lv;
  lv = new Wt::WVBoxLayout();
  lv->setContentsMargins(3, 0, 0, 0);

  tabMain = new Wt::WTabWidget();

  Wt::WTable * tP1 = new Wt::WTable(this); tP1->setObjectName("tP1");
 // Wt::WTable * tP2 = new Wt::WTable(this); tP2->setObjectName("tP2");

  tabMain->addTab(tP1, Wt::WString(L"������"));
 // tabMain->addTab(tP2, Wt::WString(L"���������"));

  lv->addWidget(tabMain);
  lh->addStretch(1);
  lh->addLayout(lv);
  lh->addStretch(1);
  setLayout(lh);

  int r(0);
    
  deTa = new TDateEdit();
  deTa->setObjectName("deTa");
  deTa->setEnabled(true);
  //  ePhone->textInput().connect(this, &TfileUpd::onOkEnable);
  //  ePhone->textInput().connect(this, &TfileUpd::onNameUserInput);
  Wt::WText * l = new Wt::WText(L"���� ���:");  l->setPadding(5);
  l->setWordWrap(false);
  tP1->elementAt(r, 0)->addWidget(l);
  tP1->elementAt(r, 1)->addWidget(deTa);
  r++;

  ePhone = new Wt::WLineEdit(); //
  ePhone->setObjectName("ePhone");
  ePhone->setEnabled(true);
//  ePhone->textInput().connect(this, &TfileUpd::onOkEnable);
//  ePhone->textInput().connect(this, &TfileUpd::onNameUserInput);
  l = new Wt::WText(L"�������:");  l->setPadding(5);
  tP1->elementAt(r, 0)->addWidget(l);
  tP1->elementAt(r, 1)->addWidget(ePhone);
  r++;

  eCustomer = new Wt::WLineEdit(); //
  eCustomer->setObjectName("eCustomer");
  eCustomer->setEnabled(true);
  //  ePhone->textInput().connect(this, &TfileUpd::onOkEnable);
  //  ePhone->textInput().connect(this, &TfileUpd::onNameUserInput);
  l = new Wt::WText(L"�.�.�.:");  l->setPadding(5);
  tP1->elementAt(r, 0)->addWidget(l);
  tP1->elementAt(r, 1)->addWidget(eCustomer);
  r++;

  Wt::WContainerWidget * cwP2 = new Wt::WContainerWidget();

  Wt::WVBoxLayout * lv2;
  lv2 = new Wt::WVBoxLayout();
  lv2->setContentsMargins(3, 0, 0, 0);

  cwP2->setLayout(lv2);

  tabMain->addTab(cwP2, Wt::WString(L"���������"));
  tabMain->setTabHidden(1, true);

  Wt::WPanel * pDate = new Wt::WPanel();
  pDate->setTitle(Wt::WString(L"�������� ���� �������"));
  pDate->setTitleBar(true);
  pDate->titleBarWidget()->addStyleClass("nav-header");

  cal = new Wt::WCalendar();
  cal->setSelectionMode(Wt::SelectionMode::SingleSelection);
  cal->setSingleClickSelect(true);
  cal->setSelectable(true);
  cal->activated().connect(this, &TrequsitionUpdDefault::onCalendarActivated);

  pDate->setCentralWidget(cal);

  Wt::WPanel * pCarLifter = new Wt::WPanel();
  pCarLifter->setTitle(Wt::WString(L"�������� ���������"));
  pCarLifter->setTitleBar(true);
  pCarLifter->titleBarWidget()->addStyleClass("nav-header");

  tCarLifter = new Wt::WTable(this); tCarLifter->setObjectName("tCarLifter");
  pCarLifter->setCentralWidget(tCarLifter);

  lv2->addWidget(pDate);
  lv2->addWidget(pCarLifter);

// footer
  Wt::WHBoxLayout * lh1 = new Wt::WHBoxLayout();
  Wt::WPushButton * bOk = new Wt::WPushButton("Ok"); bOk->setObjectName("bOk");
  bOk->clicked().connect(this, &TrequsitionUpdDefault::onbOk);
  bOk->setIcon(Wt::WLink("image/Apply.png"));

  Wt::WPushButton * bCancel = new Wt::WPushButton("Cancel");
  bCancel->setObjectName("bCancel");
  bCancel->clicked().connect(this, &Twindow::onbCancel);
  bCancel->setIcon(Wt::WLink("image/Cancel.png"));

  lh1->addStretch(1);
  lh1->addWidget(bOk);
  lh1->addWidget(bCancel);

  Wt::WContainerWidget * footer = new Wt::WContainerWidget();
  footer->addStyleClass("modal-footer");
  footer->setLayout(lh1);
  // tMain->elementAt(r, 0)->setLayout(lh1);

  lv->addWidget(footer);
}
//--------------------------------------------------------------------------
void TrequsitionUpdDefault::refresh()
{
 Twindow::refresh();
}
//--------------------------------------------------------------------------
void TrequsitionUpdDefault::onCalendarActivated(Wt::WDate p)
{
  std::set<Wt::WDate> sel = cal->selection();
  if (sel.size() > 0)
  {
    Wt::WDate d(*(sel.begin()));
    refreshCarLifterPlan(city, d);
  }
}
//--------------------------------------------------------------------------
class TrequisitionButton : public Wt::WPushButton
{
 public:
     TrequisitionButton(const Wt::WString &text, Wt::WContainerWidget *parent=0) : Wt::WPushButton(text, parent), carlifter_plan_id(0){};
     int carlifter_plan_id;
};
void TrequsitionUpdDefault::refreshCarLifterPlan(const int city, const Wt::WDate d)
{
  Wt::WStandardItemModel mt;
  Wt::WDateTime start(d), finish;

  start.setTime(Wt::WTime(0, 0));
  finish = start.addDays(1);

  tCarLifter->clear();

  if (dl->CarlifterPlanS(&mt, city, start, finish, 0) > 0)
  {
   int row(0), column(0);
   int number(0);
   for(int i = 0; i < mt.rowCount(); i++)
   {
    Wt::WTime t1, t2;
    std::stringstream stm;

    if (typeid(Wt::WDateTime) == mt.data(i, fieldByName(&mt, "dt_start")).type()) t1 = boost::any_cast<Wt::WDateTime>(mt.data(i, fieldByName(&mt, "dt_start"))).time();
    if (typeid(Wt::WDateTime) == mt.data(i, fieldByName(&mt, "dt_finish")).type()) t2 = boost::any_cast<Wt::WDateTime>(mt.data(i, fieldByName(&mt, "dt_finish"))).time();


    stm << t1.toString(Wt::WString::fromUTF8("hh:mm")).toUTF8()
        << " - "
        << t2.toString(Wt::WString::fromUTF8("hh:mm")).toUTF8();
    int carlifter_plan_id = to_int(mt.data(i, fieldByName(&mt, "carlifter_plan_id")));

    if (number != to_int(mt.data(i, fieldByName(&mt, "number"))))
    {
     number = to_int(mt.data(i, fieldByName(&mt, "number")));
     row = 0;
     column += i > 0 ? 1 : 0;
     Wt::WString label(L"��������� �{1}");

     tCarLifter->elementAt(row, column)->addWidget(new Wt::WLabel(label.arg(number)));     
    }
    row++;
    TrequisitionButton * button = new TrequisitionButton(Wt::WString::fromUTF8(stm.str()));
    button->setMargin(2);
    button->carlifter_plan_id = to_int( mt.data(i, fieldByName(&mt, "carlifter_plan_id")));
    if (to_int(mt.data(i, fieldByName(&mt, "requisition_id"))) > 0)
    {
     button->setEnabled(false);
    }
    else
    {
     button->clicked().connect(this, &TrequsitionUpdDefault::onbTime);
    }
    tCarLifter->elementAt(row, column)->addWidget(button);     

    /*  requisition_id = to_int(mt.data(0, fieldByName(&mt, "requisition_id")));
      date_ta = to_wdate(mt.data(0, fieldByName(&mt, "date_ta")));
      phone_number = to_string(mt.data(0, fieldByName(&mt, "phone")));
      customer = to_string(mt.data(0, fieldByName(&mt, "customer")));*/
   
   }
  }
}
//--------------------------------------------------------------------------
void TrequsitionUpdDefault::onbTime()
{
  TrequisitionButton * b = dynamic_cast<TrequisitionButton *>(sender());
  if (b == NULL) return;

  requisition.customer = eCustomer->text().trim().toUTF8();
  requisition.date_ta = deTa->date();
  requisition.phone_number = ePhone->text().trim().toUTF8();
  requisition.carlifter_plan_id = b->carlifter_plan_id;
  if (requisition.save() > 0)
  { Twindow::onbOk(); }
  else
  {
   ShowMessage(0, Wt::WString::fromUTF8(str2html(dl->errorstr)), "DB Error");
  }
}
//--------------------------------------------------------------------------
void TrequsitionUpdDefault::onbOk()
{
 if (tabMain->currentIndex() == 0)
 {
  if (!deTa->date().isValid())
  {
   ShowMessage(0, Wt::WString(L"������� ����"), L"������");
   return;
  }
  if (eCustomer->text().trim().empty())
  {
   ShowMessage(0, Wt::WString(L"������� �.�.�."), L"������");
   return;
  }

  tabMain->setCurrentIndex(tabMain->currentIndex() + 1);
  tabMain->setTabHidden(1, false);
  tabMain->setTabHidden(0, true);
  return;
 }
 if (tabMain->currentIndex() == 1)
 {
  ShowMessage(0, Wt::WString(L"�������� ���� � ����� ����������"), L"������");
  return;
 }

}
//--------------------------------------------------------------------------
//--------------------------------------------------------------------------
//--------------------------------------------------------------------------
TrequsitionUpd::TrequsitionUpd(Wt::WContainerWidget *parent, TDataLayer * p_dl, const int requsition_id, const int p_city) : Twindow(p_dl, "TrequsitionUpdDefault", parent), 
  requisition(p_dl), city(p_city)
{
 if (requsition_id > 0)
 {
  requisition.requisition_id = requsition_id;
  requisition.read();
 }
 refresh();
}
//--------------------------------------------------------------------------
//TrequsitionUpd::~TrequsitionUpd()
//{
//}
//--------------------------------------------------------------------------
void TrequsitionUpd::setupUi()
{
 Wt::WTable * tMain = new Wt::WTable(this); tMain->setObjectName("tMain");

 int r(0);

 deTa = new TDateEdit();
 deTa->setObjectName("deTa");
 deTa->setEnabled(true);
 Wt::WText * l = new Wt::WText(L"���� ���:");
 l->setWordWrap(false);
 tMain->elementAt(r, 0)->addWidget(l);
 tMain->elementAt(r, 1)->addWidget(deTa);
 r++;

 ePhone = new Wt::WLineEdit(); //
 ePhone->setObjectName("ePhone");
 ePhone->setEnabled(true);
 l = new Wt::WText(L"�������:");
 tMain->elementAt(r, 0)->addWidget(l);
 tMain->elementAt(r, 1)->addWidget(ePhone);
 r++;

 eCustomer = new Wt::WLineEdit(); //
 eCustomer->setObjectName("eCustomer");
 eCustomer->setEnabled(true);
 l = new Wt::WText(L"�.�.�.:");
 tMain->elementAt(r, 0)->addWidget(l);
 tMain->elementAt(r, 1)->addWidget(eCustomer);
 r++;

 cbCarLifter = new TComboBox();
 cbCarLifter->setObjectName("cbCarLifter");
 cbCarLifter->setNoSelectionEnabled(true);
 dl->CarlifterS(cbCarLifter->mt, city);
 cbCarLifter->setLabelKeyColumn(""/*"�����:"*/, "carlifter_id", "number");
 cbCarLifter->valueChanged.connect(this, &TrequsitionUpd::refreshCarlifterPlan);
 l = new Wt::WText(L"���������:");
 tMain->elementAt(r, 0)->addWidget(l);
 tMain->elementAt(r, 1)->addWidget(cbCarLifter);
 r++;

 deCl = new TDateEdit(); //
 deCl->setObjectName("deCl");
 deCl->setEnabled(true);
 deCl->textInput().connect(this, &TrequsitionUpd::refreshCarlifterPlan);
 l = new Wt::WText(L"����:");
 tMain->elementAt(r, 0)->addWidget(l);
 tMain->elementAt(r, 1)->addWidget(deCl);
 r++;

 cbCarLifterPlan = new TComboBox();
 cbCarLifterPlan->setObjectName("cbCarLifterPlan");
 cbCarLifterPlan->setNoSelectionEnabled(true);
 l = new Wt::WText(L"�����:");
 tMain->elementAt(r, 0)->addWidget(l);
 tMain->elementAt(r, 1)->addWidget(cbCarLifterPlan);
 r++;

 Wt::WHBoxLayout * lh1 = new Wt::WHBoxLayout();
 bOk = new Wt::WPushButton(Wt::WString(L"��")); bOk->setObjectName("bOk");
 bOk->setEnabled(false);
 bOk->clicked().connect(this, &TrequsitionUpd::onbOk);
 bOk->setIcon(Wt::WLink("image/Apply.png"));

 Wt::WPushButton * bCancel = new Wt::WPushButton(Wt::WString(L"������"));
 bCancel->setObjectName("bCancel");
 bCancel->clicked().connect(this, &TrequsitionUpd::onbCancel);
 bCancel->setIcon(Wt::WLink("image/Cancel.png"));

 lh1->addStretch(1);
 lh1->addWidget(bOk);
 lh1->addWidget(bCancel);

 Wt::WContainerWidget * footer = new Wt::WContainerWidget();
 footer->addStyleClass("modal-footer");
 footer->setLayout(lh1);
 // tMain->elementAt(r, 0)->setLayout(lh1);
 tMain->elementAt(r, 0)->addWidget(footer);
 tMain->elementAt(r, 0)->setColumnSpan(tMain->columnCount());

 for(int column = 0; column <= tMain->columnCount() - 1; column++)
     for(int row = 0; row <= tMain->rowCount() - 1; row++)
         tMain->elementAt(row, column)->setPadding(2);
}
//--------------------------------------------------------------------------
void TrequsitionUpd::refresh()
{
 Twindow::refresh();

// if (requisition.requisition_id > 0)
 {
     eCustomer->setText(Wt::WString::fromUTF8(requisition.customer));
     deTa->setDate(requisition.date_ta);
     ePhone->setText(Wt::WString::fromUTF8(requisition.phone_number));
     deCl->setDate(requisition.date_plan);
     cbCarLifter->setValue(requisition.carlifter_id);
     refreshCarlifterPlan();
     cbCarLifterPlan->setValue(requisition.carlifter_plan_id);
 }
}
//--------------------------------------------------------------------------
void TrequsitionUpd::refreshCarlifterPlan()
{
  cbCarLifterPlan->mt->clear();
  bOk->setEnabled(false);

  if(!cbCarLifter->valid() || deCl->date().isNull() || !deCl->date().isValid()) return;

  bOk->setEnabled(true);

  Wt::WDateTime start(deCl->date()), finish;

  start.setTime(Wt::WTime(0, 0));
  finish = start.addDays(1);

  if (dl->CarlifterPlanSFree(cbCarLifterPlan->mt, city, start, finish, cbCarLifter->value(), requisition.carlifter_plan_id) < 0)
  {
   ShowMessage(0, Wt::WString::fromUTF8(str2html(dl->errorstr)), "DB Error");
   return;
  }
  cbCarLifterPlan->setLabelKeyColumn(""/*"�����:"*/, "CARLIFTER_PLAN_ID", "label");
  //  cbCarLifter->valueChanged.connect(this, &TrequsitionUpd::refreshCarlifterPlan);
}
//--------------------------------------------------------------------------
void TrequsitionUpd::onbOk()
{
    if (!deTa->date().isValid())
    {
        ShowMessage(0, Wt::WString(L"������� ����"), L"������");
        return;
    }
    if (eCustomer->text().trim().empty())
    {
        ShowMessage(0, Wt::WString(L"������� �.�.�."), L"������");
        return;
    }
    requisition.customer = eCustomer->text().trim().toUTF8();
    requisition.date_ta = deTa->date();
    requisition.phone_number = ePhone->text().trim().toUTF8();
    requisition.carlifter_plan_id = cbCarLifterPlan->value();

    if (requisition.save() > 0)
    { onCloseInt(requisition.requisition_id); }
    else
    {
     ShowMessage(0, Wt::WString::fromUTF8(str2html(dl->errorstr)), "DB Error");
    }
}
//--------------------------------------------------------------------------
void TrequsitionUpd::onbCancel()
{
 onCloseInt(0);
}
//--------------------------------------------------------------------------
void TrequsitionUpd::fixPlan()
{
    deCl->setEnabled(false);
    cbCarLifter->setEnabled(false);
    cbCarLifterPlan->setEnabled(false);
}

void TrequsitionUpd::setRequisition(const TRequisition& p)
{
    requisition = p;
    refresh();
}