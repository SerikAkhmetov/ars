#include <Wt/WMessageBox>

#include "requisitionBrwWindow.h"
#include "requsitionUpdWindow.h"

TrequisitionBrw::TrequisitionBrw(const int p_city, Wt::WContainerWidget *parent, TDataLayer * dl): Twindow(dl, "TrequisitionBrw", parent), city(p_city)
{
 setupUi();
 refresh();
}

//TrequisitionBrw::~TrequisitionBrw()
//{
//}

void TrequisitionBrw::setupUi()
{
    clear();

    Wt::WVBoxLayout * lmain;
    lmain = new Wt::WVBoxLayout();

    setLayout(lmain);
    Wt::WHBoxLayout * lh1 = new Wt::WHBoxLayout();
    lmain->addItem(lh1);

    bNew = new Wt::WPushButton(Wt::WString(L"�����")); bNew->setObjectName("bNew");
    bNew->clicked().connect(this, &TrequisitionBrw::onNew);
    bNew->setIcon(Wt::WLink("image/New.png"));
    lh1->addWidget(bNew, 0, Wt::AlignmentFlag::AlignTop);

    bEdit = new Wt::WPushButton(Wt::WString(L"��������")); bEdit->setObjectName("bEdit");
    bEdit->clicked().connect(this, &TrequisitionBrw::onEdit);
    bEdit->setEnabled(false);
    bEdit->setIcon(Wt::WLink("image/Edit.png"));
    lh1->addWidget(bEdit, 0, Wt::AlignmentFlag::AlignTop);

    bDelete = new Wt::WPushButton(Wt::WString(L"�������")); bDelete->setObjectName("bDelete");
    bDelete->clicked().connect(this, &TrequisitionBrw::onDelete);
    bDelete->setEnabled(false);
    bDelete->setIcon(Wt::WLink("image/Delete.png"));
    lh1->addWidget(bDelete, 0, Wt::AlignmentFlag::AlignTop);

    lh1->addStretch(1);

    tvPage = new WTableViewPage(dl, this); tvPage->setObjectName("tvPage");
    tvPage->layout->addStretch(1);
    tvPage->xField.clear();
    tvPage->xField.push_back("customer");
    tvPage->xField.push_back("phone");
    tvPage->filterChanged.connect(this, &Twindow::delayedRefresh);

    Wt::WHBoxLayout * lh2 = new Wt::WHBoxLayout();
    //lh2->addWidget(tvPage->lFilter, 0, Wt::AlignmentFlag::AlignTop);
    lh2->addWidget(tvPage->eFilter, 0, Wt::AlignmentFlag::AlignTop);
    tvPage->filterChanged.connect(this, &Twindow::delayedRefresh);
    lh2->addLayout(tvPage->layout, 0, Wt::AlignmentFlag::AlignTop);
    lh2->addStretch(1);
    lmain->addLayout(lh2);

    tableView = new Wt::WTableView();// tableView->setObjectName("tableView");
    tableView->setAlternatingRowColors(true);
    tableView->setSortingEnabled(false);
    tableView->setColumnResizeEnabled(true);
    //tableView->setSelectionMode(Wt::SelectionMode::SingleSelection);
    tableView->setSelectionMode(Wt::ExtendedSelection);
    tableView->setSelectable(true);
    tableView->setEditTriggers(Wt::WAbstractItemView::NoEditTrigger);
  //  tableView->doubleClicked().connect(this, &TfileBrw::onDoubleClicked);
  //  tableView->keyPressed().connect(this, &TfileBrw::onKeyPressed);
    tableView->selectionChanged().connect(this, &TrequisitionBrw::onSelectionChanged);
    tableView->columnResized().connect(this, &Twindow::onColumnResized);
  //  tableView->setItemDelegate(item);
  //  tableView->setHeight(600);

    tvPage->tableView = tableView;

    lmain->addWidget(tableView);
}

void TrequisitionBrw::refresh()
{
 refresh(0);
}

void TrequisitionBrw::refresh(const int id)
{
    mt = new Wt::WStandardItemModel(this);
    if (dl->RequisitionS(mt, 0, city) < 0)
    {
        ShowMessage(0, Wt::WString::fromUTF8(str2html(dl->errorstr)), "DB Error");
        return;
    }

    tvPage->field.clear();
    tvPage->field.push_back(Wt::WString("CUSTOMER")); tvPage->field.push_back(Wt::WString(L"�.�.�."));
    tvPage->field.push_back(Wt::WString("PHONE")); tvPage->field.push_back(Wt::WString(L"�������"));
    tvPage->field.push_back(Wt::WString("number")); tvPage->field.push_back(Wt::WString(L"���������"));
    tvPage->field.push_back(Wt::WString("DT_START")); tvPage->field.push_back(Wt::WString(L"������"));
    tvPage->field.push_back(Wt::WString("DT_FINISH")); tvPage->field.push_back(Wt::WString(L"�����"));

   // tvPage->emptyTitleColumn.clear();
   // tvPage->emptyTitleColumn.push_back("ico_RES_REGISTED");
   // tvPage->emptyTitleColumn.push_back("ico_ITEMS_STATUS");

    tvPage->mt = mt;
    tvPage->refresh(0);

    for(int i = 0; i < tableView->model()->columnCount(); i++)
    {tableView->setHeaderAlignment(i, Wt::AlignCenter);}
}

void TrequisitionBrw::onEdit()
{
    if (tableView->model() == NULL || tableView->model()->rowCount() == 0) return;
    if (tableView->selectedIndexes().empty()) return;
    int row = tvPage->rowIndex(tableView->selectedIndexes().begin()->row());
    int reqisition_id = to_int(mt->data(row, fieldByName(mt, "REQUISITION_ID")));

    clear();
    TrequsitionUpd * frm = new TrequsitionUpd(this, dl, reqisition_id, city);
    frm->closeInt.connect(this, &TrequisitionBrw::redraw);
}

void TrequisitionBrw::onNew()
{
 clear();
 TrequsitionUpd * frm = new TrequsitionUpd(this, dl, 0, city);
 frm->closeInt.connect(this, &TrequisitionBrw::redraw);
}

void TrequisitionBrw::redraw(const int v)
{
  Twindow::refresh();
  refresh(0);
}

void TrequisitionBrw::onDelete()
{
    if (tableView->model() == NULL || tableView->model()->rowCount() == 0) return;
    if (tableView->selectedIndexes().empty()) return;
    int row = tvPage->rowIndex(tableView->selectedIndexes().begin()->row());

    if (Wt::StandardButton::Yes != Wt::WMessageBox::show(Wt::WString(L"������"), Wt::WString(L"������� ������ ?"),
        Wt::StandardButton::Yes | Wt::StandardButton::No))
    {
     return;
    }

    int reqisition_id = to_int(mt->data(row, fieldByName(mt, "REQUISITION_ID")));

    if (dl->RequisitionD(reqisition_id))
    {
     row = tvPage->rowIndex(tableView->selectedIndexes().begin()->row()) - 1;
     if (row >= 0)
     {
      reqisition_id = to_int(mt->data(row, fieldByName(mt, "REQISITION_ID")));
      refresh(reqisition_id);
     }
    }
    else
    {
     ShowMessage(0, Wt::WString::fromUTF8(str2html(dl->errorstr)), "DB Error");
    }
}

void TrequisitionBrw::onSelectionChanged()
{
    bEdit->setEnabled(tableView->model() != NULL && tableView->model()->rowCount() > 0 && !tableView->selectedIndexes().empty());
    bDelete->setEnabled(tableView->model() != NULL && tableView->model()->rowCount() > 0 && !tableView->selectedIndexes().empty());
}

void TrequisitionBrw::onCityChanged(const int n)
{
 if (city != n)
 {
  city = n;
  refresh(0);
 }
}