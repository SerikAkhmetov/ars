#ifndef WArs_H
#define WArs_H

#include <Wt/WApplication>
#include <Wt/WEnvironment>

//--------------------------------------------------------------------------
class WArsApplication : public Wt::WApplication
{
public:
  WArsApplication(const Wt::WEnvironment& env);
  virtual ~WArsApplication();

  virtual void initialize();  //Initializes the application, post-construction. More...
  virtual void finalize();  //

private:
  Wt::WLocale locale;

  TloginWindow * wLogin;
  void startMain();
  void startLogin();
};
//--------------------------------------------------------------------------
Wt::WApplication *createQueryResource(const Wt::WEnvironment& env);
void init();
int main(int argc, char **argv);
int start_wt(int argc, char **argv);
//--------------------------------------------------------------------------

#endif // WArs_H
