#ifndef USERBRWWINDOW_H
#define USERBRWWINDOW_H

#include <Wt/WWidget>
#include <Wt/WContainerWidget>
#include <Wt/WVBoxLayout>
#include <Wt/WHBoxLayout>
#include <Wt/WDialog>
#include <Wt/WLineEdit>
#include <Wt/WCheckBox>
#include <Wt/WTableView>
#include <Wt/WItemDelegate>
#include <Wt/WComboBox>

#include "uDataLayer.h"
#include "widget.h"

//--------------------------------------------------------------------------
class TuserBrw : public Twindow
{
 public:
    TuserBrw(Wt::WContainerWidget *parent, TDataLayer * dl);
    //virtual ~TuserBrw();

    void setupUi();
    void refresh();

    //void onCityChanged(const int);

    Wt::WPushButton *bNew, *bEdit, *bDelete;
    int user_id;
private:
    Wt::WTableView * tableView; WTableViewPage * tvPage; Wt::WStandardItemModel * mt;

    TComboBox * cbPartner;

    int partner_id;

    void refresh(const int);
    void redraw(const int);
    void onEdit();
    void onNew();
    void onDelete();
    void onSelectionChanged();
    void onPartnerChanged();
};
//--------------------------------------------------------------------------
bool selectUser(TDataLayer * dl, Wt::WContainerWidget * parent, int &user_id);

#endif