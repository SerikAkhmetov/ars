#ifndef WIDGET
#define WIDGET

#include <boost/thread/mutex.hpp>
#include <boost/regex.hpp>
#include <boost/asio.hpp>
#include <boost/date_time/posix_time/posix_time.hpp>
#include <boost/thread.hpp>
#if defined(WIN32)
#include <boost/uuid/detail/sha1.hpp>
#else
#include <boost/uuid/sha1.hpp>
#endif
#include <Wt/WApplication>
#include <Wt/WContainerWidget>
#include <Wt/WHBoxLayout>
#include <Wt/WComboBox>
#include <Wt/WStandardItemModel>
#include <Wt/WTableView>
#include <Wt/WText>
#include <Wt/WPanel>
#include <Wt/WItemDelegate>
#include <Wt/WAbstractTableModel>
#include <Wt/Http/Request>
#include <Wt/Http/Response>
#include <Wt/WObject>
#include <Wt/WResource>
#include <Wt/WFileResource>
#include <Wt/WDialog>
#include <Wt/WColor>
#include <Wt/WWidget>
#include <Wt/WDateEdit>
#include <Wt/WTimeEdit>
#include <Wt/WCalendar>
#include <Wt/WTable>
#include <Wt/WSpinBox>
#include <Wt/WLabel>
#include <Wt/WTimer>
#include <Wt/WDatePicker>
#include <Wt/WPushButton>
#include <Wt/WPopupMenu>
#include <Wt/WMenu>
#include <Wt/WFileUpload>
#include <Wt/Http/Request>
#include <Wt/WProgressBar>

#include "uDataLayer.h"

class TPFSFile;
class TPFSResource;
//--------------------------------------------------------------------------
/*class TMemoryTable : public Wt::WAbstractTableModel
{
 public:
  TMemoryTable(Wt::WObject * parent = 0);
  virtual ~TMemoryTable();

  virtual int rowCount (const Wt::WModelIndex &parent=Wt::WModelIndex()) const; // Returns the table row count (only for the invalid parent!)
  virtual int columnCount (const Wt::WModelIndex &parent=Wt::WModelIndex()) const; // Returns the table column count (only for the invalid parent!)
  virtual boost::any data (const Wt::WModelIndex &index, int role=Wt::DisplayRole) const; // Returns the data for an item, and a particular role
  virtual boost::any data (const int column, const int row, int role=Wt::DisplayRole) const; // Returns the data for an item, and a particular role
  virtual boost::any headerData (int section, Wt::Orientation orientation=Wt::Horizontal, int role=Wt::DisplayRole) const; // Returns the header data for a column

  virtual bool hasChildren (const Wt::WModelIndex &	index) const {return(false);};
//  virtual Wt::WModelIndex index (int row, int column, const Wt::WModelIndex &parent = Wt::WModelIndex()) const
//  {return(Wt::WModelIndex());};

  //TDynamicList * mt;

};*/
//--------------------------------------------------------------------------
class Twindow : public Wt::WContainerWidget
{
 public:
  Twindow(Wt::WContainerWidget *parent = 0);
  Twindow(TDataLayer * dl, const std::string& name = std::string(), Wt::WContainerWidget * parent = 0);

  virtual ~Twindow();

  virtual void setupUi() = 0;
  virtual void refresh();

  TDataLayer * dl;
  Wt::WContainerWidget * father;

  Wt::Signal<> close;
  Wt::Signal<int> closeInt;

  static std::string name(const Wt::WObject * o);
  std::string name() const {return (name(this));};

  virtual void setChild(Twindow *);
  virtual void onChildClose();
  virtual void onClose();
  virtual void onCloseInt(const int);
  virtual void onColumnResized(int column, Wt::WLength width);

  virtual void delayedRefresh();
  Wt::WTimer * timerRefresh;

  bool ui;
  virtual void stateSave();
          void stateSave(Wt::WObject *);
  virtual void stateRestore();
  static Twindow * createWindow(const std::string&, Wt::WContainerWidget *parent, TDataLayer * dl);

  virtual void clear ();

  virtual void onbOk();
  virtual void onbCancel();

 protected:
   std::map<std::string, std::string> state; // key->value

//  std::string xStr;
//  boost::regex xRegEx;
//  boost::smatch xResults;
public:
 static void setTablePadding(Wt::WTable * tMain, const Wt::WLength& padding = 2, Wt::WFlags<Wt::Side> sides = Wt::All);
};
//--------------------------------------------------------------------------
//--------------------------------------------------------------------------
class TProgressWindow : public /*Wt::WMessageBox */ Wt::WDialog
{
 public:
  TProgressWindow(Wt::WObject *parent = 0);
  TProgressWindow(const Wt::WString &windowTitle, Wt::WObject *parent = 0);

//  virtual ~TEditWindow();
  void setupUi();
  void setValue(const int value);
  void appendValue(const int value);
  void appendValue(const std::string& label, const int value);
  void setLabel(const std::string& value);
  void show();

 protected:
  Wt::WProgressBar * progress;
  Wt::WText * label;
};
//--------------------------------------------------------------------------
class WTableViewPageControl : public Wt::WContainerWidget
{
 public:
  WTableViewPageControl(TDataLayer * dl, Wt::WContainerWidget * parent = 0);

  virtual void setupUi();

  Wt::WPushButton * bFirst, * bLast, * bPrev, * bNext;
  Wt::WHBoxLayout * layout;
  Wt::WSpinBox * sbPage;
  Wt::WLabel * lCount;
  Wt::WLabel * lFilter;
  Wt::WLineEdit * eFilter;

  Wt::WStandardItemModel * mt;

  int /*currentPage,*/ line, lastPage;

  Wt::WTableView * tableView;

  std::vector<Wt::WString> field;
  std::list<std::string> emptyTitleColumn;
  TDataLayer * dl;

  std::list<std::string> xField; // field name for check regex
  boost::regex xRegEx;

  void selectRow(const int row);
  bool findRow(const std::string &key, const int value);
  int offset() const;

  void refresh(int page = 1);

  int rowIndex(const int tableViewIndex) const;
  int fieldColumn(const std::string& fieldName) const;

  int currentPage() const {return (sbPage->value());};

  Wt::Signal<int> curentPageChanged;
  Wt::Signal<> filterChanged;

// protected:
  bool modelCopy(Wt::WAbstractItemModel * dst,
                 const std::vector<Wt::WString> * field, const int offset, const int count_line);
  void updateXNField();
  bool xCheckLine(const int i) const;
  void updateFilterIndex();
  void onDataChanged(Wt::WModelIndex, Wt::WModelIndex);

  std::vector<int> r; // row string index
  std::vector<int> f; // filtered string index
  std::list<int> xNField; // field number for check regex

  std::map<int, int> src2dst; // column src, column dst
  std::map<int, int> dst2src; // column dst, column src
};
//--------------------------------------------------------------------------
class WTableViewPage : public WTableViewPageControl
{
 public:
  WTableViewPage(TDataLayer * dl, Wt::WContainerWidget * parent = 0);

 void onbClicked();
 void onTextInputFilter();

};
//--------------------------------------------------------------------------
class TComboBox : public Wt::WContainerWidget
{
 public:
  TComboBox(Wt::WContainerWidget *parent = 0);

  virtual void setupUi();

  bool setLabelKeyColumn(const std::string& l, const std::string& k, const std::string& c);
  void setLabel(const Wt::WString& l);
  bool setValue(const int v);
  void setModel(Wt::WAbstractItemModel * mt);
  int value() const;
  virtual void setNoSelectionEnabled(const bool enabled);
  void setEnabled(const bool enabled);

  bool valid() const;

  int key, column;
  Wt::WText * label;
  Wt::WComboBox * comboBox;
  Wt::WStandardItemModel * mt;

  Wt::Signal<int> activated;
  Wt::Signal<int> valueChanged;
 protected:
  void onActivated(int);
};

class TComboBoxDirLine : public TComboBox
{
 public:
  TComboBoxDirLine(TDataLayer * dl, const int directory_id, Wt::WContainerWidget *parent = 0);
//  void setNoSelectionEnabled(const bool enabled);
};
//--------------------------------------------------------------------------
class TPanel : public Wt::WPanel
{
 public:
  TPanel(Wt::WContainerWidget *parent = 0);
 virtual void setupUi();
 virtual void setLayout(Wt::WLayout * layout);


 Wt::WContainerWidget * container;
};
//--------------------------------------------------------------------------
class TOkFooter : public Wt::WContainerWidget
{
public:
    TOkFooter(Wt::WContainerWidget *parent = 0);
    virtual void setupUi();

    Wt::WPushButton * bOk, * bCancel;
};
//--------------------------------------------------------------------------
class TTagLabel : public Wt::WContainerWidget
{
 public:
  TTagLabel(Wt::WString label, int line_id = 0, WContainerWidget * parent = 0,  bool readOnly = false);

  void onbClose();

  Wt::WText * tLabel;
  Wt::WPushButton * bClose;
  int line_id;
  bool readOnly;

  Wt::Signal<int> remove;
};
//--------------------------------------------------------------------------
//--------------------------------------------------------------------------
//--------------------------------------------------------------------------
struct TuploadedFile
{
 std::string spoolFileName, clientFileName;
 TuploadedFile(const std::string& pspoolFileName, const std::string& pclientFileName):
  spoolFileName(pspoolFileName), clientFileName(pclientFileName) {};
};
class TFileUploadWidget: public Wt::WContainerWidget
{
public:
 TFileUploadWidget(TPFSFile * pfs, Wt::WContainerWidget * parent = 0);
 TFileUploadWidget(int parent_id, TDataLayer * dl, Wt::WContainerWidget * parent = 0);
virtual ~TFileUploadWidget();

 virtual void setupUi();

 bool canUpload() const;
 void upload();

 std::list<TuploadedFile> uploadedFile;
 //TPFSFile * pfs;
 TDataLayer * dl;
 Wt::WFileUpload * fu;
 Wt::WProgressBar * progressBar;
// Wt::WText * lTag;
// TTagShowWidget * tagShow;
 Wt::WVBoxLayout * lv;

 Wt::Signal<> fileChanged;
 Wt::Signal<int> fileUploaded;
 Wt::Signal<int64_t> fileTooLarge;

 void refreshLV();
 void remove(const std::string);

// static void copy_file(const std::string src, const std::string dst);
protected:
 void onChanged();
 void onUploaded();
 void onFileTooLarge(int64_t);
 void copy2DB();
private:
 bool remove_pfs;
};
//--------------------------------------------------------------------------
class TFileGetWidget : public Wt::WContainerWidget
{
public:
 TFileGetWidget(int pfs_file_id, TDataLayer * p_dl, Wt::WContainerWidget * parent = 0);
// virtual ~TFileGetWidget();

 Wt::WText * tLabel;
 Wt::WPushButton * bGet;

protected:
 TPFSResource * res;
};
//--------------------------------------------------------------------------
class TFileGetMenuItem: public Wt::WMenuItem
{
public:
 TFileGetMenuItem(int pfs_file_id, TDataLayer * p_dl);

protected:
 TPFSResource * res;
};
//--------------------------------------------------------------------------
//--------------------------------------------------------------------------
//--------------------------------------------------------------------------
class TThreadCommand : public Wt::WObject
{
public:
    TThreadCommand(Wt::WObject * parent = 0) : Wt::WObject(parent)
    {
        m_thread = nullptr;
    };
   virtual ~TThreadCommand()
    {
        if (m_thread)
        {
        m_thread->interrupt();
        m_thread->join();
        delete(m_thread);
        }
    };

    virtual void start()
    {
        m_thread = new boost::thread(&TThreadCommand::exec, this);
    };

    virtual void exec() = 0;

    boost::thread * m_thread;
};
//--------------------------------------------------------------------------
class TThreadCommandPreview : public TThreadCommand
{
public:
    TThreadCommandPreview(Wt::WObject * parent = 0) : TThreadCommand(parent), file_id(0)//, finished(this)
    {
      dl = DataLayerFactory();
      setObjectName("TThreadCommandPreview");
    };

virtual ~TThreadCommandPreview()
    {
        dl->close();
        delete(dl);
    }

    void exec();

    int file_id;
    TDataLayer * dl;
   // Wt::Signal<> finished;
};
//--------------------------------------------------------------------------
//--------------------------------------------------------------------------
class TCommand : public Wt::WObject
{
 public:
  TCommand(Wt::WObject * parent = 0) : Wt::WObject(parent), changed(this)
  {dl = NULL;};
  TCommand(TDataLayer * p_dl, Wt::WObject *	parent = 0) : Wt::WObject(parent), changed(this)
  { dl = p_dl; };

 virtual void exec() = 0;

 TDataLayer * dl;
 Wt::Signal<> changed;
};
class TCommandFileDocD : public TCommand
{
 public:
  TCommandFileDocD(TDataLayer * dl, const int p_file_doc_id, Wt::WObject * parent = 0) :
   TCommand(dl, parent), file_doc_id(p_file_doc_id) { };

 virtual void exec();
 int file_doc_id;
};

class TCommandParam : public TCommand
{
public:
    TCommandParam(const int p, Wt::WObject * parent = 0) : TCommand(parent), parameter(p), fire(this)
    {};

    virtual void exec();
    int parameter;

    Wt::Signal<int> fire;
};

class TCommandParamParam : public TCommand
{
public:
    TCommandParamParam(const int p, const int p2, Wt::WObject * parent = 0) : TCommand(parent), parameter(p), parameter2(p2), fire(this)
    {};

    virtual void exec();
    int parameter, parameter2;

    Wt::Signal<int, int> fire;
};

class TFileWidget : public Wt::WContainerWidget
{
public:
    TFileWidget(TDataLayer * p_dl, Wt::WContainerWidget *parent=0);
    virtual void setupUi();
    virtual void refresh();

    TFile file; 
    bool selected;

    Wt::Signal<> changed;

    void setSelected(const bool v);

protected:
    TDataLayer * dl;
    Wt::WImage * img;  Wt::WLink link;
    Wt::WFileResource * res, * preview;
    Wt::WAnchor * anchor;
    Wt::WTimer * timer;

    void onClicked(Wt::WMouseEvent event);    
};
//--------------------------------------------------------------------------
class TNameFullName
{
public:
    TNameFullName(const std::string & p1, const std::string & p2) : name(p1), fullname(p2)
    {};
    std::string name, fullname;
};
class TMultiFileResourse : public Wt::WFileResource
{
public:
    TMultiFileResourse(Wt::WObject *parent);
    virtual ~TMultiFileResourse();
    virtual void handleRequest(const Wt::Http::Request &request, Wt::Http::Response &response);

    void clear();
    void append(const std::string &name, const std::string &fullname);
    void remove(const std::string &name, const std::string &fullname);

private:
    std::list<TNameFullName> lst;
};
//--------------------------------------------------------------------------
class TFileDoc : public Wt::WContainerWidget
{
public:
    TFileDoc(TDataLayer * p_dl, Wt::WContainerWidget *parent=0);
    virtual void setupUi();
    virtual void refresh();

    void read(const int type, const int id);
    void save(const int type, const int id);

    void setFilters(const std::string & acceptAttributes){fileUpload->setFilters(acceptAttributes);};

    Wt::Signal<> changed;

protected:
    int type, id;

    Wt::WHBoxLayout * fileLayout;
    Wt::WFileUpload * fileUpload; Wt::WProgressBar * fileProgress;
    Wt::WPushButton * bDelete, * bPreview, * bDownload, * bSelectAll, * bDeselectAll;
    Wt::WTable * fView;
    TMultiFileResourse * fileRes;

    Wt::WStandardItemModel mtFile;
    std::list<TFileWidget *> fwlst;

    TDataLayer * dl;

    void createFileUpload();
    void startUpload();
    void onUploaded();
    void fileTooLarge(int64_t filesize);
    void onDelete();
    void onPreview();
    void onFileChanged();
    void onPreviewDone(Wt::WDialog::DialogCode code);
    void onSelectAll();
    void onDeselectAll();

    double current;
    int64_t total;
};
//--------------------------------------------------------------------------
class TItemDelegate : public Wt::WItemDelegate
{
 public:
  TItemDelegate(Wt::WObject * parent = 0) : Wt::WItemDelegate(parent) {};

  virtual Wt::WWidget * update (Wt::WWidget *widget, const Wt::WModelIndex &index,
                                Wt::WFlags< Wt::ViewItemRenderFlag > flags);
};
class TItemDelegateMultiLine : public Wt::WItemDelegate
{
public:
    TItemDelegateMultiLine(Wt::WObject * parent = 0) : Wt::WItemDelegate(parent) {};

    virtual Wt::WWidget * update (Wt::WWidget *widget, const Wt::WModelIndex &index,
        Wt::WFlags< Wt::ViewItemRenderFlag > flags);
};

/*class TTableViewColor : public Wt::WTableView
{
 public:
  TTableViewColor (Wt::WContainerWidget * parent = 0) : Wt::WTableView(parent) {};

  std::map<int, std::map<int, Wt::WColor > > color; //[row][column] color
};*/
class TItemDelegateColor : public TItemDelegate
{
 public:
  TItemDelegateColor(Wt::WObject * parent = 0) : TItemDelegate(parent) {};

  virtual Wt::WWidget * update (Wt::WWidget *widget, const Wt::WModelIndex &index,
                                Wt::WFlags< Wt::ViewItemRenderFlag > flags);

 std::map<int, std::map<int, Wt::WColor > > color; //[row][column] color
};
class TItemDelegateTag : public Wt::WItemDelegate
{
 public:
  TItemDelegateTag(TDataLayer * p_dl, Wt::WAbstractItemModel * p_mt, Wt::WObject * parent = 0) :
   Wt::WItemDelegate(parent)
   {setObjectName("TItemDelegateTag"); dl = p_dl; mt = p_mt;};

  virtual Wt::WWidget * update (Wt::WWidget *widget, const Wt::WModelIndex &index,
                                Wt::WFlags< Wt::ViewItemRenderFlag > flags);
  TDataLayer * dl;
  Wt::WAbstractItemModel * mt;
};
class TItemDelegateIco : public Wt::WItemDelegate
{
 public:
  TItemDelegateIco(Wt::WObject * parent = 0) : Wt::WItemDelegate(parent)
  {setObjectName("TItemDelegateIco");};

  virtual Wt::WWidget * update (Wt::WWidget *widget, const Wt::WModelIndex &index,
                                Wt::WFlags< Wt::ViewItemRenderFlag > flags);
};
class TItemDelegateCheckBox : public Wt::WItemDelegate
{
 public:
  TItemDelegateCheckBox(Wt::WObject * parent = 0) : Wt::WItemDelegate(parent), changed(this)
  {setObjectName("TItemDelegateCheckBox");};

  virtual Wt::WWidget * update (Wt::WWidget *widget, const Wt::WModelIndex &index,
                                Wt::WFlags< Wt::ViewItemRenderFlag > flags);
  Wt::Signal<int, int> changed;
 protected:
  void onChanged(int, int);
};
//--------------------------------------------------------------------------
/*class TWidget : public Wt::WWidget
{
 public:
  TWidget(){};
  void removeChild (Wt::WWidget *child){Wt::WWidget::removeChild(child);};
  void addChild (Wt::WWidget *child){Wt::WWidget::addChild(child);};

};*/
class TDateEdit : public Wt::WDateEdit
{
 public:
  TDateEdit(Wt::WContainerWidget *parent=0);

 private:
  void onTextInput();
};
//--------------------------------------------------------------------------
class TTimeEdit : public Wt::WTimeEdit
{
public:
    TTimeEdit(Wt::WContainerWidget *parent=0);

private:
    void onTextInput();
};
//--------------------------------------------------------------------------
//class TLineEditButton : public Wt::WLineEdit
//{
// public:
//  TLineEditButton(Wt::WContainerWidget *parent=0);
//};
//--------------------------------------------------------------------------
typedef struct
{
 char label[4];        // "pfdb"                                             // 4
 int  pversion;        // ������ ������� �����                               // 6
 char name_user[64];   // ������ 64 ������� ����������������� �������� ����� // 70
 char name_server[16]; // ���������� �������� �����                          // 86
 char create_year;     // ��� �������� + 1970                                // 87
 char create_month;    // ����� ��������                                     // 88
 char create_day;      // ���� ��������                                      // 89
 char create_hour;     // ��� ��������                                       // 90
 char create_min;      // ��� ��������                                       // 91
 char create_sec;      // ��� ��������                                       // 92
 char author[16];      // ��� ������������, ����� �����                      // 108
 long fdb_file_id;     // fdb_file.fdb_file_id                               // 112
 char reserved;        //compressed;   // ������� ���� ����                  // 113
 char feel[11];        // ���������������, ����������� �� 128 ����           // 128
} PFILEDB;
typedef struct
{
 uint8_t  command;     // ����������� ����
 uint32_t block_size;  // ������ �����
 uint32_t data_size;   // ������ ����� ������ (��������)
// uLong    crc32;       // ����������� ����� ����� ������ (��������, ��������� zlib.crc32)
} PFILEBLOCK;
// �����  PFILEBLOCK.command
static const uint8_t PFILEBLOCK_COMMAND_COMPRESS = 1; // ���� ������ ����

//--------------------------------------------------------------------------
//class TThreadedResource : public Wt::WResource
//{
// public:
//  TThreadedResource(Wt::WResource * res, Wt::WObject *parent = 0);
//  virtual ~TThreadedResource();

//  void handleRequest(const Wt::Http::Request &request, Wt::Http::Response &response);

//  void handleRequest2();
////  Wt::Http::Request request;
////  Wt::Http::Response response;

// private:
//  int session_id;
//};
//--------------------------------------------------------------------------
class TQueryResource;
class TQueryResourceList //: public Wt::WObject
{
 public:
  TQueryResourceList();
  ~TQueryResourceList();

  void append(TQueryResource *, std::string);

  Wt::WServer * server;

  std::map<std::string, TQueryResource *> lqr; // path, Resource
//  std::map<std::string, TQueryResource *> ls; // sessionId,

  void start();
  void stop();
//  void timerCleanTimeout();

  void thread();

 private:
//  boost::asio::io_service io;
//  boost::asio::deadline_timer timer;
//  boost::asio::io_service::work work;

//  Wt::WTimer * timerClean;
  bool running;
  boost::mutex mtx;
  boost::thread thr;
};
//--------------------------------------------------------------------------
struct TFileAccess
{
 int group_id;
 int user_id;
 int rights;
 TFileAccess():group_id(0), user_id(0), rights(0){};
 TFileAccess(int p1, int p2, int p3):group_id(p1), user_id(p2), rights(p3){};
};

class TQueryResource : public Wt::WResource
{
 public:
  TQueryResource(const int session_id = 0, Wt::WObject *parent = 0);
  TQueryResource(const Wt::Http::ParameterMap & param, const int session_id = 0, Wt::WObject *parent = 0);
  virtual ~TQueryResource();

  void handleRequest(const Wt::Http::Request &request, Wt::Http::Response &response);

  Wt::WDateTime started;
  std::string zest;
  int timeout;
  bool session_mode;
  TDataLayer * dl;

  const bool isFree() const;

 private:
  std::string mt2xml(Wt::WStandardItemModel* mt) { return std::string(); };
  void request2param(const Wt::Http::Request &request, Wt::Http::ParameterMap &param);
  void decodeFile(std::string &spool);
  void decompressFile(std::string &spool);
  std::string mt2xml(Wt::WAbstractItemModel * mt);
  int ExecuteProcedure(std::string & out, const Wt::Http::ParameterMap &param);
  int ExecuteSQL(std::string & out, const Wt::Http::ParameterMap &param);
  int Begin(std::string & out);
  int Commit(std::string & out);
  int TransactionStatus(std::string & out);
  int Session(std::string & out, const Wt::Http::ParameterMap &param);
  int Session_close(std::string & out, const Wt::Http::ParameterMap &param);

  unsigned long long mkResponse(const int out_type, const std::string & out, Wt::Http::Response &response);

  std::string to_xml(const std::string& txt) const;

  void init_db(const Wt::Http::ParameterMap &param);
//  void ontimerCleanTimeout();

  int session_id;
  boost::mutex mtx;
  std::list<TQueryResource *> lstQuery;

  bool compress;
//  Wt::WTimer * timerClean;
};
//--------------------------------------------------------------------------
class TEditWindow : public Wt::WDialog
{
 public:
  TEditWindow(Wt::WObject *parent = 0);
  TEditWindow(const Wt::WString &windowTitle, Wt::WObject *parent = 0);

  virtual ~TEditWindow();
  void setupUi();

  Wt::WLineEdit * eStr;
};
//--------------------------------------------------------------------------
class TLineEdit : public Wt::WContainerWidget
{
public:
    TLineEdit(Wt::WContainerWidget *parent = 0);

    virtual void setupUi();

    void setEnabled(const bool);
    void setText(const Wt::WString&);

    Wt::Signal<> clicked;

protected:
    Wt::WHBoxLayout * lh;
    Wt::WLineEdit * eStr;
    Wt::WPushButton * btn;
    void onClicked();
};
//--------------------------------------------------------------------------

//--------------------------------------------------------------------------
//--------------------------------------------------------------------------

bool jpeg_resize(const std::string& src_file, const std::string& dst_file, const int width, const int height);

const std::string sha12str(boost::uuids::detail::sha1 &sha1);

void ShowMessage(Wt::WObject * parent, Wt::WString message, Wt::WString label = Wt::WString());
//void saveValue(const std::string value, const std::string param, const std::string group = "", const std::string pref = "");
//void saveValue(const bool value, const std::string param, const std::string group = "", const std::string pref = "");
//std::string getValue(const std::string param, const std::string group = "", const std::string pref = "");
//bool getValueBool(const std::string param, const std::string group = "", const std::string pref = "");
//void saveColumnWidth(const Wt::WAbstractItemView * view, const std::string group = "", const std::string pref = "");
//void setColumnWidth(Wt::WAbstractItemView * view, const std::string group = "", const std::string pref = "");
//void setColumnWidth(Wt::WTreeView * view, const std::string group = "", const std::string pref = "");
std::string str2html(const std::string& s);

//--------------------------------------------------------------------------
Wt::WString to_wstring(boost::any const& a);
Wt::WDate to_wdate(boost::any const& a);
Wt::WDateTime to_wdatetime(boost::any const& value);
std::string to_string(boost::any const& a);
int to_int(boost::any const& a);
int to_int64(boost::any const& a);
TNumeric to_numeric(boost::any const& a);
bool to_bool(boost::any const& a);
template <typename BoostType>
const inline BoostType from_BoostAny(boost::any const& a)
{
 if (typeid(BoostType) == a.type())
 {
  return(boost::any_cast<BoostType>(a));
 }
 return BoostType();
};
//--------------------------------------------------------------------------
#endif // WIDGET

