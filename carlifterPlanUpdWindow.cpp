#include <Wt/WCalendar>

#include "carlifterPlanUpdWindow.h"


//--------------------------------------------------------------------------
//--------------------------------------------------------------------------
//--------------------------------------------------------------------------
TcarlifterPlanUpd::TcarlifterPlanUpd(Wt::WContainerWidget *parent, TDataLayer * p_dl, const int carlifter_plan_id, const int carlifter_id) : 
                   Twindow(p_dl, "TcarlifterPlanUpd", parent), carlifterPlan(p_dl)
{
 carlifterPlan.carlifter_id = carlifter_id;

 if (carlifter_plan_id > 0)
 {
  carlifterPlan.carlifter_plan_id = carlifter_plan_id;
  if (!carlifterPlan.read())
  {
   ShowMessage(0, Wt::WString::fromUTF8(str2html(dl->errorstr)), "DB Error");
  }
 }
 refresh();
}
//--------------------------------------------------------------------------
//TcarlifterPlanUpd::~TcarlifterPlanUpd()
//{
//}
//--------------------------------------------------------------------------
void TcarlifterPlanUpd::setupUi()
{
 Wt::WTable * tMain = new Wt::WTable(this); tMain->setObjectName("tMain");

 int r(0);

 dclp = new TDateEdit(); dclp->setObjectName("dclp");
 dclp->setEnabled(true);
 dclp->textInput().connect(this, &TcarlifterPlanUpd::refreshCarlifterPlan);
 Wt::WText * l = new Wt::WText(L"����:");
 l->setWordWrap(false);
 tMain->elementAt(r, 0)->addWidget(l);
 tMain->elementAt(r, 1)->addWidget(dclp);
 r++;

 eStart = new TTimeEdit(); eStart->setObjectName("eStart");
 eStart->setEnabled(true);
 eStart->textInput().connect(this, &TcarlifterPlanUpd::refreshCarlifterPlan);
 l = new Wt::WText(L"�����:");
 tMain->elementAt(r, 0)->addWidget(l);
 tMain->elementAt(r, 1)->addWidget(eStart);
 eFinish = new TTimeEdit(); eFinish->setObjectName("eFinish");
 eFinish->setEnabled(true);
 eFinish->textInput().connect(this, &TcarlifterPlanUpd::refreshCarlifterPlan);
 tMain->elementAt(r, 2)->addWidget(eFinish);

 r++;

 Wt::WHBoxLayout * lh1 = new Wt::WHBoxLayout();
 bOk = new Wt::WPushButton(Wt::WString(L"��")); bOk->setObjectName("bOk");
 bOk->setEnabled(false);
 bOk->clicked().connect(this, &TcarlifterPlanUpd::onbOk);
 bOk->setIcon(Wt::WLink("image/Apply.png"));

 Wt::WPushButton * bCancel = new Wt::WPushButton(Wt::WString(L"������"));
 bCancel->setObjectName("bCancel");
 bCancel->clicked().connect(this, &TcarlifterPlanUpd::onbCancel);
 bCancel->setIcon(Wt::WLink("image/Cancel.png"));

 lh1->addStretch(1);
 lh1->addWidget(bOk);
 lh1->addWidget(bCancel);

 Wt::WContainerWidget * footer = new Wt::WContainerWidget();
 footer->addStyleClass("modal-footer");
 footer->setLayout(lh1);
 // tMain->elementAt(r, 0)->setLayout(lh1);
 tMain->elementAt(r, 0)->addWidget(footer);
 tMain->elementAt(r, 0)->setColumnSpan(tMain->columnCount());

 for(int column = 0; column <= tMain->columnCount() - 1; column++)
     for(int row = 0; row <= tMain->rowCount() - 1; row++)
         tMain->elementAt(row, column)->setPadding(2);
}
//--------------------------------------------------------------------------
void TcarlifterPlanUpd::refresh()
{
 Twindow::refresh();

 if (carlifterPlan.carlifter_plan_id > 0)
 {
   dclp->setDate(carlifterPlan.dt_start.date());
   eStart->setTime(carlifterPlan.dt_start.time());
   eFinish->setTime(carlifterPlan.dt_finish.time());
 }
}
//--------------------------------------------------------------------------
void TcarlifterPlanUpd::refreshCarlifterPlan()
{
  carlifterPlan.dt_start = Wt::WDateTime(dclp->date(), eStart->time());
  carlifterPlan.dt_finish = Wt::WDateTime(dclp->date(), eFinish->time());

  bOk->setEnabled(carlifterPlan.dt_start.isValid() && !carlifterPlan.dt_start.isNull()
                  && carlifterPlan.dt_finish.isValid() && !carlifterPlan.dt_finish.isNull());
}
//--------------------------------------------------------------------------
void TcarlifterPlanUpd::onbOk()
{
    if (carlifterPlan.save() > 0)
    { onCloseInt(carlifterPlan.carlifter_plan_id); }
    else
    {
     ShowMessage(0, Wt::WString::fromUTF8(str2html(dl->errorstr)), "DB Error");
    }
}
//--------------------------------------------------------------------------
void TcarlifterPlanUpd::onbCancel()
{
 onCloseInt(0);
}
//--------------------------------------------------------------------------
//--------------------------------------------------------------------------
//--------------------------------------------------------------------------
TcarlifterPlanIns::TcarlifterPlanIns(Wt::WContainerWidget *parent, TDataLayer * p_dl, const int year, const int month, const int p_city):
    Twindow(p_dl, "TcarlifterPlanIns", parent), city(p_city)
{
 dl = p_dl;
 refresh();
 deStart->setDate(Wt::WDate(year, month, 1));
 deFinish->setDate(deStart->date().addMonths(1).addDays(-1));
 teStart->setTime(Wt::WTime(9,0));
 teFinish->setTime(Wt::WTime(19,0));
 spInterval->setValue(30);
 spDuration->setValue(30);
 onTextInput();
}
//--------------------------------------------------------------------------
void TcarlifterPlanIns::setupUi()
{
    Wt::WTable * tMain = new Wt::WTable(this); tMain->setObjectName("tMain");

    int r(0);

    deStart = new TDateEdit(); deStart->setObjectName("deStart");
    deStart->setEnabled(true);
    deStart->textInput().connect(this, &TcarlifterPlanIns::onTextInput);
    deFinish = new TDateEdit(); deFinish->setObjectName("deStart");
    deFinish->setEnabled(true);
    deFinish->textInput().connect(this, &TcarlifterPlanIns::onTextInput);
    Wt::WText * l = new Wt::WText(L"����:");
    l->setWordWrap(false);
    tMain->elementAt(r, 0)->addWidget(l);
    tMain->elementAt(r, 1)->addWidget(deStart);
    tMain->elementAt(r, 2)->addWidget(deFinish);
    r++;

    teStart = new TTimeEdit(); teStart->setObjectName("teStart");
    teStart->setEnabled(true);
    teStart->textInput().connect(this, &TcarlifterPlanIns::onTextInput);
    teFinish = new TTimeEdit(); teFinish->setObjectName("teFinish");
    teFinish->setEnabled(true);
    teFinish->textInput().connect(this, &TcarlifterPlanIns::onTextInput);
    l = new Wt::WText(L"�����:");
    tMain->elementAt(r, 0)->addWidget(l);
    tMain->elementAt(r, 1)->addWidget(teStart);
    tMain->elementAt(r, 2)->addWidget(teFinish);
    r++;

    spInterval = new Wt::WSpinBox(); spInterval->setObjectName("spInterval");
    spInterval->setEnabled(true);
    spInterval->textInput().connect(this, &TcarlifterPlanIns::onTextInput);
    spInterval->valueChanged().connect(this, &TcarlifterPlanIns::onTextInput);

    l = new Wt::WText(L"��������:");
    tMain->elementAt(r, 0)->addWidget(l);
    tMain->elementAt(r, 1)->addWidget(spInterval);
    r++;

    spDuration = new Wt::WSpinBox(); spDuration->setObjectName("spDuration");
    spDuration->setEnabled(true);
    spDuration->textInput().connect(this, &TcarlifterPlanIns::onTextInput);
    spDuration->valueChanged().connect(this, &TcarlifterPlanIns::onTextInput);
    l = new Wt::WText(L"�����������������:");
    tMain->elementAt(r, 0)->addWidget(l);
    tMain->elementAt(r, 1)->addWidget(spDuration);
    r++;

    l = new Wt::WText(L"���������:");
    tMain->elementAt(r, 0)->addWidget(l);
    tMain->elementAt(r, 1)->setColumnSpan(2);
    tMain->elementAt(r, 1)->addWidget(createCarlifterCheckBox());    
    r++;

    Wt::WHBoxLayout * lh1 = new Wt::WHBoxLayout();
    bOk = new Wt::WPushButton(Wt::WString(L"��")); bOk->setObjectName("bOk");
    bOk->setEnabled(false);
    bOk->clicked().connect(this, &TcarlifterPlanIns::onbOk);
    bOk->setIcon(Wt::WLink("image/Apply.png"));

    Wt::WPushButton * bCancel = new Wt::WPushButton(Wt::WString(L"������"));
    bCancel->setObjectName("bCancel");
    bCancel->clicked().connect(this, &TcarlifterPlanIns::onbCancel);
    bCancel->setIcon(Wt::WLink("image/Cancel.png"));

    lh1->addStretch(1);
    lh1->addWidget(bOk);
    lh1->addWidget(bCancel);

    Wt::WContainerWidget * footer = new Wt::WContainerWidget();
    footer->addStyleClass("modal-footer");
    footer->setLayout(lh1);
    // tMain->elementAt(r, 0)->setLayout(lh1);
    tMain->elementAt(r, 0)->addWidget(footer);
    tMain->elementAt(r, 0)->setColumnSpan(tMain->columnCount());

    for(int column = 0; column <= tMain->columnCount() - 1; column++)
        for(int row = 0; row <= tMain->rowCount() - 1; row++)
            tMain->elementAt(row, column)->setPadding(2);

}
//--------------------------------------------------------------------------
class TCheckBoxCarlifter : public Wt::WCheckBox
{
 public: 
     TCheckBoxCarlifter() : Wt::WCheckBox(), carlifter_id(0) {};
     int carlifter_id;
};
//--------------------------------------------------------------------------

Wt::WWidget * TcarlifterPlanIns::createCarlifterCheckBox()
{
 Wt::WStandardItemModel mt;
 Wt::WHBoxLayout * lh0 = new Wt::WHBoxLayout();
 Wt::WContainerWidget * cCarlifter = new Wt::WContainerWidget(); cCarlifter->setObjectName("cCarlifter");
 cCarlifter->setLayout(lh0);

 if (dl->CarlifterS(&mt, city) > 0)
 {
  for(int i = 0; i < mt.rowCount(); i++)
  {
   int number = to_int(mt.data(i, fieldByName(&mt, "number")));
   int carlifter_id = to_int(mt.data(i, fieldByName(&mt, "carlifter_id")));
   TCheckBoxCarlifter * cbCarlifter = new TCheckBoxCarlifter(); cbCarlifter->setObjectName("cbCarlifter");
   cbCarlifter->carlifter_id = carlifter_id;
   cbCarlifter->setText(Wt::WString("{1}").arg(number));
   cbCarlifter->setTristate(false);
   cbCarlifter->setCheckState(Wt::CheckState::Checked);            
   cbCarlifter->changed().connect(this, &TcarlifterPlanIns::oncbCarlifterChanged);
   cbCarlifter->changed().connect(this, &TcarlifterPlanIns::onTextInput);
            
   lh0->addWidget(cbCarlifter, 0, Wt::AlignmentFlag::AlignTop);

   sCarlifter.insert(carlifter_id);
  }
 }
 return(cCarlifter);
}
//--------------------------------------------------------------------------
void TcarlifterPlanIns::oncbCarlifterChanged()
{
 TCheckBoxCarlifter * b = dynamic_cast<TCheckBoxCarlifter *>(sender());
 if (b == NULL) return;
 int carlifter_id = b->carlifter_id;
 if (carlifter_id < 0) return;

 if (b->checkState() == Wt::CheckState::Checked)
 {
  sCarlifter.insert(carlifter_id);
 }
 else
 {
  auto itr = sCarlifter.find(carlifter_id);
  if (itr != sCarlifter.end()) sCarlifter.erase(itr);
 }
}
/*void TcarlifterPlanIns::refresh()
{
}*/
//--------------------------------------------------------------------------
void TcarlifterPlanIns::onTextInput()
{
 bOk->setEnabled(!deStart->date().isNull() && deStart->date().isValid() 
                 && !deFinish->date().isNull() && deFinish->date().isValid()
                 && deStart->date() <= deFinish->date()
                 && !teStart->time().isNull() && teStart->time().isValid()
                 && !teFinish->time().isNull() && teFinish->time().isValid()
                 && teStart->time() < teFinish->time()
                 && spInterval->value() > 0
                 && spDuration->value() > 0
                 && sCarlifter.size() > 0);
}
//--------------------------------------------------------------------------
void TcarlifterPlanIns::makePlan()
{
 TCarlifterPlan cPlan(dl);
 for(Wt::WDate d = deStart->date(); d <= deFinish->date(); d = d.addDays(1))
 {
  for(Wt::WTime t = teStart->time(); t < teFinish->time(); t = t.addSecs(spInterval->value() * 60))
  {
   for(auto itr = sCarlifter.begin(); itr != sCarlifter.end(); ++itr)
   {
    Wt::WDateTime dtStart(d, t);
    cPlan.clear();
    cPlan.carlifter_id = *itr;
    cPlan.dt_start = Wt::WDateTime(d, t);
    cPlan.dt_finish = cPlan.dt_start.addSecs(spDuration->value() * 60);
    cPlan.save();
   }
  }
 }
}
//--------------------------------------------------------------------------
void TcarlifterPlanIns::onbOk()
{
 makePlan();
 onCloseInt(1);
   /* if (carlifterPlan.save() > 0)
    { onCloseInt(carlifterPlan.carlifter_plan_id); }
    else
    {
        ShowMessage(0, Wt::WString::fromUTF8(str2html(dl->errorstr)), "DB Error");
    }*/
}
//--------------------------------------------------------------------------
void TcarlifterPlanIns::onbCancel()
{
    onCloseInt(0);
}
//--------------------------------------------------------------------------
