#include <Wt/WMessageBox>
#include <Wt/WStandardItem>

#include "taskBrwWindow.h"
#include "taskUpdWindow.h"

TtaskBrw::TtaskBrw(Wt::WContainerWidget *parent, TDataLayer * dl): Twindow(dl, "TtaskBrw", parent)
{
 setupUi();
 refresh(0);
}

//TtaskBrw::~TtaskBrw()
//{
//}

void TtaskBrw::setupUi()
{
    clear();

    Wt::WVBoxLayout * lmain;
    lmain = new Wt::WVBoxLayout();

    setLayout(lmain);
    Wt::WHBoxLayout * lh1 = new Wt::WHBoxLayout();
    lmain->addItem(lh1);

    bNew = new Wt::WPushButton(Wt::WString(L"�����")); bNew->setObjectName("bNew");
    bNew->clicked().connect(this, &TtaskBrw::onNew);
    bNew->setIcon(Wt::WLink("image/New.png"));
    lh1->addWidget(bNew, 0, Wt::AlignmentFlag::AlignTop);

    bEdit = new Wt::WPushButton(Wt::WString(L"��������")); bEdit->setObjectName("bEdit");
    bEdit->clicked().connect(this, &TtaskBrw::onEdit);
    bEdit->setEnabled(false);
    bEdit->setIcon(Wt::WLink("image/Edit.png"));
    lh1->addWidget(bEdit, 0, Wt::AlignmentFlag::AlignTop);

    bDelete = new Wt::WPushButton(Wt::WString(L"�������")); bDelete->setObjectName("bDelete");
    bDelete->clicked().connect(this, &TtaskBrw::onDelete);
    bDelete->setEnabled(false);
    bDelete->setIcon(Wt::WLink("image/Delete.png"));
    lh1->addWidget(bDelete, 0, Wt::AlignmentFlag::AlignTop);

    lh1->addStretch(1);

    cbExecutor = new TComboBox(); cbExecutor->setObjectName("cbExecutor");
    initUser(cbExecutor->mt);
    cbExecutor->setNoSelectionEnabled(true);
    cbExecutor->setLabelKeyColumn("", "user_id", "fio");
    cbExecutor->setLabel(L"�����������:");
    cbExecutor->setValue(dl->SYSGetUser());
    cbExecutor->valueChanged.connect(this, &TtaskBrw::onFilterChanged);
    lh1->addWidget(cbExecutor, 0,  Wt::AlignmentFlag::AlignTop);

    {
        TUser user(dl);
        user.user_id = dl->SYSGetUser();
        user.read();
        cbExecutor->setEnabled(user.can_create_user > 0);
    }

    cbTaskType = new TComboBoxDirLine(dl, 22, NULL); cbTaskType->setObjectName("cbTaskType");
    cbTaskType->setLabel(L"��� ������:");
    cbTaskType->setNoSelectionEnabled(true);
    cbTaskType->setValue(0);
    cbTaskType->valueChanged.connect(this, &TtaskBrw::onFilterChanged);
    lh1->addWidget(cbTaskType, 0, Wt::AlignmentFlag::AlignTop);
        
    cbState = new TComboBoxDirLine(dl, 21, NULL); cbState->setObjectName("cbState");
    cbState->setLabel(L"������ ������:");
    cbState->setNoSelectionEnabled(true);
    cbState->setValue(0);
    cbState->valueChanged.connect(this, &TtaskBrw::onFilterChanged);
    lh1->addWidget(cbState, 0, Wt::AlignmentFlag::AlignTop);

    cbIC = new TComboBox(); cbIC->setObjectName("cbIC");
    if (dl->TableS("BASIC_DATA", cbIC->mt) < 0)
    {
        ShowMessage(0, Wt::WString::fromUTF8(str2html(dl->errorstr)), "DB Error");
    }
    cbIC->mt->insertRow(0, new Wt::WStandardItem());
    cbIC->mt->setData(0, fieldByName(cbIC->mt, "ic_id"), 0);
    cbIC->mt->setData(0, fieldByName(cbIC->mt, "number"), Wt::WString(""));
    cbIC->setNoSelectionEnabled(true);
    cbIC->setLabelKeyColumn("", "ic_id", "number");
    cbIC->setLabel(L"��������� ����:");
    cbIC->setValue(0);
    cbIC->valueChanged.connect(this, &TtaskBrw::onFilterChanged);
    lh1->addWidget(cbIC, 0,  Wt::AlignmentFlag::AlignTop);

    tvPage = new WTableViewPage(dl, this); tvPage->setObjectName("tvPage");
    tvPage->layout->addStretch(1);
    tvPage->xField.clear();
    tvPage->xField.push_back("code");
    tvPage->xField.push_back("name");
    tvPage->xField.push_back("ic_id");
    tvPage->xField.push_back("initiator_fio");
    tvPage->xField.push_back("executor_fio");
    tvPage->filterChanged.connect(this, &Twindow::delayedRefresh);

    Wt::WHBoxLayout * lh2 = new Wt::WHBoxLayout();
    //lh2->addWidget(tvPage->lFilter, 0, Wt::AlignmentFlag::AlignTop);
    lh2->addWidget(tvPage->eFilter, 0, Wt::AlignmentFlag::AlignTop);
    //tvPage->filterChanged.connect(this, &Twindow::delayedRefresh);
    lh2->addLayout(tvPage->layout, 0, Wt::AlignmentFlag::AlignTop);
    lh2->addStretch(1);
    lmain->addLayout(lh2);

    tableView = new Wt::WTableView();// tableView->setObjectName("tableView");
    tableView->setAlternatingRowColors(true);
    tableView->setSortingEnabled(false);
    tableView->setColumnResizeEnabled(true);
    //tableView->setSelectionMode(Wt::SelectionMode::SingleSelection);
    tableView->setSelectionMode(Wt::ExtendedSelection);
    tableView->setSelectable(true);
    tableView->setEditTriggers(Wt::WAbstractItemView::NoEditTrigger);
  //  tableView->doubleClicked().connect(this, &TfileBrw::onDoubleClicked);
  //  tableView->keyPressed().connect(this, &TfileBrw::onKeyPressed);
    tableView->selectionChanged().connect(this, &TtaskBrw::onSelectionChanged);
    tableView->columnResized().connect(this, &Twindow::onColumnResized);
  //  tableView->setItemDelegate(item);
  //  tableView->setHeight(600);

    tvPage->tableView = tableView;

    lmain->addWidget(tableView);
}

void TtaskBrw::refresh()
{
 refresh(0);
}

void TtaskBrw::refresh(const int id)
{
    mt = new Wt::WStandardItemModel(this);
    if (dl->TaskS(mt, cbIC->value(), cbExecutor->value(), cbState->value(), cbTaskType->value()) < 0)
    {
        ShowMessage(0, Wt::WString::fromUTF8(str2html(dl->errorstr)), "DB Error");
        return;
    }

    tvPage->field.clear();
    tvPage->field.push_back(Wt::WString("executor_fio")); tvPage->field.push_back(Wt::WString(L"�����������"));
    tvPage->field.push_back(Wt::WString("initiator_fio")); tvPage->field.push_back(Wt::WString(L"���������"));

    tvPage->field.push_back(Wt::WString("EXECUTE_BEFORE")); tvPage->field.push_back(Wt::WString(L"����"));
    tvPage->field.push_back(Wt::WString("TASK")); tvPage->field.push_back(Wt::WString(L"������"));
    tvPage->field.push_back(Wt::WString("NUMBER")); tvPage->field.push_back(Wt::WString(L"���� �"));

    tvPage->field.push_back(Wt::WString("type_task_name")); tvPage->field.push_back(Wt::WString(L"��� ������"));
    tvPage->field.push_back(Wt::WString("state_name")); tvPage->field.push_back(Wt::WString(L"������ ������"));

   // tvPage->emptyTitleColumn.clear();
   // tvPage->emptyTitleColumn.push_back("ico_RES_REGISTED");
   // tvPage->emptyTitleColumn.push_back("ico_ITEMS_STATUS");

    tvPage->mt = mt;
    tvPage->refresh(0);

    for(int i = 0; i < tableView->model()->columnCount(); i++)
    {tableView->setHeaderAlignment(i, Wt::AlignCenter);}
}

void TtaskBrw::onEdit()
{
    if (tableView->model() == NULL || tableView->model()->rowCount() == 0) return;
    if (tableView->selectedIndexes().empty()) return;
    int row = tvPage->rowIndex(tableView->selectedIndexes().begin()->row());
    int task_id = to_int(mt->data(row, fieldByName(mt, "task_id")));

    int c = mt->rowCount();

    clear();
    TtaskUpd * frm = new TtaskUpd(this, dl, task_id);
    frm->closeInt.connect(this, &TtaskBrw::redraw);
}

void TtaskBrw::onNew()
{
 if (cbIC->value() == 0)
 {
     ShowMessage(0, L"�������� ��������� ����", "Warning");
     return;
 }

 clear();
 TtaskUpd * frm = new TtaskUpd(this, dl);
 frm->closeInt.connect(this, &TtaskBrw::redraw);

 frm->ic_id = cbIC->value();
 frm->initiator = dl->SYSGetUser();
 frm->refresh();
}

void TtaskBrw::redraw(const int v)
{
  Twindow::refresh();
  refresh(0);
}

void TtaskBrw::onDelete()
{
  /*  if (tableView->model() == NULL || tableView->model()->rowCount() == 0) return;
    if (tableView->selectedIndexes().empty()) return;
    int row = tvPage->rowIndex(tableView->selectedIndexes().begin()->row());

    if (Wt::StandardButton::Yes != Wt::WMessageBox::show(Wt::WString(L"������"), Wt::WString(L"������� ������ ?"),
        Wt::StandardButton::Yes | Wt::StandardButton::No))
    {
     return;
    }

    int user_id = to_int(mt->data(row, fieldByName(mt, "USER_ID")));

    if (dl->TableD("sys_users", user_id))
    {
     row = tvPage->rowIndex(tableView->selectedIndexes().begin()->row()) - 1;
     if (row >= 0)
     {
      user_id = to_int(mt->data(row, fieldByName(mt, "USER_ID")));
      refresh(user_id);
     }
     else
         refresh(0);
    }
    else
    {
     ShowMessage(0, Wt::WString::fromUTF8(str2html(dl->errorstr)), "DB Error");
    }*/
}

void TtaskBrw::onSelectionChanged()
{
    bEdit->setEnabled(tableView->model() != NULL && tableView->model()->rowCount() > 0 && !tableView->selectedIndexes().empty());
    bDelete->setEnabled(tableView->model() != NULL && tableView->model()->rowCount() > 0 && !tableView->selectedIndexes().empty());
}

void TtaskBrw::onFilterChanged()
{
 refresh(0);
}

void TtaskBrw::initUser(Wt::WStandardItemModel * mt)
{
    Wt::WStandardItemModel mtUser;

    TUser user(dl);
    user.user_id = dl->SYSGetUser();
    user.read();
    if (user.can_create_user == 0) return; // ��� �������� ������������ ������ �� �����

    if (user.can_create_user == 2)
      dl->UsersS(&mtUser, 0); // ��� ����� ������ ���
    if (user.can_create_user == 1)
        dl->UsersS(&mtUser, 0, user.partner_id); // ��� ������ ������ ���� ����������

    int fieldId(fieldByName(&mtUser, "USER_ID")),
        fieldFio(fieldByName(&mtUser, "FIO")),
        fieldPartner(fieldByName(&mtUser, "partner_id"));

    mt->clear();

    if (mt->columnCount() < 2)
    {
        mt->insertColumn(0); mt->setHeaderData(0, Wt::Orientation::Horizontal, "USER_ID");
        mt->insertColumn(1); mt->setHeaderData(1, Wt::Orientation::Horizontal, "FIO");

        if (user.can_create_user == 2) // ������ ������ ������ �����������
        {
          mt->insertRow(0, new Wt::WStandardItem()); 
          mt->setData(0, 0, 0); mt->setData(0, 1, Wt::WString(""));
        }
    }

    for(int i = 0; i < mtUser.rowCount(); i++)
    {
       // if(partner_id != to_int(mtUser.data(i, fieldPartner))) continue;
        if(10 > to_int(mtUser.data(i, fieldId))) continue;               // ��������� ������������� �� ����������   // TODO : ������ �������

        int row = mt->rowCount();
        mt->insertRow(row);
        mt->setData(row, 0, to_int(mtUser.data(i, fieldId)));
        mt->setData(row, 1, to_string(mtUser.data(i, fieldFio)));
    }
}