--
-- ���� ������������ � ������� SQLiteStudio v3.1.0 � �� ��� 8 13:01:33 2017
--
-- �������������� ��������� ������: System
--
PRAGMA foreign_keys = off;
BEGIN TRANSACTION;

-- �������: profile_item
CREATE TABLE profile_item (
    profile_item_id   INTEGER PRIMARY KEY
                      NOT NULL,
    profile_group_id  INTEGER REFERENCES profile_group (profile_group_id),
    first_id  INTEGER REFERENCES first (first_id),
    second_id INTEGER REFERENCES second (second_id) 
                      NOT NULL
);

-- �������: profile_group
CREATE TABLE profile_group (profile_group_id INTEGER PRIMARY KEY NOT NULL, name STRING (256) NOT NULL UNIQUE);

-- �������: logged_in
CREATE TABLE logged_in (logged_in_id INTEGER PRIMARY KEY NOT NULL, session VARCHAR (16) NOT NULL UNIQUE, db_server VARCHAR (64), db_port INTEGER, db_name VARCHAR (64) NOT NULL, db_user VARCHAR (64) NOT NULL, db_role VARCHAR (64), db_password VARCHAR (64) NOT NULL, dt DATETIME NOT NULL DEFAULT (datetime(CURRENT_TIMESTAMP, 'localtime')));

-- �������: second
CREATE TABLE second (second_id INT PRIMARY KEY NOT NULL, value VARCHAR (64) UNIQUE NOT NULL);

-- �������: db_version
CREATE TABLE db_version (
    version_id     INTEGER  PRIMARY KEY
                      NOT NULL,
    date_applied   DATETIME NOT NULL
                      DEFAULT (datetime(CURRENT_TIMESTAMP, 'localtime')),
    revision     INTEGER NOT NULL
);

-- �������: first
CREATE TABLE first (first_id INT PRIMARY KEY NOT NULL, value VARCHAR (32) UNIQUE NOT NULL);

-- �������: event
CREATE TABLE event (event_id INTEGER PRIMARY KEY NOT NULL, dt DATETIME NOT NULL DEFAULT (datetime(CURRENT_TIMESTAMP, 'localtime')));

-- �������: event_item
CREATE TABLE event_item (event_item_id INTEGER PRIMARY KEY NOT NULL, event_id INTEGER REFERENCES event (event_id), first_id INTEGER REFERENCES first (first_id), second_id INTEGER REFERENCES second (second_id) NOT NULL);

-- ������: idx_second_value
CREATE INDEX idx_second_value ON second (value);

-- ������: idx_logged_in_session
CREATE INDEX idx_logged_in_session ON logged_in (session);

-- ������: idx_revision
CREATE INDEX idx_revision ON db_version (revision);

-- ������: idx_first
CREATE INDEX idx_first ON first (value);

-- ������: idx_event_item_event_id
CREATE INDEX idx_event_item_event_id ON event_item (event_id);

-- ������: idx_event_dt
CREATE INDEX idx_event_dt ON event (dt);

-- ������: idx_event_item_first_id
CREATE INDEX idx_event_item_first_id ON event_item (first_id);

COMMIT TRANSACTION;
PRAGMA foreign_keys = on;
