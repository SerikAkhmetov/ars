#ifndef INSURANCECASEBRWWINDOW_H
#define INSURANCECASEBRWWINDOW_H

#include <Wt/WWidget>
#include <Wt/WContainerWidget>
#include <Wt/WVBoxLayout>
#include <Wt/WHBoxLayout>
#include <Wt/WDialog>
#include <Wt/WLineEdit>
#include <Wt/WCheckBox>
#include <Wt/WTableView>
#include <Wt/WItemDelegate>
#include <Wt/WComboBox>

#include "uDataLayer.h"
#include "widget.h"

class TinsuranceCaseBrw : public Twindow
{
 public:
    TinsuranceCaseBrw(Wt::WContainerWidget *parent, TDataLayer * dl);
    //virtual ~TinsuranceCaseBrw();

    void setupUi();
    void refresh();

    void onCityChanged(const int);

private:
    Wt::WPushButton *bNew, *bEdit, *bDelete;
    Wt::WTableView * tableView; WTableViewPage * tvPage; Wt::WStandardItemModel * mt;
    TComboBoxDirLine * cbStatus0, * cbStatus1;
    TDateEdit * deApplication; Wt::WDate selectedDate;

    int city;

    void refresh(const int);
    void redraw(const int);
    void onEdit();
    void onNew();
    void onDelete();
    void onSelectionChanged();
    void onStatusChanged();
    void onDateChanged();
};

#endif