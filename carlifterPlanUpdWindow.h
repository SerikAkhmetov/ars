#ifndef CARLIFTERPLANUPDWINDOW_H
#define CARLIFTERPLANUPDWINDOW_H

#include <set>

#include <Wt/WWidget>
#include <Wt/WContainerWidget>
#include <Wt/WVBoxLayout>
#include <Wt/WHBoxLayout>
#include <Wt/WDialog>
#include <Wt/WTimeEdit>
#include <Wt/WTable>
#include <Wt/WTabWidget>
#include <Wt/WTextArea>
#include <Wt/WDateEdit>

#include "uDataLayer.h"
#include "widget.h"

//--------------------------------------------------------------------------
class TcarlifterPlanUpd : public Twindow
{
public:
    TcarlifterPlanUpd(Wt::WContainerWidget *parent, TDataLayer * p_dl, const int carlifter_plan_id, const int carlifter_id);
    //virtual ~TcarlifterPlanUpd();

    void setupUi();
    void refresh();

private:
    TCarlifterPlan carlifterPlan;
    TDateEdit * dclp;
    TTimeEdit *eStart, *eFinish;
    Wt::WPushButton * bOk, * bCancel;

    void refreshCarlifterPlan();
    //void onTextInput();
    void onbOk();
    void onbCancel();
};
//--------------------------------------------------------------------------
//--------------------------------------------------------------------------
//--------------------------------------------------------------------------
class TcarlifterPlanIns : public Twindow
{
public:
    TcarlifterPlanIns(Wt::WContainerWidget *parent, TDataLayer * p_dl, const int year, const int month, const int city);
    //virtual ~TcarlifterPlanIns();

    void setupUi();
  //  void refresh();

private:
 //   TCarlifterPlan carlifterPlan;
    int city;
    std::set<int> sCarlifter; // carlifter_id
    Wt::WDateEdit * deStart, *deFinish;
    TTimeEdit *teStart, *teFinish;
    Wt::WSpinBox * spInterval, * spDuration;

    Wt::WPushButton * bOk, * bCancel;

 //   void refreshCarlifterPlan();
    void onTextInput();
    void makePlan();
    Wt::WWidget * createCarlifterCheckBox();
    void onbOk();
    void onbCancel();
    void oncbCarlifterChanged();
};

#endif // CARLIFTERPLANUPDWINDOW_H
