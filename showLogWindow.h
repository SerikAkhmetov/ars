#ifndef SHOWLOGWINDOW_H
#define SHOWLOGWINDOW_H

#include <Wt/WWidget>
#include <Wt/WContainerWidget>
#include <Wt/WVBoxLayout>
#include <Wt/WHBoxLayout>
#include <Wt/WLineEdit>
#include <Wt/WCheckBox>
#include <Wt/WTableView>
#include <Wt/WItemDelegate>
#include <Wt/WTextArea>
#include <Wt/WTabWidget>

#include "uDataLayer.h"
#include "widget.h"
//--------------------------------------------------------------------------
class TshowLogBrw : public Twindow
{
 public:

  TshowLogBrw(Wt::WContainerWidget *parent, TDataLayer * dl);
  //virtual ~TshowLogBrw();

 void setupUi();
 void refresh();
 void refresh(int page);
 void onbOk();
 void onbCancel();
 void onSelectionChanged();
 void onKeyPressed(Wt::WKeyEvent event);
 void onbClicked();
// void onbNew();
// void onbEdit();
// void onbDelete();

// Wt::WCheckBox * cbUsed;
 Wt::WTableView * tableView;
 WTableViewPageControl * tvPage;
 Wt::WPushButton * bOk, * bCancel;//, * bEdit, * bNew, * bDelete;
 Wt::WContainerWidget * footer;
 Wt::WTextArea * taPair;

 Wt::WStandardItemModel * mt;
};
//--------------------------------------------------------------------------
//--------------------------------------------------------------------------
class TshowSessionBrw : public Twindow
{
 public:
  TshowSessionBrw(Wt::WContainerWidget *parent, TDataLayer * dl);
  //virtual ~TshowLogBrw();

 void setupUi();
 void refresh();
 void refreshSession();
 void refreshResources();

// void refresh(int page);
// void onbOk();
// void onbCancel();
// void onSelectionChanged();
// void onKeyPressed(Wt::WKeyEvent event);
// void onbClicked();
// void onbNew();
// void onbEdit();
 void ontabCurrentChanged();

 Wt::WTabWidget * tab;
 Wt::WTableView * tableView1, * tableView2;
 Wt::WPushButton * bOk, * bCancel;//, * bEdit, * bNew, * bDelete;
 Wt::WContainerWidget * footer;
};
//--------------------------------------------------------------------------



#endif // SHOWLOGWINDOW_H
