#include <Wt/WMessageBox>
#include <Wt/WItemDelegate>

#include "carlifterPlanBrwWindow.h"
#include "carlifterPlanUpdWindow.h"
#include "requsitionUpdWindow.h"

TcarlifterPlanBrw::TcarlifterPlanBrw(const int p_city, Wt::WContainerWidget *parent, TDataLayer * dl): Twindow(dl, "TcarlifterPlanBrw", parent), city(p_city)
{
 currentYear = Wt::WDate::currentDate().year();
 currentMonth = Wt::WDate::currentDate().month();
 currentCarLifter = 0;
 setupUi();
 refresh(0);
}

//TcarlifterPlanBrw::~TcarlifterPlanBrw()
//{
//}

void TcarlifterPlanBrw::setupUi()
{
    clear();

    Wt::WVBoxLayout * lmain;
    lmain = new Wt::WVBoxLayout();

    setLayout(lmain);

    Wt::WHBoxLayout * lh0 = new Wt::WHBoxLayout();
    lmain->addItem(lh0);

    Wt::WText * l = new Wt::WText(L"���:");  l->setPadding(5);
    l->setWordWrap(false);
    lh0->addWidget(l, 0, Wt::AlignmentFlag::AlignTop);

    sbYear = new Wt::WSpinBox(); sbYear->setObjectName("sbYear");
    sbYear->setRange(2020, 2120);
    sbYear->setSingleStep(1);
    sbYear->setTextSize(4);
    sbYear->setValue(currentYear);
    sbYear->textInput().connect(this, &TcarlifterPlanBrw::saveStatement);
    sbYear->textInput().connect(this, &TcarlifterPlanBrw::refreshPlan);

    lh0->addWidget(sbYear, 0, Wt::AlignmentFlag::AlignTop);

    l = new Wt::WText(L"�����:");  l->setPadding(5);
    l->setWordWrap(false);
    lh0->addWidget(l, 0, Wt::AlignmentFlag::AlignTop);

    sbMonth = new Wt::WComboBox(); sbMonth->setObjectName("sbMonth");
    sbMonth->addItem(Wt::WString(L"������"));
    sbMonth->addItem(Wt::WString(L"�������"));
    sbMonth->addItem(Wt::WString(L"����"));
    sbMonth->addItem(Wt::WString(L"������"));
    sbMonth->addItem(Wt::WString(L"���"));
    sbMonth->addItem(Wt::WString(L"����"));
    sbMonth->addItem(Wt::WString(L"����"));
    sbMonth->addItem(Wt::WString(L"������"));
    sbMonth->addItem(Wt::WString(L"��������"));
    sbMonth->addItem(Wt::WString(L"�������"));
    sbMonth->addItem(Wt::WString(L"������"));
    sbMonth->addItem(Wt::WString(L"�������"));
    sbMonth->setCurrentIndex(currentMonth - 1);
    sbMonth->changed().connect(this, &TcarlifterPlanBrw::saveStatement);
    sbMonth->changed().connect(this, &TcarlifterPlanBrw::refreshPlan);
    lh0->addWidget(sbMonth, 0, Wt::AlignmentFlag::AlignTop);

    cbCarLifter = new TComboBox(); cbCarLifter->setObjectName("cbCarLifter");
    cbCarLifter->setNoSelectionEnabled(true);
    if (dl->CarlifterS(cbCarLifter->mt, city) < 0)
    {
     ShowMessage(0, Wt::WString::fromUTF8(str2html(dl->errorstr)), "DB Error");
    }
    if (currentCarLifter == 0 && cbCarLifter->mt->rowCount() > 0)
    {
     currentCarLifter = to_int(cbCarLifter->mt->data(0, fieldByName(cbCarLifter->mt, "carlifter_id")));
    }
    cbCarLifter->setLabelKeyColumn(Wt::WString(L"���������:").toUTF8(), "carlifter_id", "number");
    cbCarLifter->setValue(currentCarLifter);
    cbCarLifter->valueChanged.connect(this, &TcarlifterPlanBrw::saveStatement);
    cbCarLifter->valueChanged.connect(this, &TcarlifterPlanBrw::refreshPlan);
    lh0->addWidget(cbCarLifter, 0, Wt::AlignmentFlag::AlignTop);

    lh0->addStretch(1);

    Wt::WHBoxLayout * lh1 = new Wt::WHBoxLayout();
    lmain->addItem(lh1);//, 1, Wt::AlignmentFlag::AlignTop);

    bNew = new Wt::WPushButton(Wt::WString(L"�����")); bNew->setObjectName("bNew");
    bNew->clicked().connect(this, &TcarlifterPlanBrw::onNew);
    bNew->setIcon(Wt::WLink("image/New.png"));
    lh1->addWidget(bNew, 0, Wt::AlignmentFlag::AlignTop);

    bEdit = new Wt::WPushButton(Wt::WString(L"��������")); bEdit->setObjectName("bEdit");
    bEdit->clicked().connect(this, &TcarlifterPlanBrw::onEdit);
    bEdit->setEnabled(false);
    bEdit->setIcon(Wt::WLink("image/Edit.png"));
    bEdit->setMargin(Wt::WLength(0));
    lh1->addWidget(bEdit, 0, Wt::AlignmentFlag::AlignTop);

    bDelete = new Wt::WPushButton(Wt::WString(L"�������")); bDelete->setObjectName("bDelete");
    bDelete->clicked().connect(this, &TcarlifterPlanBrw::onDelete);
    bDelete->setEnabled(false);
    bDelete->setIcon(Wt::WLink("image/Delete.png"));
    lh1->addWidget(bDelete, 0, Wt::AlignmentFlag::AlignTop);

    lh1->addStretch(1);

    //    Wt::WText * l = new Wt::WText(L"������:");
    bNewReq = new Wt::WPushButton(Wt::WString(L"����� ������")); bNewReq->setObjectName("bNewReq");
    bNewReq->clicked().connect(this, &TcarlifterPlanBrw::onNewReq);
    bNewReq->setEnabled(false);
    bNewReq->setIcon(Wt::WLink("image/New.png"));
    lh1->addWidget(bNewReq, 0, Wt::AlignmentFlag::AlignTop);

    bEditReq = new Wt::WPushButton(Wt::WString(L"�������� ������")); bEditReq->setObjectName("bEditReq");
    bEditReq->clicked().connect(this, &TcarlifterPlanBrw::onEditReq);
    bEditReq->setEnabled(false);
    bEditReq->setIcon(Wt::WLink("image/Edit.png"));
    lh1->addWidget(bEditReq, 0, Wt::AlignmentFlag::AlignTop);

    bDeleteReq = new Wt::WPushButton(Wt::WString(L"������� ������")); bDeleteReq->setObjectName("bDeleteReq");
    bDeleteReq->clicked().connect(this, &TcarlifterPlanBrw::onDeleteReq);
    bDeleteReq->setEnabled(false);
    bDeleteReq->setIcon(Wt::WLink("image/Delete.png"));
    lh1->addWidget(bDeleteReq, 0, Wt::AlignmentFlag::AlignTop);


    tableView = new Wt::WTableView();// tableView->setObjectName("tableView");
    tableView->setAlternatingRowColors(true);
    tableView->setSortingEnabled(false);
    tableView->setColumnResizeEnabled(true);
    tableView->setSelectionMode(Wt::SelectionMode::ExtendedSelection);
    //tableView->setSelectionMode(Wt::SelectionMode::SingleSelection);
    tableView->setSelectionBehavior(Wt::SelectionBehavior::SelectItems);
    tableView->setSelectable(true);
    tableView->setEditTriggers(Wt::WAbstractItemView::NoEditTrigger);

    Wt::WItemDelegate * item = new TItemDelegateMultiLine();
    //item->setTextFormat(Wt::TextFormat::PlainText);
    tableView->setItemDelegate(item);
    tableView->setRowHeight(Wt::WLength(2.5, Wt::WLength::Unit::FontEm));
  //  tableView->doubleClicked().connect(this, &TfileBrw::onDoubleClicked);
  //  tableView->keyPressed().connect(this, &TfileBrw::onKeyPressed);
    tableView->selectionChanged().connect(this, &TcarlifterPlanBrw::onSelectionChanged);
  //  tableView->columnResized().connect(this, &Twindow::onColumnResized);
  //  tableView->setItemDelegate(item);
  //  tableView->setHeight(600);

  /*  std::string text = "first<BR/>second";
    Wt::WLabel * w = new Wt::WLabel();
    w->setWordWrap(true);
    //w->setTextFormat(Wt::TextFormat::PlainText);
    w->setTextFormat(Wt::TextFormat::XHTMLText);
    w->setText(Wt::WString::fromUTF8(text));
    lmain->addWidget(w, 0, Wt::AlignmentFlag::AlignTop);*/

    lmain->addWidget(tableView, 0, Wt::AlignmentFlag::AlignLeft);
    lmain->addStretch(1);
}

void TcarlifterPlanBrw::refreshPlan()
{
 refresh(0);
}

/*void TcarlifterPlanBrw::refresh()
{
 refresh(0);
}*/

void TcarlifterPlanBrw::refresh(const int id)
{
 Wt::WDateTime start(Wt::WDate(sbYear->value(), sbMonth->currentIndex() + 1, 1), Wt::WTime(0, 0)),
               finish(start.date().addMonths(1), Wt::WTime(0, 0));
 int daycount(finish.date().addDays(-1).day());
 
 int carlifter_id = cbCarLifter->value();


 vplan.clear();
 mt = new Wt::WStandardItemModel(this);

 for(int i = 1; i <= daycount; i++)
 {
  std::stringstream stm; stm << i;
  mt->insertColumn(i - 1);
  mt->setHeaderData(i - 1, Wt::Orientation::Horizontal, stm.str());
 }

 Wt::WStandardItemModel lmt(this);

 if (dl->CarlifterPlanS(&lmt, city, start, finish) > 0)
 {
  int row(0), day(0);
  for(int i = 0; i < lmt.rowCount(); i++)
  {
    if (carlifter_id != to_int(lmt.data(i, fieldByName(&lmt, "carlifter_id")))) continue;
    TCarlifterPlan plan(dl);
    plan.requisition_id = to_int(lmt.data(i, fieldByName(&lmt, "requisition_id")));
    plan.carlifter_plan_id = to_int(lmt.data(i, fieldByName(&lmt, "carlifter_plan_id")));
    plan.dt_start = to_wdatetime(lmt.data(i, fieldByName(&lmt, "DT_START")));
    plan.dt_finish = to_wdatetime(lmt.data(i, fieldByName(&lmt, "DT_FINISH")));
    if (day != plan.dt_start.date().day())
    {
      if (day != 0) row = 0;
      day = plan.dt_start.date().day();
    }
    int c = mt->rowCount();
    if (c - 1 < row) mt->insertRow(c);
   // Wt::WLabel * l = new Wt::WLabel();
   // l->setWordWrap(true);
   // l->setTextFormat(Wt::TextFormat::PlainText);
   // l->setText(Wt::WString::fromUTF8(plan.label()));
    mt->setData(row, day - 1, plan.label());
    mt->setData(row, day - 1, (int)vplan.size(), Wt::ItemDataRole::UserRole);
    if (plan.requisition_id > 0) mt->setData(row, day - 1, "label-success", Wt::ItemDataRole::StyleClassRole);
    vplan.push_back(plan);
    row++;
  }
 }

tableView->setModel(mt);

    /*if (dl->RequisitionS(mt, 0, city) < 0)
    {
        ShowMessage(0, Wt::WString::fromUTF8(str2html(dl->errorstr)), "DB Error");
        return;
    }*/

    for(int i = 0; tableView->model() != NULL && i < tableView->model()->columnCount(); i++)
    {tableView->setHeaderAlignment(i, Wt::AlignCenter);
     tableView->setColumnWidth(i, Wt::WLength(5.5, Wt::WLength::Unit::FontEx));}
}

void TcarlifterPlanBrw::onEdit()
{
    int id(0);
    if (tableView->model() == NULL && tableView->selectedIndexes().size() != 1) return;

    auto idxs = tableView->selectedIndexes();
    for(auto itr = idxs.begin(); itr != idxs.end(); ++itr)
    {
        if (tableView->model()->data(itr->row(), itr->column(), Wt::ItemDataRole::UserRole).empty()) continue; 
        int row(itr->row()), column(itr->column());
        int i = to_int(tableView->model()->data(itr->row(), itr->column(), Wt::ItemDataRole::UserRole));
        if (i > vplan.size() - 1) continue;
        id = vplan[i].carlifter_plan_id;
    }

    if (id > 0)
    {
        clear();
        TcarlifterPlanUpd * frm = new TcarlifterPlanUpd(this, dl, id, currentCarLifter);
        frm->closeInt.connect(this, &TcarlifterPlanBrw::redraw);   
    }
}

void TcarlifterPlanBrw::onNew()
{
 clear();
 //TcarlifterPlanUpd * frm = new TcarlifterPlanUpd(this, dl, 0, currentCarLifter);
 TcarlifterPlanIns* frm = new TcarlifterPlanIns(this, dl, currentYear, currentMonth, city);
 frm->closeInt.connect(this, &TcarlifterPlanBrw::redraw);
}

void TcarlifterPlanBrw::redraw(const int v)
{
  Twindow::refresh();
//  clear();
//  setupUi();
  refresh(0);
}

void TcarlifterPlanBrw::onDelete()
{
    if (tableView->model() == NULL || tableView->model()->rowCount() == 0) return;
    if (tableView->selectedIndexes().empty()) return;
 //   int row = tvPage->rowIndex(tableView->selectedIndexes().begin()->row());

    if (Wt::StandardButton::Yes != Wt::WMessageBox::show(Wt::WString(L"������"), Wt::WString(L"������� ������ ���������� ?"),
        Wt::StandardButton::Yes | Wt::StandardButton::No))
    {
     return;
    }

    auto idxs = tableView->selectedIndexes();
    for(auto itr = idxs.begin(); itr != idxs.end(); ++itr)
    {
      if (tableView->model()->data(itr->row(), itr->column(), Wt::ItemDataRole::UserRole).empty()) continue; 
      int row(itr->row()), column(itr->column());
      int i = to_int(tableView->model()->data(itr->row(), itr->column(), Wt::ItemDataRole::UserRole));
      if (i > vplan.size() - 1) continue;
      if (!vplan[i].remove())
      {
          ShowMessage(0, Wt::WString::fromUTF8(str2html(dl->errorstr)), "DB Error");
          break;
      }
    }
    refresh(0);
}

void TcarlifterPlanBrw::onEditReq()
{
    int requisition_id(currentRequisitionId());
    if (requisition_id < 1) return;

    clear();
    TrequsitionUpd * frm = new TrequsitionUpd(this, dl, requisition_id, city);
    frm->fixPlan();
    frm->closeInt.connect(this, &TcarlifterPlanBrw::redraw);
}

void TcarlifterPlanBrw::onNewReq()
{
    if (tableView->model() == NULL || tableView->model()->rowCount() == 0) return;
    if (tableView->selectedIndexes().empty()) return;

    auto idxs = tableView->selectedIndexes();
    auto itr = idxs.begin();
    if (tableView->model()->data(itr->row(), itr->column(), Wt::ItemDataRole::UserRole).empty()) return; 
    int row(itr->row()), column(itr->column());
    int i = to_int(tableView->model()->data(itr->row(), itr->column(), Wt::ItemDataRole::UserRole));
    if (i > vplan.size() - 1) return;
    
    TRequisition requisition(dl);

    requisition.carlifter_plan_id = vplan[i].carlifter_plan_id;
    requisition.carlifter_id = cbCarLifter->value();
    requisition.date_plan = vplan[i].dt_finish.date();

    clear();
    TrequsitionUpd * frm = new TrequsitionUpd(this, dl, 0, city);
    frm->setRequisition(requisition);
    frm->fixPlan();
    frm->closeInt.connect(this, &TcarlifterPlanBrw::redraw);
}

void TcarlifterPlanBrw::onDeleteReq()
{
    TRequisition requisition(dl);
    requisition.requisition_id = currentRequisitionId();
    if (requisition.requisition_id < 1) return;

    if (Wt::StandardButton::Yes != Wt::WMessageBox::show(Wt::WString(L"������"), Wt::WString(L"������� ������ ?"),
        Wt::StandardButton::Yes | Wt::StandardButton::No))
    {
        return;
    }

    if (requisition.remove())
    {
        refresh(0);
    }
    else
    {
        ShowMessage(0, Wt::WString::fromUTF8(str2html(dl->errorstr)), "DB Error");
    }
}

int TcarlifterPlanBrw::currentRequisitionId()
{
    if (tableView->model() != NULL && tableView->model()->rowCount() > 0 && tableView->selectedIndexes().size() == 1)
    {
        auto idxs = tableView->selectedIndexes();
        auto itr = idxs.begin();
        int row(itr->row()), column(itr->column());
        int i = to_int(tableView->model()->data(itr->row(), itr->column(), Wt::ItemDataRole::UserRole));
        if (i <= vplan.size() - 1)
        {return(vplan[i].requisition_id);}
    }
    return(-1);
}

void TcarlifterPlanBrw::onSelectionChanged()
{
  bEdit->setEnabled(tableView->model() != NULL && tableView->model()->rowCount() > 0 && tableView->selectedIndexes().size() == 1);
  bDelete->setEnabled(tableView->model() != NULL && tableView->model()->rowCount() > 0 && !tableView->selectedIndexes().empty());

  bool req = currentRequisitionId() > 0;

  bNewReq->setEnabled(!req);
  bEditReq->setEnabled(req);
  bDeleteReq->setEnabled(req);
}

void TcarlifterPlanBrw::onCityChanged(const int n)
{
 if (city != n)
 {
  city = n;
  refresh(0);
 }
}

void TcarlifterPlanBrw::saveStatement()
{
 currentYear = sbYear->value();
 currentMonth= sbMonth->currentIndex() + 1; 
 currentCarLifter = cbCarLifter->value();
}