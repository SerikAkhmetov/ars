﻿#include <boost/algorithm/string.hpp>

#include "report.h"

//--------------------------------------------------------------------------
void HPDF_STDCALL TPDFDoc_error_handler(HPDF_STATUS error_no, HPDF_STATUS detail_no, void *user_data)
{
    ((TPDFDoc *) user_data) -> error_handler(error_no, detail_no);
}
//--------------------------------------------------------------------------
TPDFDoc::TPDFDoc(TDataLayer * p_dl, Wt::WObject *parent) : Wt::WResource(parent)
{
    suggestFileName("report.pdf");

    dl = p_dl;

    pdf = HPDF_New(::TPDFDoc_error_handler, this);
    HPDF_STATUS err = HPDF_UseUTFEncodings(pdf);
    err = HPDF_SetCurrentEncoder(pdf, "UTF-8");

    std::string detail_font_name = HPDF_LoadTTFontFromFile (pdf, "C:\\Windows\\Fonts\\arial.ttf", HPDF_TRUE);
    font = HPDF_GetFont(pdf, detail_font_name.c_str(), "CP1251");

    std::string bold_font_name = HPDF_LoadTTFontFromFile (pdf, "C:\\Windows\\Fonts\\arialbd.ttf", HPDF_TRUE);
    fontBD = HPDF_GetFont(pdf, bold_font_name.c_str(), "CP1251");

    std::string i_font_name = HPDF_LoadTTFontFromFile (pdf, "C:\\Windows\\Fonts\\ariali.ttf", HPDF_TRUE);
    fontI = HPDF_GetFont(pdf, i_font_name.c_str(), "CP1251");
}

TPDFDoc::~TPDFDoc()
{
    HPDF_Free(pdf);
}

void TPDFDoc::handleRequest(const Wt::Http::Request& request, Wt::Http::Response& response)
{
    render();

    HPDF_SaveToStream(pdf);
    unsigned int size = HPDF_GetStreamSize(pdf);
    HPDF_BYTE *buf = new HPDF_BYTE[size];
    HPDF_ReadFromStream (pdf, buf, &size);
    HPDF_ResetStream(pdf);

    response.out().write((char*)buf, size);
    delete[] buf;
}

void TPDFDoc::error_handler(HPDF_STATUS p_error_no, HPDF_STATUS p_detail_no)
{
    error_no = p_error_no;
    detail_no= p_detail_no;

    std::stringstream stm;
    std::cout << "libharu error: error_no=" << (unsigned int) error_no << std::endl;
    std::cout << "detail_no=" << (int) detail_no << std::endl;

    std::map<std::string, std::string> state;
    state["_"] = __PRETTY_FUNCTION__;
    if (Wt::WApplication::instance() != NULL)
        state["sessionId"] = Wt::WApplication::instance()->sessionId();
    state["error_no"] = std::to_string(error_no);
    state["detail_no"] = std::to_string(detail_no);

    logtxt(&state);
}

void TPDFStatement::render()
{
    left = 41.4; right = 579.48; line_step = 13.68;

    renderPage1();
    renderPage2();
    renderPage3();
}

std::string UTF8_to_CP1251(const std::string & utf8)
{
    if(!utf8.empty())
    {
        int wchlen = MultiByteToWideChar(CP_UTF8, 0, utf8.c_str(), utf8.size(), NULL, 0);
        if(wchlen > 0 && wchlen != 0xFFFD)
        {
            std::vector<wchar_t> wbuf(wchlen);
            MultiByteToWideChar(CP_UTF8, 0, utf8.c_str(), utf8.size(), &wbuf[0], wchlen);
            std::vector<char> buf(wchlen);
            WideCharToMultiByte(1251, 0, &wbuf[0], wchlen, &buf[0], wchlen, 0, 0);

            return std::string(&buf[0], wchlen);
        }
    }
    return std::string();
}

void TPDFStatement::renderPage1()
{
    HPDF_REAL x, y;
    TPDFPage * page = new TPDFPage(pdf);
    
    HPDF_REAL lsymbol(0), lsymbol2;
    HPDF_REAL column1(72.22), column2;

    page->SetFontAndSize(font, 8);

  //  std::string s("Убыток № "); s += basicData->number + std::string(" от ") +  basicData-> ;
    page->TextOut(36.07, 796.09,  "Убыток № ");
    x = page->x;

    page->SetFontAndSize(font, 9);
    page->TextOut(UTF8_to_CP1251(basicData->number).c_str());
    page->x = x;
    page->SetFontAndSize(font, 8);

    page->TextOut("__________  от «____»_______г.");
     page->TextOut(36.07, 777.25,  "Заявление №________________ от «____»_______г.");

    page->SetFontAndSize(fontBD, 8);

    page->TextOut("АО СТРАХОВАЯ КОМПАНИЯ \"ГАЙДЕ\"", 756.29, 0);
    page->TextOut("ЗАЯВЛЕНИЕ О СТРАХОВОМ ВОЗМЕЩЕНИИ ИЛИ ПРЯМОМ ВОЗМЕЩЕНИИ УБЫТКОВ ПО ДОГОВОРУ ОБЯЗАТЕЛЬНОГО", 746.85, 0);
    page->TextOut("СТРАХОВАНИЯ ГРАЖДАНСКОЙ ОТВЕТСТВЕННОСТИ ВЛАДЕЛЬЦЕВ ТРАНСПОРТНЫХ СРЕДСТВ", 737.25, 0);

    //left = 41.4; right = 579.48;
    HPDF_REAL top(725.54), bottom(590.38);

    page->SetLineWidth(1.0);
    page->Rectangle(left, top - 22.6, right - left, 22.2);
    page->Rectangle(left, bottom, right - left, 113.16);

    page->TextOut(left + 25.09, top - 11.2,  "1. Потерпевший (выгодоприобретатель, представитель выгодоприобретателя) ");

    page->SetFontAndSize(fontI, 8);

    page->TextOut("(нужное подчеркнуть)");

    page->SetLineWidth(0.0);
    page->Line(left + 6.12, top - 32.06, right - 6.12, top - 31.36);

    page->SetFontAndSize(fontI, 6);
    page->TextOut(left + 140.34, top - 38.92, "(полное наименование юридического лица или фамилия, имя, отчество* физического лица)");

    page->SetFontAndSize(font, 9);
    std::string beneficiary(dBeneficiary->last_name + std::string(" ") + dBeneficiary->first_name + std::string(" ") + dBeneficiary->middle_name);
    if (beneficiary.length() < 3) beneficiary = entity->name; 
    beneficiary = UTF8_to_CP1251(beneficiary);
    page->TextOut(left + 7.12, top - 31.46, beneficiary.c_str());

    page->SetFontAndSize(fontI, 6);
    page->TextOut(left + 15.52, top - 60.64, "(дата рождения физического лица)");
    page->TextOut(left + 194.85, top - 60.64, "(ИНН юридического лица)");
    {
    page->SetFontAndSize(font, 9);
    std::stringstream day; 
    if (dBeneficiary->date_birth.isValid() && !dBeneficiary->date_birth.isNull()) day << std::setw(2) << std::setfill('0') << dBeneficiary->date_birth.day();
    page->TextBoxOut(day.str(), left + 6.12, top - 53.6, 2);

    page->TextOut(".");

    page->SetFontAndSize(font, 9);
    std::stringstream month; 
    if (dBeneficiary->date_birth.isValid() && !dBeneficiary->date_birth.isNull()) month << std::setw(2) << std::setfill('0') << dBeneficiary->date_birth.month();
    page->TextBoxOut(month.str(), page->x, page->y, 2);

    page->TextOut(".");

    page->SetFontAndSize(font, 9);
    std::stringstream year; 
    if (dBeneficiary->date_birth.isValid() && !dBeneficiary->date_birth.isNull()) year << std::setw(4) << std::setfill('0') << dBeneficiary->date_birth.year();
    page->TextBoxOut(year.str(), page->x, page->y, 4);
    }

    {
        page->SetFontAndSize(font, 9);
        std::string itn(entity->itn); boost::trim(itn);
        if (itn.length() > 10) itn = itn.substr(0, 10);
        else  for(int i = itn.length(); i < 10; i++) itn = std::string(" ") + itn;
        page->TextBoxOut(itn, left + 180.0, page->y);
    }

    if (!dBeneficiary->p_series.empty() || !dBeneficiary->p_number.empty())
    {
       page->TextOut(left + 290.12, top - 70.00, "паспорт");
       page->TextOut(right - 181.0, top - 70.00, UTF8_to_CP1251(dBeneficiary->p_series).c_str());
       page->TextOut(right - 71.0, top - 70.00, UTF8_to_CP1251(dBeneficiary->p_number).c_str());
    }

    page->Line(left + 6.12, top - 71.97, right - 193.0, top - 71.97, 0.0);
    page->Line(right - 183.0, top - 71.97, right - 83.0, top - 71.97, 0.0);
    page->Line(right - 73.0, top - 71.97, right - 6.12, top - 71.97, 0.0);

    page->SetFontAndSize(fontI, 6);
    page->TextOut(left + 7.12, top - 79.36, "(свидетельство о регистрации юридического лица либо документ, удостоверяющий личность физического лица)");
    page->TextOut(right - 163.69, top - 79.36, "(серия)");
    page->TextOut(right - 71.54, top - 79.36, "(номер)");

    page->LineLabel(left + 33.91, top - 91.05, left + 107.2, "(индекс)");
    page->LineLabel(left + 117.2, top - 91.05, left + (right - left) / 3 * 2, "(государство, республика, край, область)");
    page->LineLabel(left + (right - left) / 3 * 2 + 10, top - 91.05, right - 6.12, "(район)");

    {
    page->LineLabel(left + 6.12, top - 111.4, left + (right - left) / 3, "(населенный пункт)");
    page->LineLabel(left + (right - left) / 3 + 10, top - 111.4, left + (right - left) / 3 * 2, "(улица)");

    page->LineLabel(left + (right - left) / 3 * 2 + 10, top - 111.4, left + (right - left) / 3 * 2 + 10 + (right - left) / 9, "(дом)");
    page->LineLabel(left + (right - left) / 3 * 2 + 20 + (right - left) / 9, top - 111.4, left + (right - left) / 3 * 2 + 20 + (right - left) / 9 * 2, "(корпус)");
    page->LineLabel(left + (right - left) / 3 * 2 + 30 + (right - left) / 9 * 2, top - 111.4, right - 6.12, "(квартира)");
    }

    page->SetFontAndSize(font, 9);
    page->TextOut(left + 35.91, top - 89.05, UTF8_to_CP1251(dBeneficiary->postcode).c_str());
    page->TextOut(left + 119.2, top - 89.05, UTF8_to_CP1251(dBeneficiary->addr_field1).c_str());
    page->TextOut(left + (right - left) / 3 * 2 + 12, top - 89.05, UTF8_to_CP1251(dBeneficiary->addr_field2).c_str());

    page->TextOut(left + 8.12, top - 109.4, UTF8_to_CP1251(dBeneficiary->addr_field3).c_str());
    page->TextOut(left + (right - left) / 3 + 12, top - 109.4, UTF8_to_CP1251(dBeneficiary->addr_field4).c_str());
    page->TextOut(left + (right - left) / 3 * 2 + 12, top - 109.4, std::to_string(dBeneficiary->house).c_str());
    page->TextOut(left + (right - left) / 3 * 2 + 22 + (right - left) / 9, top - 109.4, UTF8_to_CP1251(dBeneficiary->addr_field5).c_str());
    page->TextOut(left + (right - left) / 3 * 2 + 32 + (right - left) / 9 * 2, top - 109.4, std::to_string(dBeneficiary->apartment).c_str());

    page->SetFontAndSize(font, 8);
    page->TextOut(left + 7.12, top - 91.05, "Адрес");
    page->TextOut(left + 7.12, top - 130.81, "Телефон");

    page->y -= 3;
    page->x += 3;

    page->SetFontAndSize(font, 9);
    {
    std::string phone(UTF8_to_CP1251(dBeneficiary->phone)); boost::trim(phone);
    if (phone.length() > 12) phone = phone.substr(0, 12);
    else for(int i = phone.length(); i < 12; i++) phone = std::string(" ") + phone;
    page->TextBoxOut(phone, page->x, page->y);
    }

    //left(41.4), right(579.48),
    top = bottom;   // 590.38
    bottom = 220.01;

    page->SetLineWidth(1.0);
    page->Rectangle(left, top - 22.6, right - left, 22.2);
    page->Rectangle(left, bottom, right - left, top - bottom - 22.2);
    page->SetFontAndSize(fontBD, 8);
    page->TextOut(left + 25.09, top - 11.2,  "2. Поврежденное имущество");
    {
    page->SetFontAndSize(font, 8);
    page->TextOut(left + 6.12, top - 33.82, "Собственник");
    page->SetLineWidth(0.0); 
    page->SetFontAndSize(fontI, 6);
    HPDF_REAL x1 = page->x + 2;
    page->LineLabel(x1, top - 33.82, right - 6.12, "(полное наименование юридического лица или фамилия, имя, отчество* физического лица)");

    page->SetFontAndSize(font, 9);
    std::string owner(dOwner->last_name + std::string(" ") + dOwner->first_name + std::string(" ") + dOwner->middle_name);
    owner = UTF8_to_CP1251(owner);
    page->TextOut(x1 + 2.00, top - 31.82, owner.c_str());
    }

    drawDriver(page, top - 45, dOwner);

    page->SetLineWidth(0.0);

    page->y -= line_step;
    page->x = left + 6.12;
    page->SetFontAndSize(font, 8);
    page->TextOut("Сведения о поврежденном транспортном средстве:");
    {
    page->y -= line_step; 
    page->x = left + 6.12;
    page->TextOut("Марка:");
    HPDF_REAL x1 = page->x, y1 = page->y; 
    page->Line(page->x, y1, left + (right - left) / 2 - 10, page->y);
    page->TextOut(left + (right - left) / 2, page->y, "Модель:");
    HPDF_REAL x2 = page->x;
    page->Line(page->x, page->y, right - 6.12, page->y);

    page->SetFontAndSize(font, 9);
    page->TextOut(x1 + 2, y1 + 2, UTF8_to_CP1251(vVictim->brand).c_str());
    page->TextOut(x2 + 2, y1 + 2, UTF8_to_CP1251(vVictim->model).c_str());
    page->y = y1;
    }
    {
        page->SetFontAndSize(font, 8);
        page->y -= line_step; 
        page->x = left + 6.12;
        page->TextOut("Идентификационный номер транспортного средства");
        HPDF_REAL y1 = page->y, x1 = page->x;
        page->Line(page->x, page->y, right - 6.12, page->y);
        page->SetFontAndSize(font, 9);
        page->TextOut(x1 + 2, y1 + 2, UTF8_to_CP1251(vVictim->vin).c_str());
        page->y = y1;
    }

    {
        page->SetFontAndSize(font, 8);
        page->y -= line_step; 
        page->x = left + 6.12;
        page->TextOut("Государственный регистрационный знак");
        HPDF_REAL x1 = page->x, y1 = page->y;
        page->Line(page->x, page->y, left + (right - left) / 2 - 10, page->y);
        page->TextOut(left + (right - left) / 2, page->y, "Год изготовления транспортного средства");
        HPDF_REAL x2 = page->x;
        page->Line(page->x, page->y, right - 6.12, page->y);
        page->SetFontAndSize(font, 9);
        if (vVictim->car_year > 0) page->TextOut(x2 + 2, y1 + 2, std::to_string(vVictim->car_year).c_str());
        page->TextOut(x1 + 2, y1 + 2, UTF8_to_CP1251(vVictim->registration_number).c_str());
    }

    {
        page->SetFontAndSize(font, 8);
        page->y -= line_step; 
        page->x = left + 6.12;
        page->TextOut("Документ о регистрации транспортного средства");

        page->y -= line_step; 
        page->x = left + 6.12;
        page->TextOut("Тип документа");

        HPDF_REAL x1 = page->x, y1 = page->y;

        page->x = left + (right - left) / 10 * 3;
        page->TextOut("Серия");
        HPDF_REAL x2 = page->x;

        page->x = left + (right - left) / 10 * 4.5;
        page->TextOut("Номер");
        HPDF_REAL x3 = page->x;

        page->x = left + (right - left) / 10 * 6.5;
        page->TextOut("Дата выдачи");
        HPDF_REAL x4 = page->x;

        page->SetFontAndSize(font, 9);
        page->TextOut(x1 + 2, y1 + 2, UTF8_to_CP1251(dl->DIRLineName(vVictim->vehicle_doc)).c_str());
        page->TextOut(x2 + 2, y1 + 2, UTF8_to_CP1251(vVictim->series).c_str());
        page->TextOut(x3 + 2, y1 + 2, UTF8_to_CP1251(vVictim->number).c_str());
        page->DateBoxOut(vVictim->date_doc, x4 + 2, y1);
    }

    {
        page->SetFontAndSize(fontBD, 8);
        page->y -= line_step; 
        page->x = left + 6.12;
        page->TextOut("Сведения об ином поврежденном имуществе: ");
        page->SetFontAndSize(font, 8);
        page->TextOut("Вид поврежденного имущества");
        page->SetFontAndSize(fontI, 6);
        page->LineLabel(page->x, page->y, right - 6.12, "(указать иное поврежденное имущество)");
    }

    {
        page->SetFontAndSize(font, 8);
        page->y -= line_step; 
        page->x = left + 6.12;
        page->TextOut("Вид и реквизиты документа, подтверждающего право собственности на поврежденное имущество:");
        page->y -= line_step; 
        page->x = left + 6.12;
        page->Line(page->x, page->y, right - 6.12, page->y);
        page->y -= line_step; 
        page->x = left + 6.12;
        page->TextOut("Сведения о причинении вреда жизни/здоровью:");
        page->Line(page->x, page->y, right - 6.12, page->y);
        page->y -= line_step; 
        page->x = left + 6.12;
        page->TextOut("Лицо, жизни / здоровью которого причинен вред");
        page->SetFontAndSize(fontI, 6);
        page->LineLabel(page->x, page->y, right - 6.12, "(фамилия, имя, отчество* физического лица) ");
        page->y -= line_step; 
        page->x = left + 6.12;
        page->SetFontAndSize(font, 8);
        page->TextOut("Характер и степень повреждения здоровья");
        page->Line(page->x, page->y, right - 6.12, page->y);
        page->y -= line_step; 
        page->x = left + 6.12;
        page->Line(page->x, page->y, right - 6.12, page->y);
        page->y -= line_step; 
        page->x = left + 6.12;
        page->TextOut("Имеются ли дополнительные расходы на лечение, восстановление  здоровья:");
        page->CharBoxOut(" ", page->x, page->y - 2); page->x += 2; page->y += 2;
        page->TextOut("ДА / ");
        page->CharBoxOut(" ", page->x, page->y - 2); page->x += 2; page->y += 2;
        page->TextOut("НЕТ");
        page->y -= line_step; 
        page->x = left + 6.12;
        page->TextOut("Имеется ли утраченный заработок (доход):");
        page->CharBoxOut(" ", page->x, page->y - 2); page->x += 2; page->y += 2;
        page->TextOut("ДА / ");
        page->CharBoxOut(" ", page->x, page->y - 2); page->x += 2; page->y += 2;
        page->TextOut("НЕТ");
        page->y -= line_step; 
        page->x = left + 6.12;
        page->TextOut("Отношение к погибшему лицу (степень родства)");
        page->Line(page->x, page->y, right - 6.12, page->y);
    }

    //left(41.4), right(579.48),
    top = bottom;   // 590.38
    bottom = 90.01;

    page->SetLineWidth(1.0);
    page->Rectangle(left, top - 22.6, right - left, 22.2);
    page->Rectangle(left, bottom, right - left, top - bottom - 22.2);
    page->SetFontAndSize(fontBD, 8);
    page->TextOut(left + 25.09, top - 11.2,  "3. Сведения о страховом случае");
    page->SetLineWidth(0.0);
    {
    page->SetFontAndSize(font, 8);
    page->y = top - 22.6 - line_step; 
    page->x = left + 6.12;
    page->TextOut("Дата и время страхового случая");
    page->SetFontAndSize(font, 9);
    page->x += 2;
    page->DateBoxOut(basicData->dt_ta.date(), page->x, page->y);
    page->x += 10;
    page->TimeBoxOut(basicData->dt_ta.time(), page->x, page->y);
    }
    {
        page->SetFontAndSize(font, 8);
        page->y -= line_step; 
        page->x = left + 6.12;
        page->TextOut("Адрес места ДТП:");
        HPDF_REAL x1 = page->x, y1 = page->y;
        page->Line(page->x, page->y, right - 6.12, page->y);
        page->SetFontAndSize(font, 9);
        std::stringstream adr;
        adr << basicData->postcode << " " << basicData->addr_field1;
        page->TextOut(x1 + 2, y1 + 2, UTF8_to_CP1251(adr.str()).c_str());
        page->y = y1;
    }
    {
        page->SetFontAndSize(font, 8);
        page->y -= line_step; 
        page->x = left + 6.12;
        page->TextOut("Водитель (виновник), управлявший транспортным средством, при использовании которого причинен вред:");
        page->y -= line_step; 
        page->x = left + 6.12;
        page->SetFontAndSize(font, 8);
        HPDF_REAL x1 = page->x, y1 = page->y;
        std::stringstream name;
        name << dCulprit->last_name << " " << dCulprit->first_name << " " << dCulprit->middle_name;
        page->TextOut(page->x + 2, page->y + 2, UTF8_to_CP1251(name.str()).c_str());
        page->SetFontAndSize(fontI, 6);
        page->LineLabel(x1, y1, right - 6.12, "(фамилия, имя, отчество* физического лица) ");
        //page->y = y1;
    }
    {
        page->SetFontAndSize(font, 8);
        page->y -= line_step; 
        page->x = left + 6.12;
        page->TextOut("Обстоятельства страхового случая:");
        page->Line(page->x, page->y, right - 6.12, page->y);
        page->y -= line_step; 
        page->x = left + 6.12;
        page->Line(page->x, page->y, right - 6.12, page->y);
        page->y -= line_step; 
        page->x = left + 6.12;
        page->Line(page->x, page->y, right - 6.12, page->y);
    }

    page->y -= line_step / 2; 
    page->x = left + 6.12;
    drawPageBottom(page, page->y);

}

void TPDFStatement::renderPage2()
{
    TPDFPage * page = new TPDFPage(pdf);

    HPDF_REAL top(page->Height() - left), bottom(590.38);

    page->SetLineWidth(1.0);
    page->Rectangle(left, top - 22.6, right - left, 22.2);
  //  page->Rectangle(left, bottom, right - left, 113.16);

    page->SetFontAndSize(fontBD, 10);
    page->TextOut(left + 25.09, top - 11.2,  "4. Вариант страхового возмещения / прямого возмещения убытков");

    page->y = top - 22.6 - line_step;
    page->x = left + 6.12;

    page->SetFontAndSize(font, 10);
    page->TextOut("4.1 Прошу осуществить страховое возмещение / прямое возмещение убытков ");
    page->SetFontAndSize(fontI, 8);
    page->TextOut("(нужное подчеркнуть)");

    page->y -= line_step;
    page->x = left + 6.12;

    page->SetFontAndSize(font, 10);
    page->TextOut("по договору обязательного страхования гражданской ответственности владельцев транспортных");

    page->y -= line_step;
    page->x = left + 6.12;

    page->TextOut("средств серия ");
    page->TextBoxOut(UTF8_to_CP1251(culprit->polis_series), page->x, page->y, 3, " ");
    page->TextOut(" №");
    page->TextBoxOut(culprit->policy_number, page->x, page->y, 10, " ");
    page->TextOut("**, выданному страховой организацией");

    page->y -= line_step;
    page->x = left + 6.12;

    {
        HPDF_REAL x1 = page->x, y1 = page->y;

        page->TextOut(x1, y1 + 2, UTF8_to_CP1251(culprit->insurance_company).c_str());
        page->y = y1;

        page->TextOut(left + (right - left) / 2 + 2, page->y, ", путем:");

        page->x = left + 6.12;
        page->SetFontAndSize(fontI, 6);
        page->SetLineWidth(0.0);
        page->LineLabel(page->x, page->y, left + (right - left) / 2, "(наименование Страховой компании)");
    }

    page->SetFontAndSize(font, 10);
    page->y -= line_step;
    page->x = left + 6.12;
    page->TextBoxOut(" ", page->x, page->y);
    page->TextOut(" Организации и оплаты восстановительного ремонта поврежденного транспортного средства на станции");

    page->y -= line_step;
    page->x = left + 6.12;
    page->TextOut("технического обслуживания, ");
    page->SetFontAndSize(fontBD, 10);
    page->TextOut("выбранной из предложенного страховщиком перечня:");

    page->SetFontAndSize(font, 10);
    page->y -= line_step;
    page->x = left + 6.12;
    
    page->TextOut(left + (right - left) / 2 + 2, page->y, ", по адресу");
    page->Line(page->x + 2, page->y, right - 6.12, page->y);
    page->SetFontAndSize(fontI, 6);
    page->LineLabel(left + 6.12, page->y, left + (right - left) / 2, "(наименование СТОА) ");
    
    page->y -= line_step;
    page->x = left + 6.12;
    page->Line(page->x, page->y, right - 6.12, page->y);

    page->SetFontAndSize(font, 10);
    page->y -= line_step;
    page->x = left + 6.12;
    page->TextOut("О дате передачи отремонтированного транспортного средства прошу проинформировать меня следующим");
    page->y -= line_step;
    page->x = left + 6.12;
    page->TextOut("способом (по телефону, почте, электронной почте) ");
    page->SetFontAndSize(fontI, 6);
    page->TextOut("(нужное подчеркнуть)");
    page->Line(page->x, page->y, right - 6.12, page->y);
    page->y -= line_step;
    page->x = left + 6.12;
    page->Line(page->x, page->y, right - 6.12, page->y);

    page->y -= line_step;
    page->x = left + 6.12;
    page->SetFontAndSize(fontBD, 10);
    page->TextOut("ИЛИ");

    page->SetFontAndSize(font, 10);
    page->y -= line_step;
    page->x = left + 6.12;
    page->TextBoxOut(" ", page->x, page->y);
    page->TextOut(" Путем оплаты стоимости восстановительного ремонта поврежденного транспортного средства станции");

    page->y -= line_step;
    page->x = left + 6.12;
    page->TextOut("технического обслуживания:");

    page->y -= line_step;
    page->x = left + 6.12;
    page->TextOut("Полное наименование");
    page->Line(page->x, page->y, right - 6.12, page->y);

    page->y -= line_step;
    page->x = left + 6.12;
    page->TextOut("Адрес");
    page->Line(page->x, page->y, right - 6.12, page->y);

    page->y -= line_step;
    page->x = left + 6.12;
    page->TextOut("Платежные реквизиты:");

    {
     page->y -= line_step;
     page->x = left + 6.12;
     page->TextOut("Банк получателя");
     HPDF_REAL x1 = page->x, y1 = page->y;
     page->Line(x1, y1, right - 6.12, y1);
    }

    {
    page->y -= line_step;
    page->x = left + 6.12;
    page->TextOut("Счет получателя ");
    page->TextBoxOut("", page->x, page->y, 20);
    }

    page->y -= line_step;
    page->x = left + 6.12;
    page->TextOut("Корреспондентский счет ");
    page->TextBoxOut(" ", page->x, page->y, 20);

    page->y -= line_step;
    page->x = left + 6.12;
    page->TextOut("БИК ");
    page->TextBoxOut(" ", page->x, page->y, 9);
    page->TextOut(", ИНН ");
    page->TextBoxOut(" ", page->x, page->y, 10);

    page->y -= line_step;
    page->x = left + 6.12;
    page->TextOut("Указание станции технического обслуживания не из предложенного страховщиком перечня возможно только в");
    page->y -= line_step;
    page->x = left + 6.12;
    page->TextOut("отношении легковых автомобилей, находящихся в собственности граждан и зарегистрированных в Российской");
    page->y -= line_step;
    page->x = left + 6.12;
    page->TextOut("Федерации, и при наличии согласия страховщика в письменной форме.");

    page->y -= line_step;
    page->x = left + 6.12;
    page->SetFontAndSize(fontBD, 10);
    page->TextOut("4.2 Прошу осуществить страховую выплату в размере, определенном в соответствии с");
    page->y -= line_step;
    page->x = left + 6.12;
    page->TextOut("Федеральным законом от 25 апреля 2002 года № 40-ФЗ «ОБ обязательном страховании");
    page->y -= line_step;
    page->x = left + 6.12;
    page->TextOut("гражданской ответственности владельцев транспортных средств»:");

    page->y -= line_step;
    page->x = left + 6.12;
    page->SetFontAndSize(font, 10);
    page->TextBoxOut(" ", page->x, page->y);
    page->TextOut(" - наличными");

    page->y -= line_step;
    page->x = left + 6.12;
    page->SetFontAndSize(fontBD, 10);
    page->TextOut("ИЛИ");

    page->y -= line_step;
    page->x = left + 6.12;
    page->SetFontAndSize(font, 10);
    page->TextBoxOut(" ", page->x, page->y);
    page->TextOut(" - перечислить безналичным расчетом по следующим реквизитам:");

    {
    page->y -= line_step * 1.5;
    page->x = left + 6.12;
    page->TextOut("Получатель (Ф.И.О) ");
    HPDF_REAL x1 = page->x, y1 = page->y;
   // std::stringstream stm; stm << dBeneficiary->last_name << " "<< dBeneficiary->first_name << " " << dBeneficiary->middle_name; 
   // page->TextOut(page->x + 2, page->y + 2, UTF8_to_CP1251(stm.str()).c_str());
    page->Line(x1, y1, right - 6.12, y1);
    }

    page->y -= line_step * 1.5;
    page->x = left + 6.12;
    page->TextOut("Расч. счет получателя ");
    page->TextBoxOut(/*paymentDetails != NULL ? UTF8_to_CP1251(paymentDetails->checking_account):*/ "", page->x, page->y, 20);

    {
    page->y -= line_step * 1.5;
    page->x = left + 6.12;
    page->TextOut("Наименование банка ");
    HPDF_REAL x1 = page->x, y1 = page->y;
  // if (paymentDetails != NULL) page->TextOut(page->x + 2, page->y + 2, UTF8_to_CP1251(paymentDetails->bank).c_str());
    page->Line(x1, y1, right - 6.12, y1);
    }

    {
    page->y -= line_step * 1.5;
    page->x = left + 6.12;
    page->TextOut("Филиал/отделение ");
    HPDF_REAL x1 = page->x, y1 = page->y;
  //  if (paymentDetails != NULL) page->TextOut(page->x + 2, page->y + 2, UTF8_to_CP1251(paymentDetails->department).c_str());
    page->Line(x1, y1, right - 6.12, y1);
    }

    page->y -= line_step * 1.5;
    page->x = left + 6.12;
    page->TextOut("Кор. счет ");
   /* if (paymentDetails != NULL) page->TextBoxOut(UTF8_to_CP1251(paymentDetails->correspondent_account), page->x, page->y, 20);
    else*/ page->TextBoxOut(" ", page->x, page->y, 20);

    page->y -= line_step * 1.5;
    page->x = left + 6.12;
    page->TextOut("БИК ");
  /*  if (paymentDetails != NULL) page->TextBoxOut(UTF8_to_CP1251(paymentDetails->bic), page->x, page->y, 9);
    else*/ page->TextBoxOut(" ", page->x, page->y, 9);

    page->y -= line_step * 1.5;
    page->x = left + 6.12;
    page->TextOut("ИНН ");
  /*  if (paymentDetails != NULL) page->TextBoxOut(UTF8_to_CP1251(paymentDetails->itn), page->x, page->y, 10);
    else*/ page->TextBoxOut(" ", page->x, page->y, 10);

    page->y -= line_step * 1.5;
    page->x = left + 6.12;
    page->TextOut("Номер карты ");
    for(int i = 0; i < 16; i += 4)
    {
     /*  if (paymentDetails != NULL && paymentDetails->card.length() > i) page->TextBoxOut(UTF8_to_CP1251(paymentDetails->card.substr(i, 4)), page->x, page->y, 4);
       else*/ page->TextBoxOut(" ", page->x, page->y, 4);
       page->x += 5;
    }

    page->y -= line_step * 1.5;
    page->x = left + 6.12;
    page->SetFontAndSize(font, 8);
    page->TextOut("Пункт 4.2. заполняется при осуществлении страховой выплаты в случае причинения вреда жизни или здоровью потерпевшего, а");

    page->y -= line_step  * 0.75;
    page->x = left + 6.12;
    page->TextOut("также при наличии условий, предусмотренных пунктом 16");
    page->SetFontAndSize(font, 5);
    page->y += line_step/3;
    page->TextOut("1");
    page->SetFontAndSize(font, 8);
    page->y -= line_step/3;
    page->TextOut(" статьи 12 Федерального закона от 25 апреля 2002 года №40—ФЗ «Об");

    page->y -= line_step  * 0.75;
    page->x = left + 6.12;
    page->TextOut("обязательном страховании гражданской ответственности владельцев транспортных средств».");

    page->y -= line_step / 2;
    page->SetLineWidth(1.0);
    page->Rectangle(left, page->y, right - left, top - page->y);

    page->y -= line_step/2;
    page->x = left + 6.12;

    drawPageBottom(page, page->y);
}

void TPDFStatement::renderPage3()
{
    TPDFPage * page = new TPDFPage(pdf);

    HPDF_REAL top(page->Height() - left), bottom(590.38);
    HPDF_REAL x2(left + (right - left) / 3 * 2),
              x3(left + (right - left) / 9 * 8);

    page->SetLineWidth(1.0);
    page->Rectangle(left, top - 22.6, right - left, 22.2);

    page->SetFontAndSize(fontBD, 10);
    page->TextOut(left + 25.09, top - 11.2,  "5. К настоящему заявлению прилагаю следующие документы:");

    HPDF_REAL y1 = top - 22.6;

    page->y = y1 - line_step;
    page->x = left + 6.12;

    page->SetFontAndSize(fontBD, 8);
    HPDF_REAL x1 = page->x;
    page->TextOut((x3 - left) / 2, page->y,  "Документ");
    page->TextOut(x3 + 6.12, page->y,  "Кол-во");
    page->TextOut(x3 + 6.12, page->y - line_step / 2,  "листов");

    page->SetFontAndSize(fontBD, 6);
    page->TextOut(x2 + 6.12, y1 - line_step * 2  / 10 * 3,  "Копия (К) / заверенная");
    page->TextOut(x2 + 6.12, y1 - line_step * 2  / 10 * 6,  "копия (ЗК) / оригинал");
    page->TextOut(x2 + 6.12, y1 - line_step * 2  / 10 * 9,  "(О) - указать");

    page->x = left + 6.12;
    page->y = y1 - line_step * 2;

    y1 = page->y;

    page->Rectangle(left, y1, right - left, line_step * 2);
    page->Rectangle(x2, y1, x3 - x2, line_step * 2);

    page->x = left + 6.12;
    page->y = y1 - line_step;

//    y1 = page->y;
//    page->TextOut(page->x, page->y - 3, "Документ, удостоверяющий личность");
//    page->Rectangle(left, y1, right - left, line_step);
//    page->Rectangle(x2, y1, x3 - x2, line_step);

    renderPage3Line(page, y1, "Документ, удостоверяюумент, удостоверяющий личность");
    renderPage3Line(page, page->y, "Документ, удостоверяющий полномочия представителя выгодоприобретателя");
    renderPage3Line(page, page->y, "Банковские реквизиты для перечисления страховой выплаты");
    renderPage3Line(page, page->y, "Документы подтверждающих право собственности на поврежденное имущество");
    renderPage3Line(page, page->y, "Справка о дорожно-транспортном происшествии");
    renderPage3Line(page, page->y, "Водительское удостоверение");
    renderPage3Line(page, page->y, "Извещение о дорожно-транспортном происшествии");
    renderPage3Line(page, page->y, "Протокол об административном правонарушении, постановления по делу об административном правонарушении");
    renderPage3Line(page, page->y, "Определение об отказе в возбуждении дела об административном правонарушении");

    renderPage3Header(page, page->y - line_step, "При причинении вреда имуществу");
    renderPage3Line(page, page->y, "Документы, подтверждающие право собственности на поврежденное имущество либо право на страховую выплату");
    renderPage3Line(page, page->y, "Заключение независимой экспертизы (оценки)");
    renderPage3Line(page, page->y, "Документы, подтверждающие оплату услуг эксперта-техника (оценщика)");
    renderPage3Line(page, page->y, "Документы, подтверждающие оказание и оплату услуг по эвакуации поврежденного имущества");
    renderPage3Line(page, page->y, "Документы, подтверждающие оказание и оплату услуг по хранению поврежденного имущества");

    renderPage3Header(page, page->y - line_step, "При причинении вреда жизни / здоровью");
    renderPage3Line(page, page->y, "Документы, выданные и оформленные медицинской организацией с указанием характера полученных потерпевшим травм и увечий, диагноза и периода нетрудоспособности"); 
    renderPage3Line(page, page->y, "Заключение судебно-медицинской экспертизы о степени утраты общей или профессиональной трудоспособности"); 
    renderPage3Line(page, page->y, "Справка, подтверждающая факт установления инвалидности или категории «ребенок-инвалид»"); 
    renderPage3Line(page, page->y, "Справка станции скорой медицинской помощи об оказанной медицинской помощи на месте дорожно-транспортного происшествия"); 
    renderPage3Line(page, page->y, "Справка или иной документ о среднем месячном заработке (доходе), стипендии, пенсии, пособиях"); 
    renderPage3Line(page, page->y, "Заявление, содержащее сведения о членах семьи умершего потерпевшего"); 
    renderPage3Line(page, page->y, "Копия свидетельства о смерти"); 
    renderPage3Line(page, page->y, "Свидетельство о рождении ребенка (детей)"); 
    renderPage3Line(page, page->y, "Справка образовательной организации"); 
    renderPage3Line(page, page->y, "Заключение (справка медицинской организации, органа социального обеспечения) о необходимости постороннего ухода"); 
    renderPage3Line(page, page->y, "Справка органа социального обеспечения (медицинской организации, органа местного самоуправления, службы занятости) о том, что один из родителей, супруг либо другой член семьи погибшего не работает и занят уходом за его родственниками"); 
    renderPage3Line(page, page->y, "Свидетельство о заключении брака"); 
    renderPage3Line(page, page->y, "Документы, подтверждающие произведенные расходы на погребение"); 
    renderPage3Line(page, page->y, "Выписка из истории болезни"); 
    renderPage3Line(page, page->y, "Документы, подтверждающие оплату услуг медицинской организации"); 
    renderPage3Line(page, page->y, "Документы, подтверждающие оплату приобретенных лекарств"); 

    renderPage3Header(page, page->y - line_step, "Иные документы");

    for(int i = 0; i < 7; i++)
    {
     renderPage3Line(page, page->y, "");
    }

    page->y -= line_step/2;
    page->x = left + 6.12;

    drawPageBottom(page, page->y);
}

void TPDFStatement::renderPage3Line(TPDFPage * page, HPDF_REAL top, const std::string & text)
{
    HPDF_STATUS status;
    HPDF_REAL x2(left + (right - left) / 3 * 2),
              x3(left + (right - left) / 9 * 8);
    page->SetFontAndSize(font, 8);

    int c(page->LineCount(text, x2 - left - 6.12 - 6.12));

    HPDF_REAL bottom;
    if (c == 1) bottom = top - c * line_step * 0.95;
    else bottom = top - c * line_step * 0.85;

    status = page->TextRect(left + 6.12, top, x2 - 6.12, bottom, text.c_str(), HPDF_TALIGN_LEFT);
//    page->TextOut(left + 6.12, top + 3, text.c_str());
    page->Rectangle(left, bottom, right - left, top - bottom);
    page->Rectangle(x2, bottom, x3 - x2, top - bottom);
    page->y = bottom;
}

void TPDFStatement::renderPage3Header(TPDFPage * page, HPDF_REAL top, const std::string & text)
{
    HPDF_REAL text_width(page->TextWidth(text));

    page->SetFontAndSize(fontBD, 8);

    page->TextOut(left + (right - left - text_width)/2, top + 3, text.c_str());
    page->Rectangle(left, top, right - left, line_step);
    page->y = top;
}

void TPDFStatement::drawDriver(TPDFPage * page, HPDF_REAL top, TDriver * d)
{
    top += 42; // TODO убрать эту дельту
    page->SetFontAndSize(fontI, 6);
    page->TextOut(left + 15.52, top - 60.64, "(дата рождения физического лица)");
    page->TextOut(left + 194.85, top - 60.64, "(ИНН юридического лица)");
    {
        page->SetFontAndSize(font, 9);
        page->DateBoxOut(d->date_birth, left + 6.12, top - 53.6);
    }

    {
        page->SetFontAndSize(font, 9);
        std::string itn(entity->itn); boost::trim(itn);
        if (itn.length() > 10) itn = itn.substr(0, 10);
        else  for(int i = itn.length(); i < 10; i++) itn = std::string(" ") + itn;
        page->TextBoxOut(itn, left + 180.0, page->y);
    }

    if (!d->p_series.empty() || !d->p_number.empty())
    {
        page->TextOut(left + 290.12, top - 70.00, "паспорт");
        page->TextOut(right - 181.0, top - 70.00, UTF8_to_CP1251(d->p_series).c_str());
        page->TextOut(right - 71.0, top - 70.00, UTF8_to_CP1251(d->p_number).c_str());
    }

    page->Line(left + 6.12, top - 71.97, right - 193.0, top - 71.97, 0.0);
    page->Line(right - 183.0, top - 71.97, right - 83.0, top - 71.97, 0.0);
    page->Line(right - 73.0, top - 71.97, right - 6.12, top - 71.97, 0.0);

    page->SetFontAndSize(fontI, 6);
    page->TextOut(left + 7.12, top - 79.36, "(свидетельство о регистрации юридического лица либо документ, удостоверяющий личность физического лица)");
    page->TextOut(right - 163.69, top - 79.36, "(серия)");
    page->TextOut(right - 71.54, top - 79.36, "(номер)");

    page->LineLabel(left + 33.91, top - 91.05, left + 107.2, "(индекс)");
    page->LineLabel(left + 117.2, top - 91.05, left + (right - left) / 3 * 2, "(государство, республика, край, область)");
    page->LineLabel(left + (right - left) / 3 * 2 + 10, top - 91.05, right - 6.12, "(район)");

    {
        page->LineLabel(left + 6.12, top - 111.4, left + (right - left) / 3, "(населенный пункт)");
        page->LineLabel(left + (right - left) / 3 + 10, top - 111.4, left + (right - left) / 3 * 2, "(улица)");

        page->LineLabel(left + (right - left) / 3 * 2 + 10, top - 111.4, left + (right - left) / 3 * 2 + 10 + (right - left) / 9, "(дом)");
        page->LineLabel(left + (right - left) / 3 * 2 + 20 + (right - left) / 9, top - 111.4, left + (right - left) / 3 * 2 + 20 + (right - left) / 9 * 2, "(корпус)");
        page->LineLabel(left + (right - left) / 3 * 2 + 30 + (right - left) / 9 * 2, top - 111.4, right - 6.12, "(квартира)");
    }

    page->SetFontAndSize(font, 9);
    page->TextOut(left + 35.91, top - 89.05, UTF8_to_CP1251(d->postcode).c_str());
    page->TextOut(left + 119.2, top - 89.05, UTF8_to_CP1251(d->addr_field1).c_str());
    page->TextOut(left + (right - left) / 3 * 2 + 12, top - 89.05, UTF8_to_CP1251(d->addr_field2).c_str());

    page->TextOut(left + 8.12, top - 109.4, UTF8_to_CP1251(d->addr_field3).c_str());
    page->TextOut(left + (right - left) / 3 + 12, top - 109.4, UTF8_to_CP1251(d->addr_field4).c_str());
    page->TextOut(left + (right - left) / 3 * 2 + 12, top - 109.4, std::to_string(d->house).c_str());
    page->TextOut(left + (right - left) / 3 * 2 + 22 + (right - left) / 9, top - 109.4, UTF8_to_CP1251(d->addr_field5).c_str());
    page->TextOut(left + (right - left) / 3 * 2 + 32 + (right - left) / 9 * 2, top - 109.4, std::to_string(d->apartment).c_str());

    page->SetFontAndSize(font, 8);
    page->TextOut(left + 7.12, top - 91.05, "Адрес");
    page->TextOut(left + 7.12, top - 130.81, "Телефон");

    page->y -= 3;
    page->x += 3;

    page->SetFontAndSize(font, 9);
    {
        std::string phone(UTF8_to_CP1251(d->phone)); boost::trim(phone);
        if (phone.length() > 12) phone = phone.substr(0, 12);
        else for(int i = phone.length(); i < 12; i++) phone = std::string(" ") + phone;
        page->TextBoxOut(phone, page->x, page->y);
    }


}

void TPDFStatement::drawPageBottom(TPDFPage * page, HPDF_REAL top)
{
    //left(41.4), right(579.48),
    //top = bottom;   // 590.38
    HPDF_REAL bottom = top - 59.01;

    page->SetFontAndSize(fontBD, 8);
    page->y = top - 11.2; 
    page->x = left + 6.12;
    page->TextOut("Потерпевший (выгодоприобретатель,");
    page->TextOut(left + (right - left)/2 + 6.12, page->y, "Страховщик (представитель страховщика)");

    page->y -= line_step / 3 * 2; 
    page->x = left + 6.12;

    page->TextOut("представитель выгодоприобретателя)");

    page->y -= line_step; 
    page->x = left + 6.12;

    HPDF_REAL y1 = page->y;

    page->SetFontAndSize(font, 9);
    std::stringstream stm;
    stm << dBeneficiary->last_name << " " << dBeneficiary->first_name << " " << dBeneficiary->middle_name;
    page->TextOut(page->x, page->y + 1, UTF8_to_CP1251(stm.str()).c_str());

    page->y = y1;
    page->x = left + 6.12;

    page->SetFontAndSize(fontI, 6);
    page->SetLineWidth(0);
    page->LineLabel(page->x, page->y, left + (right - left)/2/5*4 - 10, "(Ф.И.О.)");
    page->LineLabel(left + (right - left)/2/5*4, y1, left + (right - left)/2 - 6.12, "(Подпись)");

    page->Line(left + (right - left)/2 + 6.12, y1, right - 6.12, y1);

    page->y -= line_step; 

    y1 = page->y;
    page->LineLabel(left + 6.12, y1, left + (right - left)/2 - 6.12, "(дата заполнения заявления)");
    page->LineLabel(left + (right - left)/2 + 6.12, y1, right - 6.12, "(дата заполнения заявления)");

    page->y -= line_step / 3;

    page->SetLineWidth(1.0);
    page->Rectangle(left, page->y, (right - left) / 2, top - page->y);
    page->Rectangle(left + (right - left) / 2, page->y, (right - left) / 2, top - page->y);
}
//--------------------------------------------------------------------------
//--------------------------------------------------------------------------

void TPDFOrder::render()
{
    left = 41.4; right = 579.48; line_step = 13.68;

    renderPage1();
}

void TPDFOrder::renderPage1()
{
    TPDFPage * page = new TPDFPage(pdf);

    HPDF_REAL top(page->Height() - left), bottom(590.38);

    page->SetFontAndSize(fontBD, 12);

    {
        HPDF_REAL y1 = top, x1 = left + 6.12;
        std::stringstream stm;
        stm << "В экспертную компанию " << UTF8_to_CP1251(dl->DIRLineName(basicData->firm));
        page->TextRect(x1, y1 - line_step * 2, right - 6.12, y1, stm.str().c_str(), HPDF_TALIGN_CENTER);

        page->y = y1 - line_step * 4;
        page->x = left + 6.12;
    }
    {
       HPDF_REAL y1 = page->y;
       page->TextOut(left + (right - left)/2 - page->TextWidth("НАПРАВЛЕНИЕ №"), y1, "НАПРАВЛЕНИЕ №");
       page->TextOut(UTF8_to_CP1251(vVictim->registration_number).c_str());
       page->y = y1 - line_step;
       page->x = left + 6.12;
    }

    page->SetFontAndSize(fontI, 10);
    {
        HPDF_REAL y1 = page->y, x1 = left + 6.12;
        page->TextRect(x1 + 6.12, y1 + line_step, right - 6.12, y1 - line_step * 2, "на осмотр, дефектовку скрытых повреждений в условиях СТО и проведение независимой технической экспертизы",
            HPDF_TALIGN_CENTER);
        page->y = y1 - line_step * 3;
        page->x = left + 6.12;
    }
    page->SetFontAndSize(font, 12);
    page->SetLineWidth(0.0);
    {
      page->TextOut("№ Заявления:"); 
      HPDF_REAL y1 = page->y, x1 = page->x;
      page->TextOut(x1 + 2, y1 + 2, UTF8_to_CP1251(basicData->number).c_str()); 
      page->Line(x1, y1, right - 6.12, y1);
    
      page->y = y1 - line_step;
      page->x = left + 6.12;
    }
    {
        page->TextOut("Заказчик:"); 
        HPDF_REAL y1 = page->y, x1 = page->x;
        page->TextOut(x1 + 2, y1 + 2, "АО Страховая Компания «Гайде»"); 
        page->Line(x1, y1, right - 6.12, y1);

        page->y = y1 - line_step;
        page->x = left + 6.12;
    }
    {
        page->TextOut("Марка, модель:"); 
        HPDF_REAL y1 = page->y, x1 = page->x;
        std::stringstream stm; stm << vVictim->brand << " " << vVictim->model;
        page->TextOut(x1 + 2, y1 + 2, UTF8_to_CP1251(stm.str()).c_str()); 
        page->Line(x1, y1, right - 6.12, y1);

        page->y = y1 - line_step;
        page->x = left + 6.12;
    }
    {
        page->TextOut("Гос. рег. знак:"); 
        HPDF_REAL y1 = page->y, x1 = page->x;
        page->TextOut(x1 + 2, y1 + 2, UTF8_to_CP1251(vVictim->registration_number).c_str()); 
        page->Line(x1, y1, right - 6.12, y1);
        page->y = y1 - line_step;
        page->x = left + 6.12;
    }
    {
        page->TextOut("Собственник ТС (доверенное лицо):"); 
        HPDF_REAL y1 = page->y, x1 = page->x;
        std::stringstream stm; stm << dOwner->last_name << " " << dOwner->first_name << " " << dOwner->middle_name;
        page->TextOut(x1 + 2, y1 + 2, UTF8_to_CP1251(stm.str()).c_str()); 
        page->Line(x1, y1, right - 6.12, y1);
        page->y = y1 - line_step;
        page->x = left + 6.12;
    }
    {
        page->TextOut("Полис:"); 
        HPDF_REAL y1 = page->y, x1 = page->x;
        std::stringstream stm; stm << victim->policy_series << " " << victim->policy_number;
        page->TextOut(x1 + 2, y1 + 2, UTF8_to_CP1251(stm.str()).c_str()); 
        page->Line(x1, y1, left + (right - left)/2 - 10, y1);

        page->TextOut(left + (right - left)/2, y1, "Тип:"); 
        x1 = page->x;
        page->TextOut(x1 + 2, y1 + 2, "ОСАГО");
        page->Line(x1, y1, right - 6.12, y1);

        page->y = y1 - line_step;
        page->x = left + 6.12;
    }

    page->TextOut("Цель экспертизы:"); 
    page->y -= line_step;
    page->x = left + 6.12;

    page->TextBoxOut(" ", page->x, page->y);
    page->TextOut(" Определение стоимости материального ущерба АМТС (составление заключения)");

    page->y -= line_step;
    page->x = left + 6.12;

    {
       page->TextBoxOut(" ", page->x, page->y);
       HPDF_REAL y1 = page->y, x1 = page->x;
       page->TextOut(" В случае когда стоимость восстановительного ремонта АМТС без учета износа равна или");
       page->y = y1 - line_step;
       page->x = x1;
       y1 = page->y;
       page->TextOut(" превышает рыночную стоимость АМТС, определение рыночной стоимости годных");
       page->y = y1 - line_step;
       page->x = x1;
       y1 = page->y;
       page->TextOut(" остатков.");
       page->y = y1 - line_step;
       page->x = left + 6.12;
    }

    page->TextBoxOut(" ", page->x, page->y);
    page->TextOut(" УТС");
    page->y -= line_step;
    page->x = left + 6.12;

    page->TextBoxOut(" ", page->x, page->y);
    page->TextOut(" ТРАСОЛОГИЯ");
    page->y -= line_step;
    page->x = left + 6.12;

    {
        page->TextBoxOut(" ", page->x, page->y);
        HPDF_REAL y1 = page->y, x1 = page->x;
        page->TextOut(" осмотр скрытых повреждений в условиях СТО и проведение независимой технической");
        page->y = y1 - line_step;
        page->x = x1;
        y1 = page->y;
        page->TextOut(" экспертизы");
        page->y = y1 - line_step;
        page->x = left + 6.12;
    }

    page->TextBoxOut(" ", page->x, page->y);
    page->TextOut(" Осмотр АМТС + ФОТОТАБЛИЦА");
    page->y -= line_step * 2;
    page->x = left + 6.12;

    page->TextOut("Условия оплаты:");
    page->y -= line_step;
    page->x = left + 6.12;

    page->TextOut("- АО «СК ГАЙДЕ» (по безналичн. расч.)");
    page->y -= line_step;
    page->x = left + 6.12;

    page->TextOut("Согласованные потерпевшим и страховщиком:");
    page->y -= line_step;
    page->x = left + 6.12;

    page->TextOut("Дата и время осмотра: ");
    page->DateBoxOut(applicationInspection->dt_inspection.date(), page->x, page->y);
    page->TimeBoxOut(applicationInspection->dt_inspection.time(), page->x + 5, page->y);

    page->y -= line_step;
    page->x = left + 6.12;

    {
      page->TextOut("Место осмотра: ");
      page->TextOut(UTF8_to_CP1251(applicationInspection->inspection_address).c_str());
      page->y -= line_step * 2;
      page->x = left + 6.12;
    }

    {
       HPDF_REAL y1 = page->y, x1 = page->x;
       page->TextRect(x1 + 6.12, y1 + line_step, right - 6.12, y1 - line_step * 2, "Транспортное средство должно быть представлено для осмотра в чистом виде, в состоянии, позволяющем визуально обнаружить заявляемые повреждения.",
          HPDF_TALIGN_LEFT);
       page->y = y1 - line_step * 3;
       page->x = left + 6.12;
    }
    {
        HPDF_REAL y1 = page->y, x1 = page->x;
        page->TextRect(x1 + 6.12, y1 + line_step, right - 6.12, y1 - line_step * 7, "При обнаружении скрытых повреждений в процессе эксплуатации или ремонта автомобиля	 на СТО (кроме Автоцентра ГАЙДЕ), потерпевший, согласно п.7 Положения Банка России от 19 сентября 2014 г. № 433-П «О правилах проведения независимой технической экспертизы транспортного средства», обязан письменно известить страховщика о проведении повторного осмотра и повторной экспертизы для получения страховой выплаты.",
            HPDF_TALIGN_LEFT);
        page->y = y1 - line_step * 8;
        page->x = left + 6.12;
    }

    {
        page->TextOut("Представитель АО СК «Гайде»:");
        HPDF_REAL y1 = page->y, x1 = page->x;

        page->SetFontAndSize(fontI, 6);
        page->LineLabel(x1 + 2, y1, x1 + (right - x1)/2 - 10, "(Ф.И.О.)");
        page->LineLabel(x1 + (right - x1)/2, y1, right - 6.12, "(Подпись)");

        page->y = y1 - line_step * 2;
        page->x = left + 6.12;
    }

    {
        page->SetFontAndSize(font, 12);
        page->TextOut("Собственник ТС (доверенное лицо):");
        HPDF_REAL y1 = page->y, x1 = page->x;

        page->TextOut(x1 + 2, y1 + 2, UTF8_to_CP1251(dOwner->fio).c_str()); 

        page->SetFontAndSize(fontI, 6);
        page->LineLabel(x1 + 2, y1, x1 + (right - x1)/2 - 10, "(Ф.И.О.)");
        page->LineLabel(x1 + (right - x1)/2, y1, right - 6.12, "(Подпись)");

        page->y = y1 - line_step * 2;
        page->x = left + 6.12;
    }

    {
        page->SetFontAndSize(font, 12);
        page->TextOut("Направление выдано: ");
        HPDF_REAL y1 = page->y, x1 = page->x;

        page->SetFontAndSize(font, 10);
        page->DateBoxOut(applicationInspection->dt_application.date(), page->x + 5, page->y);
    }

}

//--------------------------------------------------------------------------
//--------------------------------------------------------------------------
HPDF_STATUS TPDFPage::CharBoxOut(const std::string& s, HPDF_REAL left, HPDF_REAL bottom)
{
    HPDF_STATUS status;
    HPDF_REAL l = line_width;

    HPDF_REAL w = 12.0;

    x = left;
    y = bottom;

    SetLineWidth(0.1);

    Rectangle(x, y, w, w);

    HPDF_REAL text_width = HPDF_Page_TextWidth(page, s.substr(0, 1).c_str());
    status = TextOut(x + (w - text_width) / 2, y + w/12, s.substr(0, 1).c_str());

    x = left + w;
    y = bottom;

    SetLineWidth(l);

    return(status);
}
HPDF_STATUS TPDFPage::TextBoxOut(const std::string& text, HPDF_REAL left, HPDF_REAL bottom)
{
    HPDF_STATUS status = HPDF_OK;
    HPDF_REAL s = 0.5;
    x = left;
    y = bottom;
    for(int i = 0; i < text.length() && status == HPDF_OK; i++)
    {       
        status = CharBoxOut(text.substr(i, 1), x, y);
        x += s;
        y = bottom;
    }
    return(status);
}

HPDF_STATUS TPDFPage::TextBoxOut(const std::string& s, HPDF_REAL left, HPDF_REAL bottom, const int length, const std::string& fill)
{
    std::string out(s);
    if (out.length() > length) out = out.substr(0, length);
    else for(int i = out.length(); i < length; i++) out = fill + out;
    return (TextBoxOut(out, left, bottom));
}

HPDF_STATUS TPDFPage::TextRect(HPDF_REAL left, HPDF_REAL top, HPDF_REAL right, HPDF_REAL bottom, const char *text, HPDF_TextAlignment align)
{
    HPDF_STATUS status;
    x = right;
    y = bottom;
    HPDF_Page_BeginText(page);
    status = HPDF_Page_TextRect(page, left, top, right, bottom, text, align, NULL);
    HPDF_Page_EndText(page);
    return(status);
}

HPDF_STATUS TPDFPage::DateBoxOut(const Wt::WDate& date, HPDF_REAL left, HPDF_REAL top)
{
    HPDF_STATUS status = HPDF_OK;
        std::stringstream day; 
        if (date.isValid() && !date.isNull()) day << std::setw(2) << std::setfill('0') << date.day();
        else day << "  ";
        TextBoxOut(day.str(), left, top);

        TextOut(".");

        std::stringstream month; 
        if (date.isValid() && !date.isNull()) month << std::setw(2) << std::setfill('0') << date.month();
        else month << "  ";
        TextBoxOut(month.str(), x, y);

        TextOut(".");

        std::stringstream year; 
        if (date.isValid() && !date.isNull()) year << std::setw(4) << std::setfill('0') << date.year();
        else year << "    ";
        status = TextBoxOut(year.str(), x, y);

        return(status);
}

HPDF_STATUS TPDFPage::TimeBoxOut(const Wt::WTime& time, HPDF_REAL left, HPDF_REAL top)
{
    HPDF_STATUS status = HPDF_OK;
    std::stringstream hour; 
    if (time.isValid() && !time.isNull()) hour << std::setw(2) << std::setfill('0') << time.hour();
    else hour << "  ";
    TextBoxOut(hour.str(), left, top);

    TextOut("ч.");

    x += 2;
    std::stringstream min; 
    if (time.isValid() && !time.isNull()) min << std::setw(2) << std::setfill('0') << time.minute();
    else min << "  ";
    status = TextBoxOut(min.str(), x, y);

    TextOut("мин.");

    return(status);
}

//--------------------------------------------------------------------------
//--------------------------------------------------------------------------
TPDFPage::TPDFPage(HPDF_Doc pdf)
{
    page = HPDF_AddPage(pdf);
    initPage();
}

void TPDFPage::initPage()
{
    HPDF_Page_SetSize(page, HPDF_PAGE_SIZE_A4, HPDF_PAGE_PORTRAIT);
    HPDF_Page_SetTextRenderingMode(page, HPDF_FILL);

    HPDF_Page_SetRGBFill(page, 0, 0, 0);

    HPDF_Page_SetWidth(page, 595.32007);
    HPDF_Page_SetHeight(page, 841.919983);

    HPDF_Page_SetHorizontalScalling(page, 99);

    x = y = 0;
}

HPDF_STATUS TPDFPage::SetFontAndSize(HPDF_Font  font, HPDF_REAL  size)
{
    return (HPDF_Page_SetFontAndSize(page, font, size));
}

HPDF_STATUS TPDFPage::TextOut(HPDF_REAL xpos, HPDF_REAL ypos, const char *text)
{
    x = xpos;
    y = ypos;

    return(TextOut(text));
}

HPDF_STATUS TPDFPage::TextOut(const char *text)
{
    HPDF_STATUS status;
    HPDF_Page_BeginText(page);
    status = HPDF_Page_TextOut(page, x, y, text);
    HPDF_Page_EndText(page);

    x += HPDF_Page_TextWidth(page, text);

    return(status);
}

HPDF_STATUS TPDFPage::TextOut(const char *text, const int top, const int align)
{
    HPDF_REAL page_width = HPDF_Page_GetWidth(page);
    HPDF_REAL text_width = HPDF_Page_TextWidth(page, text);

    y = top;
    if (align == 0) x = (page_width - text_width) / 2;

    return(TextOut(text));
}

HPDF_STATUS TPDFPage::SetLineWidth(HPDF_REAL p_line_width)
{
 line_width = p_line_width;
 return(HPDF_Page_SetLineWidth (page, line_width));
}

HPDF_STATUS TPDFPage::Rectangle(HPDF_REAL x, HPDF_REAL y, HPDF_REAL width, HPDF_REAL height)
{
  HPDF_STATUS status;
  
 // HPDF_Page_SetLineWidth (page, 1.0);
 // HPDF_Page_MoveTo(page, x, y);
  status = HPDF_Page_Rectangle(page, x, y, width, height);

  HPDF_Page_Stroke(page);
  return(status);
}

HPDF_STATUS TPDFPage::Line(HPDF_REAL x1, HPDF_REAL y1, HPDF_REAL x2, HPDF_REAL y2)
{
    HPDF_STATUS status;
    HPDF_Page_MoveTo(page, x1, y1);
    status = HPDF_Page_LineTo(page, x2, y2);
    HPDF_Page_Stroke(page);
    return(status);
}

HPDF_STATUS TPDFPage::Line(HPDF_REAL x1, HPDF_REAL y1, HPDF_REAL x2, HPDF_REAL y2, HPDF_REAL p_line_width)
{
    HPDF_STATUS status;
    HPDF_REAL l = line_width;
    SetLineWidth(p_line_width);
    status = Line(x1, y1, x2, y2);
    SetLineWidth(line_width);
    return(status);
}

HPDF_STATUS TPDFPage::LineLabel(HPDF_REAL x1, HPDF_REAL y1, HPDF_REAL x2, const char *text)
{
    HPDF_REAL text_width = HPDF_Page_TextWidth(page, text);
    Line(x1, y1, x2, y1);
    y = y1 - 7;
    x =  x1 + (x2 - x1 - text_width) / 2;

    return(TextOut(text));
}

HPDF_REAL TPDFPage::LineCount(const std::string& text, HPDF_REAL max_width) const
{
    int ret(0);
    std::string s(text);
    boost::trim(s);
        
    size_t start = 0, p0 = 0;
    for(size_t pos = 0; pos < s.length(); pos++)
    {
        size_t off = s.find(" ", pos);
        if (off == string::npos)
        {
            off = s.length() - 1;
        }
        pos = off;
        HPDF_REAL text_width = HPDF_Page_TextWidth(page, s.substr(start, pos - start).c_str());
        if (text_width <= max_width)
        {
            p0 = pos;
            pos = off + 1;
            continue;
        }
        else
        {
         ret++;
         start = p0;
         pos = off + 1;
        }
        
    }
    ret++;

   return(ret);
}
//--------------------------------------------------------------------------
//--------------------------------------------------------------------------
//--------------------------------------------------------------------------
//--------------------------------------------------------------------------
//--------------------------------------------------------------------------
//--------------------------------------------------------------------------
//--------------------------------------------------------------------------
//--------------------------------------------------------------------------
//--------------------------------------------------------------------------
//--------------------------------------------------------------------------
//--------------------------------------------------------------------------
//--------------------------------------------------------------------------
//--------------------------------------------------------------------------
