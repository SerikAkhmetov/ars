#ifndef PARTNERBRWWINDOW_H
#define PARTNERBRWWINDOW_H

#include <Wt/WWidget>
#include <Wt/WContainerWidget>
#include <Wt/WVBoxLayout>
#include <Wt/WHBoxLayout>
#include <Wt/WDialog>
#include <Wt/WLineEdit>
#include <Wt/WCheckBox>
#include <Wt/WTableView>
#include <Wt/WItemDelegate>
#include <Wt/WComboBox>

#include "uDataLayer.h"
#include "widget.h"

class TpartnerBrw : public Twindow
{
 public:
    TpartnerBrw(Wt::WContainerWidget *parent, TDataLayer * dl);
    //virtual ~TpartnerBrw();

    void setupUi();
    void refresh();

    //void onCityChanged(const int);
private:
    Wt::WPushButton *bNew, *bEdit, *bDelete;
    Wt::WTableView * tableView; WTableViewPage * tvPage; Wt::WStandardItemModel * mt;

    void refresh(const int);
    void redraw(const int);
    void onEdit();
    void onNew();
    void onDelete();
    void onSelectionChanged();
};

#endif