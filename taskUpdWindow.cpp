#include <Wt/WStandardItem>
#include "userBrwWindow.h"
#include "taskUpdWindow.h"

//--------------------------------------------------------------------------
//--------------------------------------------------------------------------
TtaskUpd::TtaskUpd(Wt::WContainerWidget *parent, TDataLayer * p_dl, const int p_task_id) : Twindow(p_dl, "TtaskUpd", parent), TTask(p_dl), startComment(0)
{
 dl = p_dl;
 task_id = p_task_id;

 if (task_id > 0)
 {
  read();
 }
 refresh();
}
//--------------------------------------------------------------------------
//TtaskUpd::~TtaskUpd()
//{
//}
//--------------------------------------------------------------------------
void TtaskUpd::setupUi()
{
 tMain = new Wt::WTable(this); tMain->setObjectName("tMain");

 int r(0);

 Wt::WText * l = new Wt::WText(L"��������� ����:", tMain->elementAt(r, 0));
 eNumber = new Wt::WLineEdit(tMain->elementAt(r, 1)); eNumber->setObjectName("eNumber");
 eNumber->textInput().connect(this, &TtaskUpd::onTextInput);
 eNumber->setEnabled(false);
 r++;

 l = new Wt::WText(L"���������:", tMain->elementAt(r, 0));
 eInitiator = new Wt::WLineEdit(tMain->elementAt(r, 1)); eInitiator->setObjectName("eInitiator");
 eInitiator->textInput().connect(this, &TtaskUpd::onTextInput);
 eInitiator->setEnabled(false);

 l = new Wt::WText(L"�����������:", tMain->elementAt(r, 2));
 eExecutor = new TLineEdit(tMain->elementAt(r, 3)); eExecutor->setObjectName("eExecutor");
// eExecutor->textInput().connect(this, &TtaskUpd::onTextInput);
 eExecutor->clicked.connect(this, &TtaskUpd::onExecutorSelect);
 eExecutor->setEnabled(true);
 r++;

 l = new Wt::WText(L"��� ������:", tMain->elementAt(r, 0));
 cbTypeTask = new TComboBoxDirLine(dl, 22, tMain->elementAt(r, 1)); cbTypeTask->setObjectName("cbTypeTask");
 cbTypeTask->setNoSelectionEnabled(true);
 cbTypeTask->valueChanged.connect(this, &TtaskUpd::onTextInput);
 r++;

 l = new Wt::WText(L"����:", tMain->elementAt(r, 0));
 deExecuteBefore = new Wt::WDateEdit(tMain->elementAt(r, 1)); deExecuteBefore->setObjectName("deExecuteBefore");
 deExecuteBefore->textInput().connect(this, &TtaskUpd::onTextInput);
 deExecuteBefore->setEnabled(true);
 r++;

 l = new Wt::WText(L"������:", tMain->elementAt(r, 0));
 tMain->elementAt(r, 1)->setColumnSpan(tMain->columnCount() - 1);
 //Wt::WVBoxLayout * lta = new Wt::WVBoxLayout();
 //lta->setContentsMargins(0, 2, 0, 1);
 //tMain->elementAt(r, 1)->setLayout(lta);
 teTask = new Wt::WTextArea(tMain->elementAt(r, 1)); teTask->setObjectName("teTask");
 //tMain->elementAt(r, 1)->setColumnSpan(3); // �� ������������
 //lta->addWidget(teTask, 1);
 //teTask->setWidth(tMain->elementAt(r - 1, 1)->width().toPixels() + tMain->elementAt(r - 1, 2)->width().toPixels() + tMain->elementAt(r - 1, 3)->width().toPixels());
 teTask->textInput().connect(this, &TtaskUpd::onTextInput);
 teTask->setEnabled(true);
 r++;

 TOkFooter * footer = new TOkFooter(tMain->elementAt(r, 0));
 footer->bCancel->clicked().connect(this, &TtaskUpd::onbCancel);
 bOk = footer->bOk;
 bOk->clicked().connect(this, &TtaskUpd::onbOk);
 tMain->elementAt(r, 0)->setColumnSpan(tMain->columnCount());
 r++;

 Wt::WPanel * p0 = new Wt::WPanel(); p0->setObjectName("p0");
 p0->setTitle(Wt::WString(L"������ ������"));
 p0->setTitleBar(true);
 p0->titleBarWidget()->addStyleClass("nav-header");
 //p0->setLayoutSizeAware(true);
 p0->setWidth(Wt::WLength(400));  // �������: ��� ����� ������ ����� �����, � ������� � ������ �������� ���� �� �����
 tMain->elementAt(0, 4)->addWidget(p0);

 lmain = new Wt::WGridLayout();

 //tState = new Wt::WTable(); tState->setObjectName("tState");

 Wt::WVBoxLayout * lState = new Wt::WVBoxLayout();
 lState->setContentsMargins(2, 2, 2, 2);
 lState->addItem(lmain);

 Wt::WContainerWidget * cStat = new Wt::WContainerWidget();
 cStat->setLayout(lState);

 p0->setCentralWidget(cStat);
 

 tMain->elementAt(0, 4)->setRowSpan(r);

 bState3 = new Wt::WPushButton(Wt::WString(L"����� � ������")); bState3->setObjectName("bState3");
 bState3->setEnabled(false);
 bState3->clicked().connect(this, &TtaskUpd::onbState3);
 //bState3->setIcon(Wt::WLink("image/Apply.png"));
 bState3->addStyleClass("btn-warning");
 //bState3->addStyleClass("icon-wrench");

 bState4 = new Wt::WPushButton(Wt::WString(L"���������")); bState4->setObjectName("bState4");
 bState4->setEnabled(false);
 bState4->clicked().connect(this, &TtaskUpd::onbState4);
 //bState3->setIcon(Wt::WLink("image/Apply.png"));
 bState4->addStyleClass("btn-success");

 Wt::WHBoxLayout * lh1 = new Wt::WHBoxLayout();
 lh1->addWidget(bState3, 0);
 lh1->addWidget(bState4, 0);
 lh1->setContentsMargins(1, 1, 1, 1);
 lh1->addStretch(1);
 lState->addLayout(lh1, 1);

 startComment = r;

 if (task_id > 0)
 {refreshComment();}

 setTablePadding(tMain, 2);
}
//--------------------------------------------------------------------------
void TtaskUpd::refresh()
{
  Twindow::refresh();

  if (task_id > 0 && currentState() < 2 && dl->SYSGetUser() == executor)
  {
      setState(2);
      read();
  }

  if (executor > 0)
  {
      TUser user(dl);
      user.user_id = executor;
      user.read();
      eExecutor->setText(Wt::WString::fromUTF8(user.fio));
  }

  if(ic_id > 0)
  {
   TBasicData bd(dl);
   bd.ic_id = ic_id;
   bd.read();
   eNumber->setText(Wt::WString::fromUTF8(bd.number));
  }

  if (initiator > 0)
  {
      TUser user(dl);
      user.user_id = initiator;
      user.read();
      eInitiator->setText(Wt::WString::fromUTF8(user.fio));
  }

  deExecuteBefore->setDate(execute_before);
  teTask->setText(Wt::WString::fromUTF8(task));
  cbTypeTask->setValue(type_task);

  refreshState();

  bOk->setEnabled(isValid());
}
//--------------------------------------------------------------------------
void TtaskUpd::onExecutorSelect()
{
    int user_id(0);
    if (selectUser(dl, this, user_id))
    {
        executor = user_id;
        refresh();
    }
}

void TtaskUpd::onTextInput()
{
    Wt::WObject * w = sender();
    if (w == NULL || w->objectName().empty()) return;

    if (w->objectName() == "deExecuteBefore")
    {
        execute_before = deExecuteBefore->date();
    }
    else
    if (w->objectName() == "teTask")
    {
       task = teTask->text().trim().toUTF8();
    }
    else
    if (w->objectName() == "cbTypeTask")
    {
        type_task = cbTypeTask->value();
    }
    
    bOk->setEnabled(isValid());
}
//--------------------------------------------------------------------------
void TtaskUpd::onbOk()
{
    int code;
    std::string name, comment;

    if(save() > 0)
    {
        onCloseInt(task_id);
    }
    else
    {
        ShowMessage(0, Wt::WString::fromUTF8(str2html(dl->errorstr)), "DB Error");
    }
}
//--------------------------------------------------------------------------
void TtaskUpd::onbCancel()
{
 onCloseInt(0);
}
//--------------------------------------------------------------------------

void TtaskUpd::onbState3()
{
    setState(3);
    read();
    refreshState();
}
void TtaskUpd::onbState4()
{
    setState(4);
    read();
    refreshState();
}

//--------------------------------------------------------------------------
void TtaskUpd::refreshState()
{
    Wt::WText * l;
    int i(0);

    lmain->clear();
    for(auto itr = lState.begin(); itr != lState.end(); ++itr, ++i)
    {
        l = new Wt::WText(Wt::WString::fromUTF8(itr->state_name + std::string(": ")));
        lmain->addWidget(l, i, 0);
        l = new Wt::WText(itr->dtState.toString("dd-MM-yyyy hh:mm:ss"));
        lmain->addWidget(l, i, 1);
    }

  //  setTablePadding(tState, 5, Wt::Side::Left | Wt::Side::Right);

    bState3->setHidden(task_id == 0);
    bState4->setHidden(task_id == 0);

    bState3->setEnabled(task_id > 0 && currentState() < 3 && dl->SYSGetUser() == executor);
    bState4->setEnabled(task_id > 0 && currentState() < 4 && dl->SYSGetUser() == executor);
}
//--------------------------------------------------------------------------

void TtaskUpd::createCommentWriter(Wt::WContainerWidget * c)
{
    Wt::WVBoxLayout * lmain = new Wt::WVBoxLayout();
    lmain->setContentsMargins(0, 2, 0, 1);

    c->setLayout(lmain);

    taComment = new Wt::WTextArea(); taComment->setObjectName("taComment");
    taComment->textInput().connect(this, &TtaskUpd::onCommentTextInput);
    lmain->addWidget(taComment, 1);

    Wt::WHBoxLayout * lh = new Wt::WHBoxLayout();
    lh->setContentsMargins(0, 2, 0, 1);

    lmain->addLayout(lh, 0);

    bNewComment = new  Wt::WPushButton(Wt::WString(L"��������� �����������")); bNewComment->setObjectName("bNewComment");
    bNewComment->setIcon(Wt::WLink("image/New.png"));
    bNewComment->setEnabled(false);
    bNewComment->clicked().connect(this, &TtaskUpd::onbNewComment);

    lh->addWidget(bNewComment, 0);
    lh->addStretch(1);
}

void TtaskUpd::onCommentTextInput()
{
    bNewComment->setEnabled(!taComment->text().trim().empty());
}

void TtaskUpd::onbNewComment()
{
    std::string msg = taComment->text().trim().toUTF8();
    if (msg.empty()) return;
    appendComment(msg);
    read();
    refreshComment();
}

void TtaskUpd::refreshComment()
{
    int r(startComment);
 if (startComment > 0)
 {
     for(int i = tMain->rowCount() - 1; i >= startComment; --i)
     {
         tMain->elementAt(i, 0)->clear();
     }
 }

 createCommentWriter(tMain->elementAt(r, 0));
 tMain->elementAt(r, 0)->setColumnSpan(4/*tMain->columnCount()*/);
 r++;

 for(auto itr = lComment.begin(); itr != lComment.end(); ++itr)
 {
     tMain->elementAt(r, 0)->addWidget(showComment(&(*itr)));
     tMain->elementAt(r, 0)->setColumnSpan(tMain->columnCount());
     r++;
 }
}

Wt::WWidget * TtaskUpd::showComment(TTaskComment * comment)
{
    Wt::WPanel * panel = new Wt::WPanel();
    panel->setTitleBar(true);
    Wt::WHBoxLayout * ltitle = new Wt::WHBoxLayout();
    ltitle->addWidget(new Wt::WText(Wt::WString::fromUTF8(comment->fio)), 0);
    ltitle->addStretch(1);
    ltitle->addWidget(new Wt::WText(comment->dt.toString("dd-MM-yyyy hh:mm:ss")), 0);

    panel->titleBarWidget()->setLayout(ltitle);

    Wt::WText * text = new Wt::WText(Wt::WString::fromUTF8(comment->comment));
    text->setWordWrap(true);

    panel->setCentralWidget(text);

    return(panel);
}