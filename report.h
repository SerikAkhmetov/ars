#ifndef REPORT_H
#define REPORT_H

#include <Wt/WWidget>
#include <Wt/WContainerWidget>
#include <Wt/WVBoxLayout>
#include <Wt/WHBoxLayout>
#include <hpdf.h>

#include "uDataLayer.h"
#include "widget.h"

class TPDFPage;

class TPDFDoc : public Wt::WResource
{
public:
    TPDFDoc(TDataLayer * dl, Wt::WObject *parent = 0);
    ~TPDFDoc();
    void error_handler(HPDF_STATUS error_no, HPDF_STATUS detail_no);
    virtual void handleRequest(const Wt::Http::Request& request, Wt::Http::Response& response);

    HPDF_Doc pdf;

protected:    
    virtual void render() = 0;

    HPDF_STATUS error_no; HPDF_STATUS detail_no;

    HPDF_Font font, fontBD, fontI;
    HPDF_REAL left, right; HPDF_REAL line_step;
    TDataLayer * dl;
};

class TPDFStatement : public TPDFDoc
{
public:
    TPDFStatement(TDataLayer * dl, Wt::WObject *parent = 0) : TPDFDoc(dl, parent){};
 //   ~TPDFStatement();

    void render();

    TBasicData * basicData;
    TDriver * dBeneficiary, *dOwner, *dCulprit; TEntity * entity;
    TVehicle * vVictim;
    TCulprit * culprit;
    TPaymentDetails * paymentDetails;

private:
    void renderPage1();
    void renderPage2();
    void renderPage3();
    void renderPage3Line(TPDFPage * page, HPDF_REAL top, const std::string & text);
    void renderPage3Header(TPDFPage * page, HPDF_REAL top, const std::string & text);

    void drawDriver(TPDFPage * page, HPDF_REAL top, TDriver * d);
    void drawPageBottom(TPDFPage * page, HPDF_REAL top);
};


class TPDFOrder : public TPDFDoc
{
public:
    TPDFOrder(TDataLayer * dl, Wt::WObject *parent = 0) : TPDFDoc(dl, parent){};
  //  ~TPDFOrder();

    void render();

    TBasicData * basicData;
    TDriver *dOwner;
    TVehicle * vVictim;
    TVictim * victim;
   // TCulprit * culprit;
   // TPaymentDetails * paymentDetails;
    TApplicationInspection * applicationInspection;

private:
    void renderPage1();
};


class TPDFPage
{
public:
    TPDFPage(HPDF_Doc pdf);

    HPDF_STATUS SetFontAndSize(HPDF_Font  font, HPDF_REAL  size);
    HPDF_STATUS TextOut(HPDF_REAL xpos, HPDF_REAL ypos, const char *text);
    HPDF_STATUS TextOut(const char *text);
    HPDF_STATUS TextOut(const char *text, const int top, const int align);
    HPDF_STATUS CharBoxOut(const std::string& s, HPDF_REAL x, HPDF_REAL y);
    HPDF_STATUS TextBoxOut(const std::string& s, HPDF_REAL x, HPDF_REAL y);
    HPDF_STATUS TextBoxOut(const std::string& s, HPDF_REAL x, HPDF_REAL y, const int length, const std::string& fill = std::string(" "));
    HPDF_STATUS TextRect(HPDF_REAL left, HPDF_REAL top, HPDF_REAL right, HPDF_REAL bottom, const char *text, HPDF_TextAlignment align);

    HPDF_STATUS DateBoxOut(const Wt::WDate& date, HPDF_REAL x, HPDF_REAL y);
    HPDF_STATUS TimeBoxOut(const Wt::WTime& time, HPDF_REAL x, HPDF_REAL y);

    HPDF_STATUS SetLineWidth(HPDF_REAL line_width);
    HPDF_STATUS Rectangle(HPDF_REAL x, HPDF_REAL y, HPDF_REAL width, HPDF_REAL height);
    HPDF_STATUS Line(HPDF_REAL x1, HPDF_REAL y1, HPDF_REAL x2, HPDF_REAL y2);
    HPDF_STATUS Line(HPDF_REAL x1, HPDF_REAL y1, HPDF_REAL x2, HPDF_REAL y2, HPDF_REAL line_width);
    HPDF_STATUS LineLabel(HPDF_REAL x1, HPDF_REAL y1, HPDF_REAL x2, const char *text);

    HPDF_REAL Height() const {return (HPDF_Page_GetHeight(page));};
    HPDF_REAL TextWidth(const std::string& text) const {return (HPDF_Page_TextWidth(page, text.c_str()));};
    HPDF_REAL LineCount(const std::string& text, HPDF_REAL max_width) const;

    HPDF_REAL x, y;
    HPDF_REAL line_width;

protected:
    void initPage();

    HPDF_Page page;
};

//--------------------------------------------------------------------------

//--------------------------------------------------------------------------
#endif // CERTIFICATEWINDOW_H
