#include <Wt/WCalendar>

#include "partnerUpdWindow.h"

//--------------------------------------------------------------------------
//--------------------------------------------------------------------------
TpartnerUpd::TpartnerUpd(Wt::WContainerWidget *parent, TDataLayer * p_dl, const int p_partner_id) : Twindow(p_dl, "TpartnerUpdDefault", parent), 
 partner_id(p_partner_id)
{
 refresh();
}
//--------------------------------------------------------------------------
//TpartnerUpd::~TpartnerUpd()
//{
//}
//--------------------------------------------------------------------------
void TpartnerUpd::setupUi()
{
 Wt::WTable * tMain = new Wt::WTable(this); tMain->setObjectName("tMain");

 int r(0);

 Wt::WText * l = new Wt::WText(L"������������:", tMain->elementAt(r, 0));
 eName = new Wt::WLineEdit(tMain->elementAt(r, 1)); eName->setObjectName("eName");
 eName->textInput().connect(this, &TpartnerUpd::onTextInput);
 eName->setEnabled(true);
 r++;

 /*Wt::WHBoxLayout * lh1 = new Wt::WHBoxLayout();
 bOk = new Wt::WPushButton(Wt::WString(L"��")); bOk->setObjectName("bOk");
 bOk->setEnabled(false);
 bOk->clicked().connect(this, &TpartnerUpd::onbOk);
 bOk->setIcon(Wt::WLink("image/Apply.png"));

 Wt::WPushButton * bCancel = new Wt::WPushButton(Wt::WString(L"������"));
 bCancel->setObjectName("bCancel");
 bCancel->clicked().connect(this, &TpartnerUpd::onbCancel);
 bCancel->setIcon(Wt::WLink("image/Cancel.png"));

 lh1->addStretch(1);
 lh1->addWidget(bOk);
 lh1->addWidget(bCancel);

 Wt::WContainerWidget * footer = new Wt::WContainerWidget();
 footer->addStyleClass("modal-footer");
 footer->setLayout(lh1);
 tMain->elementAt(r, 0)->addWidget(footer);
 tMain->elementAt(r, 0)->setColumnSpan(tMain->columnCount());*/

 TOkFooter * footer = new TOkFooter(tMain->elementAt(r, 0));
 footer->bCancel->clicked().connect(this, &TpartnerUpd::onbCancel);
 bOk = footer->bOk;
 bOk->clicked().connect(this, &TpartnerUpd::onbOk);
 tMain->elementAt(r, 0)->setColumnSpan(tMain->columnCount());

 setTablePadding(tMain, 2);
}
//--------------------------------------------------------------------------
void TpartnerUpd::refresh()
{
 Twindow::refresh();

 if (partner_id > 0)
 {
     Wt::WStandardItemModel lmt(this);
     if (dl->TableS("partner", &lmt, partner_id) > 0)
     {         
      for(int i = 0; i < lmt.rowCount(); i++)
      { 
       if (to_int(lmt.data(i, fieldByName(&lmt, "partner_id"))) != partner_id) continue;
       eName->setText(to_wstring(lmt.data(i, fieldByName(&lmt, "name"))));
       break;
      }
     }
 }
}
//--------------------------------------------------------------------------
void TpartnerUpd::onTextInput()
{
 bOk->setEnabled(!eName->text().trim().empty());
}
//--------------------------------------------------------------------------
void TpartnerUpd::onbOk()
{
    int code;
    std::string name, comment;

    name = eName->text().trim().toUTF8();

    int id = dl->PartnerIU(partner_id, name);
    if (id > 0)
    {
        onCloseInt(id);
    }
    else
    {
        ShowMessage(0, Wt::WString::fromUTF8(str2html(dl->errorstr)), "DB Error");
    }
}
//--------------------------------------------------------------------------
void TpartnerUpd::onbCancel()
{
 onCloseInt(0);
}
//--------------------------------------------------------------------------