#ifndef REQUISITIONBRWWINDOW_H
#define REQUISITIONBRWWINDOW_H

#include <Wt/WWidget>
#include <Wt/WContainerWidget>
#include <Wt/WVBoxLayout>
#include <Wt/WHBoxLayout>
#include <Wt/WDialog>
#include <Wt/WLineEdit>
#include <Wt/WCheckBox>
#include <Wt/WTableView>
#include <Wt/WItemDelegate>
#include <Wt/WComboBox>

#include "uDataLayer.h"
#include "widget.h"

class TrequisitionBrw : public Twindow
{
 public:
    TrequisitionBrw(const int city, Wt::WContainerWidget *parent, TDataLayer * dl);
    //virtual ~TrequisitionBrw();

    void setupUi();
    void refresh();

    void onCityChanged(const int);
private:
    Wt::WPushButton *bNew, *bEdit, *bDelete;
    Wt::WTableView * tableView; WTableViewPage * tvPage; Wt::WStandardItemModel * mt;

    int city;

    void refresh(const int);
    void redraw(const int);
    void onEdit();
    void onNew();
    void onDelete();
    void onSelectionChanged();

};

#endif