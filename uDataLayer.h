//---------------------------------------------------------------------------
#ifndef uDataLayerH
#define uDataLayerH
//#include <pthread.h>
#include <string>
#include <list>

#include <boost/thread/thread.hpp>
#include <boost/thread/mutex.hpp>
#include "sqlite3.h"

//#include <Wt/Payment/Money>

#include <Wt/WDate>
#include <Wt/WTime>
#include <Wt/WStandardItemModel>
#include <Wt/Http/Request>

//#include "ibppquery.h"
//#include "dynamic_list.h"
//#include "widget.h"

#define __PRETTY_FUNCTION__ __FUNCSIG__

//---------------------------------------------------------------------------
using namespace std;
//---------------------------------------------------------------------------
class TDataLayer;
typedef std::map<std::string, std::string> TState;
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
class TNumeric
{
protected:
    int64_t v;        // значение
    int     scale;    // кол-во знаков после запятой

    int pow10(int p) const {return (int(pow((float)10, p)));}
public:
    TNumeric():                               v(0),                  scale(4){};

    // TNumeric

    TNumeric(const TNumeric &p) {v = p.get_v(); scale = p.get_scale();};
    TNumeric& operator= (const TNumeric & tn) {v = tn.get_v(); scale = tn.get_scale(); return (*this);};
    TNumeric& operator + (TNumeric n)
    {if (scale > n.scale) n.set_scale(scale); else set_scale(n.scale); v += n.v; return (*this);};
    TNumeric& operator - (TNumeric n)
    {if (scale > n.scale) n.set_scale(scale); else set_scale(n.scale); v -= n.v; return (*this);};
    TNumeric& operator * (TNumeric n)
    {if (scale > n.scale) n.set_scale(scale); else set_scale(n.scale); v=v*n.v/pow10(scale); return (*this);};
    TNumeric& operator / (TNumeric n)
    {if (scale > n.scale) n.set_scale(scale); else set_scale(n.scale); v=v/(n.v/pow10(scale)); return (*this);};
    TNumeric& operator += (TNumeric n)
    {if (scale > n.scale) n.set_scale(scale); else set_scale(n.scale); v += n.v; return (*this);};
    TNumeric& operator -= (TNumeric n)
    {if (scale > n.scale) n.set_scale(scale); else set_scale(n.scale); v -= n.v; return (*this);};
    TNumeric& operator *= (TNumeric n)
    {if (scale > n.scale) n.set_scale(scale); else set_scale(n.scale); v=v*n.v/pow10(scale); return (*this);};
    TNumeric& operator /= (TNumeric n)
    {if (scale > n.scale) n.set_scale(scale); else set_scale(n.scale); v=v/(n.v/pow10(scale)); return (*this);};
    TNumeric& operator ++ () {v += pow10(scale); return (*this);};
    TNumeric& operator -- () {v -= pow10(scale); return (*this);};

    //  TNumeric operator ++
    //  TNumeric operator +=
    //  TNumeric operator --
    //  TNumeric operator -=
    //  TNumeric operator /=

    bool operator >  (TNumeric n) const {return ((v * pow10(scale))  <  (n.v * pow10(n.scale)) );};
    bool operator <  (TNumeric n) const {return ((v * pow10(scale))  >  (n.v * pow10(n.scale)) );};
    bool operator >= (TNumeric n) const {return ((v * pow10(scale))  <= (n.v * pow10(n.scale)) );};
    bool operator <= (TNumeric n) const {return ((v * pow10(scale))  >= (n.v * pow10(n.scale)) );};
    bool operator == (TNumeric n) const {return ((v / pow10(scale))  == (n.v / pow10(n.scale)) );};
    bool operator != (TNumeric n) const {return ((v / pow10(scale))  != (n.v / pow10(n.scale)) );};

    // int
    TNumeric(const int val):                  v(val*pow10(4)),       scale(4){};
    TNumeric(const int val, const int sc):    v(val*pow10(sc)),      scale(sc){};
    TNumeric& operator= (const int n)    {v = n * pow10(scale);     return (*this);};
    //TNumeric& operator= (const int& n)  {v = n * pow10(scale);     return (*this);};
    operator int()    { return(int    (v / pow10(scale)));};
    TNumeric& operator + (int n) {v += (n * scale); return (*this);};
    TNumeric& operator - (int n) {v -= (n * scale); return (*this);};
    TNumeric& operator * (int n) {v *= n; return (*this);};
    TNumeric& operator / (int n) {v /= n; return (*this);};

    // unsigned int
    TNumeric(const unsigned int val):         v(val*pow10(4)),       scale(4){};
    TNumeric(const unsigned int val, const int sc):    v(val*pow10(sc)),      scale(sc){};
    TNumeric& operator= (const unsigned int n)    {v = n * pow10(scale);     return (*this);};
    //TNumeric& operator= (const int& n)  {v = n * pow10(scale);     return (*this);};
#if defined(WIN32)
    operator unsigned int()    { return(unsigned int (v / pow10(scale)));};
#endif
    // unsigned long long
    /*  TNumeric(const unsigned long long val):  v(val*pow10(4)),       scale(4){};
    TNumeric(const unsigned long long val,const int sc): v(val*pow10(sc)),      scale(sc){};
    TNumeric& operator = (unsigned long long n){v = n * pow10(scale);     return (*this);};
    operator unsigned long long(){ return(unsigned long long(v / pow10(scale)));};
    */

    // long
    TNumeric(const long val):         v(val*pow10(4)),       scale(4){};
    TNumeric(const long val, const int sc):    v(val*pow10(sc)),      scale(sc){};
    TNumeric& operator= (const long n)    {v = n * pow10(scale);     return (*this);};
    //TNumeric& operator= (const int& n)  {v = n * pow10(scale);     return (*this);};
    operator long()    { return(long (v / pow10(scale)));};

    // unsigned long
    TNumeric(const unsigned long val):  v(val*pow10(4)),       scale(4){};
    TNumeric(const unsigned long val,const int sc): v(val*pow10(sc)),      scale(sc){};
    TNumeric& operator = (unsigned long n){v = n * pow10(scale);     return (*this);};
#if defined(WIN32)
    operator unsigned long(){ return(unsigned long(v / pow10(scale)));};
#endif
    // int64_t
    TNumeric(const int64_t val):              v(val*pow10(4)),       scale(4){};
    TNumeric(const int64_t val,const int sc): v(val*pow10(sc)),      scale(sc){};
    TNumeric& operator = (int64_t n){v = n * pow10(scale);     return (*this);};
    operator int64_t(){ return(int64_t(v / pow10(scale)));};

    // long double
    TNumeric(const long double val):               v(int(val*pow10(4))),  scale(4){};
    TNumeric& operator= (const long double n){v = int(n * pow10(scale)); return (*this);};
    TNumeric& operator= (const long double& n){v = int(n * pow10(scale)); return (*this);};
#if defined(WIN32)
    operator long double() { return(long double ((long double)v / pow10(scale)));};
#endif
    TNumeric(const long double val, const int sc): v(int(val*pow10(sc))), scale(sc){};

    // double
    TNumeric(const double val):               v(int(val*pow10(4))),  scale(4){};
    TNumeric& operator= (const double n){v = int(n * pow10(scale)); return (*this);};
    //  TNumeric& operator= (const double& n){v = int(n * pow10(scale)); return (*this);};
    operator double() { return(double (v / pow10(scale)));};

    TNumeric(const double val, const int sc): v(int(val*pow10(sc))), scale(sc){};

    // float
    TNumeric(const float val):                v(int(val*pow10(4))),  scale(4){};
    TNumeric& operator= (const float n){v = int(n * pow10(scale)); return (*this);};
    operator float() { return(float ((float)v / pow10(scale)));};
    float toFloat() const {return(float ((float)v / pow10(scale)));};

    TNumeric(const float val, const int sc):  v(int(val*pow10(sc))), scale(sc){};

    // std::string
    operator std::string()
    {return (toString());};

    std::string toString() const
    {std::stringstream stm; int64_t x,y;
    x = v/pow10(scale); y = v - x * pow10(scale);
    stm << x << "." << std::setw(scale) << std::setfill ('0') << std::right << y;
    return (stm.str()); };

    bool fromString(const std::string& str) {
        std::string s(str);
        s.erase(0, s.find_first_not_of(' '));       //prefixing spaces
        s.erase(s.find_last_not_of(' ') + 1);
        float Cnt = 0;
        if (!s.empty())
        {
            try{Cnt = std::stof(s);}
            catch(...){return(false);}
        }
        v = Cnt * pow10(scale);
        return(true);
    };

    void set_scale(const int sc)
    {if (sc>scale) v *= pow10(sc-scale); if (sc<scale) v /= pow10(scale-sc); scale = sc;};
    int  get_scale(void) const {return (scale);};
    int64_t get_v(void) const {return (v);};

    void set_v(const int64_t val) {v = val;};
};
//---------------------------------------------------------------------------

class Currency : public TNumeric
{
public:
    Currency() : TNumeric(){};
    Currency(const int value, const int cents, const std::string& curr)
    {
        set_scale(2);
        set_v(value * 100);
        v += cents;
    }
    Currency(const TNumeric &p) {v = p.get_v(); scale = p.get_scale();};

    Currency& operator= (const TNumeric & tn) {v = tn.get_v(); scale = tn.get_scale(); return (*this);};

    //Currency& operator = (const int &n)   {v = n * pow10(scale);     return (*this);};
    Currency& operator= (const int n)   {v = n * pow10(scale);     return (*this);};
    Currency& operator= (const double n) {v = int(n * pow10(scale)); return (*this);};

    Wt::WString toString() const
    {std::stringstream stm; int x,y;
    x = int(v/pow10(scale)); y = v - x * pow10(scale);
    stm << x << "." << std::setw(scale) << std::setfill ('0') << std::right << y;
    return Wt::WString(stm.str()); };
};

//typedef double Currency;
//---------------------------------------------------------------------------

//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
class TRequisition //: public Wt::WObject
{
 public:
  int requisition_id;
  Wt::WDate date_ta;
  std::string phone_number;
  std::string customer;
  int carlifter_plan_id;

  int carlifter_id;
  Wt::WDate date_plan;

  TRequisition(TDataLayer * dl);
  void clear();
  bool read();
  int save();
  bool remove();

 private:
  TDataLayer * dl;
};
//---------------------------------------------------------------------------
class TCarlifterPlan
{
 public:
  int carlifter_plan_id;
  int carlifter_id;
  int number;
  int requisition_id;
  Wt::WDateTime dt_start, dt_finish;

  TCarlifterPlan(TDataLayer * dl);
  void clear();
  bool read();
  int save();
  bool remove();
  std::string label() const;
 private:
  TDataLayer * dl;
};
//---------------------------------------------------------------------------
class TDriver
{
public:
    int driver_id; // insurance_case_id;
    bool resident;
    int  citizenship;
    Wt::WDate date_birth;
    int gender;
    std::string last_name, first_name, middle_name, fio;
    bool without_mname;
    std::string phone, dl_series, dl_number;
    bool dl_no_data;
    std::string p_series, p_number, issued;
    Wt::WDate date_issue;
    std::string postcode, addr_field1, addr_field2,
        addr_field3, addr_field4, addr_field5;
    int house, apartment;

    TDriver(TDataLayer * dl);
    void clear();
    bool read();
    int save();
    bool remove();
   // bool isEmpty();

protected:
    TDataLayer * dl;
};
//---------------------------------------------------------------------------
class TVehicle
{
public:
    int vehicle_id;
    int vehicle_type;
    std::string brand, model;
    std::string vin;
    bool right_hand_drive;
    int car_year;
    std::string registration_number;
    int vehicle_doc;
    std::string series, number;
    Wt::WDate date_doc, date_start;
    bool td_passport;
    Wt::WDate date_sale, last_sale;
    bool vehicle_warranty;

    TVehicle(TDataLayer * dl);
    void clear();
    bool read();
    int save();
    bool remove();

protected:
    TDataLayer * dl;
};
//---------------------------------------------------------------------------
class TUser
{
public:
    int user_id;
    std::string user_name, first_name, middle_name, last_name, pass, email, fio;
    int partner_id, can_create_user, job;

    bool isActive;

    TUser(TDataLayer * dl);
    void clear();
    bool read();
    int save();
    bool remove();

protected:

    bool isValid() const;
    bool isInDBActive() const;
    bool createUser() const;
    bool removeUser() const;

    TDataLayer * dl;
};
//---------------------------------------------------------------------------
class TTaskState
{
 public:
     TTaskState() : code(0), state_name(std::string()), dtState(Wt::WDateTime())
     {};
     TTaskState(const int p0, const std::string &p1, const Wt::WDateTime &p2) : code(p0), state_name(p1), dtState(p2)
     {};

    int code;
    std::string state_name;
    Wt::WDateTime dtState;
};

class TTaskComment
{
  public:
      int task_comment_id, task_id;
      std::string comment;
      int author;
      Wt::WDateTime dt;
      std::string fio;

      TTaskComment();
      void clear();
};

class TTask
{
public:
    int task_id, ic_id, initiator, executor, type_task, job; 
    std::string task;
    Wt::WDate execute_before;

    std::list<TTaskState> lState;
    std::list<TTaskComment> lComment;

    TTask(TDataLayer * dl);
    void clear();
    bool read();
    int save();
    bool remove();
    bool isValid() const;

    int currentState() const;
    void setState(const int v);
    void appendComment(const std::string & msg);
protected:
    TDataLayer * dl;
};
//---------------------------------------------------------------------------
class TBasic
{
 public:
     TBasic(TDataLayer * dl);

     int ic_id; // insurance_case_id;

     virtual void clear();
     virtual bool read() = 0;
     virtual int save() = 0;
     virtual bool remove() = 0;
     virtual bool isValid() const;

 protected:
     TDataLayer * dl;
};

class TICState : public TBasic
{
public:
    int state_id, partner_id, state;
    Wt::WDateTime dt_state; 

    TICState(const int partner_id, TDataLayer * dl);
    void clear();
    bool read();
    bool read(Wt::WStandardItemModel * mt);
    int save();
    bool remove();
};

class TBasicData : public TBasic
{
 public:
     bool settlement;
     bool reimbursement_lmv;
     bool calculation_lmv_possible;
     std::string number;
     int insurance_type;
     Wt::WDateTime dt_ta;
     std::string inspection;
     int participants; 
     int culprits;
     int ta_certificate;
     bool optional_equipment;
     int inspection_type;
     bool application; 
     int  incident_type;
     int  cause_damage;
     int  ta_location_type;
     bool franchise;
     std::string postcode, addr_field1;
     int partner_id, firm, beneficiary;
     bool night, minimal_contact, casualties, transfer, mobile_app, contract_sale;

     TBasicData(TDataLayer * dl);
     void clear();
     bool read();
     int save();
     bool remove();
     bool isValid() const;
};

class TApplicationInspection : public TBasic
{
public:
    bool unlimited_ep, disagreements, exit_ad;
    int  region_circulation;
    Wt::WDateTime dt_application;
    int  who_applied, scheme_number, applicants, inspection_type, region_inspection;
    std::string /*subdivision,*/ responsible, inspection_address;
    Wt::WDateTime dt_inspection;
    bool on_move, flip, frontal, airbags;
    Wt::WDate rereferral, receiving;
    Currency repair_cost, without_wear, agreed_cost, including_wear;
    int terms_payment;
    Currency expertise_cost;
    bool search_defects, determination, residual_value, loss_value, trace_evidence, independent, photo;

    TApplicationInspection(TDataLayer * dl);
    void clear();
    bool read();
    int save();
    bool remove();
   // bool isValid() const;
};

class TCulprit : public TBasic
{
public:
    std::string insurance_company, polis_series, policy_number;
    Wt::WDate date_conclusion;
    std::string vehicle_type, brand_model, vin, body_number, registration_number;
    bool inspection_culprit;
    int vehicle_id, driver_id, vehicle_owner;

    TCulprit(TDataLayer * dl);
    void clear();
    bool read();
    int save();
    bool remove();
};

class TVictim : public TBasic
{
public:
    std::string insurance_company, policy_series, policy_number;
    int vehicle_id;
    int driver_id, vehicle_owner;

    TVictim(TDataLayer * dl);
    void clear();
    bool read();
    int save();
    bool remove();
};

class TDamage : public TBasic
{
 public:
     TDamage(TDataLayer * dl);
     void clear();
     bool read();
     int save();
     bool remove();

     void setKind(const int element_id, const int kind_id);
     int getKind(const int element_id);

 Wt::WStandardItemModel mtDamage, mtElement, mtKind;

protected:
    int getElementIdx(const int element_id);
};

class TEntity : public TBasic
{
public:
    int ic_id;
    std::string name, itn;

    TEntity(TDataLayer * dl);
    void clear();
    bool read();
    int save();
    bool remove();
};

class TPaymentDetails : public TBasic
{
public:
    int ic_id;
    std::string checking_account, bank, department, correspondent_account, bic, itn, card, recipient;

    TPaymentDetails(TDataLayer * dl);
    void clear();
    bool read();
    int save();
    bool remove();
};

class TNode 
{
 public:
   int node_id;
   std::string p3, p2, p1, name_server;
   int64_t node_size;
   std::string crc;

   TNode(TDataLayer * dl);
   void clear();
   bool read();
   int save(const std::string& spoolFile);
   int save();
   
   std::string fullName() const;

   std::string delimiter;

 protected:
    TDataLayer * dl;
    std::string fdb;

    bool copy(const std::string& src, const std::string& dst);
    std::string fullPath() const;
};

class TFile
{
 public:
   int file_id;
   std::string name_user;
   int64_t file_size;
   int node_id, preview;
   
   TFile(TDataLayer * dl);
   void clear();
   bool read();
   int save();

   static int64_t fileSize(const std::string & file);

 protected:
    TDataLayer * dl;
};

/*class Currency : public Wt::Payment::Money
{
 public:
 Currency() : Wt::Payment::Money(){};
 Currency(long long value, int cents, const std::string &currency) : Wt::Payment::Money(value, cents, currency){};

 float asFloat(){return(value() + cents() / 100);};
}; */

class TBytea                     // class for BLOB field
{
// friend class TBytea;
 private:
  int64_t         size;
  int64_t         position;
  unsigned char * data;
  unsigned int    paddedCharacters;
 public:
  TBytea();
  TBytea(const int64_t size);
  TBytea(const TBytea&);
  virtual ~TBytea();
  int64_t       set_size(const int64_t);
  const int64_t get_size(void) const;
  int64_t       set_position(const int64_t);
  const int64_t get_position(void) const;
  void          clear(void);
  void          free(void);
  int64_t       read(void *, const int64_t size);   // чтение из потока
  int64_t       write(void *, const int64_t size);  // запись в поток
// оператор =, [], +, <<, >>
  int64_t       load_from_file(const std::string&);
  int64_t       save_to_file(const std::string&) const;
  const std::string base64(void) const;
  const std::string sha1(void) const;

  unsigned char * operator[](const int64_t) const;
  bool operator == (const TBytea n);
  TBytea& operator = (const int64_t& n) {set_size(n); return (*this);};
  TBytea& operator = (const TBytea& r);
  TBytea& operator = (const std::string& r);
};
//class TMemoryStream: public std::istream, std::ostream
//{
//  public:
//   TMemoryStream(){};

//   TBytea bytea;
//};
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
class TMemoryTable;
class TDoc;
//---------------------------------------------------------------------------
//class Tthread
//{
// public:
//  Tthread();
// ~Tthread();
// void start();
// void finish();

// private:
//   boost::mutex mtx;
//   bool running;
//   boost::thread thr;
//};
//---------------------------------------------------------------------------
class TSQLayer
{
 public:
  TSQLayer();
  virtual ~TSQLayer();
  int open();
  bool isOpen();
  void close();

 protected:
  int setQuery(const std::string&);
  int exec(Wt::WAbstractItemModel * mt);
  bool exec();
  int setParam(const std::string& name, const std::string& v);
  int setParam(const std::string& name, const int v);
//  setParam(const std::string name, const int v);

  /// transaction start
  int begin(void);
  /// commit
  int commit(void);
  /// rollback
  void rollback(void);

  public:

  std::string errmsg();

  std::string dbname;

//  int profile_group_id();
  int profile_group_s(Wt::WAbstractItemModel * mt, const std::string& name = std::string());
  int profile_group_iu(const int profile_group_id, const std::string& name);
  int get_id(const std::string& name);
  int first_s(Wt::WAbstractItemModel * mt, const std::string& value);
  int first_s(Wt::WAbstractItemModel * mt, const std::list<std::string>& value);
  int first_iu(const int first_id, const std::string& value);
  int second_s(Wt::WAbstractItemModel * mt, const std::string& value);
  int second_s(Wt::WAbstractItemModel * mt, const std::list<std::string>& value);
  int second_iu(const int second_id, const std::string& value);
  int profile_item_s(Wt::WAbstractItemModel * mt, const int group_id, const int first_id = 0, const int second_id = 0);
  int profile_item_iu(const int profile_item_id, const int profile_group_id, const int first_id, const int second_id);
  bool profileSaveValue(const std::string& value, const std::string& param, const std::string& group, const std::string& pref);
  int event_i();
  int event_item_i(const int event_id, const int first_id, const int second_id);

  int stateSave(const TState * state, int event_id = 0);
  int stateSave_a(const TState * state, int event_id = 0);

  int event_count(const std::string& second_value = std::string());
  int event_s(Wt::WAbstractItemModel * mt, const int offset = -1, const int limit = -1, const std::string& second_value = std::string());

  int table_info(Wt::WAbstractItemModel * mt, const std::string& table);
  int table_columnt_length(const std::string& table, const std::string& column);

  int logged_in_iu(const int logged_in_id, const std::string& session,
                   const std::string& db_server, const int db_port, const std::string& db_name,
                   const std::string& db_user, const std::string& db_role, const std::string& db_password);
  int logged_in_s(Wt::WAbstractItemModel * mt, const std::string& session);

 protected:
  sqlite3 * db;
  sqlite3_stmt * st;
  int trN;
  std::string sqlquery;
  char *zErrMsg;

  int st_to_mt(Wt::WAbstractItemModel * mt, sqlite3_stmt *) const;

  int create_struct();
  int check_version();
  std::string read_file(const std::string& filename);

  int first_value_length, second_value_length;
  void init_length();

 private:
   boost::mutex mtx;
   std::map<std::string, int> nameid; // name, id
 //  int fprofile_group_id, ffirst_id;

 private:
   boost::mutex state_mtx;
   std::map<int, TState> state_lst; // event_id
   void push_state(const TState * state, const int event_id);
   void thread();
   void stop();
   bool running;
   boost::thread thr;

 public:
   void start();
};
//---------------------------------------------------------------------------
class TDataLayer
{
 public:
   std::string errorstr;
   int SQLException_SqlCode;
   int SQLException_EngineCode;

 TDataLayer() {};
 virtual ~TDataLayer(){};

 virtual void setHostName(const std::string& s) = 0;
 virtual void setPort(const int p) = 0;
 virtual void setDatabaseName(const std::string& s) = 0;
 virtual void setUserName(const std::string& s) = 0;
 virtual void setRole(const std::string& s) = 0;
 virtual void setPassword(const std::string& s) = 0;

 virtual const std::string userName() const = 0;
 virtual const std::string hostName() const = 0;
// virtual const std::string port() const = 0;
 virtual const int port() const = 0;
 virtual const std::string role() const = 0;
 virtual const std::string dbName() const = 0;
 virtual const std::string password() const = 0;

 virtual bool open() = 0;
 virtual void close() = 0;

 virtual bool isOpen() = 0;

 virtual bool GlbVariablesS(const std::string& vname, int &vint, std::string &vstr, float &vnum) = 0;
 virtual int DIRLineIU(const int line_id, const int directory_id, const int code, const std::string& name, const std::string& comment = std::string()) = 0;
 virtual int DIRLineS(Wt::WAbstractItemModel * mt, const int directory_id = 0) = 0;
 virtual int DIRLineOne(Wt::WAbstractItemModel * mt, const int line_id = 0) = 0;
 virtual std::string DIRLineName(const int line_id) = 0;

 virtual int DIRLineD(const int line_id) = 0;
 virtual int ExecuteProcedure(Wt::WAbstractItemModel * mt, const std::string& sql_proc,
                              const Wt::Http::ParameterMap & parameter) = 0;
 virtual int ExecuteSQL(Wt::WAbstractItemModel * mt, const std::string& sql,
                              const Wt::Http::ParameterMap & parameter) = 0;
 virtual int SecDescriptorUsersIU(const int sid, const int user_id, const int rights, const int meta_rights) = 0;
 virtual int SecDescriptorGroupsIU(const int sid, const int group_id, const int rights, const int meta_rights) = 0;
 virtual int Begin() = 0;
 virtual int Commit() = 0;
 virtual int Rollback() = 0;
 virtual int TransactionStatus() = 0;
 virtual int SYSGetUser() = 0;
 virtual int SYSCurrentUserFirm() = 0;

 virtual int RequisitionS(Wt::WAbstractItemModel * mt, const int requsition_id, const int city) = 0;
 virtual int RequisitionIU(const int requisition_id, const Wt::WDate& date_ta, const std::string & phone_number, const std::string& customer, const int carlifter_plan_i) = 0;
 virtual bool RequisitionD(const int requisition_id) = 0;
 virtual int CarlifterS(Wt::WAbstractItemModel * mt, const int city) = 0;
 virtual int CarlifterIU(const int carlifter_id, const int city, const int number, const std::string& description) = 0;
 virtual int CarlifterPlanS(Wt::WAbstractItemModel * mt, const int city, const Wt::WDateTime& start, const Wt::WDateTime& finish, const int carlifter_plan_id = 0) = 0;
 virtual int CarlifterPlanSFree(Wt::WAbstractItemModel * mt, const int city, const Wt::WDateTime& start, const Wt::WDateTime& finish, const int carlifter, const int carlifter_plan_id) = 0;
 virtual int CarlifterPlanIU(const int carlifter_plan_id, const int carlifter_id, const Wt::WDateTime& start, const Wt::WDateTime& finish) = 0;
 virtual bool CarlifterPlanD(const int carlifter_plan_id) = 0;

 virtual int BasicDataS(Wt::WAbstractItemModel * mt, const int ic_id, const int status0 = 0, const int status1 = 0, const Wt::WDate deApplication = Wt::WDate()) = 0;
 virtual int BasicDataIU(const TBasicData * bd) = 0;
 virtual bool BasicDataD(const int ic_id) = 0;

 virtual int ApplicationInspectionS(Wt::WAbstractItemModel * mt, const int ic_id) = 0;
 virtual int ApplicationInspectionIU(const TApplicationInspection * bd) = 0;
 virtual bool ApplicationInspectionD(const int ic_id) = 0;

 virtual int CulpritS(Wt::WAbstractItemModel * mt, const int ic_id) = 0;
 virtual int CulpritIU(const TCulprit * bd) = 0;
 virtual bool CulpritD(const int ic_id) = 0;

 virtual int VictimS(Wt::WAbstractItemModel * mt, const int ic_id) = 0;
 virtual int VictimIU(const TVictim * bd) = 0;
 virtual bool VictimD(const int ic_id) = 0;

 virtual int DriverS(Wt::WAbstractItemModel * mt, const int driver_id) = 0;
 virtual int DriverIU(const TDriver * bd) = 0;
 virtual bool DriverD(const int driver_id) = 0;

 virtual int TableS(const std::string& table, Wt::WAbstractItemModel * mt, const int id = 0) = 0;
 virtual bool TableD(const std::string& table, const int id) = 0;

 virtual int NodeIU(const TNode * bd) = 0;
 virtual int FileIU(const TFile * bd) = 0;

 virtual int FileDocS(Wt::WAbstractItemModel * mt, const int type, const int id) = 0;
 virtual int FileDocIU(const int file_doc_id, const int file_id, const int type, const int id) = 0;

 virtual int VehicleS(Wt::WAbstractItemModel * mt, const int vehicle_id) = 0;
 virtual int VehicleIU(const TVehicle * bd) = 0;

 virtual int DamageS(Wt::WAbstractItemModel * mt, const int ic_id) = 0;
 virtual int DamageIU(const int damage_id, const int ic_id, const int element, const int kind) = 0;

 virtual int PartnerIU(const int partner_id, const std::string& name) = 0;

 virtual int UsersS(Wt::WAbstractItemModel * mt, const int user_id, const int partner_id = 0) = 0;
 virtual int UsersIU(const TUser * db) = 0;

 virtual int TaskS(Wt::WAbstractItemModel * mt, const int ic_id = 0, const int executor = 0, const int state = 0, const int type = 0) = 0;
 virtual int TaskIU(const TTask * db) = 0;

 virtual int TaskStateID(const int code) = 0;
 virtual int TaskStateIU(const int task_state_id, const int task_id, const int state) = 0;
 virtual int TaskStateS(Wt::WAbstractItemModel * mt, const int task_id) = 0;

 virtual int TaskCommentIU(const int task_comment_id, const int task_id, const std::string & comment) = 0;

 virtual int StateIU(const int state_id, const int ic_id, const int partner_id, const int state) = 0;

 virtual int EntityIU(const int ic_id, const std::string& name, const std::string& itn) = 0;
 
 virtual int PaymentDetailsIU(const int ic_id, const std::string& checking_account, const std::string& bank, const std::string& department, 
                              const std::string& correspondent_account, const std::string& bic, const std::string& itn, const std::string& card, const std::string& recipient) = 0;

 virtual int GrandRole(const std::string &role, const std::string &user) = 0;

 virtual int CarS(Wt::WAbstractItemModel * mt, const int line_id) = 0;

 virtual int GetNext(const std::string& generator) = 0;

 //   void test(void);

   public:
 virtual std::string getCookie(const std::string& key, const std::string& def=std::string()) = 0;
 virtual void setCookie(const std::string& key, const std::string& value) = 0;
 virtual void saveColumnWidth(const Wt::WAbstractItemView * view, const std::string& pref) = 0;
 virtual void setColumnWidth(Wt::WAbstractItemView * view, const std::string& pref) = 0;

 virtual std::string cp2koi(const std::string& src, int direction = 1) const = 0 ;
 virtual std::string cp2koi(const char * src, int direction = 1) const {return cp2koi(std::string(src), direction);};
};
//---------------------------------------------------------------------------
TDataLayer * DataLayerFactory();

std::string Upper(const std::string& p);
std::wstring Upper(const std::wstring p);
std::string Lower(const std::string& p);

int fieldByName(const Wt::WAbstractItemModel * mt, const std::string&);
bool modelCopy(const Wt::WAbstractItemModel * src, Wt::WAbstractItemModel * dst,
               const std::vector<std::string> * field, const int offset = 0, const int count_line = 0);
bool modelCopy(const Wt::WAbstractItemModel * src, Wt::WAbstractItemModel * dst);
bool modelFilter(Wt::WAbstractItemModel * mt,
                 const std::list<std::string> * filter, const std::string& value);
std::string MatchForSQL(std::string str);
void logtxt(const std::string& file, const std::string& s, const bool showDateTime=true);
void logtxt(const std::string& s, const bool showDateTime=true);

int logtxt(const TState * state, const int event_id = 0);
//---------------------------------------------------------------------------

#endif


