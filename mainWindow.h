#ifndef mainWindowH
#define mainWindowH

#include <Wt/WWidget>
#include <Wt/WContainerWidget>
#include <Wt/WVBoxLayout>
#include <Wt/WHBoxLayout>
#include <Wt/WContainerWidget>

#include "uDataLayer.h"
#include "widget.h"
//--------------------------------------------------------------------------
//--------------------------------------------------------------------------
class TmainWindow : public Twindow
{
 public:
  TmainWindow(Wt::WContainerWidget * parent = 0);
 ~TmainWindow();

  void setupUi();

  void onMenu();

  void createWindow(const std::string& name);

  void onLogout();
  void onCloseWindow();

 private:

     Wt::WContainerWidget * container;
     Wt::WPanel *panel;
     Wt::WVBoxLayout * lmain;
     //Wt::WHBoxLayout * lmenu1;
     Wt::WMenu * m, *menu1;
     TComboBox * cbCity;
     Wt::WTable * tLogin; Wt::WLineEdit * eLogin, * ePassword;
     Wt::WPushButton * bMenu;
     Wt::WContainerWidget * doc;
     Wt::WContainerWidget * wLogOut;
     Wt::WText * lLogin;

  void setDefaultConnect(const bool);
  bool isDefaultConnect;

  void defaultConnect();
  bool isCurrentUserAdmin();

  void onbOk();
  void onKeyPressed(Wt::WKeyEvent);
  void onExitClecked(Wt::WMouseEvent);

  class TMenu
  {
   public:
   std::string menu0; std::string menu1; std::string label; std::string ico;
   TMenu(const std::string& p0, const std::string& p1, const std::string& p2 = std::string(""), const std::string& p3 = std::string("")) :
        menu0(p0), menu1(p1), label(p2), ico(p3) {if (label.empty()) label = menu1;};
  };

  void createMenu(const std::list<TMenu *>&);
  void createMenu(const std::string& name);

  std::list<TMenu *> menuDefault; // menu - 0, menu - 1
  std::list<TMenu *> menuLogged;
};
//--------------------------------------------------------------------------

#endif
