#include <Wt/WMessageBox>

#include "dirLineBrwWindow.h"
#include "dirLineUpdWindow.h"

TdirLineBrw::TdirLineBrw(Wt::WContainerWidget *parent, TDataLayer * dl, const int p_directory_id): Twindow(dl, "TdirLineBrw", parent), directory_id(p_directory_id)
{
 setupUi();
 refresh(0);
}

//TdirLineBrw::~TdirLineBrw()
//{
//}

void TdirLineBrw::setupUi()
{
    clear();

    Wt::WVBoxLayout * lmain;
    lmain = new Wt::WVBoxLayout();

    setLayout(lmain);
    Wt::WHBoxLayout * lh1 = new Wt::WHBoxLayout();
    lmain->addItem(lh1);

    bNew = new Wt::WPushButton(Wt::WString(L"�����")); bNew->setObjectName("bNew");
    bNew->clicked().connect(this, &TdirLineBrw::onNew);
    bNew->setIcon(Wt::WLink("image/New.png"));
    lh1->addWidget(bNew, 0, Wt::AlignmentFlag::AlignTop);

    bEdit = new Wt::WPushButton(Wt::WString(L"��������")); bEdit->setObjectName("bEdit");
    bEdit->clicked().connect(this, &TdirLineBrw::onEdit);
    bEdit->setEnabled(false);
    bEdit->setIcon(Wt::WLink("image/Edit.png"));
    lh1->addWidget(bEdit, 0, Wt::AlignmentFlag::AlignTop);

    bDelete = new Wt::WPushButton(Wt::WString(L"�������")); bDelete->setObjectName("bDelete");
    bDelete->clicked().connect(this, &TdirLineBrw::onDelete);
    bDelete->setEnabled(false);
    bDelete->setIcon(Wt::WLink("image/Delete.png"));
    lh1->addWidget(bDelete, 0, Wt::AlignmentFlag::AlignTop);

    lh1->addStretch(1);

    tvPage = new WTableViewPage(dl, this); tvPage->setObjectName("tvPage");
    tvPage->layout->addStretch(1);
    tvPage->xField.clear();
    tvPage->xField.push_back("code");
    tvPage->xField.push_back("name");
    tvPage->filterChanged.connect(this, &Twindow::delayedRefresh);

    Wt::WHBoxLayout * lh2 = new Wt::WHBoxLayout();
    //lh2->addWidget(tvPage->lFilter, 0, Wt::AlignmentFlag::AlignTop);
    lh2->addWidget(tvPage->eFilter, 0, Wt::AlignmentFlag::AlignTop);
    //tvPage->filterChanged.connect(this, &Twindow::delayedRefresh);
    lh2->addLayout(tvPage->layout, 0, Wt::AlignmentFlag::AlignTop);
    lh2->addStretch(1);
    lmain->addLayout(lh2);

    tableView = new Wt::WTableView();// tableView->setObjectName("tableView");
    tableView->setAlternatingRowColors(true);
    tableView->setSortingEnabled(false);
    tableView->setColumnResizeEnabled(true);
    //tableView->setSelectionMode(Wt::SelectionMode::SingleSelection);
    tableView->setSelectionMode(Wt::ExtendedSelection);
    tableView->setSelectable(true);
    tableView->setEditTriggers(Wt::WAbstractItemView::NoEditTrigger);
  //  tableView->doubleClicked().connect(this, &TfileBrw::onDoubleClicked);
  //  tableView->keyPressed().connect(this, &TfileBrw::onKeyPressed);
    tableView->selectionChanged().connect(this, &TdirLineBrw::onSelectionChanged);
    tableView->columnResized().connect(this, &Twindow::onColumnResized);
  //  tableView->setItemDelegate(item);
  //  tableView->setHeight(600);

    tvPage->tableView = tableView;

    lmain->addWidget(tableView);
}

void TdirLineBrw::refresh()
{
 refresh(0);
}

void TdirLineBrw::refresh(const int id)
{
    mt = new Wt::WStandardItemModel(this);
    if (dl->DIRLineS(mt, directory_id) < 0)
    {
        ShowMessage(0, Wt::WString::fromUTF8(str2html(dl->errorstr)), "DB Error");
        return;
    }

    tvPage->field.clear();
    tvPage->field.push_back(Wt::WString("code")); tvPage->field.push_back(Wt::WString(L"���"));
    tvPage->field.push_back(Wt::WString("name")); tvPage->field.push_back(Wt::WString(L"������������"));
  /*  tvPage->field.push_back(Wt::WString("number")); tvPage->field.push_back(Wt::WString(L"���������"));
    tvPage->field.push_back(Wt::WString("DT_START")); tvPage->field.push_back(Wt::WString(L"������"));
    tvPage->field.push_back(Wt::WString("DT_FINISH")); tvPage->field.push_back(Wt::WString(L"�����"));*/

   // tvPage->emptyTitleColumn.clear();
   // tvPage->emptyTitleColumn.push_back("ico_RES_REGISTED");
   // tvPage->emptyTitleColumn.push_back("ico_ITEMS_STATUS");

    tvPage->mt = mt;
    tvPage->refresh(0);

    for(int i = 0; i < tableView->model()->columnCount(); i++)
    {tableView->setHeaderAlignment(i, Wt::AlignCenter);}
}

void TdirLineBrw::onEdit()
{
    if (tableView->model() == NULL || tableView->model()->rowCount() == 0) return;
    if (tableView->selectedIndexes().empty()) return;
    int row = tvPage->rowIndex(tableView->selectedIndexes().begin()->row());
    int line_id = to_int(mt->data(row, fieldByName(mt, "LINE_ID")));

    int c = mt->rowCount();

    clear();
    TdirLineUpd * frm = new TdirLineUpd(this, dl, directory_id, line_id);
    frm->closeInt.connect(this, &TdirLineBrw::redraw);
}

void TdirLineBrw::onNew()
{
 clear();
 TdirLineUpd * frm = new TdirLineUpd(this, dl, directory_id);
 frm->closeInt.connect(this, &TdirLineBrw::redraw);
}

void TdirLineBrw::redraw(const int v)
{
  Twindow::refresh();
  refresh(0);
}

void TdirLineBrw::onDelete()
{
    if (tableView->model() == NULL || tableView->model()->rowCount() == 0) return;
    if (tableView->selectedIndexes().empty()) return;
    int row = tvPage->rowIndex(tableView->selectedIndexes().begin()->row());

    if (Wt::StandardButton::Yes != Wt::WMessageBox::show(Wt::WString(L"������"), Wt::WString(L"������� ������ ?"),
        Wt::StandardButton::Yes | Wt::StandardButton::No))
    {
     return;
    }

    int line_id = to_int(mt->data(row, fieldByName(mt, "LINE_ID")));

    if (dl->DIRLineD(line_id))
    {
     row = tvPage->rowIndex(tableView->selectedIndexes().begin()->row()) - 1;
     if (row >= 0)
     {
      line_id = to_int(mt->data(row, fieldByName(mt, "LINE_ID")));
      refresh(line_id);
     }
     else
         refresh(0);
    }
    else
    {
     ShowMessage(0, Wt::WString::fromUTF8(str2html(dl->errorstr)), "DB Error");
    }
}

void TdirLineBrw::onSelectionChanged()
{
    bEdit->setEnabled(tableView->model() != NULL && tableView->model()->rowCount() > 0 && !tableView->selectedIndexes().empty());
    bDelete->setEnabled(tableView->model() != NULL && tableView->model()->rowCount() > 0 && !tableView->selectedIndexes().empty());
}

