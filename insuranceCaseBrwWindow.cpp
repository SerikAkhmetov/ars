#include <Wt/WMessageBox>

#include "insuranceCaseBrwWindow.h"
#include "insuranceCaseUpdWindow.h"

TinsuranceCaseBrw::TinsuranceCaseBrw(Wt::WContainerWidget *parent, TDataLayer * dl): Twindow(dl, "TinsuranceCaseBrw", parent)
{
 setupUi();
 refresh();
}

//TinsuranceCaseBrw::~TinsuranceCaseBrw()
//{
//}

void TinsuranceCaseBrw::setupUi()
{
    clear();

    Wt::WVBoxLayout * lmain;
    lmain = new Wt::WVBoxLayout();

    setLayout(lmain);
    Wt::WHBoxLayout * lh1 = new Wt::WHBoxLayout();
    lmain->addItem(lh1);

    bNew = new Wt::WPushButton(Wt::WString(L"�����")); bNew->setObjectName("bNew");
    bNew->clicked().connect(this, &TinsuranceCaseBrw::onNew);
    bNew->setIcon(Wt::WLink("image/New.png"));
    lh1->addWidget(bNew, 0, Wt::AlignmentFlag::AlignTop);

    bEdit = new Wt::WPushButton(Wt::WString(L"��������")); bEdit->setObjectName("bEdit");
    bEdit->clicked().connect(this, &TinsuranceCaseBrw::onEdit);
    bEdit->setEnabled(false);
    bEdit->setIcon(Wt::WLink("image/Edit.png"));
    lh1->addWidget(bEdit, 0, Wt::AlignmentFlag::AlignTop);

    bDelete = new Wt::WPushButton(Wt::WString(L"�������")); bDelete->setObjectName("bDelete");
    bDelete->clicked().connect(this, &TinsuranceCaseBrw::onDelete);
    bDelete->setEnabled(false);
    bDelete->setIcon(Wt::WLink("image/Delete.png"));
    lh1->addWidget(bDelete, 0, Wt::AlignmentFlag::AlignTop);

    lh1->addStretch(1);

    Wt::WText * l = new Wt::WText(L"���� ������ ���������:");
    lh1->addWidget(l, 0, Wt::AlignmentFlag::AlignTop);
    deApplication = new TDateEdit(); deApplication->setObjectName("deApplication");
    deApplication->textInput().connect(this, &TinsuranceCaseBrw::onDateChanged);
    lh1->addWidget(deApplication, 0, Wt::AlignmentFlag::AlignTop);

    cbStatus0 = new TComboBoxDirLine(dl, 23, NULL); cbStatus0->setObjectName("cbStatus0");
    cbStatus0->setLabel(L"������ ���:");
    cbStatus0->setNoSelectionEnabled(true);
    cbStatus0->setValue(0);
    cbStatus0->valueChanged.connect(this, &TinsuranceCaseBrw::onStatusChanged);
    lh1->addWidget(cbStatus0, 0, Wt::AlignmentFlag::AlignTop);

    cbStatus1 = new TComboBoxDirLine(dl, 24, NULL); cbStatus1->setObjectName("cbStatus1");
    cbStatus1->setLabel(L"������ ��������:");
    cbStatus1->setNoSelectionEnabled(true);
    cbStatus1->setValue(0);
    cbStatus1->valueChanged.connect(this, &TinsuranceCaseBrw::onStatusChanged);
    lh1->addWidget(cbStatus1, 0, Wt::AlignmentFlag::AlignTop);

    tvPage = new WTableViewPage(dl, this); tvPage->setObjectName("tvPage");
    tvPage->layout->addStretch(1);
    tvPage->xField.clear();
    tvPage->xField.push_back("number");
    tvPage->xField.push_back("culprit_fio");
    tvPage->xField.push_back("victim_fio");
    tvPage->xField.push_back("culprit_vin");
    tvPage->xField.push_back("victim_vin");

    tvPage->filterChanged.connect(this, &Twindow::delayedRefresh);

    Wt::WHBoxLayout * lh2 = new Wt::WHBoxLayout();
    //lh2->addWidget(tvPage->lFilter, 0, Wt::AlignmentFlag::AlignTop);
    lh2->addWidget(tvPage->eFilter, 0, Wt::AlignmentFlag::AlignTop);
    lh2->addLayout(tvPage->layout, 0, Wt::AlignmentFlag::AlignTop);
    lh2->addStretch(1);
    lmain->addLayout(lh2);

    tableView = new Wt::WTableView();// tableView->setObjectName("tableView");
    tableView->setAlternatingRowColors(true);
    tableView->setSortingEnabled(false);
    tableView->setColumnResizeEnabled(true);
    //tableView->setSelectionMode(Wt::SelectionMode::SingleSelection);
    tableView->setSelectionMode(Wt::ExtendedSelection);
    tableView->setSelectable(true);
    tableView->setEditTriggers(Wt::WAbstractItemView::NoEditTrigger);
  //  tableView->doubleClicked().connect(this, &TfileBrw::onDoubleClicked);
  //  tableView->keyPressed().connect(this, &TfileBrw::onKeyPressed);
    tableView->selectionChanged().connect(this, &TinsuranceCaseBrw::onSelectionChanged);
    tableView->columnResized().connect(this, &Twindow::onColumnResized);
  //  tableView->setItemDelegate(item);
  //  tableView->setHeight(600);

    tvPage->tableView = tableView;

    lmain->addWidget(tableView);
}

void TinsuranceCaseBrw::refresh()
{
 refresh(0);
}

void TinsuranceCaseBrw::refresh(const int id)
{
    int status0(cbStatus0->value()), status1(cbStatus1->value()); 

    mt = new Wt::WStandardItemModel(this);
    if (dl->BasicDataS(mt, 0, status0, status1, selectedDate) < 0)
    {
        ShowMessage(0, Wt::WString::fromUTF8(str2html(dl->errorstr)), "DB Error");
        return;
    }

    tvPage->field.clear();
    tvPage->field.push_back(Wt::WString("NUMBER")); tvPage->field.push_back(Wt::WString(L"�����"));
    tvPage->field.push_back(Wt::WString("culprit_fio")); tvPage->field.push_back(Wt::WString(L"��������"));
    tvPage->field.push_back(Wt::WString("culprit_vin")); tvPage->field.push_back(Wt::WString(L"VIN ���������"));
    tvPage->field.push_back(Wt::WString("victim_fio")); tvPage->field.push_back(Wt::WString(L"�����������"));
    tvPage->field.push_back(Wt::WString("victim_vin")); tvPage->field.push_back(Wt::WString(L"VIN ������������"));
    tvPage->field.push_back(Wt::WString("state0")); tvPage->field.push_back(Wt::WString(L"������ ���"));
    tvPage->field.push_back(Wt::WString("state1")); tvPage->field.push_back(Wt::WString(L"������ ��������"));
    tvPage->field.push_back(Wt::WString("INSURANCE_TYPE_NAME")); tvPage->field.push_back(Wt::WString(L"��� �����������"));
    tvPage->field.push_back(Wt::WString("dt_application")); tvPage->field.push_back(Wt::WString(L"��������� ������"));

   // tvPage->emptyTitleColumn.clear();
   // tvPage->emptyTitleColumn.push_back("ico_RES_REGISTED");
   // tvPage->emptyTitleColumn.push_back("ico_ITEMS_STATUS");

    tvPage->mt = mt;
    tvPage->refresh(0);

    for(int i = 0; i < tableView->model()->columnCount(); i++)
    {tableView->setHeaderAlignment(i, Wt::AlignCenter);}
}

void TinsuranceCaseBrw::onEdit()
{
    if (tableView->model() == NULL || tableView->model()->rowCount() == 0) return;
    if (tableView->selectedIndexes().empty()) return;
    int row = tvPage->rowIndex(tableView->selectedIndexes().begin()->row());
    int ic_id = to_int(mt->data(row, fieldByName(mt, "IC_ID")));

    clear();
    TinsuranceCaseUpd * frm = new TinsuranceCaseUpd(this, dl, ic_id);
    frm->closeInt.connect(this, &TinsuranceCaseBrw::redraw);}

void TinsuranceCaseBrw::onNew()
{
 clear();
 TinsuranceCaseUpd * frm = new TinsuranceCaseUpd(this, dl);
 frm->closeInt.connect(this, &TinsuranceCaseBrw::redraw);
}

void TinsuranceCaseBrw::redraw(const int v)
{
  Twindow::refresh();
  refresh(0);
}

void TinsuranceCaseBrw::onDelete()
{
  /*  if (tableView->model() == NULL || tableView->model()->rowCount() == 0) return;
    if (tableView->selectedIndexes().empty()) return;
    int row = tvPage->rowIndex(tableView->selectedIndexes().begin()->row());

    if (Wt::StandardButton::Yes != Wt::WMessageBox::show(Wt::WString(L"������"), Wt::WString(L"������� ������ ?"),
        Wt::StandardButton::Yes | Wt::StandardButton::No))
    {
     return;
    }

    int reqisition_id = to_int(mt->data(row, fieldByName(mt, "REQUISITION_ID")));

    if (dl->RequisitionD(reqisition_id))
    {
     row = tvPage->rowIndex(tableView->selectedIndexes().begin()->row()) - 1;
     if (row >= 0)
     {
      reqisition_id = to_int(mt->data(row, fieldByName(mt, "REQISITION_ID")));
      refresh(reqisition_id);
     }
    }
    else
    {
     ShowMessage(0, Wt::WString::fromUTF8(str2html(dl->errorstr)), "DB Error");
    }*/
}

void TinsuranceCaseBrw::onSelectionChanged()
{
    bEdit->setEnabled(tableView->model() != NULL && tableView->model()->rowCount() > 0 && !tableView->selectedIndexes().empty());
    bDelete->setEnabled(tableView->model() != NULL && tableView->model()->rowCount() > 0 && !tableView->selectedIndexes().empty());
}

void TinsuranceCaseBrw::onStatusChanged()
{
    refresh();
}

void TinsuranceCaseBrw::onDateChanged()
{
    if (selectedDate != deApplication->date())
    {
        selectedDate = deApplication->date();
        refresh();
    }
}

void TinsuranceCaseBrw::onCityChanged(const int n)
{
 if (city != n)
 {
  city = n;
  refresh(0);
 }
}