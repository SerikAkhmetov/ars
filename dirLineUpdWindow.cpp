#include <Wt/WCalendar>

#include "dirLineUpdWindow.h"

//--------------------------------------------------------------------------
//--------------------------------------------------------------------------
TdirLineUpd::TdirLineUpd(Wt::WContainerWidget *parent, TDataLayer * p_dl, const int p_directory_id, const int p_line_id) : Twindow(p_dl, "TdirLineUpdDefault", parent), 
 line_id(p_line_id), directory_id(p_directory_id)
{
 refresh();
}
//--------------------------------------------------------------------------
//TdirLineUpd::~TdirLineUpd()
//{
//}
//--------------------------------------------------------------------------
void TdirLineUpd::setupUi()
{
 Wt::WTable * tMain = new Wt::WTable(this); tMain->setObjectName("tMain");

 int r(0);

 spCode = new Wt::WSpinBox(tMain->elementAt(r, 1)); spCode->setObjectName("spCode");
 spCode->textInput().connect(this, &TdirLineUpd::onTextInput);
 spCode->valueChanged().connect(this, &TdirLineUpd::onTextInput);
 Wt::WText * l = new Wt::WText(L"���:", tMain->elementAt(r, 0));
 r++;

 eName = new Wt::WLineEdit(tMain->elementAt(r, 1)); eName->setObjectName("eName");
 eName->textInput().connect(this, &TdirLineUpd::onTextInput);
 eName->setEnabled(true);
 l = new Wt::WText(L"������������:", tMain->elementAt(r, 0));
 r++;

 eComment = new Wt::WLineEdit(tMain->elementAt(r, 1)); eComment->setObjectName("eComment");
 eComment->setEnabled(true);
 eComment->textInput().connect(this, &TdirLineUpd::onTextInput);
 l = new Wt::WText(L"�����������:", tMain->elementAt(r, 0));
 r++;

 Wt::WHBoxLayout * lh1 = new Wt::WHBoxLayout();
 bOk = new Wt::WPushButton(Wt::WString(L"��")); bOk->setObjectName("bOk");
 bOk->setEnabled(false);
 bOk->clicked().connect(this, &TdirLineUpd::onbOk);
 bOk->setIcon(Wt::WLink("image/Apply.png"));

 Wt::WPushButton * bCancel = new Wt::WPushButton(Wt::WString(L"������"));
 bCancel->setObjectName("bCancel");
 bCancel->clicked().connect(this, &TdirLineUpd::onbCancel);
 bCancel->setIcon(Wt::WLink("image/Cancel.png"));

 lh1->addStretch(1);
 lh1->addWidget(bOk);
 lh1->addWidget(bCancel);

 Wt::WContainerWidget * footer = new Wt::WContainerWidget();
 footer->addStyleClass("modal-footer");
 footer->setLayout(lh1);
 tMain->elementAt(r, 0)->addWidget(footer);
 tMain->elementAt(r, 0)->setColumnSpan(tMain->columnCount());

 /*TOkFooter * footer = new TOkFooter(tMain->elementAt(r, 0));
 footer->bCancel->clicked().connect(this, &TdirLineUpd::onbCancel);
 bOk = footer->bOk;
 bOk->clicked().connect(this, &TdirLineUpd::onbOk);
 tMain->elementAt(r, 0)->setColumnSpan(tMain->columnCount());*/

 setTablePadding(tMain, 2);
}
//--------------------------------------------------------------------------
void TdirLineUpd::refresh()
{
 Twindow::refresh();

 if (line_id > 0)
 {
     Wt::WStandardItemModel lmt(this);
     if (dl->DIRLineS(&lmt, directory_id) > 0)
     {         
      for(int i = 0; i < lmt.rowCount(); i++)
      { 
       if (to_int(lmt.data(i, fieldByName(&lmt, "line_id"))) != line_id) continue;
       spCode->setValue(to_int(lmt.data(i, fieldByName(&lmt, "code"))));
       eName->setText(to_wstring(lmt.data(i, fieldByName(&lmt, "name"))));
       eComment->setText(to_wstring(lmt.data(i, fieldByName(&lmt, "comment"))));
       break;
      }
     }
 }
}
//--------------------------------------------------------------------------
void TdirLineUpd::onTextInput()
{
 bOk->setEnabled(!eName->text().trim().empty());
}
//--------------------------------------------------------------------------
void TdirLineUpd::onbOk()
{
    int code;
    std::string name, comment;

    code = spCode->value();
    name = eName->text().trim().toUTF8();
    comment = eComment->text().trim().toUTF8();

    int id = dl->DIRLineIU(line_id, directory_id, code, name, comment);
    if (id > 0)
    {
        onCloseInt(id);
    }
    else
    {
        ShowMessage(0, Wt::WString::fromUTF8(str2html(dl->errorstr)), "DB Error");
    }
}
//--------------------------------------------------------------------------
void TdirLineUpd::onbCancel()
{
 onCloseInt(0);
}
//--------------------------------------------------------------------------