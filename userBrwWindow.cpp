#include <Wt/WMessageBox>
#include <Wt/WStandardItem>

#include "userBrwWindow.h"
#include "userUpdWindow.h"
//--------------------------------------------------------------------------
bool selectUser(TDataLayer * dl, Wt::WContainerWidget * parent, int &user_id)
{
    Wt::WDialog * dialog = new Wt::WDialog("User", parent);
    TuserBrw * frm = new TuserBrw(dialog->contents(), dl);
 /*   Wt::WHBoxLayout * lh1 = new Wt::WHBoxLayout();
    lh1->addStretch(1);
    lh1->addWidget(frm->bOk);
    lh1->addWidget(frm->bCancel);
    dialog->footer()->setLayout(lh1);*/
    dialog->contents()->setMaximumSize(Wt::WLength(1600), Wt::WLength(1000));
    dialog->contents()->resize(Wt::WLength(800), Wt::WLength(500));
    dialog->setClosable(true);
    // dialog->setResizable(true);
    dialog->setTitleBarEnabled(true);
//    frm->bOk->clicked().connect(dialog, &Wt::WDialog::accept);
//    frm->bCancel->clicked().connect(dialog, &Wt::WDialog::reject);
 //   frm->footer->hide();
    frm->bEdit->hide(); frm->bNew->hide(); frm->bDelete->hide();
  /*  frm->lStore->setValue(str_id);
    frm->lStore->comboBox->setEnabled(false);

    frm->tableView->resize(Wt::WLength(700), Wt::WLength(320));
    frm->nom_id = nom_id;
    frm->pack_id = pack_id;
    frm->cbUsed->setCheckState(Wt::Checked);*/

    TOkFooter * footer = new TOkFooter(dialog->footer());
    footer->bOk->clicked().connect(dialog, &Wt::WDialog::accept);
    footer->bCancel->clicked().connect(dialog, &Wt::WDialog::reject);
    footer->bOk->setEnabled(true);

    frm->refresh();

    if (dialog->exec() == Wt::WDialog::DialogCode::Accepted && frm->user_id > 0)
    {
        user_id = frm->user_id;
        return(true);
    }
    else
    {return(false);}
}
//--------------------------------------------------------------------------
TuserBrw::TuserBrw(Wt::WContainerWidget *parent, TDataLayer * dl): Twindow(dl, "TuserBrw", parent), user_id(0)
{
 setupUi();
 refresh(0);
}

//TuserBrw::~TuserBrw()
//{
//}

void TuserBrw::setupUi()
{
    clear();

    Wt::WVBoxLayout * lmain;
    lmain = new Wt::WVBoxLayout();

    setLayout(lmain);
    Wt::WHBoxLayout * lh1 = new Wt::WHBoxLayout();
    lmain->addItem(lh1);

    bNew = new Wt::WPushButton(Wt::WString(L"�����")); bNew->setObjectName("bNew");
    bNew->clicked().connect(this, &TuserBrw::onNew);
    bNew->setIcon(Wt::WLink("image/New.png"));
    lh1->addWidget(bNew, 0, Wt::AlignmentFlag::AlignTop);

    bEdit = new Wt::WPushButton(Wt::WString(L"��������")); bEdit->setObjectName("bEdit");
    bEdit->clicked().connect(this, &TuserBrw::onEdit);
    bEdit->setEnabled(false);
    bEdit->setIcon(Wt::WLink("image/Edit.png"));
    lh1->addWidget(bEdit, 0, Wt::AlignmentFlag::AlignTop);

    bDelete = new Wt::WPushButton(Wt::WString(L"�������")); bDelete->setObjectName("bDelete");
    bDelete->clicked().connect(this, &TuserBrw::onDelete);
    bDelete->setEnabled(false);
    bDelete->setIcon(Wt::WLink("image/Delete.png"));
    lh1->addWidget(bDelete, 0, Wt::AlignmentFlag::AlignTop);

    lh1->addStretch(1);

    Wt::WText * l = new Wt::WText(L"�������:");
    cbPartner = new TComboBox(); cbPartner->setObjectName("cbPartner");
    cbPartner->setNoSelectionEnabled(true);
    if (dl->TableS("PARTNER", cbPartner->mt) < 0)
    {
        ShowMessage(0, Wt::WString::fromUTF8(str2html(dl->errorstr)), "DB Error");
    }
    cbPartner->mt->insertRow(0, new Wt::WStandardItem());
    cbPartner->mt->setData(0, 0, 0);
    cbPartner->mt->setData(0, 1, Wt::WString(""));

    TUser user(dl);
    user.user_id = dl->SYSGetUser();
    user.read();
    partner_id = user.partner_id;

    cbPartner->setLabelKeyColumn("", "partner_id", "name");
    cbPartner->setValue(partner_id);

    if (user.can_create_user == 2)
      cbPartner->valueChanged.connect(this, &TuserBrw::onPartnerChanged);
    else
      cbPartner->setEnabled(false);

    lh1->addWidget(l, 0,  Wt::AlignmentFlag::AlignTop);
    lh1->addWidget(cbPartner, 0,  Wt::AlignmentFlag::AlignTop);

    tvPage = new WTableViewPage(dl, this); tvPage->setObjectName("tvPage");
    tvPage->layout->addStretch(1);
    tvPage->xField.clear();
    tvPage->xField.push_back("code");
    tvPage->xField.push_back("name");
    tvPage->filterChanged.connect(this, &Twindow::delayedRefresh);

    Wt::WHBoxLayout * lh2 = new Wt::WHBoxLayout();
    //lh2->addWidget(tvPage->lFilter, 0, Wt::AlignmentFlag::AlignTop);
    lh2->addWidget(tvPage->eFilter, 0, Wt::AlignmentFlag::AlignTop);
    //tvPage->filterChanged.connect(this, &Twindow::delayedRefresh);
    lh2->addLayout(tvPage->layout, 0, Wt::AlignmentFlag::AlignTop);
    lh2->addStretch(1);
    lmain->addLayout(lh2);

    tableView = new Wt::WTableView();// tableView->setObjectName("tableView");
    tableView->setAlternatingRowColors(true);
    tableView->setSortingEnabled(false);
    tableView->setColumnResizeEnabled(true);
    //tableView->setSelectionMode(Wt::SelectionMode::SingleSelection);
    tableView->setSelectionMode(Wt::ExtendedSelection);
    tableView->setSelectable(true);
    tableView->setEditTriggers(Wt::WAbstractItemView::NoEditTrigger);
  //  tableView->doubleClicked().connect(this, &TfileBrw::onDoubleClicked);
  //  tableView->keyPressed().connect(this, &TfileBrw::onKeyPressed);
    tableView->selectionChanged().connect(this, &TuserBrw::onSelectionChanged);
    tableView->columnResized().connect(this, &Twindow::onColumnResized);
  //  tableView->setItemDelegate(item);
  //  tableView->setHeight(600);

    tvPage->tableView = tableView;

    lmain->addWidget(tableView);
}

void TuserBrw::refresh()
{
 refresh(0);
}

void TuserBrw::refresh(const int id)
{
    mt = new Wt::WStandardItemModel(this);
    if (dl->UsersS(mt, 0, partner_id) < 0)
    {
        ShowMessage(0, Wt::WString::fromUTF8(str2html(dl->errorstr)), "DB Error");
        return;
    }

    tvPage->field.clear();
//    tvPage->field.push_back(Wt::WString("user_name")); tvPage->field.push_back(Wt::WString(L"�����"));
    tvPage->field.push_back(Wt::WString("fio")); tvPage->field.push_back(Wt::WString(L"�.�.�."));
    tvPage->field.push_back(Wt::WString("job_name")); tvPage->field.push_back(Wt::WString(L"���������"));
    tvPage->field.push_back(Wt::WString("partner_name")); tvPage->field.push_back(Wt::WString(L"�������"));
//    tvPage->field.push_back(Wt::WString("last_name")); tvPage->field.push_back(Wt::WString(L"�������"));
//    tvPage->field.push_back(Wt::WString("first_name")); tvPage->field.push_back(Wt::WString(L"���"));
//    tvPage->field.push_back(Wt::WString("moddle_name")); tvPage->field.push_back(Wt::WString(L"��������"));

   // tvPage->emptyTitleColumn.clear();
   // tvPage->emptyTitleColumn.push_back("ico_RES_REGISTED");
   // tvPage->emptyTitleColumn.push_back("ico_ITEMS_STATUS");

    tvPage->mt = mt;
    tvPage->refresh(0);

    for(int i = 0; i < tableView->model()->columnCount(); i++)
    {tableView->setHeaderAlignment(i, Wt::AlignCenter);}
    user_id = 0;
}

void TuserBrw::onEdit()
{
    if (tableView->model() == NULL || tableView->model()->rowCount() == 0) return;
    if (tableView->selectedIndexes().empty()) return;
    int row = tvPage->rowIndex(tableView->selectedIndexes().begin()->row());
    int user_id = to_int(mt->data(row, fieldByName(mt, "user_id")));

    int c = mt->rowCount();

    clear();
    TuserUpd * frm = new TuserUpd(this, dl, user_id);
    frm->closeInt.connect(this, &TuserBrw::redraw);
}

void TuserBrw::onNew()
{
 clear();
 TuserUpd * frm = new TuserUpd(this, dl);
 frm->closeInt.connect(this, &TuserBrw::redraw);
}

void TuserBrw::redraw(const int v)
{
  Twindow::refresh();
  refresh(0);
}

void TuserBrw::onDelete()
{
    if (tableView->model() == NULL || tableView->model()->rowCount() == 0) return;
    if (tableView->selectedIndexes().empty()) return;
    int row = tvPage->rowIndex(tableView->selectedIndexes().begin()->row());

    if (Wt::StandardButton::Yes != Wt::WMessageBox::show(Wt::WString(L"������"), Wt::WString(L"������� ������ ?"),
        Wt::StandardButton::Yes | Wt::StandardButton::No))
    {
     return;
    }

    int user_id = to_int(mt->data(row, fieldByName(mt, "USER_ID")));

    if (dl->TableD("sys_users", user_id))
    {
     row = tvPage->rowIndex(tableView->selectedIndexes().begin()->row()) - 1;
     if (row >= 0)
     {
      user_id = to_int(mt->data(row, fieldByName(mt, "USER_ID")));
      refresh(user_id);
     }
     else
         refresh(0);
    }
    else
    {
     ShowMessage(0, Wt::WString::fromUTF8(str2html(dl->errorstr)), "DB Error");
    }
}

void TuserBrw::onSelectionChanged()
{
    user_id = 0;
    bEdit->setEnabled(tableView->model() != NULL && tableView->model()->rowCount() > 0 && !tableView->selectedIndexes().empty());
    bDelete->setEnabled(tableView->model() != NULL && tableView->model()->rowCount() > 0 && !tableView->selectedIndexes().empty());

    if (!tableView->selectedIndexes().empty())
    {
      int row = tvPage->rowIndex(tableView->selectedIndexes().begin()->row());
      user_id = to_int(mt->data(row, fieldByName(mt, "USER_ID")));
    }
}

void TuserBrw::onPartnerChanged()
{
 partner_id = cbPartner->value();
 refresh(0);
}